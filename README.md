# DCS
Este projeto é conjunto de outros "sub-projetos" segmentados para uma funcionalidade específica. Neste momento, todos os sub-projetos estão conectados
através de uma tecnologia de comunicação chamada [NATS Streaming](https://docs.nats.io/nats-streaming-concepts/intro).
O projeto num todo, e resumidamente, é responsável pelo workflow entre a **Banka (Aplicação Core)** e o **DCS (Aplicação Externa)**.

- - -

Os sub-projetos conseguem comunicar através de um tópico e de uma mensagem JSON. Por exemplo:

1. Projeto A --> **Publica** no NATS Streaming o tópico "XPTO" juntamente com uma mensagem JSON;  
2. NATS Streaming --> Recebe a publicação da mensagem com o respetivo tópico e notifica todos os projetos que estejam a **subscrever** o mesmo;  
3. Projeto B --> Subscreve a mensagem notificada pelo NATS Streaming e efetua a lógica desejada.  

**NOTA:** Podem haver 1 ou mais sub-projetos a subscrever e/ou publicarem o mesmo tópico.

A nomenclatura usada nos tópicos é a seguinte: "{ação}:{Request/Response}".  
Por exemplo, no caso da criação de uma entidade particular é o seguinte: "ParticularCreated:Request".

## **Sub-Projetos**
* [BankaMW](https://bitbucket.org/bai-europa/dcs/src/dev/BankaMW/) - Middleware responsável por receber pedidos feitos pela Banka e publicar no NATS com o intuito de serem avaliados pelo DCS;
* [DCS_REST_Async](https://bitbucket.org/bai-europa/dcs/src/dev/DCS_REST_Async/) - Este é o projeto responsável por subscrever à grande maioria dos tópicos, para no fundo redirecionar para os serviços REST do DCS;
* [ProcessAMLWorkflow](https://bitbucket.org/bai-europa/processamlworkflow/src) - Consoante a resposta do DCS, este projeto subscreve ao NATS a mesma e realiza alterações diretamente na Banka;
* [DCS_STORAGE](https://bitbucket.org/bai-europa/dcs/src/dev/DCS_STORAGE/) - Projeto onde tem a maioria dos pojos necessários, devido à necessidade de reutilização dos mesmos.

### **Situação Real**
Neste exemplo, encontra-se uma situação real na criação de uma entidade na Banka:

1. É feita a criação da Entidade na Banka;
2. O **BankaMW** recebe a transação feita pela Banka;  
   2.1 Deteta que tipo de operação foi efetuada (neste caso a criação de uma entidade, mas poderia ser uma alteração de documentos, morada, etc);  
   2.2 É chamado o microsserviço interno de Parties;
   2.3 Depois da resposta obtida, é feita alguma lógica e enviada o pedido para o **NATS**.  
3. O **NATS** notifica os projetos que estão a subscrever o tópico enviado;
4. O **DCS_REST_Async** subcreve ao tópico enviado pelo BankaMW;  
   4.1 É feito um mapeamento da mensagem JSON recebida para o objeto correto de pedido para o DCS;  
   4.2 O pedido é enviado para o serviço REST do DCS;  
   4.3 Caso a resposta tenha sido de sucesso, é verificado se é para desbloquear a Entidade;  
      4.3.1 Se sim, é enviado para o NATS o pedido de desbloqueio que vai ser subscrito pelo **ProcessAMLWorkflow** e a alteração é feita na Banka;  
   4.4 Também é feito o pedido para o **ProcessAMLWorkflow** da atualização do risco e do estado de filtering da Entidade;
5. O workflow termina quando as alterações foram todas feitas na Banka.

- - -

Ficheiro atualizado a 21/05/2021 11:15h por Fernando Caria.
