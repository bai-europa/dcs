package pt.baieuropa.storage.WSDLPojos.DesactivarBloqueioTransacao;

import javax.validation.constraints.NotNull;


public class ObjectInputDesactivarBloqueioTransacao {

    @NotNull(message = "The property [RequestId] can't be NULL.")
    protected String requestId;
    @NotNull(message = "The property [transactionNumberExt] can't be NULL.")
    protected String transactionNumberExt;
    @NotNull(message = "The property [newStatusExt] can't be NULL.")
    protected String newStatusExt;

    public String getRequestId() {
        return requestId;
    }

    public void setRequestId(String requestId) {
        this.requestId = requestId;
    }

    public String getTransactionNumberExt() {
        return transactionNumberExt;
    }

    public void setTransactionNumberExt(String transactionNumberExt) {
        this.transactionNumberExt = transactionNumberExt;
    }

    public String getNewStatusExt() {
        return newStatusExt;
    }

    public void setNewStatusExt(String newStatusExt) {
        this.newStatusExt = newStatusExt;
    }
}
