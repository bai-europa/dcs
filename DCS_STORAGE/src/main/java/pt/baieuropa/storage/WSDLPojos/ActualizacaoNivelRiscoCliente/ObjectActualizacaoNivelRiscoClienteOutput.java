
package pt.baieuropa.storage.WSDLPojos.ActualizacaoNivelRiscoCliente;

import javax.xml.bind.annotation.*;

@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ObjectActualizacaoNivelRiscoClienteOutput", namespace = "http://actualizacaonivelriscocliente.consultas/xsd", propOrder = {
    "codigoRetorno",
    "mensagemErro"
})
public class ObjectActualizacaoNivelRiscoClienteOutput {

    @XmlElement(namespace = "http://actualizacaonivelriscocliente.consultas/xsd")
    protected Integer codigoRetorno;
    @XmlElementRef(name = "mensagemErro", namespace = "http://actualizacaonivelriscocliente.consultas/xsd", required = false)
    protected String mensagemErro;

    public Integer getCodigoRetorno() {
        return codigoRetorno;
    }

    public void setCodigoRetorno(Integer value) {
        this.codigoRetorno = value;
    }

    public String getMensagemErro() {
        return mensagemErro;
    }

    public void setMensagemErro(String value) {
        this.mensagemErro = value;
    }

}
