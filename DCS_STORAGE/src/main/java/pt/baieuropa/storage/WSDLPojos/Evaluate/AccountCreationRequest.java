
package pt.baieuropa.storage.WSDLPojos.Evaluate;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.math.BigDecimal;
import java.util.List;

@JsonPropertyOrder({
    "accountNumber",
    "accountType",
    "accountCategory",
    "accountEntityType",
    "ClientNumber_Ext",
    "accountStatus",
    "segmentType",
    "subscriptionChannel",
    "accountCurrency",
    "entityAccountList",
    "requestUser",
    "requestId",
    "ClientCreationDate_Ext",
    "accountCreationDate",
    "balanceAverage",
    "dateBalanceAverage",
    "action",
    "flexfields"
})
public class AccountCreationRequest {

    @JsonProperty("accountNumber")
    protected String accountNumber;
    @JsonProperty("accountType")
    protected String accountType;
    @JsonProperty("accountCategory")
    protected String accountCategory;
    @JsonProperty("accountEntityType")
    protected String accountEntityType;
    @JsonProperty("ClientNumber_Ext")
    protected String clientNumberExt;
    @JsonProperty("accountStatus")
    protected String accountStatus;
    @JsonProperty("segmentType")
    protected String segmentType;
    @JsonProperty("subscriptionChannel")
    protected String subscriptionChannel;
    @JsonProperty("accountCurrency")
    protected String accountCurrency;
    @JsonProperty("entityAccountList")
    protected List<EntityAccount> entityAccountList;
    @JsonProperty("requestUser")
    protected String requestUser;
    @JsonProperty("requestId")
    protected String requestId;
    @JsonProperty("ClientCreationDate_Ext")
    protected String clientCreationDateExt;
    @JsonProperty("accountCreationDate")
    protected String accountCreationDate;
    @JsonProperty("balanceAverage")
    protected BigDecimal balanceAverage;
    @JsonProperty("dateBalanceAverage")
    protected String dateBalanceAverage;
    @JsonProperty("action")
    protected String action;
    @JsonProperty("flexfields")
    protected ArrayOfFlexfieldStructure flexfields;

    public String getAccountNumber() {
        return accountNumber;
    }

    public void setAccountNumber(String value) {
        this.accountNumber = value;
    }

    public String getAccountType() {
        return accountType;
    }

    public void setAccountType(String value) {
        this.accountType = value;
    }

    public String getAccountCategory() {
        return accountCategory;
    }

    public void setAccountCategory(String value) {
        this.accountCategory = value;
    }

    public String getAccountEntityType() {
        return accountEntityType;
    }

    public void setAccountEntityType(String value) {
        this.accountEntityType = value;
    }

    public String getClientNumberExt() {
        return clientNumberExt;
    }

    public void setClientNumberExt(String value) {
        this.clientNumberExt = value;
    }

    public String getAccountStatus() {
        return accountStatus;
    }

    public void setAccountStatus(String value) {
        this.accountStatus = value;
    }

    public String getSegmentType() {
        return segmentType;
    }

    public void setSegmentType(String value) {
        this.segmentType = value;
    }

    public String getSubscriptionChannel() {
        return subscriptionChannel;
    }

    public void setSubscriptionChannel(String value) {
        this.subscriptionChannel = value;
    }

    public String getAccountCurrency() {
        return accountCurrency;
    }

    public void setAccountCurrency(String value) {
        this.accountCurrency = value;
    }

    public List<EntityAccount> getEntityAccountList() {
        return entityAccountList;
    }

    public void setEntityAccountList(List<EntityAccount> entityAccountList) {
        this.entityAccountList = entityAccountList;
    }

    public String getRequestUser() {
        return requestUser;
    }

    public void setRequestUser(String value) {
        this.requestUser = value;
    }

    public String getRequestId() {
        return requestId;
    }

    public void setRequestId(String requestId) {
        this.requestId = requestId;
    }

    public String getClientCreationDateExt() {
        return clientCreationDateExt;
    }

    public void setClientCreationDateExt(String value) {
        this.clientCreationDateExt = value;
    }

    public String getAccountCreationDate() {
        return accountCreationDate;
    }

    public void setAccountCreationDate(String value) {
        this.accountCreationDate = value;
    }

    public BigDecimal getBalanceAverage() {
        return balanceAverage;
    }

    public void setBalanceAverage(BigDecimal value) {
        this.balanceAverage = value;
    }

    public String getDateBalanceAverage() {
        return dateBalanceAverage;
    }

    public void setDateBalanceAverage(String value) {
        this.dateBalanceAverage = value;
    }

    public String getAction() {
        return action;
    }

    public void setAction(String value) {
        this.action = value;
    }

    public ArrayOfFlexfieldStructure getFlexfields() {
        return flexfields;
    }

    public void setFlexfields(ArrayOfFlexfieldStructure value) {
        this.flexfields = value;
    }

}
