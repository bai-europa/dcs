
package pt.baieuropa.storage.WSDLPojos.DesactivarBloqueioEntidade;

import javax.validation.constraints.NotNull;


public class ObjectInputDesactivarBloqueioEntidade {

    @NotNull(message = "The property [autoriz] can't be NULL.")
    protected String autoriz;
    @NotNull(message = "The property [codigoNotaBloqueio] can't be NULL.")
    protected Integer codigoNotaBloqueio;
    @NotNull(message = "The property [numeroEntidade] can't be NULL.")
    protected Integer numeroEntidade;
    @NotNull(message = "The property [observacoes] can't be NULL.")
    protected String observacoes;
    @NotNull(message = "The property [siglaParaMensagemAlerta] can't be NULL.")
    protected String siglaParaMensagemAlerta;
    @NotNull(message = "The property [tipoBloqueio] can't be NULL.")
    protected String tipoBloqueio;
    @NotNull(message = "The property [utilizador] can't be NULL.")
    protected String utilizador;
    @NotNull(message = "The property [validarOuAplicar] can't be NULL.")
    protected String validarOuAplicar;

    public String getAutoriz() {
        return autoriz;
    }

    public void setAutoriz(String autoriz) {
        this.autoriz = autoriz;
    }

    public Integer getCodigoNotaBloqueio() {
        return codigoNotaBloqueio;
    }

    public void setCodigoNotaBloqueio(Integer codigoNotaBloqueio) {
        this.codigoNotaBloqueio = codigoNotaBloqueio;
    }

    public Integer getNumeroEntidade() {
        return numeroEntidade;
    }

    public void setNumeroEntidade(Integer numeroEntidade) {
        this.numeroEntidade = numeroEntidade;
    }

    public String getObservacoes() {
        return observacoes;
    }

    public void setObservacoes(String observacoes) {
        this.observacoes = observacoes;
    }

    public String getSiglaParaMensagemAlerta() {
        return siglaParaMensagemAlerta;
    }

    public void setSiglaParaMensagemAlerta(String siglaParaMensagemAlerta) {
        this.siglaParaMensagemAlerta = siglaParaMensagemAlerta;
    }

    public String getTipoBloqueio() {
        return tipoBloqueio;
    }

    public void setTipoBloqueio(String tipoBloqueio) {
        this.tipoBloqueio = tipoBloqueio;
    }

    public String getUtilizador() {
        return utilizador;
    }

    public void setUtilizador(String utilizador) {
        this.utilizador = utilizador;
    }

    public String getValidarOuAplicar() {
        return validarOuAplicar;
    }

    public void setValidarOuAplicar(String validarOuAplicar) {
        this.validarOuAplicar = validarOuAplicar;
    }
}
