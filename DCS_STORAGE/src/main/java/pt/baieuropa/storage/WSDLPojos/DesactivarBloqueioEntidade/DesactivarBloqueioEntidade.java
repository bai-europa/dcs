
package pt.baieuropa.storage.WSDLPojos.DesactivarBloqueioEntidade;

import javax.xml.bind.annotation.*;

@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "dados"
})
@XmlRootElement(name = "desactivarBloqueioEntidade", namespace = "http://desactivarBloqueioEntidade.entidadesclientes")
public class DesactivarBloqueioEntidade {

    @XmlElementRef(name = "dados", namespace = "http://desactivarBloqueioEntidade.entidadesclientes", required = false)
    protected ObjectInputDesactivarBloqueioEntidade dados;

    public ObjectInputDesactivarBloqueioEntidade getDados() {
        return dados;
    }

    public void setDados(ObjectInputDesactivarBloqueioEntidade value) {
        this.dados = value;
    }

}
