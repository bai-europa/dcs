package pt.baieuropa.storage.WSDLPojos.Transacoes;

import pt.baieuropa.storage.WSDLPojos.Evaluate.FlexfieldStructure;

import java.math.BigDecimal;

public class EvaluateTransactionCB {

    String TransactionNumber;
    String TransactionOperationCode;
    BigDecimal TransactionAmount_Original;
    String TransactionCurrency_Original;
    BigDecimal TransactionAmount_Converted;
    String TransactionDateTime;
    String TransactionStatus;
    String OriginNClientID;
    String OriginNClientName;
    String OriginCountryISO;
    String OriginCorrespondingBank;
    String DestinationNClientID;
    String DestinationNClientName;
    String DestinationCountryISO;
    String DestinationCorrespondingBank;
    String Field70;
    String RequestUser;
    FlexfieldStructure[] Flexfields;

    public EvaluateTransactionCB() {
    }

    public EvaluateTransactionCB(String transactionNumber, String transactionOperationCode, BigDecimal transactionAmount_Original, String transactionCurrency_Original, BigDecimal transactionAmount_Converted, String transactionDateTime, String transactionStatus, String originNClientID, String originNClientName, String originCountryISO, String originCorrespondingBank, String destinationNClientID, String destinationNClientName, String destinationCountryISO, String destinationCorrespondingBank, String field70, String requestUser, FlexfieldStructure[] flexfields) {
        TransactionNumber = transactionNumber;
        TransactionOperationCode = transactionOperationCode;
        TransactionAmount_Original = transactionAmount_Original;
        TransactionCurrency_Original = transactionCurrency_Original;
        TransactionAmount_Converted = transactionAmount_Converted;
        TransactionDateTime = transactionDateTime;
        TransactionStatus = transactionStatus;
        OriginNClientID = originNClientID;
        OriginNClientName = originNClientName;
        OriginCountryISO = originCountryISO;
        OriginCorrespondingBank = originCorrespondingBank;
        DestinationNClientID = destinationNClientID;
        DestinationNClientName = destinationNClientName;
        DestinationCountryISO = destinationCountryISO;
        DestinationCorrespondingBank = destinationCorrespondingBank;
        Field70 = field70;
        RequestUser = requestUser;
        Flexfields = flexfields;
    }

    public String getTransactionNumber() {
        return TransactionNumber;
    }

    public void setTransactionNumber(String transactionNumber) {
        TransactionNumber = transactionNumber;
    }

    public String getTransactionOperationCode() {
        return TransactionOperationCode;
    }

    public void setTransactionOperationCode(String transactionOperationCode) {
        TransactionOperationCode = transactionOperationCode;
    }

    public BigDecimal getTransactionAmount_Original() {
        return TransactionAmount_Original;
    }

    public void setTransactionAmount_Original(BigDecimal transactionAmount_Original) {
        TransactionAmount_Original = transactionAmount_Original;
    }

    public String getTransactionCurrency_Original() {
        return TransactionCurrency_Original;
    }

    public void setTransactionCurrency_Original(String transactionCurrency_Original) {
        TransactionCurrency_Original = transactionCurrency_Original;
    }

    public BigDecimal getTransactionAmount_Converted() {
        return TransactionAmount_Converted;
    }

    public void setTransactionAmount_Converted(BigDecimal transactionAmount_Converted) {
        TransactionAmount_Converted = transactionAmount_Converted;
    }

    public String getTransactionDateTime() {
        return TransactionDateTime;
    }

    public void setTransactionDateTime(String transactionDateTime) {
        TransactionDateTime = transactionDateTime;
    }

    public String getTransactionStatus() {
        return TransactionStatus;
    }

    public void setTransactionStatus(String transactionStatus) {
        TransactionStatus = transactionStatus;
    }

    public String getOriginNClientID() {
        return OriginNClientID;
    }

    public void setOriginNClientID(String originNClientID) {
        OriginNClientID = originNClientID;
    }

    public String getOriginNClientName() {
        return OriginNClientName;
    }

    public void setOriginNClientName(String originNClientName) {
        OriginNClientName = originNClientName;
    }

    public String getOriginCountryISO() {
        return OriginCountryISO;
    }

    public void setOriginCountryISO(String originCountryISO) {
        OriginCountryISO = originCountryISO;
    }

    public String getOriginCorrespondingBank() {
        return OriginCorrespondingBank;
    }

    public void setOriginCorrespondingBank(String originCorrespondingBank) {
        OriginCorrespondingBank = originCorrespondingBank;
    }

    public String getDestinationNClientID() {
        return DestinationNClientID;
    }

    public void setDestinationNClientID(String destinationNClientID) {
        DestinationNClientID = destinationNClientID;
    }

    public String getDestinationNClientName() {
        return DestinationNClientName;
    }

    public void setDestinationNClientName(String destinationNClientName) {
        DestinationNClientName = destinationNClientName;
    }

    public String getDestinationCountryISO() {
        return DestinationCountryISO;
    }

    public void setDestinationCountryISO(String destinationCountryISO) {
        DestinationCountryISO = destinationCountryISO;
    }

    public String getDestinationCorrespondingBank() {
        return DestinationCorrespondingBank;
    }

    public void setDestinationCorrespondingBank(String destinationCorrespondingBank) {
        DestinationCorrespondingBank = destinationCorrespondingBank;
    }

    public String getField70() {
        return Field70;
    }

    public void setField70(String field70) {
        Field70 = field70;
    }

    public String getRequestUser() {
        return RequestUser;
    }

    public void setRequestUser(String requestUser) {
        RequestUser = requestUser;
    }

    public FlexfieldStructure[] getFlexfields() {
        return Flexfields;
    }

    public void setFlexfields(FlexfieldStructure[] flexfields) {
        Flexfields = flexfields;
    }
}
