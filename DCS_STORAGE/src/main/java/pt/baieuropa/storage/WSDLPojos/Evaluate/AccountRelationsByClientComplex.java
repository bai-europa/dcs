
package pt.baieuropa.storage.WSDLPojos.Evaluate;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;
import java.util.List;

@JsonPropertyOrder({
        "ClientNumber_Ext",
        "EntityAccountList",
        "RequestUser"
})
public class AccountRelationsByClientComplex {

    protected String clientNumberExt;
    protected List<EntityAccount> entityAccountList;
    protected String requestUser;

    @JsonProperty("ClientNumber_Ext")
    public String getClientNumberExt() {
        return clientNumberExt;
    }

    public void setClientNumberExt(String value) {
        this.clientNumberExt = value;
    }

    @JsonProperty("EntityAccountList")
    public List<EntityAccount> getEntityAccountList() {
        return entityAccountList;
    }

    public void setEntityAccountList(List<EntityAccount> entityAccountList) {
        this.entityAccountList = entityAccountList;
    }    

    @JsonProperty("RequestUser")
    public String getRequestUser() {
        return requestUser;
    }

    public void setRequestUser(String value) {
        this.requestUser = value;
    }

}
