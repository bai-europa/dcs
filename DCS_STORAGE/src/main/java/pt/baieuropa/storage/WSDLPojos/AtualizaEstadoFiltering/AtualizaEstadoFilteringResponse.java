
package pt.baieuropa.storage.WSDLPojos.AtualizaEstadoFiltering;

import javax.xml.bind.annotation.*;

@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "_return"
})
@XmlRootElement(name = "atualizaEstadoFilteringResponse", namespace = "http://atualizaestadofiltering.consultas")
public class AtualizaEstadoFilteringResponse {

    @XmlElementRef(name = "return", namespace = "http://atualizaestadofiltering.consultas", required = false)
    protected ObjectAtualizaEstadoFilteringOutput _return;

    public ObjectAtualizaEstadoFilteringOutput getReturn() {
        return _return;
    }

    public void setReturn(ObjectAtualizaEstadoFilteringOutput value) {
        this._return = value;
    }

}
