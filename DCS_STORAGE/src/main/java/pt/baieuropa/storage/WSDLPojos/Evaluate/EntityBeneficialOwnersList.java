
package pt.baieuropa.storage.WSDLPojos.Evaluate;

import java.math.BigDecimal;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;
import javax.xml.datatype.XMLGregorianCalendar;


/**
 * <p>Java class for EntityBeneficialOwnersList complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="EntityBeneficialOwnersList">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="Name" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="EntityType" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="Number_Ext" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="DocumentNumberDirectCompany" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="DocumentTypeDirectCompany" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="PercentageInDirectCompany" type="{http://www.w3.org/2001/XMLSchema}decimal"/>
 *         &lt;element name="DocumentNumber" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="DocumentType" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="BirthDate" type="{http://www.w3.org/2001/XMLSchema}date"/>
 *         &lt;element name="ConstitutionDate" type="{http://www.w3.org/2001/XMLSchema}date"/>
 *         &lt;element name="Gender" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="MaritalStatus" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="Nationality" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="CountryResidence" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="BirthPlace" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="ProfessionalActivity" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="Profession" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="EducationalQualifications" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="SocialCapital" type="{http://www.w3.org/2001/XMLSchema}decimal"/>
 *         &lt;element name="CAE" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="CAE2" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="SocietyType" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="StockMarketCode" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "EntityBeneficialOwnersList", namespace = "http://www.outsystems.com", propOrder = {
    "name",
    "entityType",
    "numberExt",
    "documentNumberDirectCompany",
    "documentTypeDirectCompany",
    "percentageInDirectCompany",
    "documentNumber",
    "documentType",
    "birthDate",
    "constitutionDate",
    "gender",
    "maritalStatus",
    "nationality",
    "countryResidence",
    "birthPlace",
    "professionalActivity",
    "profession",
    "educationalQualifications",
    "socialCapital",
    "cae",
    "cae2",
    "societyType",
    "stockMarketCode"
})
public class EntityBeneficialOwnersList {

    @XmlElement(name = "Name", namespace = "http://www.outsystems.com")
    protected String name;
    @XmlElement(name = "EntityType", namespace = "http://www.outsystems.com")
    protected String entityType;
    @XmlElement(name = "Number_Ext", namespace = "http://www.outsystems.com")
    protected String numberExt;
    @XmlElement(name = "DocumentNumberDirectCompany", namespace = "http://www.outsystems.com")
    protected String documentNumberDirectCompany;
    @XmlElement(name = "DocumentTypeDirectCompany", namespace = "http://www.outsystems.com")
    protected String documentTypeDirectCompany;
    @XmlElement(name = "PercentageInDirectCompany", namespace = "http://www.outsystems.com", required = true)
    protected BigDecimal percentageInDirectCompany;
    @XmlElement(name = "DocumentNumber", namespace = "http://www.outsystems.com")
    protected String documentNumber;
    @XmlElement(name = "DocumentType", namespace = "http://www.outsystems.com")
    protected String documentType;
    @XmlElement(name = "BirthDate", namespace = "http://www.outsystems.com", required = true)
    @XmlSchemaType(name = "date")
    protected XMLGregorianCalendar birthDate;
    @XmlElement(name = "ConstitutionDate", namespace = "http://www.outsystems.com", required = true)
    @XmlSchemaType(name = "date")
    protected XMLGregorianCalendar constitutionDate;
    @XmlElement(name = "Gender", namespace = "http://www.outsystems.com")
    protected String gender;
    @XmlElement(name = "MaritalStatus", namespace = "http://www.outsystems.com")
    protected String maritalStatus;
    @XmlElement(name = "Nationality", namespace = "http://www.outsystems.com")
    protected String nationality;
    @XmlElement(name = "CountryResidence", namespace = "http://www.outsystems.com")
    protected String countryResidence;
    @XmlElement(name = "BirthPlace", namespace = "http://www.outsystems.com")
    protected String birthPlace;
    @XmlElement(name = "ProfessionalActivity", namespace = "http://www.outsystems.com")
    protected String professionalActivity;
    @XmlElement(name = "Profession", namespace = "http://www.outsystems.com")
    protected String profession;
    @XmlElement(name = "EducationalQualifications", namespace = "http://www.outsystems.com")
    protected String educationalQualifications;
    @XmlElement(name = "SocialCapital", namespace = "http://www.outsystems.com", required = true)
    protected BigDecimal socialCapital;
    @XmlElement(name = "CAE", namespace = "http://www.outsystems.com")
    protected String cae;
    @XmlElement(name = "CAE2", namespace = "http://www.outsystems.com")
    protected String cae2;
    @XmlElement(name = "SocietyType", namespace = "http://www.outsystems.com")
    protected String societyType;
    @XmlElement(name = "StockMarketCode", namespace = "http://www.outsystems.com")
    protected String stockMarketCode;

    /**
     * Gets the value of the name property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getName() {
        return name;
    }

    /**
     * Sets the value of the name property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setName(String value) {
        this.name = value;
    }

    /**
     * Gets the value of the entityType property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getEntityType() {
        return entityType;
    }

    /**
     * Sets the value of the entityType property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setEntityType(String value) {
        this.entityType = value;
    }

    /**
     * Gets the value of the numberExt property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getNumberExt() {
        return numberExt;
    }

    /**
     * Sets the value of the numberExt property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setNumberExt(String value) {
        this.numberExt = value;
    }

    /**
     * Gets the value of the documentNumberDirectCompany property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDocumentNumberDirectCompany() {
        return documentNumberDirectCompany;
    }

    /**
     * Sets the value of the documentNumberDirectCompany property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDocumentNumberDirectCompany(String value) {
        this.documentNumberDirectCompany = value;
    }

    /**
     * Gets the value of the documentTypeDirectCompany property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDocumentTypeDirectCompany() {
        return documentTypeDirectCompany;
    }

    /**
     * Sets the value of the documentTypeDirectCompany property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDocumentTypeDirectCompany(String value) {
        this.documentTypeDirectCompany = value;
    }

    /**
     * Gets the value of the percentageInDirectCompany property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getPercentageInDirectCompany() {
        return percentageInDirectCompany;
    }

    /**
     * Sets the value of the percentageInDirectCompany property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setPercentageInDirectCompany(BigDecimal value) {
        this.percentageInDirectCompany = value;
    }

    /**
     * Gets the value of the documentNumber property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDocumentNumber() {
        return documentNumber;
    }

    /**
     * Sets the value of the documentNumber property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDocumentNumber(String value) {
        this.documentNumber = value;
    }

    /**
     * Gets the value of the documentType property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDocumentType() {
        return documentType;
    }

    /**
     * Sets the value of the documentType property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDocumentType(String value) {
        this.documentType = value;
    }

    /**
     * Gets the value of the birthDate property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getBirthDate() {
        return birthDate;
    }

    /**
     * Sets the value of the birthDate property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setBirthDate(XMLGregorianCalendar value) {
        this.birthDate = value;
    }

    /**
     * Gets the value of the constitutionDate property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getConstitutionDate() {
        return constitutionDate;
    }

    /**
     * Sets the value of the constitutionDate property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setConstitutionDate(XMLGregorianCalendar value) {
        this.constitutionDate = value;
    }

    /**
     * Gets the value of the gender property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getGender() {
        return gender;
    }

    /**
     * Sets the value of the gender property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setGender(String value) {
        this.gender = value;
    }

    /**
     * Gets the value of the maritalStatus property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getMaritalStatus() {
        return maritalStatus;
    }

    /**
     * Sets the value of the maritalStatus property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setMaritalStatus(String value) {
        this.maritalStatus = value;
    }

    /**
     * Gets the value of the nationality property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getNationality() {
        return nationality;
    }

    /**
     * Sets the value of the nationality property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setNationality(String value) {
        this.nationality = value;
    }

    /**
     * Gets the value of the countryResidence property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCountryResidence() {
        return countryResidence;
    }

    /**
     * Sets the value of the countryResidence property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCountryResidence(String value) {
        this.countryResidence = value;
    }

    /**
     * Gets the value of the birthPlace property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getBirthPlace() {
        return birthPlace;
    }

    /**
     * Sets the value of the birthPlace property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setBirthPlace(String value) {
        this.birthPlace = value;
    }

    /**
     * Gets the value of the professionalActivity property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getProfessionalActivity() {
        return professionalActivity;
    }

    /**
     * Sets the value of the professionalActivity property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setProfessionalActivity(String value) {
        this.professionalActivity = value;
    }

    /**
     * Gets the value of the profession property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getProfession() {
        return profession;
    }

    /**
     * Sets the value of the profession property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setProfession(String value) {
        this.profession = value;
    }

    /**
     * Gets the value of the educationalQualifications property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getEducationalQualifications() {
        return educationalQualifications;
    }

    /**
     * Sets the value of the educationalQualifications property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setEducationalQualifications(String value) {
        this.educationalQualifications = value;
    }

    /**
     * Gets the value of the socialCapital property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getSocialCapital() {
        return socialCapital;
    }

    /**
     * Sets the value of the socialCapital property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setSocialCapital(BigDecimal value) {
        this.socialCapital = value;
    }

    /**
     * Gets the value of the cae property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCAE() {
        return cae;
    }

    /**
     * Sets the value of the cae property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCAE(String value) {
        this.cae = value;
    }

    /**
     * Gets the value of the cae2 property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCAE2() {
        return cae2;
    }

    /**
     * Sets the value of the cae2 property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCAE2(String value) {
        this.cae2 = value;
    }

    /**
     * Gets the value of the societyType property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getSocietyType() {
        return societyType;
    }

    /**
     * Sets the value of the societyType property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setSocietyType(String value) {
        this.societyType = value;
    }

    /**
     * Gets the value of the stockMarketCode property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getStockMarketCode() {
        return stockMarketCode;
    }

    /**
     * Sets the value of the stockMarketCode property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setStockMarketCode(String value) {
        this.stockMarketCode = value;
    }

}
