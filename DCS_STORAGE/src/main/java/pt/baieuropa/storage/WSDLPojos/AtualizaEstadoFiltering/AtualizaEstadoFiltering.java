
package pt.baieuropa.storage.WSDLPojos.AtualizaEstadoFiltering;

import javax.xml.bind.annotation.*;

@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "dados"
})
@XmlRootElement(name = "atualizaEstadoFiltering", namespace = "http://atualizaestadofiltering.consultas")
public class AtualizaEstadoFiltering {

    @XmlElementRef(name = "dados", namespace = "http://atualizaestadofiltering.consultas", required = false)
    protected ObjectAtualizaEstadoFilteringInput dados;

    public ObjectAtualizaEstadoFilteringInput getDados() {
        return dados;
    }

    public void setDados(ObjectAtualizaEstadoFilteringInput value) {
        this.dados = value;
    }

}
