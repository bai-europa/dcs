
package pt.baieuropa.storage.WSDLPojos.AtualizaEstadoFiltering;

import javax.xml.bind.annotation.*;

@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ObjectAtualizaEstadoFilteringOutput", namespace = "http://atualizaestadofiltering.consultas/xsd", propOrder = {
    "codigoRetorno",
    "mensagemErro"
})
public class ObjectAtualizaEstadoFilteringOutput {

    @XmlElement(namespace = "http://atualizaestadofiltering.consultas/xsd")
    protected Integer codigoRetorno;
    @XmlElementRef(name = "mensagemErro", namespace = "http://atualizaestadofiltering.consultas/xsd", required = false)
    protected String mensagemErro;

    public Integer getCodigoRetorno() {
        return codigoRetorno;
    }

    public void setCodigoRetorno(Integer codigoRetorno) {
        this.codigoRetorno = codigoRetorno;
    }

    public String getMensagemErro() {
        return mensagemErro;
    }

    public void setMensagemErro(String mensagemErro) {
        this.mensagemErro = mensagemErro;
    }
}
