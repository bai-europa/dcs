
package pt.baieuropa.storage.WSDLPojos.CriarBloqueioConta;

import javax.xml.bind.annotation.*;

@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ObjectOutputCriarBloqueioConta", namespace = "http://criarBloqueioConta.entidadesclientes/xsd", propOrder = {
    "codigoRetorno",
    "mensagemErro"
})
public class ObjectOutputCriarBloqueioConta {

    @XmlElement(namespace = "http://criarBloqueioConta.entidadesclientes/xsd")
    protected Integer codigoRetorno;
    @XmlElementRef(name = "mensagemErro", namespace = "http://criarBloqueioConta.entidadesclientes/xsd", required = false)
    protected String mensagemErro;

    public Integer getCodigoRetorno() {
        return codigoRetorno;
    }

    public void setCodigoRetorno(Integer codigoRetorno) {
        this.codigoRetorno = codigoRetorno;
    }

    public String getMensagemErro() {
        return mensagemErro;
    }

    public void setMensagemErro(String mensagemErro) {
        this.mensagemErro = mensagemErro;
    }
}
