
package pt.baieuropa.storage.WSDLPojos.ActualizacaoNivelRiscoCliente;

import javax.validation.constraints.NotNull;

public class ObjectActualizacaoNivelRiscoClienteInput {

    @NotNull(message = "The property [accao] can't be NULL.")
    protected String accao;
    @NotNull(message = "The property [autorizacao] can't be NULL.")
    protected String autorizacao;
    @NotNull(message = "The property [codigoCampo] can't be NULL.")
    protected String codigoCampo;
    @NotNull(message = "The property [numeroBalcao] can't be NULL.")
    protected Integer numeroBalcao;
    @NotNull(message = "The property [numeroCliente] can't be NULL.")
    protected Integer numeroCliente;
    @NotNull(message = "The property [utilizador] can't be NULL.")
    protected String utilizador;
    @NotNull(message = "The property [valorCampo] can't be NULL.")
    protected String valorCampo;

    public String getAccao() {
        return accao;
    }

    public void setAccao(String accao) {
        this.accao = accao;
    }

    public String getAutorizacao() {
        return autorizacao;
    }

    public void setAutorizacao(String autorizacao) {
        this.autorizacao = autorizacao;
    }

    public String getCodigoCampo() {
        return codigoCampo;
    }

    public void setCodigoCampo(String codigoCampo) {
        this.codigoCampo = codigoCampo;
    }

    public Integer getNumeroBalcao() {
        return numeroBalcao;
    }

    public void setNumeroBalcao(Integer numeroBalcao) {
        this.numeroBalcao = numeroBalcao;
    }

    public Integer getNumeroCliente() {
        return numeroCliente;
    }

    public void setNumeroCliente(Integer numeroCliente) {
        this.numeroCliente = numeroCliente;
    }

    public String getUtilizador() {
        return utilizador;
    }

    public void setUtilizador(String utilizador) {
        this.utilizador = utilizador;
    }

    public String getValorCampo() {
        return valorCampo;
    }

    public void setValorCampo(String valorCampo) {
        this.valorCampo = valorCampo;
    }
}
