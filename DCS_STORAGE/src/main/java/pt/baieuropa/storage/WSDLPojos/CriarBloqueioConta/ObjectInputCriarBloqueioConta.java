
package pt.baieuropa.storage.WSDLPojos.CriarBloqueioConta;

import javax.validation.constraints.*;

public class ObjectInputCriarBloqueioConta {

    @NotNull(message = "The property [autoriz] can't be NULL.")
    protected String autoriz;
    @NotNull(message = "The property [classeBloqueada] can't be NULL.")
    protected String classeBloqueada;
    @NotNull(message = "The property [codigoNotaBloqueio] can't be NULL.")
    protected Integer codigoNotaBloqueio;
    @NotNull(message = "The property [motivoBloqueio] can't be NULL.")
    protected String motivoBloqueio;
    @NotNull(message = "The property [numeroCliente] can't be NULL.")
    protected Integer numeroCliente;
    @NotNull(message = "The property [numeroConta] can't be NULL.")
    protected String numeroConta;
    @NotNull(message = "The property [observacoes] can't be NULL.")
    protected String observacoes;
    @NotNull(message = "The property [siglaParaMensagemAlerta] can't be NULL.")
    protected String siglaParaMensagemAlerta;
    @NotNull(message = "The property [tipoBloqueio] can't be NULL.")
    protected String tipoBloqueio;
    @NotNull(message = "The property [utilizador] can't be NULL.")
    protected String utilizador;
    @NotNull(message ="The property [validarOuAplicar] can't be NULL.")
    protected String validarOuAplicar;

    public String getAutoriz() {
        return autoriz;
    }

    public void setAutoriz(String autoriz) {
        this.autoriz = autoriz;
    }

    public String getClasseBloqueada() {
        return classeBloqueada;
    }

    public void setClasseBloqueada(String classeBloqueada) {
        this.classeBloqueada = classeBloqueada;
    }

    public Integer getCodigoNotaBloqueio() {
        return codigoNotaBloqueio;
    }

    public void setCodigoNotaBloqueio(Integer codigoNotaBloqueio) {
        this.codigoNotaBloqueio = codigoNotaBloqueio;
    }

    public String getMotivoBloqueio() {
        return motivoBloqueio;
    }

    public void setMotivoBloqueio(String motivoBloqueio) {
        this.motivoBloqueio = motivoBloqueio;
    }

    public Integer getNumeroCliente() {
        return numeroCliente;
    }

    public void setNumeroCliente(Integer numeroCliente) {
        this.numeroCliente = numeroCliente;
    }

    public String getNumeroConta() {
        return numeroConta;
    }

    public void setNumeroConta(String numeroConta) {
        this.numeroConta = numeroConta;
    }

    public String getObservacoes() {
        return observacoes;
    }

    public void setObservacoes(String observacoes) {
        this.observacoes = observacoes;
    }

    public String getSiglaParaMensagemAlerta() {
        return siglaParaMensagemAlerta;
    }

    public void setSiglaParaMensagemAlerta(String siglaParaMensagemAlerta) {
        this.siglaParaMensagemAlerta = siglaParaMensagemAlerta;
    }

    public String getTipoBloqueio() {
        return tipoBloqueio;
    }

    public void setTipoBloqueio(String tipoBloqueio) {
        this.tipoBloqueio = tipoBloqueio;
    }

    public String getUtilizador() {
        return utilizador;
    }

    public void setUtilizador(String utilizador) {
        this.utilizador = utilizador;
    }

    public String getValidarOuAplicar() {
        return validarOuAplicar;
    }

    public void setValidarOuAplicar(String validarOuAplicar) {
        this.validarOuAplicar = validarOuAplicar;
    }
}
