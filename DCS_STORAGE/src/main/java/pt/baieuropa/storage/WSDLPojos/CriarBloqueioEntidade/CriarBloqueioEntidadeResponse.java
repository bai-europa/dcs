
package pt.baieuropa.storage.WSDLPojos.CriarBloqueioEntidade;

import javax.xml.bind.annotation.*;

@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "_return"
})
@XmlRootElement(name = "criarBloqueioEntidadeResponse", namespace = "http://criarBloqueioEntidade.entidadesclientes")
public class CriarBloqueioEntidadeResponse {

    @XmlElementRef(name = "return", namespace = "http://criarBloqueioEntidade.entidadesclientes", required = false)
    protected ObjectOutputCriarBloqueioEntidade _return;

    public ObjectOutputCriarBloqueioEntidade getReturn() {
        return _return;
    }

    public void setReturn(ObjectOutputCriarBloqueioEntidade value) {
        this._return = value;
    }

}
