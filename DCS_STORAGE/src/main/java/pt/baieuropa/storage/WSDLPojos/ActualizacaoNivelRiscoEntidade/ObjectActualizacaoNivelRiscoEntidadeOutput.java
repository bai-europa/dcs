
package pt.baieuropa.storage.WSDLPojos.ActualizacaoNivelRiscoEntidade;

import javax.xml.bind.annotation.*;

@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ObjectActualizacaoNivelRiscoEntidadeOutput", namespace = "http://actualizacaonivelriscoentidade.consultas/xsd", propOrder = {
    "codigoRetorno",
    "mensagemErro"
})
public class ObjectActualizacaoNivelRiscoEntidadeOutput {

    @XmlElement(namespace = "http://actualizacaonivelriscoentidade.consultas/xsd")
    protected Integer codigoRetorno;
    @XmlElementRef(name = "mensagemErro", namespace = "http://actualizacaonivelriscoentidade.consultas/xsd", required = false)
    protected String mensagemErro;

    public Integer getCodigoRetorno() {
        return codigoRetorno;
    }

    public void setCodigoRetorno(Integer codigoRetorno) {
        this.codigoRetorno = codigoRetorno;
    }

    public String getMensagemErro() {
        return mensagemErro;
    }

    public void setMensagemErro(String mensagemErro) {
        this.mensagemErro = mensagemErro;
    }
}
