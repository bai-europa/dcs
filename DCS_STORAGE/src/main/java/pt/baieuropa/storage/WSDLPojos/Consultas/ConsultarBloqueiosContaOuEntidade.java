package pt.baieuropa.storage.WSDLPojos.Consultas;


import com.fasterxml.jackson.annotation.JsonProperty;

import javax.validation.constraints.NotNull;

public class ConsultarBloqueiosContaOuEntidade {

    @NotNull(message = "The property [EntityNumber_Ext] can't be NULL.")
    @JsonProperty("EntityNumber_Ext")
    protected String entityNumber_Ext = "";
    @NotNull(message = "The property [AccountNumber_Ext] can't be NULL.")
    @JsonProperty("AccountNumber_Ext")
    protected String accountNumber_Ext = "";

    public ConsultarBloqueiosContaOuEntidade() {
    }

    public ConsultarBloqueiosContaOuEntidade(String entityNumber_Ext, String accountNumber_Ext) {
        this.entityNumber_Ext = entityNumber_Ext;
        this.accountNumber_Ext = accountNumber_Ext;
    }

    public String getEntityNumber_Ext() {
        return entityNumber_Ext;
    }

    public void setEntityNumber_Ext(String entityNumber_Ext) {
        this.entityNumber_Ext = entityNumber_Ext;
    }

    public String getAccountNumber_Ext() {
        return accountNumber_Ext;
    }

    public void setAccountNumber_Ext(String accountNumber_Ext) {
        this.accountNumber_Ext = accountNumber_Ext;
    }
}
