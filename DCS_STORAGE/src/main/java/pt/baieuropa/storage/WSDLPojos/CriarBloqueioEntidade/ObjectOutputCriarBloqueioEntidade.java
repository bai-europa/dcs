
package pt.baieuropa.storage.WSDLPojos.CriarBloqueioEntidade;

import javax.xml.bind.annotation.*;

@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ObjectOutputCriarBloqueioEntidade", namespace = "http://criarBloqueioEntidade.entidadesclientes/xsd", propOrder = {
    "codigoRetorno",
    "mensagemErro"
})
public class ObjectOutputCriarBloqueioEntidade {

    @XmlElement(namespace = "http://criarBloqueioEntidade.entidadesclientes/xsd")
    protected Integer codigoRetorno;
    @XmlElementRef(name = "mensagemErro", namespace = "http://criarBloqueioEntidade.entidadesclientes/xsd", required = false)
    protected String mensagemErro;

    public Integer getCodigoRetorno() {
        return codigoRetorno;
    }

    public void setCodigoRetorno(Integer codigoRetorno) {
        this.codigoRetorno = codigoRetorno;
    }

    public String getMensagemErro() {
        return mensagemErro;
    }

    public void setMensagemErro(String mensagemErro) {
        this.mensagemErro = mensagemErro;
    }
}
