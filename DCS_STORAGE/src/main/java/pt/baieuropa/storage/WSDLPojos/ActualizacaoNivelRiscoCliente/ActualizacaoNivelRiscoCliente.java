
package pt.baieuropa.storage.WSDLPojos.ActualizacaoNivelRiscoCliente;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import javax.xml.bind.annotation.*;

@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "dados"
})
@XmlRootElement(name = "actualizacaoNivelRiscoCliente", namespace = "http://actualizacaonivelriscocliente.consultas")
@JsonPropertyOrder({
        "dados"
})
public class ActualizacaoNivelRiscoCliente {

    @XmlElementRef(name = "dados", namespace = "http://actualizacaonivelriscocliente.consultas", required = false)
    @JsonProperty("dados")
    protected ObjectActualizacaoNivelRiscoClienteInput dados;

    public ObjectActualizacaoNivelRiscoClienteInput getDados() {
        return dados;
    }

    public void setDados(ObjectActualizacaoNivelRiscoClienteInput value) {
        this.dados = value;
    }

}
