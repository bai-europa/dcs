
package pt.baieuropa.storage.WSDLPojos.ActualizacaoNivelRiscoCliente;

import javax.xml.bind.annotation.*;

@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "_return"
})
@XmlRootElement(name = "actualizacaoNivelRiscoClienteResponse", namespace = "http://actualizacaonivelriscocliente.consultas")
public class ActualizacaoNivelRiscoClienteResponse {

    @XmlElementRef(name = "return", namespace = "http://actualizacaonivelriscocliente.consultas",  required = false)
    protected ObjectActualizacaoNivelRiscoClienteOutput _return;

    public ObjectActualizacaoNivelRiscoClienteOutput getReturn() {
        return _return;
    }

    public void setReturn(ObjectActualizacaoNivelRiscoClienteOutput value) {
        this._return = value;
    }

}
