
package pt.baieuropa.storage.WSDLPojos.Evaluate;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for ArrayOfValidateService complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="ArrayOfValidateService">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="ValidateService" type="{http://www.outsystems.com}ValidateService" maxOccurs="unbounded" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ArrayOfValidateService", namespace = "http://www.outsystems.com", propOrder = {
    "validateService"
})
public class ArrayOfValidateService {

    @XmlElement(name = "ValidateService", namespace = "http://www.outsystems.com", nillable = true)
    protected List<ValidateService> validateService;

    /**
     * Gets the value of the validateService property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the validateService property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getValidateService().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link ValidateService }
     * 
     * 
     */
    public List<ValidateService> getValidateService() {
        if (validateService == null) {
            validateService = new ArrayList<ValidateService>();
        }
        return this.validateService;
    }

}
