
package pt.baieuropa.storage.WSDLPojos.CriarBloqueioConta;

import javax.xml.bind.annotation.*;

@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "_return"
})
@XmlRootElement(name = "criarBloqueioContaResponse", namespace = "http://criarBloqueioConta.entidadesclientes")
public class CriarBloqueioContaResponse {

    @XmlElementRef(name = "return", namespace = "http://criarBloqueioConta.entidadesclientes", required = false)
    protected ObjectOutputCriarBloqueioConta _return;

    public ObjectOutputCriarBloqueioConta getReturn() {
        return _return;
    }

    public void setReturn(ObjectOutputCriarBloqueioConta value) {
        this._return = value;
    }

}
