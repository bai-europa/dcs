
package pt.baieuropa.storage.WSDLPojos.Evaluate;

import com.fasterxml.jackson.annotation.JsonProperty;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


public class EntityAccount {

    protected String entityNumberExt;
    protected String entityAccountRelationType;

    @JsonProperty("EntityNumber_Ext")
    public String getEntityNumberExt() {
        return entityNumberExt;
    }

    public void setEntityNumberExt(String value) {
        this.entityNumberExt = value;
    }

    @JsonProperty("EntityAccountRelationType")
    public String getEntityAccountRelationType() {
        return entityAccountRelationType;
    }

    public void setEntityAccountRelationType(String value) {
        this.entityAccountRelationType = value;
    }

}
