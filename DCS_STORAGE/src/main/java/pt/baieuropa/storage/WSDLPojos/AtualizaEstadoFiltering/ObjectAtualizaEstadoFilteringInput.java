
package pt.baieuropa.storage.WSDLPojos.AtualizaEstadoFiltering;

import com.fasterxml.jackson.annotation.JsonInclude;

import javax.validation.constraints.NotNull;

public class ObjectAtualizaEstadoFilteringInput {

    @NotNull(message = "The property [codigoElemento] can't be NULL.")
    protected String codigoElemento;
    @NotNull(message = "The property [novoValor] can't be NULL.")
    protected String novoValor;
    @NotNull(message = "The property [numeroEntidade] can't be NULL.")
    protected Integer numeroEntidade;
    @NotNull(message = "The property [utilizador] can't be NULL.")
    protected String utilizador;

    public String getCodigoElemento() {
        return codigoElemento;
    }

    public void setCodigoElemento(String codigoElemento) {
        this.codigoElemento = codigoElemento;
    }

    public String getNovoValor() {
        return novoValor;
    }

    public void setNovoValor(String novoValor) {
        this.novoValor = novoValor;
    }

    public Integer getNumeroEntidade() {
        return numeroEntidade;
    }

    public void setNumeroEntidade(Integer numeroEntidade) {
        this.numeroEntidade = numeroEntidade;
    }

    public String getUtilizador() {
        return utilizador;
    }

    public void setUtilizador(String utilizador) {
        this.utilizador = utilizador;
    }
}
