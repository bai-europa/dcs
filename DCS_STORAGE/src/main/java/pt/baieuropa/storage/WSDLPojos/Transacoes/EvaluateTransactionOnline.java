package pt.baieuropa.storage.WSDLPojos.Transacoes;

import com.fasterxml.jackson.annotation.JsonProperty;
import pt.baieuropa.storage.WSDLPojos.Evaluate.FlexfieldStructure;

import java.math.BigDecimal;

public class EvaluateTransactionOnline {

    String AccountNumber_Ext;
    String TransactionNumber;
    String TransactionOperationCode;
    BigDecimal TransactionAmount_Original;
    String TransactionCurrency_Original;
    BigDecimal TransactionAmount_Converted;
    BigDecimal TransactionAmount_Account = new BigDecimal(0);
    String TransactionCurrency_Account = "";
    String TransactionDateTime;
    String MovementLaunchDateTime;
    String CreditDebit;
    String CorrespondingBankCode = "";
    String CounterpartBankCode = "";
    String CounterpartName = "";
    String CounterpartCountryCode = "";
    String ClientNumber_Ext = "";
    String CounterpartAccountNumber = "";
    String TransactionType;
    String TransactionStatus;
    String CAE = "";
    String CAE2 = "";
    String Branch = "";
    String Field70 = "";
    String RequestUser = "";
    FlexfieldStructure[] Flexfields = new FlexfieldStructure[0];

    public EvaluateTransactionOnline() { }

    @JsonProperty("AccountNumber_Ext")
    public String getAccountNumber_Ext() {
        return AccountNumber_Ext;
    }

    public void setAccountNumber_Ext(String accountNumber_Ext) {
        AccountNumber_Ext = accountNumber_Ext;
    }

    @JsonProperty("TransactionNumber")
    public String getTransactionNumber() {
        return TransactionNumber;
    }

    public void setTransactionNumber(String transactionNumber) {
        TransactionNumber = transactionNumber;
    }

    @JsonProperty("TransactionOperationCode")
    public String getTransactionOperationCode() {
        return TransactionOperationCode;
    }

    public void setTransactionOperationCode(String transactionOperationCode) {
        TransactionOperationCode = transactionOperationCode;
    }

    @JsonProperty("TransactionAmount_Original")
    public BigDecimal getTransactionAmount_Original() {
        return TransactionAmount_Original;
    }

    public void setTransactionAmount_Original(BigDecimal transactionAmount_Original) {
        TransactionAmount_Original = transactionAmount_Original;
    }

    @JsonProperty("TransactionCurrency_Original")
    public String getTransactionCurrency_Original() {
        return TransactionCurrency_Original;
    }

    public void setTransactionCurrency_Original(String transactionCurrency_Original) {
        TransactionCurrency_Original = transactionCurrency_Original;
    }

    @JsonProperty("TransactionAmount_Converted")
    public BigDecimal getTransactionAmount_Converted() {
        return TransactionAmount_Converted;
    }

    public void setTransactionAmount_Converted(BigDecimal transactionAmount_Converted) {
        TransactionAmount_Converted = transactionAmount_Converted;
    }

    @JsonProperty("TransactionAmount_Account")
    public BigDecimal getTransactionAmount_Account() {
        return TransactionAmount_Account;
    }

    public void setTransactionAmount_Account(BigDecimal transactionAmount_Account) {
        TransactionAmount_Account = transactionAmount_Account;
    }

    @JsonProperty("TransactionCurrency_Account")
    public String getTransactionCurrency_Account() {
        return TransactionCurrency_Account;
    }

    public void setTransactionCurrency_Account(String transactionCurrency_Account) {
        TransactionCurrency_Account = transactionCurrency_Account;
    }

    @JsonProperty("TransactionDateTime")
    public String getTransactionDateTime() {
        return TransactionDateTime;
    }

    public void setTransactionDateTime(String transactionDateTime) {
        TransactionDateTime = transactionDateTime;
    }

    @JsonProperty("MovementLaunchDateTime")
    public String getMovementLaunchDateTime() {
        return MovementLaunchDateTime;
    }

    public void setMovementLaunchDateTime(String movementLaunchDateTime) {
        MovementLaunchDateTime = movementLaunchDateTime;
    }

    @JsonProperty("CreditDebit")
    public String getCreditDebit() {
        return CreditDebit;
    }

    public void setCreditDebit(String creditDebit) {
        CreditDebit = creditDebit;
    }

    @JsonProperty("CorrespondingBankCode")
    public String getCorrespondingBankCode() {
        return CorrespondingBankCode;
    }

    public void setCorrespondingBankCode(String correspondingBankCode) {
        CorrespondingBankCode = correspondingBankCode;
    }

    @JsonProperty("CounterpartBankCode")
    public String getCounterpartBankCode() {
        return CounterpartBankCode;
    }

    public void setCounterpartBankCode(String counterpartBankCode) {
        CounterpartBankCode = counterpartBankCode;
    }

    @JsonProperty("CounterpartName")
    public String getCounterpartName() {
        return CounterpartName;
    }

    public void setCounterpartName(String counterpartName) {
        CounterpartName = counterpartName;
    }

    @JsonProperty("CounterpartCountryCode")
    public String getCounterpartCountryCode() {
        return CounterpartCountryCode;
    }

    public void setCounterpartCountryCode(String counterpartCountryCode) {
        CounterpartCountryCode = counterpartCountryCode;
    }

    @JsonProperty("ClientNumber_Ext")
    public String getClientNumber_Ext() {
        return ClientNumber_Ext;
    }

    public void setClientNumber_Ext(String clientNumber_Ext) {
        ClientNumber_Ext = clientNumber_Ext;
    }

    @JsonProperty("CounterpartAccountNumber")
    public String getCounterpartAccountNumber() {
        return CounterpartAccountNumber;
    }

    public void setCounterpartAccountNumber(String counterpartAccountNumber) {
        CounterpartAccountNumber = counterpartAccountNumber;
    }

    @JsonProperty("TransactionType")
    public String getTransactionType() {
        return TransactionType;
    }

    public void setTransactionType(String transactionType) {
        TransactionType = transactionType;
    }

    @JsonProperty("TransactionStatus")
    public String getTransactionStatus() {
        return TransactionStatus;
    }

    public void setTransactionStatus(String transactionStatus) {
        TransactionStatus = transactionStatus;
    }

    @JsonProperty("CAE")
    public String getCAE() {
        return CAE;
    }

    public void setCAE(String CAE) {
        this.CAE = CAE;
    }

    @JsonProperty("CAE2")
    public String getCAE2() {
        return CAE2;
    }

    public void setCAE2(String CAE2) {
        this.CAE2 = CAE2;
    }

    @JsonProperty("Branch")
    public String getBranch() {
        return Branch;
    }

    public void setBranch(String branch) {
        Branch = branch;
    }

    @JsonProperty("Field70")
    public String getField70() {
        return Field70;
    }

    public void setField70(String field70) {
        Field70 = field70;
    }

    @JsonProperty("RequestUser")
    public String getRequestUser() {
        return RequestUser;
    }

    public void setRequestUser(String requestUser) {
        RequestUser = requestUser;
    }

    @JsonProperty("Flexfields")
    public FlexfieldStructure[] getFlexfields() {
        return Flexfields;
    }

    public void setFlexfields(FlexfieldStructure[] flexfields) {
        Flexfields = flexfields;
    }
}
