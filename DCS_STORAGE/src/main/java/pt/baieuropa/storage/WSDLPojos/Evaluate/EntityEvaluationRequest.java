package pt.baieuropa.storage.WSDLPojos.Evaluate;

import com.fasterxml.jackson.annotation.JsonProperty;
import java.math.BigDecimal;

public class EntityEvaluationRequest {

    protected String numberExt;
    protected String name;
    protected String entityCategory;
    protected String entityType;
    protected String birthDate = null;
    protected String gender = "";
    protected String maritalStatus = "";
    protected String nationality = "";
    protected String nationality2 = "";
    protected String nationality3 = "";
    protected String countryResidence = "";
    protected String residenceCode = "";
    protected String birthPlace = "";
    protected String constitutionDate = null;
    protected String profession = "";
    protected String professionalActivity = "";
    protected String educationalQualifications = "";
    protected String sectorialCode = "";
    protected BigDecimal socialCapital = new BigDecimal(0);
    protected BigDecimal entityIncome = new BigDecimal(0);
    protected String parent1Name = "";
    protected String parent2Name = "";
    protected String currency = "";
    protected String cae = "";
    protected String cae2 = "";
    protected String cae3 = "";
    protected String societyType = "";
    protected String stockMarketCode = "";
    protected String createdAtExt = null;
    protected String requestUser = "";
    protected String requestId = "";
    protected String action;
    protected boolean isPEPDeclared = false;
    protected String isPEPDescription = "";
    protected String importDate = null;
    protected boolean isPresencial = false;
    protected FlexfieldStructure[] flexfields = new FlexfieldStructure[0];
    protected Document[] documentList = new Document[0];
    protected Contact[] contactList = new Contact[0];
    protected byte[] photo = new byte[0];
    protected EntityBeneficialOwnersList[] beneficialOwnersList = new EntityBeneficialOwnersList[0];

    @JsonProperty("Number_Ext")
    public String getNumberExt() {
        return numberExt;
    }

    public void setNumberExt(String numberExt) {
        this.numberExt = numberExt;
    }

    @JsonProperty("Name")
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @JsonProperty("EntityCategory")
    public String getEntityCategory() {
        return entityCategory;
    }

    public void setEntityCategory(String entityCategory) {
        this.entityCategory = entityCategory;
    }

    @JsonProperty("EntityType")
    public String getEntityType() {
        return entityType;
    }

    public void setEntityType(String entityType) {
        this.entityType = entityType;
    }

    @JsonProperty("BirthDate")
    public String getBirthDate() {
        return birthDate;
    }

    public void setBirthDate(String birthDate) {
        this.birthDate = birthDate;
    }

    @JsonProperty("Gender")
    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    @JsonProperty("MaritalStatus")
    public String getMaritalStatus() {
        return maritalStatus;
    }

    public void setMaritalStatus(String maritalStatus) {
        this.maritalStatus = maritalStatus;
    }

    @JsonProperty("Nationality")
    public String getNationality() {
        return nationality;
    }

    public void setNationality(String nationality) {
        this.nationality = nationality;
    }

    @JsonProperty("Nationality2")
    public String getNationality2() {
        return nationality2;
    }

    public void setNationality2(String nationality2) {
        this.nationality2 = nationality2;
    }

    @JsonProperty("Nationality3")
    public String getNationality3() {
        return nationality3;
    }

    public void setNationality3(String nationality3) {
        this.nationality3 = nationality3;
    }

    @JsonProperty("CountryResidence")
    public String getCountryResidence() {
        return countryResidence;
    }

    public void setCountryResidence(String countryResidence) {
        this.countryResidence = countryResidence;
    }

    @JsonProperty("ResidenceCode")
    public String getResidenceCode() {
        return residenceCode;
    }

    public void setResidenceCode(String residenceCode) {
        this.residenceCode = residenceCode;
    }

    @JsonProperty("BirthPlace")
    public String getBirthPlace() {
        return birthPlace;
    }

    public void setBirthPlace(String birthPlace) {
        this.birthPlace = birthPlace;
    }

    @JsonProperty("ConstitutionDate")
    public String getConstitutionDate() {
        return constitutionDate;
    }

    public void setConstitutionDate(String constitutionDate) {
        this.constitutionDate = constitutionDate;
    }

    @JsonProperty("Profession")
    public String getProfession() {
        return profession;
    }

    public void setProfession(String profession) {
        this.profession = profession;
    }

    @JsonProperty("ProfessionalActivity")
    public String getProfessionalActivity() {
        return professionalActivity;
    }

    public void setProfessionalActivity(String professionalActivity) {
        this.professionalActivity = professionalActivity;
    }

    @JsonProperty("EducationalQualifications")
    public String getEducationalQualifications() {
        return educationalQualifications;
    }

    public void setEducationalQualifications(String educationalQualifications) {
        this.educationalQualifications = educationalQualifications;
    }

    @JsonProperty("SectorialCode")
    public String getSectorialCode() {
        return sectorialCode;
    }

    public void setSectorialCode(String sectorialCode) {
        this.sectorialCode = sectorialCode;
    }

    @JsonProperty("SocialCapital")
    public BigDecimal getSocialCapital() {
        return socialCapital;
    }

    public void setSocialCapital(BigDecimal socialCapital) {
        this.socialCapital = socialCapital;
    }

    @JsonProperty("EntityIncome")
    public BigDecimal getEntityIncome() {
        return entityIncome;
    }

    public void setEntityIncome(BigDecimal entityIncome) {
        this.entityIncome = entityIncome;
    }

    @JsonProperty("Parent1Name")
    public String getParent1Name() {
        return parent1Name;
    }

    public void setParent1Name(String parent1Name) {
        this.parent1Name = parent1Name;
    }

    @JsonProperty("Parent2Name")
    public String getParent2Name() {
        return parent2Name;
    }

    public void setParent2Name(String parent2Name) {
        this.parent2Name = parent2Name;
    }

    @JsonProperty("Currency")
    public String getCurrency() {
        return currency;
    }

    public void setCurrency(String currency) {
        this.currency = currency;
    }

    @JsonProperty("CAE")
    public String getCae() {
        return cae;
    }

    public void setCae(String cae) {
        this.cae = cae;
    }

    @JsonProperty("CAE2")
    public String getCae2() {
        return cae2;
    }

    public void setCae2(String cae2) {
        this.cae2 = cae2;
    }

    @JsonProperty("CAE3")
    public String getCae3() {
        return cae3;
    }

    public void setCae3(String cae3) {
        this.cae3 = cae3;
    }

    @JsonProperty("SocietyType")
    public String getSocietyType() {
        return societyType;
    }

    public void setSocietyType(String societyType) {
        this.societyType = societyType;
    }

    @JsonProperty("StockMarketCode")
    public String getStockMarketCode() {
        return stockMarketCode;
    }

    public void setStockMarketCode(String stockMarketCode) {
        this.stockMarketCode = stockMarketCode;
    }

    @JsonProperty("CreatedAt_Ext")
    public String getCreatedAtExt() {
        return createdAtExt;
    }

    public void setCreatedAtExt(String createdAtExt) {
        this.createdAtExt = createdAtExt;
    }

    @JsonProperty("RequestUser")
    public String getRequestUser() {
        return requestUser;
    }

    public void setRequestUser(String requestUser) {
        this.requestUser = requestUser;
    }

    @JsonProperty("RequestId")
    public String getRequestId() {
        return requestId;
    }

    public void setRequestId(String requestId) {
        this.requestId = requestId;
    }

    @JsonProperty("Action")
    public String getAction() {
        return action;
    }

    public void setAction(String action) {
        this.action = action;
    }

    @JsonProperty("IsPEPDeclared")
    public boolean isPEPDeclared() {
        return isPEPDeclared;
    }

    public void setPEPDeclared(boolean PEPDeclared) {
        isPEPDeclared = PEPDeclared;
    }

    @JsonProperty("IsPEPDescription")
    public String getIsPEPDescription() {
        return isPEPDescription;
    }

    public void setIsPEPDescription(String isPEPDescription) {
        this.isPEPDescription = isPEPDescription;
    }

    @JsonProperty("ImportDate")
    public String getImportDate() {
        return importDate;
    }

    public void setImportDate(String importDate) {
        this.importDate = importDate;
    }

    @JsonProperty("IsPresencial")
    public boolean isPresencial() {
        return isPresencial;
    }

    public void setPresencial(boolean presencial) {
        isPresencial = presencial;
    }

    @JsonProperty("Flexfields")
    public FlexfieldStructure[] getFlexfields() {
        return flexfields;
    }

    public void setFlexfields(FlexfieldStructure[] flexfields) {
        this.flexfields = flexfields;
    }

    @JsonProperty("DocumentList")
    public Document[] getDocumentList() {
        return documentList;
    }

    public void setDocumentList(Document[] documentList) {
        this.documentList = documentList;
    }

    @JsonProperty("ContactList")
    public Contact[] getContactList() {
        return contactList;
    }

    public void setContactList(Contact[] contactList) {
        this.contactList = contactList;
    }

    @JsonProperty("Photo")
    public byte[] getPhoto() {
        return photo;
    }

    public void setPhoto(byte[] photo) {
        this.photo = photo;
    }

    @JsonProperty("BeneficialOwnersList")
    public EntityBeneficialOwnersList[] getBeneficialOwnersList() {
        return beneficialOwnersList;
    }

    public void setBeneficialOwnersList(EntityBeneficialOwnersList[] beneficialOwnersList) {
        this.beneficialOwnersList = beneficialOwnersList;
    }
}
