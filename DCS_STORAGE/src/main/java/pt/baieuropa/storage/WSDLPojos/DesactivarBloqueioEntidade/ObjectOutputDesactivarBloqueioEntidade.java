
package pt.baieuropa.storage.WSDLPojos.DesactivarBloqueioEntidade;

import javax.xml.bind.annotation.*;

@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ObjectOutputDesactivarBloqueioEntidade", namespace = "http://desactivarBloqueioEntidade.entidadesclientes/xsd", propOrder = {
    "codigoRetorno",
    "mensagemErro"
})
public class ObjectOutputDesactivarBloqueioEntidade {

    @XmlElement(namespace = "http://desactivarBloqueioEntidade.entidadesclientes/xsd")
    protected Integer codigoRetorno;
    @XmlElementRef(name = "mensagemErro", namespace = "http://desactivarBloqueioEntidade.entidadesclientes/xsd", required = false)
    protected String mensagemErro;

    public Integer getCodigoRetorno() {
        return codigoRetorno;
    }

    public void setCodigoRetorno(Integer value) {
        this.codigoRetorno = value;
    }

    public String getMensagemErro() {
        return mensagemErro;
    }

    public void setMensagemErro(String mensagemErro) {
        this.mensagemErro = mensagemErro;
    }
}
