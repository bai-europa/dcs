
package pt.baieuropa.storage.WSDLPojos.DesactivarBloqueioConta;

import javax.xml.bind.annotation.*;


@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ObjectOutputDesactivarBloqueioConta", namespace = "http://desactivarBloqueioConta.entidadesclientes/xsd", propOrder = {
    "codigoRetorno",
    "mensagemErro"
})
public class ObjectOutputDesactivarBloqueioConta {

    @XmlElement(namespace = "http://desactivarBloqueioConta.entidadesclientes/xsd")
    protected Integer codigoRetorno;
    @XmlElementRef(name = "mensagemErro", namespace = "http://desactivarBloqueioConta.entidadesclientes/xsd", required = false)
    protected String mensagemErro;

    public Integer getCodigoRetorno() {
        return codigoRetorno;
    }

    public void setCodigoRetorno(Integer value) {
        this.codigoRetorno = value;
    }

    public String getMensagemErro() {
        return mensagemErro;
    }

    public void setMensagemErro(String value) {
        this.mensagemErro = value;
    }

}
