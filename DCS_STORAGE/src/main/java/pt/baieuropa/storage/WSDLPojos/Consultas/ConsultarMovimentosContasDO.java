package pt.baieuropa.storage.WSDLPojos.Consultas;

import com.fasterxml.jackson.annotation.JsonProperty;

import javax.validation.constraints.NotNull;

public class ConsultarMovimentosContasDO {

    @NotNull(message = "The property [dataInicio] can't be NULL.")
    @JsonProperty("dataInicio ")
    private String dataInicio ;

    @NotNull(message = "The property [dataFim] can't be NULL.")
    @JsonProperty("dataFim  ")
    private String dataFim;

    @NotNull(message = "The property [numeroConta] can't be NULL.")
    @JsonProperty("numeroConta")
    private String numeroConta;

    @NotNull(message = "The property [numeroMovimentosConsultar] can't be NULL.")
    @JsonProperty("numeroMovimentosConsultar")
    private int numeroMovimentosConsultar;

    @NotNull(message = "The property [numeroOrdemParaPosicionar] can't be NULL.")
    @JsonProperty("numeroOrdemParaPosicionar")
    private int numeroOrdemParaPosicionar;

    @NotNull(message = "The property [utilizador] can't be NULL.")
    @JsonProperty("utilizador")
    private String utilizador;

    public ConsultarMovimentosContasDO() {
    }

    public ConsultarMovimentosContasDO(@NotNull(message = "The property [dataInicio] can't be NULL.") String dataInicio, @NotNull(message = "The property [dataFim] can't be NULL.") String dataFim, @NotNull(message = "The property [numeroConta] can't be NULL.") String numeroConta, @NotNull(message = "The property [numeroMovimentosConsultar] can't be NULL.") int numeroMovimentosConsultar, @NotNull(message = "The property [numeroOrdemParaPosicionar] can't be NULL.") int numeroOrdemParaPosicionar, @NotNull(message = "The property [utilizador] can't be NULL.") String utilizador) {
        this.dataInicio = dataInicio;
        this.dataFim = dataFim;
        this.numeroConta = numeroConta;
        this.numeroMovimentosConsultar = numeroMovimentosConsultar;
        this.numeroOrdemParaPosicionar = numeroOrdemParaPosicionar;
        this.utilizador = utilizador;
    }

    public String getDataInicio() {
        return dataInicio;
    }

    public void setDataInicio(String dataInicio) {
        this.dataInicio = dataInicio;
    }

    public String getDataFim() {
        return dataFim;
    }

    public void setDataFim(String dataFim) {
        this.dataFim = dataFim;
    }

    public String getNumeroConta() {
        return numeroConta;
    }

    public void setNumeroConta(String numeroConta) {
        this.numeroConta = numeroConta;
    }

    public int getNumeroMovimentosConsultar() {
        return numeroMovimentosConsultar;
    }

    public void setNumeroMovimentosConsultar(int numeroMovimentosConsultar) {
        this.numeroMovimentosConsultar = numeroMovimentosConsultar;
    }

    public int getNumeroOrdemParaPosicionar() {
        return numeroOrdemParaPosicionar;
    }

    public void setNumeroOrdemParaPosicionar(int numeroOrdemParaPosicionar) {
        this.numeroOrdemParaPosicionar = numeroOrdemParaPosicionar;
    }

    public String getUtilizador() {
        return utilizador;
    }

    public void setUtilizador(String utilizador) {
        this.utilizador = utilizador;
    }
}
