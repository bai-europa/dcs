
package pt.baieuropa.storage.WSDLPojos.Evaluate;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="AccountCreationResponse" type="{http://www.outsystems.com}WSResponseAccountOnline" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "accountCreationResponse"
})
@XmlRootElement(name = "EvaluateAccountOnlineResponse", namespace = "http://www.outsystems.com")
public class EvaluateAccountOnlineResponse {

    @XmlElement(name = "AccountCreationResponse", namespace = "http://www.outsystems.com")
    protected WSResponseAccountOnline accountCreationResponse;

    /**
     * Gets the value of the accountCreationResponse property.
     * 
     * @return
     *     possible object is
     *     {@link WSResponseAccountOnline }
     *     
     */
    public WSResponseAccountOnline getAccountCreationResponse() {
        return accountCreationResponse;
    }

    /**
     * Sets the value of the accountCreationResponse property.
     * 
     * @param value
     *     allowed object is
     *     {@link WSResponseAccountOnline }
     *     
     */
    public void setAccountCreationResponse(WSResponseAccountOnline value) {
        this.accountCreationResponse = value;
    }

}
