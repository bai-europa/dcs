package pt.baieuropa.storage.WSDLPojos.Consultas;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;
import java.util.LinkedList;

@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(
        propOrder = {
            "balcaoDaConta",
            "codigoRetorno",
            "listaMovimentos",
            "mensagemErro",
            "moeda",
            "numeroMovimentosConsultados",
            "saldo",
            "saldoDisponivel",
            "saldoNaSegundaMoeda",
            "segundaMoeda",
            "statusDaTransaccao",
        }
)
public class ObjectConsultaDeMovimentosContaDOOutput {

    private String balcaoDaConta;
    private int codigoRetorno;
    private LinkedList<Movimento> listaMovimentos;
    private String mensagemErro;
    private String moeda;
    private String numeroMovimentosConsultados;
    private String saldo;
    private String saldoDisponivel;
    private String saldoNaSegundaMoeda;
    private String segundaMoeda;
    private String statusDaTransaccao;

    public ObjectConsultaDeMovimentosContaDOOutput() {
    }

    public ObjectConsultaDeMovimentosContaDOOutput(String balcaoDaConta, int codigoRetorno, LinkedList<Movimento> listaMovimentos, String mensagemErro, String moeda, String numeroMovimentosConsultados, String saldo, String saldoDisponivel, String saldoNaSegundaMoeda, String segundaMoeda, String statusDaTransaccao) {
        this.balcaoDaConta = balcaoDaConta;
        this.codigoRetorno = codigoRetorno;
        this.listaMovimentos = listaMovimentos;
        this.mensagemErro = mensagemErro;
        this.moeda = moeda;
        this.numeroMovimentosConsultados = numeroMovimentosConsultados;
        this.saldo = saldo;
        this.saldoDisponivel = saldoDisponivel;
        this.saldoNaSegundaMoeda = saldoNaSegundaMoeda;
        this.segundaMoeda = segundaMoeda;
        this.statusDaTransaccao = statusDaTransaccao;
    }

    public String getBalcaoDaConta() {
        return balcaoDaConta;
    }

    public void setBalcaoDaConta(String balcaoDaConta) {
        this.balcaoDaConta = balcaoDaConta;
    }

    public int getCodigoRetorno() {
        return codigoRetorno;
    }

    public void setCodigoRetorno(int codigoRetorno) {
        this.codigoRetorno = codigoRetorno;
    }

    public LinkedList<Movimento> getListaMovimentos() {
        return listaMovimentos;
    }

    public void setListaMovimentos(LinkedList<Movimento> listaMovimentos) {
        this.listaMovimentos = listaMovimentos;
    }

    public String getMensagemErro() {
        return mensagemErro;
    }

    public void setMensagemErro(String mensagemErro) {
        this.mensagemErro = mensagemErro;
    }

    public String getMoeda() {
        return moeda;
    }

    public void setMoeda(String moeda) {
        this.moeda = moeda;
    }

    public String getNumeroMovimentosConsultados() {
        return numeroMovimentosConsultados;
    }

    public void setNumeroMovimentosConsultados(String numeroMovimentosConsultados) {
        this.numeroMovimentosConsultados = numeroMovimentosConsultados;
    }

    public String getSaldo() {
        return saldo;
    }

    public void setSaldo(String saldo) {
        this.saldo = saldo;
    }

    public String getSaldoDisponivel() {
        return saldoDisponivel;
    }

    public void setSaldoDisponivel(String saldoDisponivel) {
        this.saldoDisponivel = saldoDisponivel;
    }

    public String getSaldoNaSegundaMoeda() {
        return saldoNaSegundaMoeda;
    }

    public void setSaldoNaSegundaMoeda(String saldoNaSegundaMoeda) {
        this.saldoNaSegundaMoeda = saldoNaSegundaMoeda;
    }

    public String getSegundaMoeda() {
        return segundaMoeda;
    }

    public void setSegundaMoeda(String segundaMoeda) {
        this.segundaMoeda = segundaMoeda;
    }

    public String getStatusDaTransaccao() {
        return statusDaTransaccao;
    }

    public void setStatusDaTransaccao(String statusDaTransaccao) {
        this.statusDaTransaccao = statusDaTransaccao;
    }
}
