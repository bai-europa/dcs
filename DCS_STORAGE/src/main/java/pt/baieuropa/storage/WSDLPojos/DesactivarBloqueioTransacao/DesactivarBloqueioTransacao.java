package pt.baieuropa.storage.WSDLPojos.DesactivarBloqueioTransacao;

public class DesactivarBloqueioTransacao {

    protected ObjectInputDesactivarBloqueioTransacao dados;

    public ObjectInputDesactivarBloqueioTransacao getDados() {
        return dados;
    }

    public void setDados(ObjectInputDesactivarBloqueioTransacao value) {
        this.dados = value;
    }
}
