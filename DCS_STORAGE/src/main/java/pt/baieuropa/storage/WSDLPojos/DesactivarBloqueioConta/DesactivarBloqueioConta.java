
package pt.baieuropa.storage.WSDLPojos.DesactivarBloqueioConta;

import javax.xml.bind.annotation.*;


@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "dados"
})
@XmlRootElement(name = "desactivarBloqueioConta", namespace = "http://desactivarBloqueioConta.entidadesclientes")
public class DesactivarBloqueioConta {

    @XmlElementRef(name = "dados", namespace = "http://desactivarBloqueioConta.entidadesclientes", required = false)
    protected ObjectInputDesactivarBloqueioConta dados;

    public ObjectInputDesactivarBloqueioConta getDados() {
        return dados;
    }

    public void setDados(ObjectInputDesactivarBloqueioConta value) {
        this.dados = value;
    }

}
