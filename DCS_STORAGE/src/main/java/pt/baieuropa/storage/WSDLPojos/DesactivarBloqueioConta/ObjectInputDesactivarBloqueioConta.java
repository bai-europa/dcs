
package pt.baieuropa.storage.WSDLPojos.DesactivarBloqueioConta;

import javax.validation.constraints.NotNull;

public class ObjectInputDesactivarBloqueioConta {
    
    @NotNull(message = "The property [autoriz] can't be NULL.")
    protected String autoriz;
    @NotNull(message = "The property [codigoNotaDesbloqueio] can't be NULL.")
    protected Integer codigoNotaDesbloqueio;
    @NotNull(message = "The property [numeroCliente] can't be NULL.")
    protected Integer numeroCliente;
    @NotNull(message = "The property [numeroConta] can't be NULL.")
    protected String numeroConta;
    @NotNull(message = "The property [observacoesDesativacao] can't be NULL.")
    protected String observacoesDesativacao;
    @NotNull(message = "The property [siglaParaMensagemAlerta] can't be NULL.")
    protected String siglaParaMensagemAlerta;
    @NotNull(message = "The property [tipoBloqueio] can't be NULL.")
    protected String tipoBloqueio;
    @NotNull(message = "The property [utilizador] can't be NULL.")
    protected String utilizador;
    @NotNull(message = "The property [validarOuAplicar] can't be NULL.")
    protected String validarOuAplicar;

    public String getAutoriz() {
        return autoriz;
    }

    public void setAutoriz(String value) {
        this.autoriz = value;
    }

    public Integer getCodigoNotaDesbloqueio() {
        return codigoNotaDesbloqueio;
    }

    public void setCodigoNotaDesbloqueio(Integer value) {
        this.codigoNotaDesbloqueio = value;
    }

    public Integer getNumeroCliente() {
        return numeroCliente;
    }

    public void setNumeroCliente(Integer value) {
        this.numeroCliente = value;
    }

    public String getNumeroConta() {
        return numeroConta;
    }

    public void setNumeroConta(String value) {
        this.numeroConta = value;
    }

    public String getObservacoesDesativacao() {
        return observacoesDesativacao;
    }

    public void setObservacoesDesativacao(String value) {
        this.observacoesDesativacao = value;
    }

    public String getSiglaParaMensagemAlerta() {
        return siglaParaMensagemAlerta;
    }

    public void setSiglaParaMensagemAlerta(String value) {
        this.siglaParaMensagemAlerta = value;
    }

    public String getTipoBloqueio() {
        return tipoBloqueio;
    }

    public void setTipoBloqueio(String value) {
        this.tipoBloqueio = value;
    }

    public String getUtilizador() {
        return utilizador;
    }

    public void setUtilizador(String value) {
        this.utilizador = value;
    }

    public String getValidarOuAplicar() {
        return validarOuAplicar;
    }

    public void setValidarOuAplicar(String value) {
        this.validarOuAplicar = value;
    }

}
