
package pt.baieuropa.storage.WSDLPojos.DesactivarBloqueioConta;

import javax.xml.bind.annotation.*;


@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "_return"
})
@XmlRootElement(name = "desactivarBloqueioContaResponse", namespace = "http://desactivarBloqueioConta.entidadesclientes")
public class DesactivarBloqueioContaResponse {

    @XmlElementRef(name = "return", namespace = "http://desactivarBloqueioConta.entidadesclientes", required = false)
    protected ObjectOutputDesactivarBloqueioConta _return;

    public ObjectOutputDesactivarBloqueioConta getReturn() {
        return _return;
    }

    public void setReturn(ObjectOutputDesactivarBloqueioConta value) {
        this._return = value;
    }

}
