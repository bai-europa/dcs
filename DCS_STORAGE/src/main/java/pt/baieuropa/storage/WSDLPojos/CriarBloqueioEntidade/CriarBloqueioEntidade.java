
package pt.baieuropa.storage.WSDLPojos.CriarBloqueioEntidade;

import javax.xml.bind.annotation.*;

@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "dados"
})
@XmlRootElement(name = "criarBloqueioEntidade", namespace = "http://criarBloqueioEntidade.entidadesclientes")
public class CriarBloqueioEntidade {

    @XmlElementRef(name = "dados", namespace = "http://criarBloqueioEntidade.entidadesclientes", required = false)
    protected ObjectInputCriarBloqueioEntidade dados;

    public ObjectInputCriarBloqueioEntidade getDados() {
        return dados;
    }

    public void setDados(ObjectInputCriarBloqueioEntidade value) {
        this.dados = value;
    }

}
