package pt.baieuropa.storage.WSDLPojos.Consultas;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;

@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(
        propOrder = {
                "balcaoMovimento",
                "codigoOperacao",
                "dataDeLancamento",
                "dataEmissaoExtracto",
                "dataValor",
                "debitoCredito",
                "descritivoMovimento",
                "dualCurrency",
                "executorOrigem",
                "moedaAfectaConta",
                "moedaTransaccao",
                "numeroDeDocumento",
                "numeroDeOperacao",
                "numeroOrdem",
                "saldoAposMovimento",
                "tipoDeDocumento",
                "valorParaExtracto",
                "valorTransaccao",
        }
)
public class Movimento {

    private String balcaoMovimento;
    private String codigoOperacao;
    private String dataDeLancamento;
    private String dataEmissaoExtracto;
    private String dataValor;
    private String debitoCredito;
    private String descritivoMovimento;
    private String dualCurrency;
    private String executorOrigem;
    private String moedaAfectaConta;
    private String moedaTransaccao;
    private String numeroDeDocumento;
    private String numeroDeOperacao;
    private String numeroOrdem;
    private String saldoAposMovimento;
    private String tipoDeDocumento;
    private String valorParaExtracto;
    private String valorTransaccao;

    public Movimento() {
    }

    public Movimento(String balcaoMovimento, String codigoOperacao, String dataDeLancamento, String dataEmissaoExtracto, String dataValor, String debitoCredito, String descritivoMovimento, String dualCurrency, String executorOrigem, String moedaAfectaConta, String moedaTransaccao, String numeroDeDocumento, String numeroDeOperacao, String numeroOrdem, String saldoAposMovimento, String tipoDeDocumento, String valorParaExtracto, String valorTransaccao) {
        this.balcaoMovimento = balcaoMovimento;
        this.codigoOperacao = codigoOperacao;
        this.dataDeLancamento = dataDeLancamento;
        this.dataEmissaoExtracto = dataEmissaoExtracto;
        this.dataValor = dataValor;
        this.debitoCredito = debitoCredito;
        this.descritivoMovimento = descritivoMovimento;
        this.dualCurrency = dualCurrency;
        this.executorOrigem = executorOrigem;
        this.moedaAfectaConta = moedaAfectaConta;
        this.moedaTransaccao = moedaTransaccao;
        this.numeroDeDocumento = numeroDeDocumento;
        this.numeroDeOperacao = numeroDeOperacao;
        this.numeroOrdem = numeroOrdem;
        this.saldoAposMovimento = saldoAposMovimento;
        this.tipoDeDocumento = tipoDeDocumento;
        this.valorParaExtracto = valorParaExtracto;
        this.valorTransaccao = valorTransaccao;
    }

    public String getBalcaoMovimento() {
        return balcaoMovimento;
    }

    public void setBalcaoMovimento(String balcaoMovimento) {
        this.balcaoMovimento = balcaoMovimento;
    }

    public String getCodigoOperacao() {
        return codigoOperacao;
    }

    public void setCodigoOperacao(String codigoOperacao) {
        this.codigoOperacao = codigoOperacao;
    }

    public String getDataDeLancamento() {
        return dataDeLancamento;
    }

    public void setDataDeLancamento(String dataDeLancamento) {
        this.dataDeLancamento = dataDeLancamento;
    }

    public String getDataEmissaoExtracto() {
        return dataEmissaoExtracto;
    }

    public void setDataEmissaoExtracto(String dataEmissaoExtracto) {
        this.dataEmissaoExtracto = dataEmissaoExtracto;
    }

    public String getDataValor() {
        return dataValor;
    }

    public void setDataValor(String dataValor) {
        this.dataValor = dataValor;
    }

    public String getDebitoCredito() {
        return debitoCredito;
    }

    public void setDebitoCredito(String debitoCredito) {
        this.debitoCredito = debitoCredito;
    }

    public String getDescritivoMovimento() {
        return descritivoMovimento;
    }

    public void setDescritivoMovimento(String descritivoMovimento) {
        this.descritivoMovimento = descritivoMovimento;
    }

    public String getDualCurrency() {
        return dualCurrency;
    }

    public void setDualCurrency(String dualCurrency) {
        this.dualCurrency = dualCurrency;
    }

    public String getExecutorOrigem() {
        return executorOrigem;
    }

    public void setExecutorOrigem(String executorOrigem) {
        this.executorOrigem = executorOrigem;
    }

    public String getMoedaAfectaConta() {
        return moedaAfectaConta;
    }

    public void setMoedaAfectaConta(String moedaAfectaConta) {
        this.moedaAfectaConta = moedaAfectaConta;
    }

    public String getMoedaTransaccao() {
        return moedaTransaccao;
    }

    public void setMoedaTransaccao(String moedaTransaccao) {
        this.moedaTransaccao = moedaTransaccao;
    }

    public String getNumeroDeDocumento() {
        return numeroDeDocumento;
    }

    public void setNumeroDeDocumento(String numeroDeDocumento) {
        this.numeroDeDocumento = numeroDeDocumento;
    }

    public String getNumeroDeOperacao() {
        return numeroDeOperacao;
    }

    public void setNumeroDeOperacao(String numeroDeOperacao) {
        this.numeroDeOperacao = numeroDeOperacao;
    }

    public String getNumeroOrdem() {
        return numeroOrdem;
    }

    public void setNumeroOrdem(String numeroOrdem) {
        this.numeroOrdem = numeroOrdem;
    }

    public String getSaldoAposMovimento() {
        return saldoAposMovimento;
    }

    public void setSaldoAposMovimento(String saldoAposMovimento) {
        this.saldoAposMovimento = saldoAposMovimento;
    }

    public String getTipoDeDocumento() {
        return tipoDeDocumento;
    }

    public void setTipoDeDocumento(String tipoDeDocumento) {
        this.tipoDeDocumento = tipoDeDocumento;
    }

    public String getValorParaExtracto() {
        return valorParaExtracto;
    }

    public void setValorParaExtracto(String valorParaExtracto) {
        this.valorParaExtracto = valorParaExtracto;
    }

    public String getValorTransaccao() {
        return valorTransaccao;
    }

    public void setValorTransaccao(String valorTransaccao) {
        this.valorTransaccao = valorTransaccao;
    }
}
