
package pt.baieuropa.storage.WSDLPojos.CriarBloqueioConta;

import javax.xml.bind.annotation.*;

@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "dados"
})
@XmlRootElement(name = "criarBloqueioConta", namespace = "http://criarBloqueioConta.entidadesclientes")
public class CriarBloqueioConta {

    protected ObjectInputCriarBloqueioConta dados;

    public ObjectInputCriarBloqueioConta getDados() {
        return dados;
    }

    public void setDados(ObjectInputCriarBloqueioConta value) {
        this.dados = value;
    }

}
