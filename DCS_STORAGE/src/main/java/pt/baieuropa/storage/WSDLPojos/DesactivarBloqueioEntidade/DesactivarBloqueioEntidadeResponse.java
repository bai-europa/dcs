
package pt.baieuropa.storage.WSDLPojos.DesactivarBloqueioEntidade;

import javax.xml.bind.annotation.*;


@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "_return"
})
@XmlRootElement(name = "desactivarBloqueioEntidadeResponse", namespace = "http://desactivarBloqueioEntidade.entidadesclientes")
public class DesactivarBloqueioEntidadeResponse {

    @XmlElementRef(name = "return", namespace = "http://desactivarBloqueioEntidade.entidadesclientes", required = false)
    protected ObjectOutputDesactivarBloqueioEntidade _return;

    public ObjectOutputDesactivarBloqueioEntidade getReturn() {
        return _return;
    }

    public void setReturn(ObjectOutputDesactivarBloqueioEntidade value) {
        this._return = value;
    }

}
