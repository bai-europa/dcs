
package pt.baieuropa.storage.WSDLPojos.ActualizacaoNivelRiscoEntidade;

import javax.xml.bind.annotation.*;

@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "_return"
})
@XmlRootElement(name = "actualizacaoNivelRiscoEntidadeResponse", namespace = "http://actualizacaonivelriscoentidade.consultas")
public class ActualizacaoNivelRiscoEntidadeResponse {

    @XmlElementRef(name = "return", namespace = "http://actualizacaonivelriscoentidade.consultas", required = false)
    protected ObjectActualizacaoNivelRiscoEntidadeOutput _return;

    public ObjectActualizacaoNivelRiscoEntidadeOutput getReturn() {
        return _return;
    }

    public void setReturn(ObjectActualizacaoNivelRiscoEntidadeOutput value) {
        this._return = value;
    }

}
