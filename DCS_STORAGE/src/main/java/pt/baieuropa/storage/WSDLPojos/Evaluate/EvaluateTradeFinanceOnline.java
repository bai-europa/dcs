package pt.baieuropa.storage.WSDLPojos.Evaluate;

import com.fasterxml.jackson.annotation.JsonProperty;

public class EvaluateTradeFinanceOnline {

    @JsonProperty("MessageType")
    public String messageType;
    @JsonProperty("AccountNumber")
    public String accountNumber;
    @JsonProperty("AmountConverted")
    public double amountConverted;
    @JsonProperty("TransactionStatus")
    public String transactionStatus;
    @JsonProperty("SenderNumberDC")
    public String senderNumberDC;
    @JsonProperty("MyNumberDC")
    public String myNumberDC;
    @JsonProperty("IssuerIdentification")
    public String issuerIdentification;
    @JsonProperty("AccountIdentification")
    public String accountIdentification;
    @JsonProperty("TotalSequence")
    public String totalSequence;
    @JsonProperty("OperationDate")
    public String operationDate;
    @JsonProperty("DayPeriod")
    public String dayPeriod;
    @JsonProperty("InstructionConfirmation")
    public String instructionConfirmation;
    @JsonProperty("Applicant")
    public String applicant;
    @JsonProperty("Beneficiary")
    public String beneficiary;
    @JsonProperty("InformationSent")
    public String informationSent;
    @JsonProperty("Instructions78")
    public String instructions78;
    @JsonProperty("Narrative79")
    public String narrative79;
    @JsonProperty("DcNumber")
    public String dcNumber;
    @JsonProperty("UndertakingForm")
    public String undertakingForm;
    @JsonProperty("MessageFunction")
    public String messageFunction;
    @JsonProperty("FileIdentification")
    public String fileIdentification;
    @JsonProperty("AmendmentNumber")
    public String amendmentNumber;
    @JsonProperty("IssueDate")
    public String issueDate;
    @JsonProperty("DCExpiry")
    public String dCExpiry;
    @JsonProperty("DCNewExpiry")
    public String dCNewExpiry;
    @JsonProperty("ChargeAmount")
    public String chargeAmount;
    @JsonProperty("CurrencyCode")
    public String currencyCode;
    @JsonProperty("OriginalAmount")
    public String originalAmount;
    @JsonProperty("AmountPaid")
    public double amountPaid;
    @JsonProperty("TotalAmount34a")
    public double totalAmount34a;
    @JsonProperty("NewAmountDC")
    public double newAmountDC;
    @JsonProperty("PercentageDC")
    public String percentageDC;
    @JsonProperty("AmountCovernaed")
    public double amountCovernaed;
    @JsonProperty("FormDC40A")
    public String formDC40A;
    @JsonProperty("FormDC40B")
    public String formDC40B;
    @JsonProperty("RuleApplicable40C")
    public String ruleApplicable40C;
    @JsonProperty("RuleApplicable40E")
    public String ruleApplicable40E;
    @JsonProperty("Field41a")
    public String field41a;
    @JsonProperty("Drawee")
    public String drawee;
    @JsonProperty("Drafts")
    public String drafts;
    @JsonProperty("MixedPaymentDetail")
    public String mixedPaymentDetail;
    @JsonProperty("NegotiationDetails")
    public String negotiationDetails;
    @JsonProperty("PartialShipment")
    public String partialShipment;
    @JsonProperty("Transhipment")
    public String transhipment;
    @JsonProperty("ReceiptPlace")
    public String receiptPlace;
    @JsonProperty("DeliveryPlace")
    public String deliveryPlace;
    @JsonProperty("LatestDateShipment")
    public String latestDateShipment;
    @JsonProperty("ShipmentPeriod")
    public String shipmentPeriod;
    @JsonProperty("LoadingPort")
    public String loadingPort;
    @JsonProperty("DischargePort")
    public String dischargePort;
    @JsonProperty("ProductDescription45")
    public String productDescription45;
    @JsonProperty("RequiredDocument46")
    public String requiredDocument46;
    @JsonProperty("AdditionalCondition47")
    public String additionalCondition47;
    @JsonProperty("PaymentBeneficiary49")
    public String paymentBeneficiary49;
    @JsonProperty("NonBankDC")
    public String nonBankDC;
    @JsonProperty("ApplicantBank")
    public String applicantBank;
    @JsonProperty("OriginalBankIssuerDC")
    public String originalBankIssuerDC;
    @JsonProperty("CounterpartBank")
    public String counterpartBank;
    @JsonProperty("Receiver")
    public String receiver;
    @JsonProperty("BankAccount")
    public String bankAccount;
    @JsonProperty("BeneficiaryBank")
    public String beneficiaryBank;
    @JsonProperty("ChargeDetail")
    public String chargeDetail;
    @JsonProperty("ChargeInformation")
    public String chargeInformation;
    @JsonProperty("ChargeAmendment")
    public String chargeAmendment;
    @JsonProperty("InformationSent72Z")
    public String informationSent72Z;
    @JsonProperty("Charges")
    public String charges;
    @JsonProperty("NonPaymentReason")
    public String nonPaymentReason;
    @JsonProperty("ReimbursementDisposal")
    public String reimbursementDisposal;
    @JsonProperty("Detail")
    public String detail;
    @JsonProperty("Discrepancies")
    public String discrepancies;
    @JsonProperty("RequestUser")
    public String requestUser;
    @JsonProperty("IncreaseAmount")
    public double increaseAmount;
    @JsonProperty("DecreaseAmount")
    public double decreaseAmount;

    public EvaluateTradeFinanceOnline() {
    }

    public String getMessageType() {
        return messageType;
    }

    public void setMessageType(String messageType) {
        this.messageType = messageType;
    }

    public String getAccountNumber() {
        return accountNumber;
    }

    public void setAccountNumber(String accountNumber) {
        this.accountNumber = accountNumber;
    }

    public double getAmountConverted() {
        return amountConverted;
    }

    public void setAmountConverted(double amountConverted) {
        this.amountConverted = amountConverted;
    }

    public String getTransactionStatus() {
        return transactionStatus;
    }

    public void setTransactionStatus(String transactionStatus) {
        this.transactionStatus = transactionStatus;
    }

    public String getSenderNumberDC() {
        return senderNumberDC;
    }

    public void setSenderNumberDC(String senderNumberDC) {
        this.senderNumberDC = senderNumberDC;
    }

    public String getMyNumberDC() {
        return myNumberDC;
    }

    public void setMyNumberDC(String myNumberDC) {
        this.myNumberDC = myNumberDC;
    }

    public String getIssuerIdentification() {
        return issuerIdentification;
    }

    public void setIssuerIdentification(String issuerIdentification) {
        this.issuerIdentification = issuerIdentification;
    }

    public String getAccountIdentification() {
        return accountIdentification;
    }

    public void setAccountIdentification(String accountIdentification) {
        this.accountIdentification = accountIdentification;
    }

    public String getTotalSequence() {
        return totalSequence;
    }

    public void setTotalSequence(String totalSequence) {
        this.totalSequence = totalSequence;
    }

    public String getOperationDate() {
        return operationDate;
    }

    public void setOperationDate(String operationDate) {
        this.operationDate = operationDate;
    }

    public String getDayPeriod() {
        return dayPeriod;
    }

    public void setDayPeriod(String dayPeriod) {
        this.dayPeriod = dayPeriod;
    }

    public String getInstructionConfirmation() {
        return instructionConfirmation;
    }

    public void setInstructionConfirmation(String instructionConfirmation) {
        this.instructionConfirmation = instructionConfirmation;
    }

    public String getApplicant() {
        return applicant;
    }

    public void setApplicant(String applicant) {
        this.applicant = applicant;
    }

    public String getBeneficiary() {
        return beneficiary;
    }

    public void setBeneficiary(String beneficiary) {
        this.beneficiary = beneficiary;
    }

    public String getInformationSent() {
        return informationSent;
    }

    public void setInformationSent(String informationSent) {
        this.informationSent = informationSent;
    }

    public String getInstructions78() {
        return instructions78;
    }

    public void setInstructions78(String instructions78) {
        this.instructions78 = instructions78;
    }

    public String getNarrative79() {
        return narrative79;
    }

    public void setNarrative79(String narrative79) {
        this.narrative79 = narrative79;
    }

    public String getDcNumber() {
        return dcNumber;
    }

    public void setDcNumber(String dcNumber) {
        this.dcNumber = dcNumber;
    }

    public String getUndertakingForm() {
        return undertakingForm;
    }

    public void setUndertakingForm(String undertakingForm) {
        this.undertakingForm = undertakingForm;
    }

    public String getMessageFunction() {
        return messageFunction;
    }

    public void setMessageFunction(String messageFunction) {
        this.messageFunction = messageFunction;
    }

    public String getFileIdentification() {
        return fileIdentification;
    }

    public void setFileIdentification(String fileIdentification) {
        this.fileIdentification = fileIdentification;
    }

    public String getAmendmentNumber() {
        return amendmentNumber;
    }

    public void setAmendmentNumber(String amendmentNumber) {
        this.amendmentNumber = amendmentNumber;
    }

    public String getIssueDate() {
        return issueDate;
    }

    public void setIssueDate(String issueDate) {
        this.issueDate = issueDate;
    }

    public String getdCExpiry() {
        return dCExpiry;
    }

    public void setdCExpiry(String dCExpiry) {
        this.dCExpiry = dCExpiry;
    }

    public String getdCNewExpiry() {
        return dCNewExpiry;
    }

    public void setdCNewExpiry(String dCNewExpiry) {
        this.dCNewExpiry = dCNewExpiry;
    }

    public String getChargeAmount() {
        return chargeAmount;
    }

    public void setChargeAmount(String chargeAmount) {
        this.chargeAmount = chargeAmount;
    }

    public String getCurrencyCode() {
        return currencyCode;
    }

    public void setCurrencyCode(String currencyCode) {
        this.currencyCode = currencyCode;
    }

    public String getOriginalAmount() {
        return originalAmount;
    }

    public void setOriginalAmount(String originalAmount) {
        this.originalAmount = originalAmount;
    }

    public double getAmountPaid() {
        return amountPaid;
    }

    public void setAmountPaid(double amountPaid) {
        this.amountPaid = amountPaid;
    }

    public double getTotalAmount34a() {
        return totalAmount34a;
    }

    public void setTotalAmount34a(double totalAmount34a) {
        this.totalAmount34a = totalAmount34a;
    }

    public double getNewAmountDC() {
        return newAmountDC;
    }

    public void setNewAmountDC(double newAmountDC) {
        this.newAmountDC = newAmountDC;
    }

    public String getPercentageDC() {
        return percentageDC;
    }

    public void setPercentageDC(String percentageDC) {
        this.percentageDC = percentageDC;
    }

    public double getAmountCovernaed() {
        return amountCovernaed;
    }

    public void setAmountCovernaed(double amountCovernaed) {
        this.amountCovernaed = amountCovernaed;
    }

    public String getFormDC40A() {
        return formDC40A;
    }

    public void setFormDC40A(String formDC40A) {
        this.formDC40A = formDC40A;
    }

    public String getFormDC40B() {
        return formDC40B;
    }

    public void setFormDC40B(String formDC40B) {
        this.formDC40B = formDC40B;
    }

    public String getRuleApplicable40C() {
        return ruleApplicable40C;
    }

    public void setRuleApplicable40C(String ruleApplicable40C) {
        this.ruleApplicable40C = ruleApplicable40C;
    }

    public String getRuleApplicable40E() {
        return ruleApplicable40E;
    }

    public void setRuleApplicable40E(String ruleApplicable40E) {
        this.ruleApplicable40E = ruleApplicable40E;
    }

    public String getField41a() {
        return field41a;
    }

    public void setField41a(String field41a) {
        this.field41a = field41a;
    }

    public String getDrawee() {
        return drawee;
    }

    public void setDrawee(String drawee) {
        this.drawee = drawee;
    }

    public String getDrafts() {
        return drafts;
    }

    public void setDrafts(String drafts) {
        this.drafts = drafts;
    }

    public String getMixedPaymentDetail() {
        return mixedPaymentDetail;
    }

    public void setMixedPaymentDetail(String mixedPaymentDetail) {
        this.mixedPaymentDetail = mixedPaymentDetail;
    }

    public String getNegotiationDetails() {
        return negotiationDetails;
    }

    public void setNegotiationDetails(String negotiationDetails) {
        this.negotiationDetails = negotiationDetails;
    }

    public String getPartialShipment() {
        return partialShipment;
    }

    public void setPartialShipment(String partialShipment) {
        this.partialShipment = partialShipment;
    }

    public String getTranshipment() {
        return transhipment;
    }

    public void setTranshipment(String transhipment) {
        this.transhipment = transhipment;
    }

    public String getReceiptPlace() {
        return receiptPlace;
    }

    public void setReceiptPlace(String receiptPlace) {
        this.receiptPlace = receiptPlace;
    }

    public String getDeliveryPlace() {
        return deliveryPlace;
    }

    public void setDeliveryPlace(String deliveryPlace) {
        this.deliveryPlace = deliveryPlace;
    }

    public String getLatestDateShipment() {
        return latestDateShipment;
    }

    public void setLatestDateShipment(String latestDateShipment) {
        this.latestDateShipment = latestDateShipment;
    }

    public String getShipmentPeriod() {
        return shipmentPeriod;
    }

    public void setShipmentPeriod(String shipmentPeriod) {
        this.shipmentPeriod = shipmentPeriod;
    }

    public String getLoadingPort() {
        return loadingPort;
    }

    public void setLoadingPort(String loadingPort) {
        this.loadingPort = loadingPort;
    }

    public String getDischargePort() {
        return dischargePort;
    }

    public void setDischargePort(String dischargePort) {
        this.dischargePort = dischargePort;
    }

    public String getProductDescription45() {
        return productDescription45;
    }

    public void setProductDescription45(String productDescription45) {
        this.productDescription45 = productDescription45;
    }

    public String getRequiredDocument46() {
        return requiredDocument46;
    }

    public void setRequiredDocument46(String requiredDocument46) {
        this.requiredDocument46 = requiredDocument46;
    }

    public String getAdditionalCondition47() {
        return additionalCondition47;
    }

    public void setAdditionalCondition47(String additionalCondition47) {
        this.additionalCondition47 = additionalCondition47;
    }

    public String getPaymentBeneficiary49() {
        return paymentBeneficiary49;
    }

    public void setPaymentBeneficiary49(String paymentBeneficiary49) {
        this.paymentBeneficiary49 = paymentBeneficiary49;
    }

    public String getNonBankDC() {
        return nonBankDC;
    }

    public void setNonBankDC(String nonBankDC) {
        this.nonBankDC = nonBankDC;
    }

    public String getApplicantBank() {
        return applicantBank;
    }

    public void setApplicantBank(String applicantBank) {
        this.applicantBank = applicantBank;
    }

    public String getOriginalBankIssuerDC() {
        return originalBankIssuerDC;
    }

    public void setOriginalBankIssuerDC(String originalBankIssuerDC) {
        this.originalBankIssuerDC = originalBankIssuerDC;
    }

    public String getCounterpartBank() {
        return counterpartBank;
    }

    public void setCounterpartBank(String counterpartBank) {
        this.counterpartBank = counterpartBank;
    }

    public String getReceiver() {
        return receiver;
    }

    public void setReceiver(String receiver) {
        this.receiver = receiver;
    }

    public String getBankAccount() {
        return bankAccount;
    }

    public void setBankAccount(String bankAccount) {
        this.bankAccount = bankAccount;
    }

    public String getBeneficiaryBank() {
        return beneficiaryBank;
    }

    public void setBeneficiaryBank(String beneficiaryBank) {
        this.beneficiaryBank = beneficiaryBank;
    }

    public String getChargeDetail() {
        return chargeDetail;
    }

    public void setChargeDetail(String chargeDetail) {
        this.chargeDetail = chargeDetail;
    }

    public String getChargeInformation() {
        return chargeInformation;
    }

    public void setChargeInformation(String chargeInformation) {
        this.chargeInformation = chargeInformation;
    }

    public String getChargeAmendment() {
        return chargeAmendment;
    }

    public void setChargeAmendment(String chargeAmendment) {
        this.chargeAmendment = chargeAmendment;
    }

    public String getInformationSent72Z() {
        return informationSent72Z;
    }

    public void setInformationSent72Z(String informationSent72Z) {
        this.informationSent72Z = informationSent72Z;
    }

    public String getCharges() {
        return charges;
    }

    public void setCharges(String charges) {
        this.charges = charges;
    }

    public String getNonPaymentReason() {
        return nonPaymentReason;
    }

    public void setNonPaymentReason(String nonPaymentReason) {
        this.nonPaymentReason = nonPaymentReason;
    }

    public String getReimbursementDisposal() {
        return reimbursementDisposal;
    }

    public void setReimbursementDisposal(String reimbursementDisposal) {
        this.reimbursementDisposal = reimbursementDisposal;
    }

    public String getDetail() {
        return detail;
    }

    public void setDetail(String detail) {
        this.detail = detail;
    }

    public String getDiscrepancies() {
        return discrepancies;
    }

    public void setDiscrepancies(String discrepancies) {
        this.discrepancies = discrepancies;
    }

    public String getRequestUser() {
        return requestUser;
    }

    public void setRequestUser(String requestUser) {
        this.requestUser = requestUser;
    }

    public double getIncreaseAmount() {
        return increaseAmount;
    }

    public void setIncreaseAmount(double increaseAmount) {
        this.increaseAmount = increaseAmount;
    }

    public double getDecreaseAmount() {
        return decreaseAmount;
    }

    public void setDecreaseAmount(double decreaseAmount) {
        this.decreaseAmount = decreaseAmount;
    }

}
