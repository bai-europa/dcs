
package pt.baieuropa.storage.WSDLPojos.Evaluate;

import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


public class ArrayOfEntityAccount {

    protected List<EntityAccount> entityAccount;

    public List<EntityAccount> getEntityAccount() {
        if (entityAccount == null) {
            entityAccount = new ArrayList<EntityAccount>();
        }
        return this.entityAccount;
    }

    public void setEntityAccount(List<EntityAccount> entityAccount) {
        this.entityAccount = entityAccount;
    }
}
