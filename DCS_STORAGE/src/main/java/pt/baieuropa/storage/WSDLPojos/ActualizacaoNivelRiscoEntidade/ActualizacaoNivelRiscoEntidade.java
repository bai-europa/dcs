
package pt.baieuropa.storage.WSDLPojos.ActualizacaoNivelRiscoEntidade;

import javax.xml.bind.annotation.*;


@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "dados"
})
@XmlRootElement(name = "actualizacaoNivelRiscoEntidade", namespace = "http://actualizacaonivelriscoentidade.consultas")
public class ActualizacaoNivelRiscoEntidade {

    @XmlElementRef(name = "dados", namespace = "http://actualizacaonivelriscoentidade.consultas", required = false)
    protected ObjectActualizacaoNivelRiscoEntidadeInput dados;

    public ObjectActualizacaoNivelRiscoEntidadeInput getDados() {
        return dados;
    }

    public void setDados(ObjectActualizacaoNivelRiscoEntidadeInput value) {
        this.dados = value;
    }

}
