/*
  Created by: loliveira
*/

package pt.baieuropa.baieserv.processor;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.yaml.snakeyaml.Yaml;
import pt.baieuropa.baieserv.exceptions.MissingParametersException;
import pt.baieuropa.baieserv.exceptions.NatsRequestException;
import pt.baieuropa.baieserv.service.managers.NATSRequestsManager;
import pt.baieuropa.nats.Stan;

import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.security.InvalidParameterException;
import java.util.Map;
import java.util.concurrent.TimeoutException;

public class NatsController {

    private static final Logger log = LoggerFactory.getLogger(NatsController.class);
    public static Stan stanController;

    private String hostname_String;
    private String port_String;
    private String client_ID;
    private String cluster_ID;
    private String parameterError;

    public boolean initializeNats(String serviceName) {

        try {

            stanController = new Stan();

            // readPropertiesFromYAML("nats.yaml");
            readPropertiesFromYAML(System.getenv("NATS-CONFIG"));

            stanController.connectToNats(this.hostname_String, this.port_String, cluster_ID, client_ID);
            try{

                // BAIE *********************************
                // BAIE *           REQUESTS            *
                // BAIE *********************************

                // BAIE - Evaluate Transaction Online
                stanController.subscribeToTopic("EvaluateTransactionOnline:Request", serviceName, new NATSRequestsManager("EvaluateTransactionOnline:Request"));
                stanController.subscribeToTopic("CurrentAccountMovementDeposit:Request", serviceName, new NATSRequestsManager("CurrentAccountMovementDeposit:Request"));
                stanController.subscribeToTopic("CurrentAccountMovementWithdrawal:Request", serviceName, new NATSRequestsManager("CurrentAccountMovementWithdrawal:Request"));
                stanController.subscribeToTopic("CurrentAccountMovementWithdrawalBAI:Request", serviceName, new NATSRequestsManager("CurrentAccountMovementWithdrawalBAI:Request"));
                stanController.subscribeToTopic("CurrentAccountMovementDP:Request", serviceName, new NATSRequestsManager("CurrentAccountMovementDP:Request"));
                stanController.subscribeToTopic("CurrentAccountMovementCheckDeposit:Request", serviceName, new NATSRequestsManager("CurrentAccountMovementCheckDeposit:Request"));
                stanController.subscribeToTopic("CurrentAccountMovementMixedDeposit:Request", serviceName, new NATSRequestsManager("CurrentAccountMovementMixedDeposit:Request"));
                stanController.subscribeToTopic("CashierCheck:Request", serviceName, new NATSRequestsManager("CashierCheck:Request"));
                stanController.subscribeToTopic("ClearingCheck:Request", serviceName, new NATSRequestsManager("ClearingCheck:Request"));
                stanController.subscribeToTopic("CertifiedCheck:Request", serviceName, new NATSRequestsManager("CertifiedCheck:Request"));
                stanController.subscribeToTopic("BankCheck:Request", serviceName, new NATSRequestsManager("BankCheck:Request"));
                stanController.subscribeToTopic("DebitTransfer:Request", serviceName, new NATSRequestsManager("DebitTransfer:Request"));
                stanController.subscribeToTopic("CreditTransfer:Request", serviceName, new NATSRequestsManager("CreditTransfer:Request"));
                stanController.subscribeToTopic("PermanentDebitTransfer:Request", serviceName, new NATSRequestsManager("PermanentDebitTransfer:Request"));
                stanController.subscribeToTopic("PermanentCreditTransfer:Request", serviceName, new NATSRequestsManager("PermanentCreditTransfer:Request"));
                stanController.subscribeToTopic("OperationsDebitUNIBANCO:Request", serviceName, new NATSRequestsManager("OperationsDebitUNIBANCO:Request"));
                stanController.subscribeToTopic("CardOperationsUNIBANCO:Request", serviceName, new NATSRequestsManager("CardOperationsUNIBANCO:Request"));
                stanController.subscribeToTopic("TermDepositTransfer:Request", serviceName, new NATSRequestsManager("TermDepositTransfer:Request"));
                stanController.subscribeToTopic("CreditMovementDODOCataloged:Request", serviceName, new NATSRequestsManager("CreditMovementDODOCataloged:Request"));
                stanController.subscribeToTopic("DebitMovementDODOCataloged:Request", serviceName, new NATSRequestsManager("DebitMovementDODOCataloged:Request"));
                stanController.subscribeToTopic("SecuredAccountDebit:Request", serviceName, new NATSRequestsManager("SecuredAccountDebit:Request"));
                stanController.subscribeToTopic("SecuredAccountCredit:Request", serviceName, new NATSRequestsManager("SecuredAccountCredit:Request"));
                stanController.subscribeToTopic("CreditDOWithUseOfCorCC:Request", serviceName, new NATSRequestsManager("CreditDOWithUseOfCorCC:Request"));
                stanController.subscribeToTopic("CurrentAccountMovementInternalSent:Request", serviceName, new NATSRequestsManager("CurrentAccountMovementInternalSent:Request"));
                stanController.subscribeToTopic("CurrentAccountMovementInternalReceived:Request", serviceName, new NATSRequestsManager("CurrentAccountMovementInternalReceived:Request"));
                stanController.subscribeToTopic("CurrentAccountMovementDB:Request", serviceName, new NATSRequestsManager("CurrentAccountMovementDB:Request"));
                stanController.subscribeToTopic("CurrentAccountMovementCR:Request", serviceName, new NATSRequestsManager("CurrentAccountMovementCR:Request"));
                stanController.subscribeToTopic("CreditTransferSameDate:Request", serviceName, new NATSRequestsManager("CreditTransferSameDate:Request"));
                stanController.subscribeToTopic("ValuesDepositBAIECheck:Request", serviceName, new NATSRequestsManager("ValuesDepositBAIECheck:Request"));
                stanController.subscribeToTopic("ValuesDepositBankCheck:Request", serviceName, new NATSRequestsManager("ValuesDepositBankCheck:Request"));
                stanController.subscribeToTopic("ValuesDepositCertifiedCheck:Request", serviceName, new NATSRequestsManager("ValuesDepositCertifiedCheck:Request"));
                stanController.subscribeToTopic("CoinSelling:Request", serviceName, new NATSRequestsManager("CoinSelling:Request"));
                stanController.subscribeToTopic("CoinPurchase:Request", serviceName, new NATSRequestsManager("CoinPurchase:Request"));
                stanController.subscribeToTopic("IssuingForeignCheck:Request", serviceName, new NATSRequestsManager("IssuingForeignCheck:Request"));
                stanController.subscribeToTopic("ExportDocumentaryCredit:Request", serviceName, new NATSRequestsManager("ExportDocumentaryCredit:Request"));
                stanController.subscribeToTopic("ForeignExchangePurchase:Request", serviceName, new NATSRequestsManager("ForeignExchangePurchase:Request"));
                stanController.subscribeToTopic("ForeignExchangeSale:Request", serviceName, new NATSRequestsManager("ForeignExchangeSale:Request"));
                stanController.subscribeToTopic("ImportDocumentaryCredit:Request", serviceName, new NATSRequestsManager("ImportDocumentaryCredit:Request"));
                stanController.subscribeToTopic("CurrentAccountMovementDebitSEPASent:Request", serviceName, new NATSRequestsManager("CurrentAccountMovementDebitSEPASent:Request"));
                stanController.subscribeToTopic("CurrentAccountMovementCreditSEPASent:Request", serviceName, new NATSRequestsManager("CurrentAccountMovementCreditSEPASent:Request"));
                stanController.subscribeToTopic("CurrentAccountMovementCreditSEPAReceived:Request", serviceName, new NATSRequestsManager("CurrentAccountMovementCreditSEPAReceived:Request"));
                stanController.subscribeToTopic("ReturnRequestSEPA:Request", serviceName, new NATSRequestsManager("ReturnRequestSEPA:Request"));
                stanController.subscribeToTopic("CurrentAccountReturnCreditSEPA:Request", serviceName, new NATSRequestsManager("CurrentAccountReturnCreditSEPA:Request"));
                stanController.subscribeToTopic("ForeignCreditTEIDevolution:Request", serviceName, new NATSRequestsManager("ForeignCreditTEIDevolution:Request"));
                stanController.subscribeToTopic("ConstitutionDP:Request", serviceName, new NATSRequestsManager("ConstitutionDP:Request"));
                stanController.subscribeToTopic("SaleOffDP:Request", serviceName, new NATSRequestsManager("SaleOffDP:Request"));
                stanController.subscribeToTopic("IncrementDP:Request", serviceName, new NATSRequestsManager("IncrementDP:Request"));
                stanController.subscribeToTopic("DebitCardTransaction:Request", serviceName, new NATSRequestsManager("DebitCardTransaction:Request"));
                stanController.subscribeToTopic("Uploads:Request", serviceName, new NATSRequestsManager("Uploads:Request"));
                stanController.subscribeToTopic("Deposits:Request", serviceName, new NATSRequestsManager("Deposits:Request"));
                stanController.subscribeToTopic("CashAdvance:Request", serviceName, new NATSRequestsManager("CashAdvance:Request"));
                stanController.subscribeToTopic("Authorizations:Request", serviceName, new NATSRequestsManager("Autorizations:Request"));
                stanController.subscribeToTopic("Compensations:Request", serviceName, new NATSRequestsManager("Compensations:Request"));
                stanController.subscribeToTopic("Commissions:Request", serviceName, new NATSRequestsManager("Commissions:Request"));
                stanController.subscribeToTopic("CommissionsSettlements:Request", serviceName, new NATSRequestsManager("CommissionsSettlements:Request"));
                stanController.subscribeToTopic("OtherSettlements:Request", serviceName, new NATSRequestsManager("OtherSettlements:Request"));
                stanController.subscribeToTopic("GeneralDebit:Request", serviceName, new NATSRequestsManager("GeneralDebit:Request"));
                stanController.subscribeToTopic("GeneralCredit:Request", serviceName, new NATSRequestsManager("GeneralCredit:Request"));
                stanController.subscribeToTopic("ReturnCredit:Request", serviceName, new NATSRequestsManager("ReturnCredit:Request"));
                stanController.subscribeToTopic("UseTransferCredit:Request", serviceName, new NATSRequestsManager("UseTransferCredit:Request"));
                stanController.subscribeToTopic("UseTransfer:Request", serviceName, new NATSRequestsManager("UseTransfer:Request"));
                stanController.subscribeToTopic("LoanRepayment:Request", serviceName, new NATSRequestsManager("LoanRepayment:Request"));
                stanController.subscribeToTopic("CheckWithoutOtherBanks:Request", serviceName, new NATSRequestsManager("CheckWithoutOtherBanks:Request"));
                stanController.subscribeToTopic("CheckFromDepositedBank:Request", serviceName, new NATSRequestsManager("CheckFromDepositedBank:Request"));
                stanController.subscribeToTopic("DepositedCheckReturn:Request", serviceName, new NATSRequestsManager("DepositedCheckReturn:Request"));
                stanController.subscribeToTopic("SEPADDCharged:Request", serviceName, new NATSRequestsManager("SEPADDCharged:Request"));
                stanController.subscribeToTopic("DirectDebitInstruction:Request", serviceName, new NATSRequestsManager("DirectDebitInstruction:Request"));

                // BAIE - Evaluate Transaction Online Correspondent Bank
                stanController.subscribeToTopic("EvaluateTransactionCB:Request", serviceName, new NATSRequestsManager("EvaluateTransactionCB:Request"));

                // BAIE - Evaluate Trade Finance Online
                stanController.subscribeToTopic("LettersOfCredit:Request", serviceName, new NATSRequestsManager("LettersOfCredit:Request"));

                // BAIE - Evaluate Entity Online
                stanController.subscribeToTopic("ParticularCreated:Request", serviceName, new NATSRequestsManager("ParticularCreated:Request"));
                stanController.subscribeToTopic("ParticularUpdated:Request", serviceName, new NATSRequestsManager("ParticularUpdated:Request"));
                stanController.subscribeToTopic("EnterpriseCreated:Request", serviceName, new NATSRequestsManager("EnterpriseCreated:Request"));
                stanController.subscribeToTopic("EnterpriseUpdated:Request", serviceName, new NATSRequestsManager("EnterpriseUpdated:Request"));

                // BAIE - Evaluate Account Online
                stanController.subscribeToTopic("CurrentAccountCreated:Request", serviceName, new NATSRequestsManager("CurrentAccountCreated:Request"));
                stanController.subscribeToTopic("CurrentAccountUpdated:Request", serviceName, new NATSRequestsManager("CurrentAccountUpdated:Request"));

                // BAIE - Sem certeza se estão a funcionar:
                // stanController.subscribeToTopic("CurrentAccountMovementSEPASent:Request", serviceName, new NATSRequestsManager("CurrentAccountMovementSEPASent:Request"));
                // stanController.subscribeToTopic("CurrentAccountMovementSEPAReceived:Request", serviceName, new NATSRequestsManager("CurrentAccountMovementSEPAReceived:Request"));


                stanController.subscribeToTopic("DepositAccountCreated:Request", serviceName, new NATSRequestsManager("DepositAccountCreated:Request"));
                stanController.subscribeToTopic("DepositAccountUpdated:Request", serviceName, new NATSRequestsManager("DepositAccountUpdated:Request"));
                stanController.subscribeToTopic("CreditAccountCreated:Request", serviceName, new NATSRequestsManager("CreditAccountCreated:Request"));
                stanController.subscribeToTopic("CreditAccountUpdated:Request", serviceName, new NATSRequestsManager("CreditAccountUpdated:Request"));


                // BAIE - Account Relations by Client
                stanController.subscribeToTopic("CurrentAccountRelationsCreated:Request", serviceName, new NATSRequestsManager("CurrentAccountRelationsCreated:Request"));
                stanController.subscribeToTopic("CurrentAccountRelationsUpdated:Request", serviceName, new NATSRequestsManager("CurrentAccountRelationsUpdated:Request"));


                // BAIE *********************************
                // BAIE *           RESPONSES           *
                // BAIE *********************************

                stanController.subscribeToTopic("Accumulated:Response", serviceName, new NATSRequestsManager("Accumulated:Response"));

            } catch (Exception ex) {
                ex.printStackTrace();
            }

            return true;
        } catch (MissingParametersException | IOException | InterruptedException | TimeoutException e) {
            log.error(e.getMessage());
            return false;
        }
    }

    public boolean insertRequest(String topic, String content) throws NatsRequestException {
        try {
            stanController.insertRequestOnTopic(topic, content);
            return true;
        } catch (IOException | TimeoutException | InterruptedException e) {
            log.error(e.getMessage());
            throw new NatsRequestException(e.getMessage());
        } finally {
//            log.info("Number of requests on topic "+ this.topic + " is : "+ stanController.getPendingRequestsFromTopic(this.topic));
        }
    }


    private void readPropertiesFromYAML(String AS400YAMLFileName) throws MissingParametersException, InvalidParameterException {

        log.info("File AS400 Name: " + AS400YAMLFileName);

        // TODO change this to a method
        try {
            Yaml yaml = new Yaml();
            InputStream inputStream = new FileInputStream(AS400YAMLFileName);
//            InputStream inputStream = this.getClass()
//                    .getClassLoader()
//                    .getResourceAsStream(AS400YAMLFileName);
            Map<String, Object> yamlObject = yaml.load(inputStream);

            yamlObject.forEach((name, value) -> {

                switch ((String) name) {
                    case "hostname_String":
                        hostname_String = (String) value;
                        break;
                    case "port_String":
                        port_String = (String) value;
                        break;
                    case "cluster_ID":
                        cluster_ID = (String) value;
                        break;
                    case "client_ID":
                        client_ID = (String) value;
                        break;
                    default:
                        parameterError = (String) name;
                        break;
                }

            });
        } catch (Exception ex) {
            log.error(ex.getMessage());
        } finally {
            if (parameterError != null)
                throw new InvalidParameterException("The parameter " + parameterError + " is not recognized by the application. (hostname_String, port_String)");

            if ((hostname_String == null) || (port_String == null))
                throw new MissingParametersException("The YAML file with the configuration to the AS400 is missing one or more parameters");
        }
    }

}
