/*
   Created by: loliveira

 */
package pt.baieuropa.baieserv.processor;


/**
 * Responsible for the processing of a request
 * <p>
 * The implementation of every request must be made in here
 */
public interface Processor {
    /**
     * Execute all the needed steps to initialize the service
     */
    boolean initializeService();

    /**
     * If it can be possible, stop the service in a controlled way, closing all connections
     * <p>
     * Inform the logfile that the service will be stopped.
     * <p>
     * Can use System.exit afterwards.
     */
    boolean controlledStopService();

    /**
     * Inform the logfile that the service will be forced to stop and stop the service.
     * <p>
     * Can use System.exit afterwards.
     */
    boolean forceStopService();

}
