/*
  Created by: loliveira
*/

package pt.baieuropa.baieserv.processor;


import org.apache.log4j.Logger;
import pt.baieuropa.baieserv.exceptions.NatsRequestException;

public class Process implements Processor {

    private static Logger log = Logger.getLogger(Process.class.getName());

    private NatsController natsController;

    public Process(String serviceName) {
        natsController = new NatsController();
        natsController.initializeNats(serviceName);
    }

    @Override
    public boolean initializeService() {
        return false;
    }

    @Override
    public boolean controlledStopService() {
        return false;
    }

    @Override
    public boolean forceStopService() {
        return false;
    }

    public boolean publish(String topic, String result) {
        try {
            natsController.insertRequest(topic, result);
            return true;
        } catch (NatsRequestException e) {
            log.warn(e.getMessage());
            return false;
        }
    }
}
