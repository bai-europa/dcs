/*
  Created by: loliveira
*/

package pt.baieuropa.baieserv.models.PersonPartyInfo;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.HashCodeBuilder;
import org.apache.commons.lang.builder.ToStringBuilder;

@JsonPropertyOrder({
        "father",
        "mother"
})
public class Parents {

    @JsonProperty("father")
    private String father;
    @JsonProperty("mother")
    private String mother;

    @JsonProperty("father")
    public String getFather() {
        return father;
    }

    public void setFather(String father) {
        this.father = father;
    }

    @JsonProperty("mother")
    public String getMother() {
        return mother;
    }

    public void setMother(String mother) {
        this.mother = mother;
    }

    @Override
    public String toString() {
        return ToStringBuilder.reflectionToString(this);
    }



    @Override
    public int hashCode() {
        return new HashCodeBuilder().append(father).append(mother).toHashCode();
    }

    @Override
    public boolean equals(Object other) {
        if (other == this) {
            return true;
        }
        if ((other instanceof Parents) == false) {
            return false;
        }
        Parents rhs = ((Parents) other);
        return new EqualsBuilder().append(father, rhs.father).append(mother, rhs.mother).isEquals();
    }


}

