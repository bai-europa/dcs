
package pt.baieuropa.baieserv.models.PartyAcct.AcctRec;

import com.fasterxml.jackson.annotation.*;
import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.HashCodeBuilder;
import org.apache.commons.lang.builder.ToStringBuilder;

import java.util.HashMap;
import java.util.Map;

@JsonPropertyOrder({
    "acctIdentValue",
    "acctIdentType"
})
public class AcctIdent {

    @JsonProperty("acctIdentValue")
    private String acctIdentValue;
    @JsonProperty("acctIdentType")
    private String acctIdentType;
    @JsonIgnore
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();

    public AcctIdent() {
    }

    public AcctIdent(String acctIdentValue, String acctIdentType, Map<String, Object> additionalProperties) {
        this.acctIdentValue = acctIdentValue;
        this.acctIdentType = acctIdentType;
        this.additionalProperties = additionalProperties;
    }

    @JsonProperty("acctIdentValue")
    public String getAcctIdentValue() {
        return acctIdentValue;
    }

    @JsonProperty("acctIdentValue")
    public void setAcctIdentValue(String acctIdentValue) {
        this.acctIdentValue = acctIdentValue;
    }

    public AcctIdent withAcctIdentValue(String acctIdentValue) {
        this.acctIdentValue = acctIdentValue;
        return this;
    }

    @JsonProperty("acctIdentType")
    public String getAcctIdentType() {
        return acctIdentType;
    }

    @JsonProperty("acctIdentType")
    public void setAcctIdentType(String acctIdentType) {
        this.acctIdentType = acctIdentType;
    }

    public AcctIdent withAcctIdentType(String acctIdentType) {
        this.acctIdentType = acctIdentType;
        return this;
    }

    @Override
    public String toString() {
        return ToStringBuilder.reflectionToString(this);
    }

    @JsonAnyGetter
    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    @JsonAnySetter
    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }

    public AcctIdent withAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
        return this;
    }

    @Override
    public int hashCode() {
        return new HashCodeBuilder().append(acctIdentValue).append(acctIdentType).append(additionalProperties).toHashCode();
    }

    @Override
    public boolean equals(Object other) {
        if (other == this) {
            return true;
        }
        if ((other instanceof AcctIdent) == false) {
            return false;
        }
        AcctIdent rhs = ((AcctIdent) other);
        return new EqualsBuilder().append(acctIdentValue, rhs.acctIdentValue).append(acctIdentType, rhs.acctIdentType).append(additionalProperties, rhs.additionalProperties).isEquals();
    }

}
