
package pt.baieuropa.baieserv.models.PartyAcct.AcctRec;

import com.fasterxml.jackson.annotation.*;
import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.HashCodeBuilder;
import org.apache.commons.lang.builder.ToStringBuilder;

import java.util.HashMap;
import java.util.Map;

@JsonPropertyOrder({
    "amt",
    "curCode"
})
public class CurAmt {

    @JsonProperty("amt")
    private Double amt;
    @JsonProperty("curCode")
    private CurCode curCode;
    @JsonIgnore
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();

    public CurAmt() {
    }

    public CurAmt(Double amt, CurCode curCode, Map<String, Object> additionalProperties) {
        this.amt = amt;
        this.curCode = curCode;
        this.additionalProperties = additionalProperties;
    }

    @JsonProperty("amt")
    public Double getAmt() {
        return amt;
    }

    @JsonProperty("amt")
    public void setAmt(Double amt) {
        this.amt = amt;
    }

    public CurAmt withAmt(Double amt) {
        this.amt = amt;
        return this;
    }

    @JsonProperty("curCode")
    public CurCode getCurCode() {
        return curCode;
    }

    @JsonProperty("curCode")
    public void setCurCode(CurCode curCode) {
        this.curCode = curCode;
    }

    public CurAmt withCurCode(CurCode curCode) {
        this.curCode = curCode;
        return this;
    }

    @Override
    public String toString() {
        return ToStringBuilder.reflectionToString(this);
    }

    @JsonAnyGetter
    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    @JsonAnySetter
    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }

    public CurAmt withAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
        return this;
    }

    @Override
    public int hashCode() {
        return new HashCodeBuilder().append(amt).append(curCode).append(additionalProperties).toHashCode();
    }

    @Override
    public boolean equals(Object other) {
        if (other == this) {
            return true;
        }
        if ((other instanceof CurAmt) == false) {
            return false;
        }
        CurAmt rhs = ((CurAmt) other);
        return new EqualsBuilder().append(amt, rhs.amt).append(curCode, rhs.curCode).append(additionalProperties, rhs.additionalProperties).isEquals();
    }

}
