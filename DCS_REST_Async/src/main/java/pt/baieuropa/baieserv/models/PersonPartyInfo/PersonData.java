
package pt.baieuropa.baieserv.models.PersonPartyInfo;

import com.fasterxml.jackson.annotation.*;
import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.HashCodeBuilder;
import org.apache.commons.lang.builder.ToStringBuilder;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@JsonPropertyOrder({
    "personName",
    "contacts",
        "IssuedIdents",
        "parents",
        "educationalQualifications"
})
public class PersonData {

    @JsonProperty("personName")
    private PersonName personName;
    @JsonProperty("contacts")
    private List<Contact> contacts = new ArrayList<Contact>();
    @JsonProperty("IssuedIdents")
    private List<IssuedIdent> IssuedIdents = new ArrayList<>();
    @JsonProperty("parents")
    private Parents parents;
    @JsonProperty("educationalQualifications")
    private EducationalQualifications educationalQualifications;


    @JsonIgnore
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();


    @JsonProperty("personName")
    public PersonName getPersonName() {
        return personName;
    }

    @JsonProperty("personName")
    public void setPersonName(PersonName personName) {
        this.personName = personName;
    }

    public PersonData withPersonName(PersonName personName) {
        this.personName = personName;
        return this;
    }

    @JsonProperty("contacts")
    public List<Contact> getContacts() {
        return contacts;
    }

    @JsonProperty("contacts")
    public void setContacts(List<Contact> contacts) {
        this.contacts = contacts;
    }


    @JsonProperty("parents")
    public Parents getParents() {
        return parents;
    }


    public void setParents(Parents parents) {
        this.parents = parents;
    }

    @JsonProperty("educationalQualifications")
    public EducationalQualifications getEducationalQualifications() {
        return educationalQualifications;
    }

    public void setEducationalQualifications(EducationalQualifications educationalQualifications) {
        this.educationalQualifications = educationalQualifications;
    }


    public PersonData withContacts(List<Contact> contacts) {
        this.contacts = contacts;
        return this;
    }

    @Override
    public String toString() {
        return ToStringBuilder.reflectionToString(this);
    }

    @JsonAnyGetter
    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    @JsonAnySetter
    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }

    public PersonData withAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
        return this;
    }

    @JsonProperty("IssuedIdents")
    public List<IssuedIdent> getIssuedIdents() {
        return IssuedIdents;
    }

    public void setIssuedIdents(List<IssuedIdent> issuedIdents) {
        IssuedIdents = issuedIdents;
    }

    public void setAdditionalProperties(Map<String, Object> additionalProperties) {
        this.additionalProperties = additionalProperties;
    }

    @Override
    public int hashCode() {
        return new HashCodeBuilder().append(personName).append(contacts).append(additionalProperties).append(IssuedIdents).append(parents).append(educationalQualifications).toHashCode();
    }

    @Override
    public boolean equals(Object other) {
        if (other == this) {
            return true;
        }
        if ((other instanceof PersonData) == false) {
            return false;
        }
        PersonData rhs = ((PersonData) other);
        return new EqualsBuilder().append(personName, rhs.personName).append(contacts, rhs.contacts).append(additionalProperties, rhs.additionalProperties).append(IssuedIdents, rhs.IssuedIdents).append(parents, rhs.parents).append(educationalQualifications, rhs.educationalQualifications).isEquals();
    }

}
