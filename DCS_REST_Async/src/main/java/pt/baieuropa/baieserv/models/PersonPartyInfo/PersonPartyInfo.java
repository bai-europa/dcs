
package pt.baieuropa.baieserv.models.PersonPartyInfo;

import com.fasterxml.jackson.annotation.*;
import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.HashCodeBuilder;
import org.apache.commons.lang.builder.ToStringBuilder;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

@JsonPropertyOrder({
    "personData",
    "residenceCountry",
    "partyPref",
    "birthDt",
    "gender",
    "employment",
    "immigrationStat",
    "establishedDt",
    "birthPlace",
    "maritalStat",
    "taxResidenceCountry",
    "nationality",
    "educationLevel",
    "relationshipMgr",
    "dependents",
    "PEP"
})
public class PersonPartyInfo {

    @JsonProperty("residenceCountry")
    private ResidenceCountry residenceCountry;
    @JsonProperty("partyPref")
    private PartyPref partyPref;
    @JsonProperty("birthDt")
    private String birthDt;
    @JsonProperty("gender")
    private String gender;
    @JsonProperty("employment")
    private Employment employment;
    @JsonProperty("immigrationStat")
    private String immigrationStat;
    @JsonProperty("establishedDt")
    private String establishedDt;
    @JsonProperty("birthPlace")
    private String birthPlace;
    @JsonProperty("personData")
    private PersonData personData;
    @JsonProperty("maritalStat")
    private String maritalStat;
    @JsonProperty("taxResidenceCountry")
    private TaxResidenceCountry taxResidenceCountry;
    @JsonProperty("nationality")
    private String nationality;
    @JsonProperty("educationLevel")
    private String educationLevel;
    @JsonProperty("relationshipMgr")
    private RelationshipMgr relationshipMgr;
    @JsonProperty("dependents")
    private String dependents;
    @JsonProperty("PEP")
    private boolean PEP;
    @JsonIgnore
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();

    @JsonProperty("residenceCountry")
    public ResidenceCountry getResidenceCountry() {
        return residenceCountry;
    }

    @JsonProperty("residenceCountry")
    public void setResidenceCountry(ResidenceCountry residenceCountry) {
        this.residenceCountry = residenceCountry;
    }

    public PersonPartyInfo withResidenceCountry(ResidenceCountry residenceCountry) {
        this.residenceCountry = residenceCountry;
        return this;
    }

    @JsonProperty("partyPref")
    public PartyPref getPartyPref() {
        return partyPref;
    }

    @JsonProperty("partyPref")
    public void setPartyPref(PartyPref partyPref) {
        this.partyPref = partyPref;
    }

    public PersonPartyInfo withPartyPref(PartyPref partyPref) {
        this.partyPref = partyPref;
        return this;
    }

    @JsonProperty("birthDt")
    public String getBirthDt() throws ParseException {
        if(birthDt != null && birthDt.equals("0")) {
            birthDt = null;
        }

        if(birthDt != null) {
            Date date = new SimpleDateFormat("yyyyMMdd").parse(birthDt);
            SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd");
            birthDt = simpleDateFormat.format(date);
        }

        return birthDt;
    }

    @JsonProperty("birthDt")
    public void setBirthDt(String birthDt) {
        this.birthDt = birthDt;
    }

    public PersonPartyInfo withBirthDt(String birthDt) {
        this.birthDt = birthDt;
        return this;
    }

    @JsonProperty("gender")
    public String getGender() {
        return gender;
    }

    @JsonProperty("gender")
    public void setGender(String gender) {
        this.gender = gender;
    }

    public PersonPartyInfo withGender(String gender) {
        this.gender = gender;
        return this;
    }

    @JsonProperty("employment")
    public Employment getEmployment() {
        return employment;
    }

    @JsonProperty("employment")
    public void setEmployment(Employment employment) {
        this.employment = employment;
    }

    public PersonPartyInfo withEmployment(Employment employment) {
        this.employment = employment;
        return this;
    }

    @JsonProperty("immigrationStat")
    public String getImmigrationStat() {
        return immigrationStat;
    }

    @JsonProperty("immigrationStat")
    public void setImmigrationStat(String immigrationStat) {
        this.immigrationStat = immigrationStat;
    }

    public PersonPartyInfo withImmigrationStat(String immigrationStat) {
        this.immigrationStat = immigrationStat;
        return this;
    }

    @JsonProperty("establishedDt")
    public String getEstablishedDt() throws ParseException {
        if(establishedDt != null && establishedDt.equals("0")) {
            establishedDt = null;
        }

        if(establishedDt != null) {
            Date date = new SimpleDateFormat("yyyyMMdd").parse(establishedDt);
            SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd");
            establishedDt = simpleDateFormat.format(date);
        }

        return establishedDt;
    }

    @JsonProperty("establishedDt")
    public void setEstablishedDt(String establishedDt) {
        this.establishedDt = establishedDt;
    }

    public PersonPartyInfo withEstablishedDt(String establishedDt) {
        this.establishedDt = establishedDt;
        return this;
    }

    @JsonProperty("birthPlace")
    public String getBirthPlace() {
        return birthPlace;
    }

    @JsonProperty("birthPlace")
    public void setBirthPlace(String birthPlace) {
        this.birthPlace = birthPlace;
    }

    public PersonPartyInfo withBirthPlace(String birthPlace) {
        this.birthPlace = birthPlace;
        return this;
    }

    @JsonProperty("personData")
    public PersonData getPersonData() {
        return personData;
    }

    @JsonProperty("personData")
    public void setPersonData(PersonData personData) {
        this.personData = personData;
    }

    public PersonPartyInfo withPersonData(PersonData personData) {
        this.personData = personData;
        return this;
    }

    @JsonProperty("maritalStat")
    public String getMaritalStat() {
        return maritalStat;
    }

    @JsonProperty("maritalStat")
    public void setMaritalStat(String maritalStat) {
        this.maritalStat = maritalStat;
    }

    public PersonPartyInfo withMaritalStat(String maritalStat) {
        this.maritalStat = maritalStat;
        return this;
    }

    @JsonProperty("taxResidenceCountry")
    public TaxResidenceCountry getTaxResidenceCountry() {
        return taxResidenceCountry;
    }

    @JsonProperty("taxResidenceCountry")
    public void setTaxResidenceCountry(TaxResidenceCountry taxResidenceCountry) {
        this.taxResidenceCountry = taxResidenceCountry;
    }

    public PersonPartyInfo withTaxResidenceCountry(TaxResidenceCountry taxResidenceCountry) {
        this.taxResidenceCountry = taxResidenceCountry;
        return this;
    }

    @JsonProperty("nationality")
    public String getNationality() {
        return nationality;
    }

    @JsonProperty("nationality")
    public void setNationality(String nationality) {
        this.nationality = nationality;
    }

    public PersonPartyInfo withNationality(String nationality) {
        this.nationality = nationality;
        return this;
    }

    @JsonProperty("educationLevel")
    public String getEducationLevel() {
        return educationLevel;
    }

    @JsonProperty("educationLevel")
    public void setEducationLevel(String educationLevel) {
        this.educationLevel = educationLevel;
    }

    public PersonPartyInfo withEducationLevel(String educationLevel) {
        this.educationLevel = educationLevel;
        return this;
    }

    @JsonProperty("relationshipMgr")
    public RelationshipMgr getRelationshipMgr() {
        return relationshipMgr;
    }

    @JsonProperty("relationshipMgr")
    public void setRelationshipMgr(RelationshipMgr relationshipMgr) {
        this.relationshipMgr = relationshipMgr;
    }

    public PersonPartyInfo withRelationshipMgr(RelationshipMgr relationshipMgr) {
        this.relationshipMgr = relationshipMgr;
        return this;
    }

    @JsonProperty("dependents")
    public String getDependents() {
        return dependents;
    }

    @JsonProperty("dependents")
    public void setDependents(String dependents) {
        this.dependents = dependents;
    }

    @JsonProperty("PEP")
    public boolean getPEP() {
        return PEP;
    }

    @JsonProperty("PEP")
    public void setPEP(boolean PEP) {
        this.PEP = PEP;
    }



    public PersonPartyInfo withDependents(String dependents) {
        this.dependents = dependents;
        return this;
    }

    @Override
    public String toString() {
        return ToStringBuilder.reflectionToString(this);
    }

    @JsonAnyGetter
    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    @JsonAnySetter
    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }

    public PersonPartyInfo withAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
        return this;
    }

    @Override
    public int hashCode() {
        return new HashCodeBuilder().append(residenceCountry).append(partyPref).append(birthDt).append(gender).append(employment).append(immigrationStat).append(establishedDt).append(birthPlace).append(personData).append(maritalStat).append(taxResidenceCountry).append(nationality).append(educationLevel).append(relationshipMgr).append(dependents).append(additionalProperties).toHashCode();
    }

    @Override
    public boolean equals(Object other) {
        if (other == this) {
            return true;
        }
        if ((other instanceof PersonPartyInfo) == false) {
            return false;
        }
        PersonPartyInfo rhs = ((PersonPartyInfo) other);
        return new EqualsBuilder().append(residenceCountry, rhs.residenceCountry).append(partyPref, rhs.partyPref).append(birthDt, rhs.birthDt).append(gender, rhs.gender).append(employment, rhs.employment).append(immigrationStat, rhs.immigrationStat).append(establishedDt, rhs.establishedDt).append(birthPlace, rhs.birthPlace).append(personData, rhs.personData).append(maritalStat, rhs.maritalStat).append(taxResidenceCountry, rhs.taxResidenceCountry).append(nationality, rhs.nationality).append(educationLevel, rhs.educationLevel).append(relationshipMgr, rhs.relationshipMgr).append(dependents, rhs.dependents).append(additionalProperties, rhs.additionalProperties).isEquals();
    }

}
