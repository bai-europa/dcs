
package pt.baieuropa.baieserv.models.PartyAcctRelRec;

import com.fasterxml.jackson.annotation.*;
import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.HashCodeBuilder;
import org.apache.commons.lang.builder.ToStringBuilder;

import java.util.HashMap;
import java.util.Map;

@JsonPropertyOrder({
    "acctRef",
    "partyAcctRelData",
    "partyRef"
})
public class PartyAcctRelInfo {

    @JsonProperty("acctRef")
    private AcctRef acctRef;
    @JsonProperty("partyAcctRelData")
    private PartyAcctRelData partyAcctRelData;
    @JsonProperty("partyRef")
    private PartyRef partyRef;
    @JsonIgnore
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();

    @JsonProperty("acctRef")
    public AcctRef getAcctRef() {
        return acctRef;
    }

    @JsonProperty("acctRef")
    public void setAcctRef(AcctRef acctRef) {
        this.acctRef = acctRef;
    }

    public PartyAcctRelInfo withAcctRef(AcctRef acctRef) {
        this.acctRef = acctRef;
        return this;
    }

    @JsonProperty("partyAcctRelData")
    public PartyAcctRelData getPartyAcctRelData() {
        return partyAcctRelData;
    }

    @JsonProperty("partyAcctRelData")
    public void setPartyAcctRelData(PartyAcctRelData partyAcctRelData) {
        this.partyAcctRelData = partyAcctRelData;
    }

    public PartyAcctRelInfo withPartyAcctRelData(PartyAcctRelData partyAcctRelData) {
        this.partyAcctRelData = partyAcctRelData;
        return this;
    }

    @JsonProperty("partyRef")
    public PartyRef getPartyRef() {
        return partyRef;
    }

    @JsonProperty("partyRef")
    public void setPartyRef(PartyRef partyRef) {
        this.partyRef = partyRef;
    }

    public PartyAcctRelInfo withPartyRef(PartyRef partyRef) {
        this.partyRef = partyRef;
        return this;
    }

    @Override
    public String toString() {
        return ToStringBuilder.reflectionToString(this);
    }

    @JsonAnyGetter
    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    @JsonAnySetter
    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }

    public PartyAcctRelInfo withAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
        return this;
    }

    @Override
    public int hashCode() {
        return new HashCodeBuilder().append(acctRef).append(partyAcctRelData).append(partyRef).append(additionalProperties).toHashCode();
    }

    @Override
    public boolean equals(Object other) {
        if (other == this) {
            return true;
        }
        if ((other instanceof PartyAcctRelInfo) == false) {
            return false;
        }
        PartyAcctRelInfo rhs = ((PartyAcctRelInfo) other);
        return new EqualsBuilder().append(acctRef, rhs.acctRef).append(partyAcctRelData, rhs.partyAcctRelData).append(partyRef, rhs.partyRef).append(additionalProperties, rhs.additionalProperties).isEquals();
    }

}
