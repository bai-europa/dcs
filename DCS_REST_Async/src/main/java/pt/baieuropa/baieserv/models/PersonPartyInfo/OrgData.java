
package pt.baieuropa.baieserv.models.PersonPartyInfo;

import com.fasterxml.jackson.annotation.*;
import org.apache.commons.lang.builder.ToStringBuilder;

import java.util.HashMap;
import java.util.Map;

@JsonPropertyOrder({
    "orgName",
    "orgCategory"
})
public class OrgData {
    @JsonProperty("orgName")
    private OrgName orgName;
    @JsonProperty("orgCategory")
    private OrgCategory orgCategory;


    @JsonIgnore
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();

    @JsonProperty("orgName")
    public OrgName getOrgName() {
        return orgName;
    }

    @JsonProperty("orgName")
    public void setOrgName(OrgName orgName) {
        this.orgName = orgName;
    }

    public OrgData withOrgName(OrgName orgName) {
        this.orgName = orgName;
        return this;
    }

    @JsonProperty("orgCategory")
    public OrgCategory getOrgCategory() {
        return orgCategory;
    }

    @JsonProperty("orgCategory")
    public void setOrgCategory(OrgCategory orgCategory) {
        this.orgCategory = orgCategory;
    }

    public OrgData withOrgCategory(OrgCategory orgCategory) {
        this.orgCategory = orgCategory;
        return this;
    }

    @Override
    public String toString() {
        return ToStringBuilder.reflectionToString(this);
    }

    @JsonAnyGetter
    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    @JsonAnySetter
    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }

    public OrgData withAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
        return this;
    }
}
