package pt.baieuropa.baieserv.models.PersonPartyInfo;


import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonPropertyOrder({
        "orgCatIdentValue"
})
public class OrgCatIdent {
    @JsonProperty("orgCatIdentValue")
    private String orgCatIdentValue;


    @JsonProperty("orgCatIdentValue")
    public String getOrgCatIdentValue() {
        return orgCatIdentValue;
    }

    @JsonProperty("orgCatIdentValue")
    public void setOrgCatIdentValue(String orgCatIdentValue) {
        this.orgCatIdentValue = orgCatIdentValue;
    }
}
