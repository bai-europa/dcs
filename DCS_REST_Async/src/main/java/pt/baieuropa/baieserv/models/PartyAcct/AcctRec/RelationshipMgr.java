
package pt.baieuropa.baieserv.models.PartyAcct.AcctRec;

import com.fasterxml.jackson.annotation.*;
import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.HashCodeBuilder;
import org.apache.commons.lang.builder.ToStringBuilder;

import java.util.HashMap;
import java.util.Map;

@JsonPropertyOrder({
    "relationshipRole",
    "relationshipMgrIdent",
    "desc"
})
public class RelationshipMgr {

    @JsonProperty("relationshipRole")
    private String relationshipRole;
    @JsonProperty("relationshipMgrIdent")
    private String relationshipMgrIdent;
    @JsonProperty("desc")
    private String desc;
    @JsonIgnore
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();

    public RelationshipMgr() {
    }

    public RelationshipMgr(String relationshipRole, String relationshipMgrIdent, String desc, Map<String, Object> additionalProperties) {
        this.relationshipRole = relationshipRole;
        this.relationshipMgrIdent = relationshipMgrIdent;
        this.desc = desc;
        this.additionalProperties = additionalProperties;
    }

    @JsonProperty("relationshipRole")
    public String getRelationshipRole() {
        return relationshipRole;
    }

    @JsonProperty("relationshipRole")
    public void setRelationshipRole(String relationshipRole) {
        this.relationshipRole = relationshipRole;
    }

    public RelationshipMgr withRelationshipRole(String relationshipRole) {
        this.relationshipRole = relationshipRole;
        return this;
    }

    @JsonProperty("relationshipMgrIdent")
    public String getRelationshipMgrIdent() {
        return relationshipMgrIdent;
    }

    @JsonProperty("relationshipMgrIdent")
    public void setRelationshipMgrIdent(String relationshipMgrIdent) {
        this.relationshipMgrIdent = relationshipMgrIdent;
    }

    public RelationshipMgr withRelationshipMgrIdent(String relationshipMgrIdent) {
        this.relationshipMgrIdent = relationshipMgrIdent;
        return this;
    }

    @JsonProperty("desc")
    public String getDesc() {
        return desc;
    }

    @JsonProperty("desc")
    public void setDesc(String desc) {
        this.desc = desc;
    }

    public RelationshipMgr withDesc(String desc) {
        this.desc = desc;
        return this;
    }

    @Override
    public String toString() {
        return ToStringBuilder.reflectionToString(this);
    }

    @JsonAnyGetter
    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    @JsonAnySetter
    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }

    public RelationshipMgr withAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
        return this;
    }

    @Override
    public int hashCode() {
        return new HashCodeBuilder().append(relationshipRole).append(relationshipMgrIdent).append(desc).append(additionalProperties).toHashCode();
    }

    @Override
    public boolean equals(Object other) {
        if (other == this) {
            return true;
        }
        if ((other instanceof RelationshipMgr) == false) {
            return false;
        }
        RelationshipMgr rhs = ((RelationshipMgr) other);
        return new EqualsBuilder().append(relationshipRole, rhs.relationshipRole).append(relationshipMgrIdent, rhs.relationshipMgrIdent).append(desc, rhs.desc).append(additionalProperties, rhs.additionalProperties).isEquals();
    }

}
