/*
  Created by: loliveira
*/

package pt.baieuropa.baieserv.models.PartyService;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonPropertyOrder({
        "partyId"
})
public class partyKeys{

    @JsonProperty("partyId")
    private String partyId;

    public partyKeys() {
    }

    public partyKeys(String partyId) {
        this.partyId = partyId;
    }

    @JsonProperty("partyId")
    public String getPartyId() {
        return partyId;
    }

    @JsonProperty("partyId")
    public void setPartyId(String partyId) {
        this.partyId = partyId;
    }
}
