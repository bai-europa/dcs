
package pt.baieuropa.baieserv.models.PartyAcct.AcctRec;

import com.fasterxml.jackson.annotation.*;
import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.HashCodeBuilder;
import org.apache.commons.lang.builder.ToStringBuilder;

import java.util.HashMap;
import java.util.Map;

@JsonPropertyOrder({
    "curCodeType",
    "curCodeValue"
})
public class CurCode {

    @JsonProperty("curCodeType")
    private String curCodeType;
    @JsonProperty("curCodeValue")
    private String curCodeValue;
    @JsonIgnore
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();

    public CurCode() {
    }

    public CurCode(String curCodeType, String curCodeValue, Map<String, Object> additionalProperties) {
        this.curCodeType = curCodeType;
        this.curCodeValue = curCodeValue;
        this.additionalProperties = additionalProperties;
    }

    @JsonProperty("curCodeType")
    public String getCurCodeType() {
        return curCodeType;
    }

    @JsonProperty("curCodeType")
    public void setCurCodeType(String curCodeType) {
        this.curCodeType = curCodeType;
    }

    public CurCode withCurCodeType(String curCodeType) {
        this.curCodeType = curCodeType;
        return this;
    }

    @JsonProperty("curCodeValue")
    public String getCurCodeValue() {
        return curCodeValue;
    }

    @JsonProperty("curCodeValue")
    public void setCurCodeValue(String curCodeValue) {
        this.curCodeValue = curCodeValue;
    }

    public CurCode withCurCodeValue(String curCodeValue) {
        this.curCodeValue = curCodeValue;
        return this;
    }

    @Override
    public String toString() {
        return ToStringBuilder.reflectionToString(this);
    }

    @JsonAnyGetter
    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    @JsonAnySetter
    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }

    public CurCode withAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
        return this;
    }

    @Override
    public int hashCode() {
        return new HashCodeBuilder().append(curCodeType).append(curCodeValue).append(additionalProperties).toHashCode();
    }

    @Override
    public boolean equals(Object other) {
        if (other == this) {
            return true;
        }
        if ((other instanceof CurCode) == false) {
            return false;
        }
        CurCode rhs = ((CurCode) other);
        return new EqualsBuilder().append(curCodeType, rhs.curCodeType).append(curCodeValue, rhs.curCodeValue).append(additionalProperties, rhs.additionalProperties).isEquals();
    }

}
