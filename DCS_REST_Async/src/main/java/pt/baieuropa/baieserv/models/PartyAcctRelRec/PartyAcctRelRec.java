
package pt.baieuropa.baieserv.models.PartyAcctRelRec;

import com.fasterxml.jackson.annotation.*;
import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.HashCodeBuilder;
import org.apache.commons.lang.builder.ToStringBuilder;

import java.util.HashMap;
import java.util.Map;

@JsonPropertyOrder({
    "partyAcctRelId",
    "partyAcctRelInfo"
})
public class PartyAcctRelRec {

    @JsonProperty("partyAcctRelId")
    private String partyAcctRelId;
    @JsonProperty("partyAcctRelInfo")
    private PartyAcctRelInfo partyAcctRelInfo;
    @JsonIgnore
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();

    @JsonProperty("partyAcctRelId")
    public String getPartyAcctRelId() {
        return partyAcctRelId;
    }

    @JsonProperty("partyAcctRelId")
    public void setPartyAcctRelId(String partyAcctRelId) {
        this.partyAcctRelId = partyAcctRelId;
    }

    public PartyAcctRelRec withPartyAcctRelId(String partyAcctRelId) {
        this.partyAcctRelId = partyAcctRelId;
        return this;
    }

    @JsonProperty("partyAcctRelInfo")
    public PartyAcctRelInfo getPartyAcctRelInfo() {
        return partyAcctRelInfo;
    }

    @JsonProperty("partyAcctRelInfo")
    public void setPartyAcctRelInfo(PartyAcctRelInfo partyAcctRelInfo) {
        this.partyAcctRelInfo = partyAcctRelInfo;
    }

    public PartyAcctRelRec withPartyAcctRelInfo(PartyAcctRelInfo partyAcctRelInfo) {
        this.partyAcctRelInfo = partyAcctRelInfo;
        return this;
    }

    @Override
    public String toString() {
        return ToStringBuilder.reflectionToString(this);
    }

    @JsonAnyGetter
    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    @JsonAnySetter
    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }

    public PartyAcctRelRec withAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
        return this;
    }

    @Override
    public int hashCode() {
        return new HashCodeBuilder().append(partyAcctRelId).append(partyAcctRelInfo).append(additionalProperties).toHashCode();
    }

    @Override
    public boolean equals(Object other) {
        if (other == this) {
            return true;
        }
        if ((other instanceof PartyAcctRelRec) == false) {
            return false;
        }
        PartyAcctRelRec rhs = ((PartyAcctRelRec) other);
        return new EqualsBuilder().append(partyAcctRelId, rhs.partyAcctRelId).append(partyAcctRelInfo, rhs.partyAcctRelInfo).append(additionalProperties, rhs.additionalProperties).isEquals();
    }

}
