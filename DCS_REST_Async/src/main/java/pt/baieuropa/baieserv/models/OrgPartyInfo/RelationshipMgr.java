
package pt.baieuropa.baieserv.models.OrgPartyInfo;

import com.fasterxml.jackson.annotation.*;
import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.HashCodeBuilder;
import org.apache.commons.lang.builder.ToStringBuilder;

import java.util.HashMap;
import java.util.Map;

@JsonPropertyOrder({
    "relationshipMgrIdent"
})
public class RelationshipMgr {

    @JsonProperty("relationshipMgrIdent")
    private String relationshipMgrIdent;
    @JsonIgnore
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();

    @JsonProperty("relationshipMgrIdent")
    public String getRelationshipMgrIdent() {
        return relationshipMgrIdent;
    }

    @JsonProperty("relationshipMgrIdent")
    public void setRelationshipMgrIdent(String relationshipMgrIdent) {
        this.relationshipMgrIdent = relationshipMgrIdent;
    }

    public RelationshipMgr withRelationshipMgrIdent(String relationshipMgrIdent) {
        this.relationshipMgrIdent = relationshipMgrIdent;
        return this;
    }

    @Override
    public String toString() {
        return ToStringBuilder.reflectionToString(this);
    }

    @JsonAnyGetter
    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    @JsonAnySetter
    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }

    public RelationshipMgr withAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
        return this;
    }

    @Override
    public int hashCode() {
        return new HashCodeBuilder().append(relationshipMgrIdent).append(additionalProperties).toHashCode();
    }

    @Override
    public boolean equals(Object other) {
        if (other == this) {
            return true;
        }
        if ((other instanceof RelationshipMgr) == false) {
            return false;
        }
        RelationshipMgr rhs = ((RelationshipMgr) other);
        return new EqualsBuilder().append(relationshipMgrIdent, rhs.relationshipMgrIdent).append(additionalProperties, rhs.additionalProperties).isEquals();
    }

}
