package pt.baieuropa.baieserv.models.OrgPartyInfo;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.math.BigDecimal;

@JsonPropertyOrder({
        "Name",
        "entityType",
        "Number_Ext",
        "documentNumberDirectCompany",
        "documentTypeDirectCompany",
        "PercentageInDirectCompany",
        "DocumentNumber",
        "DocumentType",
        "birthDate",
        "constitutionDate",
        "gender",
        "maritalStatus",
        "nationality",
        "countryResidence",
        "birthPlace",
        "professionalActivity",
        "profession",
        "educationalQualifications",
        "socialCapital",
        "cae",
        "cae2",
        "societyType",
        "stockMarketCode"
})
public class BeneficialOwners {

    @JsonProperty("Name")
    protected String name;
    @JsonProperty("EntityType")
    protected String entityType;
    @JsonProperty("Number_Ext")
    protected String numberExt;
    @JsonProperty("DocumentNumberDirectCompany")
    protected String documentNumberDirectCompany;
    @JsonProperty("DocumentTypeDirectCompany")
    protected String documentTypeDirectCompany;
    @JsonProperty("PercentageInDirectCompany")
    protected BigDecimal percentageInDirectCompany;
    @JsonProperty("DocumentNumber")
    protected String documentNumber;
    @JsonProperty("DocumentType")
    protected String documentType;
    @JsonProperty("BirthDate")
    protected String birthDate;
    @JsonProperty("ConstitutionDate")
    protected String constitutionDate;
    @JsonProperty("Gender")
    protected String gender;
    @JsonProperty("MaritalStatus")
    protected String maritalStatus;
    @JsonProperty("Nationality")
    protected String nationality;
    @JsonProperty("CountryResidence")
    protected String countryResidence;
    @JsonProperty("BirthPlace")
    protected String birthPlace;
    @JsonProperty("ProfessionalActivity")
    protected String professionalActivity;
    @JsonProperty("Profession")
    protected String profession;
    @JsonProperty("EducationalQualifications")
    protected String educationalQualifications;
    @JsonProperty("SocialCapital")
    protected BigDecimal socialCapital;
    @JsonProperty("CAE")
    protected String cae;
    @JsonProperty("CAE2")
    protected String cae2;
    @JsonProperty("SocietyType")
    protected String societyType;
    @JsonProperty("StockMarketCode")
    protected String stockMarketCode;

    public BeneficialOwners() {
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getEntityType() {
        return entityType;
    }

    public void setEntityType(String entityType) {
        this.entityType = entityType;
    }

    public String getNumberExt() {
        return numberExt;
    }

    public void setNumberExt(String numberExt) {
        this.numberExt = numberExt;
    }

    public String getDocumentNumberDirectCompany() {
        return documentNumberDirectCompany;
    }

    public void setDocumentNumberDirectCompany(String documentNumberDirectCompany) {
        this.documentNumberDirectCompany = documentNumberDirectCompany;
    }

    public String getDocumentTypeDirectCompany() {
        return documentTypeDirectCompany;
    }

    public void setDocumentTypeDirectCompany(String documentTypeDirectCompany) {
        this.documentTypeDirectCompany = documentTypeDirectCompany;
    }

    public BigDecimal getPercentageInDirectCompany() {
        return percentageInDirectCompany;
    }

    public void setPercentageInDirectCompany(BigDecimal percentageInDirectCompany) {
        this.percentageInDirectCompany = percentageInDirectCompany;
    }

    public String getDocumentNumber() {
        return documentNumber;
    }

    public void setDocumentNumber(String documentNumber) {
        this.documentNumber = documentNumber;
    }

    public String getDocumentType() {
        return documentType;
    }

    public void setDocumentType(String documentType) {
        this.documentType = documentType;
    }

    public String getBirthDate() {
        return birthDate;
    }

    public void setBirthDate(String birthDate) {
        this.birthDate = birthDate;
    }

    public String getConstitutionDate() {
        return constitutionDate;
    }

    public void setConstitutionDate(String constitutionDate) {
        this.constitutionDate = constitutionDate;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public String getMaritalStatus() {
        return maritalStatus;
    }

    public void setMaritalStatus(String maritalStatus) {
        this.maritalStatus = maritalStatus;
    }

    public String getNationality() {
        return nationality;
    }

    public void setNationality(String nationality) {
        this.nationality = nationality;
    }

    public String getCountryResidence() {
        return countryResidence;
    }

    public void setCountryResidence(String countryResidence) {
        this.countryResidence = countryResidence;
    }

    public String getBirthPlace() {
        return birthPlace;
    }

    public void setBirthPlace(String birthPlace) {
        this.birthPlace = birthPlace;
    }

    public String getProfessionalActivity() {
        return professionalActivity;
    }

    public void setProfessionalActivity(String professionalActivity) {
        this.professionalActivity = professionalActivity;
    }

    public String getProfession() {
        return profession;
    }

    public void setProfession(String profession) {
        this.profession = profession;
    }

    public String getEducationalQualifications() {
        return educationalQualifications;
    }

    public void setEducationalQualifications(String educationalQualifications) {
        this.educationalQualifications = educationalQualifications;
    }

    public BigDecimal getSocialCapital() {
        return socialCapital;
    }

    public void setSocialCapital(BigDecimal socialCapital) {
        this.socialCapital = socialCapital;
    }

    public String getCae() {
        return cae;
    }

    public void setCae(String cae) {
        this.cae = cae;
    }

    public String getCae2() {
        return cae2;
    }

    public void setCae2(String cae2) {
        this.cae2 = cae2;
    }

    public String getSocietyType() {
        return societyType;
    }

    public void setSocietyType(String societyType) {
        this.societyType = societyType;
    }

    public String getStockMarketCode() {
        return stockMarketCode;
    }

    public void setStockMarketCode(String stockMarketCode) {
        this.stockMarketCode = stockMarketCode;
    }
}
