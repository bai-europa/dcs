
package pt.baieuropa.baieserv.models.PartyAcct.AcctRec;

import com.fasterxml.jackson.annotation.*;
import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.HashCodeBuilder;
import org.apache.commons.lang.builder.ToStringBuilder;

import java.util.HashMap;
import java.util.Map;

@JsonPropertyOrder({
    "acctTypeSource",
    "acctTypeValue"
})
public class AcctType {

    @JsonProperty("acctTypeSource")
    private String acctTypeSource;
    @JsonProperty("acctTypeValue")
    private String acctTypeValue;
    @JsonIgnore
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();

    public AcctType() {
    }

    public AcctType(String acctTypeSource, String acctTypeValue, Map<String, Object> additionalProperties) {
        this.acctTypeSource = acctTypeSource;
        this.acctTypeValue = acctTypeValue;
        this.additionalProperties = additionalProperties;
    }

    @JsonProperty("acctTypeSource")
    public String getAcctTypeSource() {
        return acctTypeSource;
    }

    @JsonProperty("acctTypeSource")
    public void setAcctTypeSource(String acctTypeSource) {
        this.acctTypeSource = acctTypeSource;
    }

    public AcctType withAcctTypeSource(String acctTypeSource) {
        this.acctTypeSource = acctTypeSource;
        return this;
    }

    @JsonProperty("acctTypeValue")
    public String getAcctTypeValue() {
        return acctTypeValue;
    }

    @JsonProperty("acctTypeValue")
    public void setAcctTypeValue(String acctTypeValue) {
        this.acctTypeValue = acctTypeValue;
    }

    public AcctType withAcctTypeValue(String acctTypeValue) {
        this.acctTypeValue = acctTypeValue;
        return this;
    }

    @Override
    public String toString() {
        return ToStringBuilder.reflectionToString(this);
    }

    @JsonAnyGetter
    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    @JsonAnySetter
    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }

    public AcctType withAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
        return this;
    }

    @Override
    public int hashCode() {
        return new HashCodeBuilder().append(acctTypeSource).append(acctTypeValue).append(additionalProperties).toHashCode();
    }

    @Override
    public boolean equals(Object other) {
        if (other == this) {
            return true;
        }
        if ((other instanceof AcctType) == false) {
            return false;
        }
        AcctType rhs = ((AcctType) other);
        return new EqualsBuilder().append(acctTypeSource, rhs.acctTypeSource).append(acctTypeValue, rhs.acctTypeValue).append(additionalProperties, rhs.additionalProperties).isEquals();
    }

}
