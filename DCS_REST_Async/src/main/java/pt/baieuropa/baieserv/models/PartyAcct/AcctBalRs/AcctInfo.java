
package pt.baieuropa.baieserv.models.PartyAcct.AcctBalRs;

import com.fasterxml.jackson.annotation.*;
import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.HashCodeBuilder;
import org.apache.commons.lang.builder.ToStringBuilder;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@JsonPropertyOrder({
    "acctBal"
})
public class AcctInfo {

    @JsonProperty("acctBal")
    private List<AcctBal> acctBal = new ArrayList<AcctBal>();
    @JsonIgnore
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();

    public AcctInfo() {
    }

    public AcctInfo(List<AcctBal> acctBal, Map<String, Object> additionalProperties) {
        this.acctBal = acctBal;
        this.additionalProperties = additionalProperties;
    }

    @JsonProperty("acctBal")
    public List<AcctBal> getAcctBal() {
        return acctBal;
    }

    @JsonProperty("acctBal")
    public void setAcctBal(List<AcctBal> acctBal) {
        this.acctBal = acctBal;
    }

    public AcctInfo withAcctBal(List<AcctBal> acctBal) {
        this.acctBal = acctBal;
        return this;
    }

    @Override
    public String toString() {
        return ToStringBuilder.reflectionToString(this);
    }

    @JsonAnyGetter
    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    @JsonAnySetter
    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }

    public AcctInfo withAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
        return this;
    }

    @Override
    public int hashCode() {
        return new HashCodeBuilder().append(acctBal).append(additionalProperties).toHashCode();
    }

    @Override
    public boolean equals(Object other) {
        if (other == this) {
            return true;
        }
        if ((other instanceof AcctInfo) == false) {
            return false;
        }
        AcctInfo rhs = ((AcctInfo) other);
        return new EqualsBuilder().append(acctBal, rhs.acctBal).append(additionalProperties, rhs.additionalProperties).isEquals();
    }

}
