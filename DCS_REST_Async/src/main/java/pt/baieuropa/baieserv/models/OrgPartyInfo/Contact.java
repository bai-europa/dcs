package pt.baieuropa.baieserv.models.OrgPartyInfo;

import com.fasterxml.jackson.annotation.*;
import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.HashCodeBuilder;
import org.apache.commons.lang.builder.ToStringBuilder;

import java.util.HashMap;
import java.util.Map;

@JsonPropertyOrder({
    "language",
    "locator",
    "preferredInd"
})
public class Contact {

    @JsonProperty("language")
    private String language;
    @JsonProperty("locator")
    private Locator locator;
    @JsonProperty("preferredInd")
    private String preferredInd;
    @JsonIgnore
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();

    @JsonProperty("language")
    public String getLanguage() {
        return language;
    }

    @JsonProperty("language")
    public void setLanguage(String language) {
        this.language = language;
    }

    public Contact withLanguage(String language) {
        this.language = language;
        return this;
    }

    @JsonProperty("locator")
    public Locator getLocator() {
        return locator;
    }

    @JsonProperty("locator")
    public void setLocator(Locator locator) {
        this.locator = locator;
    }


    @JsonProperty("preferredInd")
    public String getPreferredInd() {
        return preferredInd;
    }

    @JsonProperty("preferredInd")
    public void setPreferredInd(String preferredInd) {
        this.preferredInd = preferredInd;
    }

    public Contact withPreferredInd(String preferredInd) {
        this.preferredInd = preferredInd;
        return this;
    }

    @Override
    public String toString() {
        return ToStringBuilder.reflectionToString(this);
    }

    @JsonAnyGetter
    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    @JsonAnySetter
    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }

    public Contact withAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
        return this;
    }

    @Override
    public int hashCode() {
        return new HashCodeBuilder().append(language).append(locator).append(preferredInd).append(additionalProperties).toHashCode();
    }

    @Override
    public boolean equals(Object other) {
        if (other == this) {
            return true;
        }
        if ((other instanceof Contact) == false) {
            return false;
        }
        Contact rhs = ((Contact) other);
        return new EqualsBuilder().append(language, rhs.language).append(locator, rhs.locator).append(preferredInd, rhs.preferredInd).append(additionalProperties, rhs.additionalProperties).isEquals();
    }

}
