package pt.baieuropa.baieserv.models.PartyAcct;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import pt.baieuropa.baieserv.models.Party.PartyStatus;

@JsonPropertyOrder({
        "acctInfo",
        "acctStatus"
})
public class PartyAcct {

    @JsonProperty("acctInfo")
    private AcctInfo acctInfo;
    @JsonProperty("acctStatus")
    private AcctStatus acctStatus;


}
