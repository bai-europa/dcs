
package pt.baieuropa.baieserv.models.PartyAcct.AcctRec;

import com.fasterxml.jackson.annotation.*;
import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.HashCodeBuilder;
import org.apache.commons.lang.builder.ToStringBuilder;

import java.util.HashMap;
import java.util.Map;

@JsonPropertyOrder({
    "acctInfo",
    "acctStatus",
    "acctID"
})
public class AcctRec {

    @JsonProperty("acctInfo")
    private AcctInfo acctInfo;
    @JsonProperty("acctStatus")
    private AcctStatus acctStatus;
    @JsonProperty("acctID")
    private String acctID;
    @JsonIgnore
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();

    public AcctRec() {
    }

    public AcctRec(AcctInfo acctInfo, AcctStatus acctStatus, String acctID, Map<String, Object> additionalProperties) {
        this.acctInfo = acctInfo;
        this.acctStatus = acctStatus;
        this.acctID = acctID;
        this.additionalProperties = additionalProperties;
    }

    @JsonProperty("acctInfo")
    public AcctInfo getAcctInfo() {
        return acctInfo;
    }

    @JsonProperty("acctInfo")
    public void setAcctInfo(AcctInfo acctInfo) {
        this.acctInfo = acctInfo;
    }

    public AcctRec withAcctInfo(AcctInfo acctInfo) {
        this.acctInfo = acctInfo;
        return this;
    }

    @JsonProperty("acctStatus")
    public AcctStatus getAcctStatus() {
        return acctStatus;
    }

    @JsonProperty("acctStatus")
    public void setAcctStatus(AcctStatus acctStatus) {
        this.acctStatus = acctStatus;
    }

    public AcctRec withAcctStatus(AcctStatus acctStatus) {
        this.acctStatus = acctStatus;
        return this;
    }

    @JsonProperty("acctID")
    public String getAcctID() {
        return acctID;
    }

    @JsonProperty("acctID")
    public void setAcctID(String acctID) {
        this.acctID = acctID;
    }

    public AcctRec withAcctID(String acctID) {
        this.acctID = acctID;
        return this;
    }

    @Override
    public String toString() {
        return ToStringBuilder.reflectionToString(this);
    }

    @JsonAnyGetter
    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    @JsonAnySetter
    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }

    public AcctRec withAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
        return this;
    }

    @Override
    public int hashCode() {
        return new HashCodeBuilder().append(acctInfo).append(acctStatus).append(acctID).append(additionalProperties).toHashCode();
    }

    @Override
    public boolean equals(Object other) {
        if (other == this) {
            return true;
        }
        if ((other instanceof AcctRec) == false) {
            return false;
        }
        AcctRec rhs = ((AcctRec) other);
        return new EqualsBuilder().append(acctInfo, rhs.acctInfo).append(acctStatus, rhs.acctStatus).append(acctID, rhs.acctID).append(additionalProperties, rhs.additionalProperties).isEquals();
    }

}
