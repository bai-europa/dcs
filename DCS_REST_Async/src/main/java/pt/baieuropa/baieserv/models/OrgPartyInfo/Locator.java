
package pt.baieuropa.baieserv.models.OrgPartyInfo;

import com.fasterxml.jackson.annotation.*;
import org.apache.commons.lang.builder.ToStringBuilder;

import java.util.HashMap;
import java.util.Map;

@JsonPropertyOrder({
        "addr1",
        "addr2",
        "addr3",
        "addr4",
        "city",
        "stateProv",
        "countryCode",
        "postalCode",
        "addressIdent",
        "addrType",
        "emailType",
        "emailAddr",
        "phoneType",
        "phone"
})
public class Locator {
    @JsonProperty("addr1")
    private String addr1;
    @JsonProperty("addr2")
    private String addr2;
    @JsonProperty("addr3")
    private String addr3;
    @JsonProperty("addr4")
    private String addr4;
    @JsonProperty("city")
    private String city;
    @JsonProperty("stateProv")
    private String stateProv;
    @JsonProperty("countryCode")
    private String countryCode;
    @JsonProperty("postalCode")
    private String postalCode;
    @JsonProperty("addressIdent")
    private String addressIdent;
    @JsonProperty("addrType")
    private String addrType;
    @JsonProperty("emailType")
    private String emailType;
    @JsonProperty("emailAddr")
    private String emailAddr;
    @JsonProperty("phoneType")
    private String phoneType;
    @JsonProperty("phone")
    private String phone;

    @JsonIgnore
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();

    @JsonProperty("addr1")
    public String getAddr1() {
        return addr1;
    }

    @JsonProperty("addr1")
    public void setAddr1(String addr1) {
        this.addr1 = addr1;
    }

    @JsonProperty("addr2")
    public String getAddr2() {
        return addr2;
    }

    @JsonProperty("addr2")
    public void setAddr2(String addr2) {
        this.addr2 = addr2;
    }

    @JsonProperty("addr3")
    public String getAddr3() {
        return addr3;
    }

    @JsonProperty("addr3")
    public void setAddr3(String addr3) {
        this.addr3 = addr3;
    }

    @JsonProperty("addr4")
    public String getAddr4() {
        return addr4;
    }

    @JsonProperty("addr4")
    public void setAddr4(String addr4) {
        this.addr4 = addr4;
    }

    @JsonProperty("city")
    public String getCity() {
        return city;
    }

    @JsonProperty("city")
    public void setCity(String city) {
        this.city = city;
    }

    @JsonProperty("stateProv")
    public String getStateProv() {
        return stateProv;
    }

    @JsonProperty("stateProv")
    public void setStateProv(String stateProv) {
        this.stateProv = stateProv;
    }

    @JsonProperty("countryCode")
    public String getCountryCode() {
        return countryCode;
    }

    @JsonProperty("countryCode")
    public void setCountryCode(String countryCode) {
        this.countryCode = countryCode;
    }

    @JsonProperty("postalCode")
    public String getPostalCode() {
        return postalCode;
    }

    @JsonProperty("postalCode")
    public void setPostalCode(String postalCode) {
        this.postalCode = postalCode;
    }

    @JsonProperty("addressIdent")
    public String getAddressIdent() {
        return addressIdent;
    }

    @JsonProperty("addressIdent")
    public void setAddressIdent(String addressIdent) {
        this.addressIdent = addressIdent;
    }

    @JsonProperty("addrType")
    public String getAddrType() {
        return addrType;
    }

    @JsonProperty("addrType")
    public void setAddrType(String addrType) {
        this.addrType = addrType;
    }

    @JsonProperty("emailType")
    public String getEmailType() {
        return emailType;
    }

    @JsonProperty("emailType")
    public void setEmailType(String emailType) {
        this.emailType = emailType;
    }

    @JsonProperty("emailAddr")
    public String getEmailAddr() {
        return emailAddr;
    }

    @JsonProperty("emailAddr")
    public void setEmailAddr(String emailAddr) {
        this.emailAddr = emailAddr;
    }

    @JsonProperty("phoneType")
    public String getPhoneType() {
        return phoneType;
    }

    @JsonProperty("phoneType")
    public void setPhoneType(String phoneType) {
        this.phoneType = phoneType;
    }

    @JsonProperty("phone")
    public String getPhone() {
        return phone;
    }

    @JsonProperty("phone")
    public void setPhone(String phone) {
        this.phone = phone;
    }

    @Override
    public String toString() {
        return ToStringBuilder.reflectionToString(this);
    }

    @JsonAnyGetter
    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    @JsonAnySetter
    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }

}
