
package pt.baieuropa.baieserv.models.Party;

import com.fasterxml.jackson.annotation.*;
import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.HashCodeBuilder;
import org.apache.commons.lang.builder.ToStringBuilder;
import pt.baieuropa.baieserv.models.OrgPartyInfo.OrgPartyInfo;
import pt.baieuropa.baieserv.models.PersonPartyInfo.PersonPartyInfo;

import java.util.HashMap;
import java.util.Map;

@JsonPropertyOrder({
        "partyId",
        "orgPartyInfo",
        "personPartyInfo",
        "partyStatus",
        "RequestUser",
        "RequestId",
        "IsPresencial",
        "PartyType"

})
public class Party {

    @JsonProperty("partyStatus")
    private PartyStatus partyStatus;
    @JsonProperty("orgPartyInfo")
    private OrgPartyInfo orgPartyInfo;
    @JsonProperty("personPartyInfo")
    private PersonPartyInfo personPartyInfo;
    @JsonProperty("partyId")
    private String partyId;
    @JsonProperty("PartyType")
    private String partyType;
    @JsonProperty("RequestUser")
    private String requestUser;
    @JsonProperty("RequestId")
    private String requestId;
    @JsonProperty("IsPresencial")
    private boolean isPresencial;
    @JsonIgnore
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();

    @JsonProperty("partyStatus")
    public PartyStatus getPartyStatus() {
        return partyStatus;
    }

    @JsonProperty("partyStatus")
    public void setPartyStatus(PartyStatus partyStatus) {
        this.partyStatus = partyStatus;
    }

    public Party withPartyStatus(PartyStatus partyStatus) {
        this.partyStatus = partyStatus;
        return this;
    }

    @JsonProperty("orgPartyInfo")
    public OrgPartyInfo getOrgPartyInfo() {
        return orgPartyInfo;
    }

    @JsonProperty("orgPartyInfo")
    public void setOrgPartyInfo(OrgPartyInfo orgPartyInfo) {
        this.orgPartyInfo = orgPartyInfo;
    }

    @JsonProperty("personPartyInfo")
    public PersonPartyInfo getPersonPartyInfo() {
        return personPartyInfo;
    }

    @JsonProperty("personPartyInfo")
    public void setPersonPartyInfo(PersonPartyInfo personPartyInfo) {
        this.personPartyInfo = personPartyInfo;
    }

    @JsonProperty("partyId")
    public String getPartyId() {
        return partyId;
    }

    @JsonProperty("partyId")
    public void setPartyId(String partyId) {
        this.partyId = partyId;
    }

    @JsonProperty("PartyType")
    public String getPartyType() {
        return partyType;
    }

    @JsonProperty("PartyType")
    public void setPartyType(String partyType) {
        this.partyType = partyType;
    }

    public void setAdditionalProperties(Map<String, Object> additionalProperties) {
        this.additionalProperties = additionalProperties;
    }

    @JsonProperty("RequestUser")
    public String getRequestUser() {
        return requestUser;
    }

    @JsonProperty("RequestUser")
    public void setRequestUser(String requestUser) {
        this.requestUser = requestUser;
    }

    @JsonProperty("RequestId")
    public String getRequestId() {
        return requestId;
    }

    @JsonProperty("RequestId")
    public void setRequestId(String requestId) {
        this.requestId = requestId;
    }

    @JsonProperty("IsPresencial")
    public boolean isPresencial() {
        return this.isPresencial;
    }

    @JsonProperty("IsPresencial")
    public void setPresencial(boolean isPresencial) {
        this.isPresencial = isPresencial;
    }



    public Party withPartyId(String partyId) {
        this.partyId = partyId;
        return this;
    }

    @Override
    public String toString() {
        return ToStringBuilder.reflectionToString(this);
    }

    @JsonAnyGetter
    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    @JsonAnySetter
    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }

    public Party withAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
        return this;
    }

    @Override
    public int hashCode() {
        return new HashCodeBuilder().append(partyStatus).append(orgPartyInfo).append(personPartyInfo).append(partyId).append(additionalProperties).toHashCode();
    }

    @Override
    public boolean equals(Object other) {
        if (other == this) {
            return true;
        }
        if ((other instanceof Party) == false) {
            return false;
        }
        Party rhs = ((Party) other);
        return new EqualsBuilder().append(partyStatus, rhs.partyStatus).append(orgPartyInfo, rhs.orgPartyInfo).append(personPartyInfo, rhs.personPartyInfo).append(partyId, rhs.partyId).append(additionalProperties, rhs.additionalProperties).isEquals();
    }

}
