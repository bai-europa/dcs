
package pt.baieuropa.baieserv.models.PersonPartyInfo;

import com.fasterxml.jackson.annotation.*;
import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.HashCodeBuilder;
import org.apache.commons.lang.builder.ToStringBuilder;

import java.util.HashMap;
import java.util.Map;

@JsonPropertyOrder({
        "orgData",
        "jobTitle",
        "timeFrame",
        "jobName",
        "jobId"
})
public class Employment {

    @JsonProperty("orgData")
    private OrgData orgData;
    @JsonProperty("jobTitle")
    private String jobTitle;
    @JsonProperty("timeFrame")
    private String timeFrame;

    @JsonProperty("jobName")
    private String jobName;
    @JsonProperty("jobId")
    private String jobId;

    @JsonIgnore
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();

    @JsonProperty("orgData")
    public OrgData getOrgData() {
        return orgData;
    }

    @JsonProperty("orgData")
    public void setOrgData(OrgData orgData) {
        this.orgData = orgData;
    }

    public Employment withOrgData(OrgData orgData) {
        this.orgData = orgData;
        return this;
    }

    @JsonProperty("jobTitle")
    public String getJobTitle() {
        return jobTitle;
    }

    @JsonProperty("jobTitle")
    public void setJobTitle(String jobTitle) {
        this.jobTitle = jobTitle;
    }

    @JsonProperty("timeFrame")
    public String getTimeFrame() {
        return timeFrame;
    }


    public void setTimeFrame(String timeFrame) {
        this.timeFrame = timeFrame;
    }

    @JsonProperty("jobName")
    public String getJobName() {
        return jobName;
    }

    public void setJobName(String jobName) {
        this.jobName = jobName;
    }

    @JsonProperty("jobId")
    public String getJobId() {
        return jobId;
    }

    public void setJobId(String jobId) {
        this.jobId = jobId;
    }

    @Override
    public String toString() {
        return ToStringBuilder.reflectionToString(this);
    }

    @JsonAnyGetter
    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    @JsonAnySetter
    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }

    public Employment withAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
        return this;
    }

    @Override
    public int hashCode() {
        return new HashCodeBuilder().append(orgData).append(additionalProperties).append(jobName).append(jobId).toHashCode();
    }

    @Override
    public boolean equals(Object other) {
        if (other == this) {
            return true;
        }
        if ((other instanceof Employment) == false) {
            return false;
        }
        Employment rhs = ((Employment) other);
        return new EqualsBuilder().append(orgData, rhs.orgData).append(additionalProperties, rhs.additionalProperties).append(jobName, rhs.jobName).append(jobId, rhs.jobId).isEquals();
    }
}
