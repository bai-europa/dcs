
package pt.baieuropa.baieserv.models.PartyAcctRelRec;

import com.fasterxml.jackson.annotation.*;
import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.HashCodeBuilder;
import org.apache.commons.lang.builder.ToStringBuilder;

import java.util.HashMap;
import java.util.Map;

@JsonPropertyOrder({
    "partyAcctRelType"
})
public class PartyAcctRelData {

    @JsonProperty("partyAcctRelType")
    private String partyAcctRelType;
    @JsonIgnore
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();

    @JsonProperty("partyAcctRelType")
    public String getPartyAcctRelType() {
        return partyAcctRelType;
    }

    @JsonProperty("partyAcctRelType")
    public void setPartyAcctRelType(String partyAcctRelType) {
        this.partyAcctRelType = partyAcctRelType;
    }

    public PartyAcctRelData withPartyAcctRelType(String partyAcctRelType) {
        this.partyAcctRelType = partyAcctRelType;
        return this;
    }

    @Override
    public String toString() {
        return ToStringBuilder.reflectionToString(this);
    }

    @JsonAnyGetter
    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    @JsonAnySetter
    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }

    public PartyAcctRelData withAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
        return this;
    }

    @Override
    public int hashCode() {
        return new HashCodeBuilder().append(partyAcctRelType).append(additionalProperties).toHashCode();
    }

    @Override
    public boolean equals(Object other) {
        if (other == this) {
            return true;
        }
        if ((other instanceof PartyAcctRelData) == false) {
            return false;
        }
        PartyAcctRelData rhs = ((PartyAcctRelData) other);
        return new EqualsBuilder().append(partyAcctRelType, rhs.partyAcctRelType).append(additionalProperties, rhs.additionalProperties).isEquals();
    }

}
