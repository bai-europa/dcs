package pt.baieuropa.baieserv.models.Party;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonPropertyOrder({
        "partyId"
})
public class acctKeys{

    @JsonProperty("partyId")
    private String acctId;

    public acctKeys() {}

    @JsonProperty("partyId")
    public String getAcctId() {
        return acctId;
    }

    @JsonProperty("partyId")
    public void setAcctId(String acctId) {
        this.acctId = acctId;
    }
}
