package pt.baieuropa.baieserv.models.Party;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonPropertyOrder({
        "partyKeys"
})
public class acctRef{

    @JsonProperty("partyKeys")
    private acctKeys acctKeys;

    public acctRef() { }

    @JsonProperty("partyKeys")
    public acctKeys getAcctKeys() {
        return acctKeys;
    }

    @JsonProperty("partyKeys")
    public void setAcctKeys(acctKeys acctKeys) {
        this.acctKeys = acctKeys;
    }

}
