
package pt.baieuropa.baieserv.models.PartyAcct.AcctRec;

import com.fasterxml.jackson.annotation.*;
import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.HashCodeBuilder;
import org.apache.commons.lang.builder.ToStringBuilder;

import java.util.HashMap;
import java.util.Map;

@JsonPropertyOrder({
    "acctStatusCode"
})
public class AcctStatus {

    @JsonProperty("acctStatusCode")
    private String acctStatusCode;
    @JsonIgnore
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();

    public AcctStatus() {
    }

    public AcctStatus(String acctStatusCode, Map<String, Object> additionalProperties) {
        this.acctStatusCode = acctStatusCode;
        this.additionalProperties = additionalProperties;
    }

    @JsonProperty("acctStatusCode")
    public String getAcctStatusCode() {
        return acctStatusCode;
    }

    @JsonProperty("acctStatusCode")
    public void setAcctStatusCode(String acctStatusCode) {
        this.acctStatusCode = acctStatusCode;
    }

    public AcctStatus withAcctStatusCode(String acctStatusCode) {
        this.acctStatusCode = acctStatusCode;
        return this;
    }

    @Override
    public String toString() {
        return ToStringBuilder.reflectionToString(this);
    }

    @JsonAnyGetter
    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    @JsonAnySetter
    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }

    public AcctStatus withAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
        return this;
    }

    @Override
    public int hashCode() {
        return new HashCodeBuilder().append(acctStatusCode).append(additionalProperties).toHashCode();
    }

    @Override
    public boolean equals(Object other) {
        if (other == this) {
            return true;
        }
        if ((other instanceof AcctStatus) == false) {
            return false;
        }
        AcctStatus rhs = ((AcctStatus) other);
        return new EqualsBuilder().append(acctStatusCode, rhs.acctStatusCode).append(additionalProperties, rhs.additionalProperties).isEquals();
    }

}
