
package pt.baieuropa.baieserv.models.PartyAcct.AcctBalRs;

import com.fasterxml.jackson.annotation.*;
import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.HashCodeBuilder;
import org.apache.commons.lang.builder.ToStringBuilder;

import java.util.HashMap;
import java.util.Map;

@JsonPropertyOrder({
    "balType",
    "curAmt"
})
public class AcctBal {

    @JsonProperty("balType")
    private BalType balType;
    @JsonProperty("curAmt")
    private CurAmt curAmt;
    @JsonIgnore
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();

    public AcctBal() {
    }

    public AcctBal(BalType balType, CurAmt curAmt, Map<String, Object> additionalProperties) {
        this.balType = balType;
        this.curAmt = curAmt;
        this.additionalProperties = additionalProperties;
    }

    @JsonProperty("balType")
    public BalType getBalType() {
        return balType;
    }

    @JsonProperty("balType")
    public void setBalType(BalType balType) {
        this.balType = balType;
    }

    public AcctBal withBalType(BalType balType) {
        this.balType = balType;
        return this;
    }

    @JsonProperty("curAmt")
    public CurAmt getCurAmt() {
        return curAmt;
    }

    @JsonProperty("curAmt")
    public void setCurAmt(CurAmt curAmt) {
        this.curAmt = curAmt;
    }

    public AcctBal withCurAmt(CurAmt curAmt) {
        this.curAmt = curAmt;
        return this;
    }

    @Override
    public String toString() {
        return ToStringBuilder.reflectionToString(this);
    }

    @JsonAnyGetter
    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    @JsonAnySetter
    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }

    public AcctBal withAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
        return this;
    }

    @Override
    public int hashCode() {
        return new HashCodeBuilder().append(balType).append(curAmt).append(additionalProperties).toHashCode();
    }

    @Override
    public boolean equals(Object other) {
        if (other == this) {
            return true;
        }
        if ((other instanceof AcctBal) == false) {
            return false;
        }
        AcctBal rhs = ((AcctBal) other);
        return new EqualsBuilder().append(balType, rhs.balType).append(curAmt, rhs.curAmt).append(additionalProperties, rhs.additionalProperties).isEquals();
    }

}
