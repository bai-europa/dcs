
package pt.baieuropa.baieserv.models.PersonPartyInfo;

import com.fasterxml.jackson.annotation.*;
import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.HashCodeBuilder;
import org.apache.commons.lang.builder.ToStringBuilder;

import java.util.HashMap;
import java.util.Map;

@JsonPropertyOrder({
    "countryCodeSource",
    "countryCodeValue"
})
public class ResidenceCountry {

    @JsonProperty("countryCodeSource")
    private String countryCodeSource;
    @JsonProperty("countryCodeValue")
    private String countryCodeValue;
    @JsonIgnore
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();

    @JsonProperty("countryCodeSource")
    public String getCountryCodeSource() {
        return countryCodeSource;
    }

    @JsonProperty("countryCodeSource")
    public void setCountryCodeSource(String countryCodeSource) {
        this.countryCodeSource = countryCodeSource;
    }

    public ResidenceCountry withCountryCodeSource(String countryCodeSource) {
        this.countryCodeSource = countryCodeSource;
        return this;
    }

    @JsonProperty("countryCodeValue")
    public String getCountryCodeValue() {
        return countryCodeValue;
    }

    @JsonProperty("countryCodeValue")
    public void setCountryCodeValue(String countryCodeValue) {
        this.countryCodeValue = countryCodeValue;
    }

    public ResidenceCountry withCountryCodeValue(String countryCodeValue) {
        this.countryCodeValue = countryCodeValue;
        return this;
    }

    @Override
    public String toString() {
        return ToStringBuilder.reflectionToString(this);
    }

    @JsonAnyGetter
    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    @JsonAnySetter
    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }

    public ResidenceCountry withAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
        return this;
    }

    @Override
    public int hashCode() {
        return new HashCodeBuilder().append(countryCodeSource).append(countryCodeValue).append(additionalProperties).toHashCode();
    }

    @Override
    public boolean equals(Object other) {
        if (other == this) {
            return true;
        }
        if ((other instanceof ResidenceCountry) == false) {
            return false;
        }
        ResidenceCountry rhs = ((ResidenceCountry) other);
        return new EqualsBuilder().append(countryCodeSource, rhs.countryCodeSource).append(countryCodeValue, rhs.countryCodeValue).append(additionalProperties, rhs.additionalProperties).isEquals();
    }

}
