
package pt.baieuropa.baieserv.models.Party;

import com.fasterxml.jackson.annotation.*;
import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.HashCodeBuilder;
import org.apache.commons.lang.builder.ToStringBuilder;

import java.util.HashMap;
import java.util.Map;

@JsonPropertyOrder({
    "partyStatusCode"
})
public class PartyStatus {

    @JsonProperty("partyStatusCode")
    private String partyStatusCode;
    @JsonIgnore
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();

    @JsonProperty("partyStatusCode")
    public String getPartyStatusCode() {
        return partyStatusCode;
    }

    @JsonProperty("partyStatusCode")
    public void setPartyStatusCode(String partyStatusCode) {
        this.partyStatusCode = partyStatusCode;
    }

    public PartyStatus withPartyStatusCode(String partyStatusCode) {
        this.partyStatusCode = partyStatusCode;
        return this;
    }

    @Override
    public String toString() {
        return ToStringBuilder.reflectionToString(this);
    }

    @JsonAnyGetter
    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    @JsonAnySetter
    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }

    public PartyStatus withAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
        return this;
    }

    @Override
    public int hashCode() {
        return new HashCodeBuilder().append(partyStatusCode).append(additionalProperties).toHashCode();
    }

    @Override
    public boolean equals(Object other) {
        if (other == this) {
            return true;
        }
        if ((other instanceof PartyStatus) == false) {
            return false;
        }
        PartyStatus rhs = ((PartyStatus) other);
        return new EqualsBuilder().append(partyStatusCode, rhs.partyStatusCode).append(additionalProperties, rhs.additionalProperties).isEquals();
    }

}
