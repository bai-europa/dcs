
package pt.baieuropa.baieserv.models.PartyAcct.AcctRec;

import com.fasterxml.jackson.annotation.*;
import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.HashCodeBuilder;
import org.apache.commons.lang.builder.ToStringBuilder;

import java.util.HashMap;
import java.util.Map;

@JsonPropertyOrder({
    "FIIdent",
    "branchIdent",
    "FIIdentType"
})
public class FIData {

    @JsonProperty("FIIdent")
    private String fIIdent;
    @JsonProperty("branchIdent")
    private String branchIdent;
    @JsonProperty("FIIdentType")
    private String fIIdentType;
    @JsonIgnore
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();

    public FIData() {
    }

    public FIData(String fIIdent, String branchIdent, String fIIdentType, Map<String, Object> additionalProperties) {
        this.fIIdent = fIIdent;
        this.branchIdent = branchIdent;
        this.fIIdentType = fIIdentType;
        this.additionalProperties = additionalProperties;
    }

    @JsonProperty("FIIdent")
    public String getFIIdent() {
        return fIIdent;
    }

    @JsonProperty("FIIdent")
    public void setFIIdent(String fIIdent) {
        this.fIIdent = fIIdent;
    }

    public FIData withFIIdent(String fIIdent) {
        this.fIIdent = fIIdent;
        return this;
    }

    @JsonProperty("branchIdent")
    public String getBranchIdent() {
        return branchIdent;
    }

    @JsonProperty("branchIdent")
    public void setBranchIdent(String branchIdent) {
        this.branchIdent = branchIdent;
    }

    public FIData withBranchIdent(String branchIdent) {
        this.branchIdent = branchIdent;
        return this;
    }

    @JsonProperty("FIIdentType")
    public String getFIIdentType() {
        return fIIdentType;
    }

    @JsonProperty("FIIdentType")
    public void setFIIdentType(String fIIdentType) {
        this.fIIdentType = fIIdentType;
    }

    public FIData withFIIdentType(String fIIdentType) {
        this.fIIdentType = fIIdentType;
        return this;
    }

    @Override
    public String toString() {
        return ToStringBuilder.reflectionToString(this);
    }

    @JsonAnyGetter
    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    @JsonAnySetter
    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }

    public FIData withAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
        return this;
    }

    @Override
    public int hashCode() {
        return new HashCodeBuilder().append(fIIdent).append(branchIdent).append(fIIdentType).append(additionalProperties).toHashCode();
    }

    @Override
    public boolean equals(Object other) {
        if (other == this) {
            return true;
        }
        if ((other instanceof FIData) == false) {
            return false;
        }
        FIData rhs = ((FIData) other);
        return new EqualsBuilder().append(fIIdent, rhs.fIIdent).append(branchIdent, rhs.branchIdent).append(fIIdentType, rhs.fIIdentType).append(additionalProperties, rhs.additionalProperties).isEquals();
    }

}
