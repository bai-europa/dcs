package pt.baieuropa.baieserv.models.PartyAcct.AcctBalRs;

import com.fasterxml.jackson.annotation.*;
import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.HashCodeBuilder;
import org.apache.commons.lang.builder.ToStringBuilder;

import java.util.HashMap;
import java.util.Map;

@JsonPropertyOrder({
    "acctInfo",
    "acctStatus",
    "acctId"
})
public class AcctBalRs {

    @JsonProperty("acctInfo")
    private AcctInfo acctInfo;
    @JsonProperty("acctStatus")
    private AcctStatus acctStatus;
    @JsonProperty("acctId")
    private String acctId;
    @JsonIgnore
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();

    public AcctBalRs() {
    }

    public AcctBalRs(AcctInfo acctInfo, AcctStatus acctStatus, String acctId, Map<String, Object> additionalProperties) {
        this.acctInfo = acctInfo;
        this.acctStatus = acctStatus;
        this.acctId = acctId;
        this.additionalProperties = additionalProperties;
    }

    @JsonProperty("acctInfo")
    public AcctInfo getAcctInfo() {
        return acctInfo;
    }

    @JsonProperty("acctInfo")
    public void setAcctInfo(AcctInfo acctInfo) {
        this.acctInfo = acctInfo;
    }

    public AcctBalRs withAcctInfo(AcctInfo acctInfo) {
        this.acctInfo = acctInfo;
        return this;
    }

    @JsonProperty("acctStatus")
    public AcctStatus getAcctStatus() {
        return acctStatus;
    }

    @JsonProperty("acctStatus")
    public void setAcctStatus(AcctStatus acctStatus) {
        this.acctStatus = acctStatus;
    }

    public AcctBalRs withAcctStatus(AcctStatus acctStatus) {
        this.acctStatus = acctStatus;
        return this;
    }

    @JsonProperty("acctId")
    public String getAcctId() {
        return acctId;
    }

    @JsonProperty("acctId")
    public void setAcctId(String acctId) {
        this.acctId = acctId;
    }

    public AcctBalRs withAcctId(String acctId) {
        this.acctId = acctId;
        return this;
    }

    @Override
    public String toString() {
        return ToStringBuilder.reflectionToString(this);
    }

    @JsonAnyGetter
    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    @JsonAnySetter
    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }

    public AcctBalRs withAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
        return this;
    }

    @Override
    public int hashCode() {
        return new HashCodeBuilder().append(acctInfo).append(acctStatus).append(acctId).append(additionalProperties).toHashCode();
    }

    @Override
    public boolean equals(Object other) {
        if (other == this) {
            return true;
        }
        if ((other instanceof AcctBalRs) == false) {
            return false;
        }
        AcctBalRs rhs = ((AcctBalRs) other);
        return new EqualsBuilder().append(acctInfo, rhs.acctInfo).append(acctStatus, rhs.acctStatus).append(acctId, rhs.acctId).append(additionalProperties, rhs.additionalProperties).isEquals();
    }

}
