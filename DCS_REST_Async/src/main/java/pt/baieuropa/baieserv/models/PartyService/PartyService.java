/*
  Created by: loliveira
*/

package pt.baieuropa.baieserv.models.PartyService;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonPropertyOrder({
        "partySvcRelInfo",
        "svcRef",
        "discRef"
})
public class PartyService {

    @JsonProperty("partySvcRelInfo")
    private partySvcRelInfo partySvcRelInfo;
    @JsonProperty("svcRef")
    private svcRef svcRef;
    @JsonProperty("discRef")
    private discRef discRef;

    public PartyService() {
    }

    public PartyService(PartyService.partySvcRelInfo partySvcRelInfo, PartyService.svcRef svcRef, PartyService.discRef discRef) {
        this.partySvcRelInfo = partySvcRelInfo;
        this.svcRef = svcRef;
        this.discRef = discRef;
    }

    @JsonProperty("partySvcRelInfo")
    public PartyService.partySvcRelInfo getPartySvcRelInfo() {
        return partySvcRelInfo;
    }

    public void setPartySvcRelInfo(PartyService.partySvcRelInfo partySvcRelInfo) {
        this.partySvcRelInfo = partySvcRelInfo;
    }

    @JsonProperty("svcRef")
    public PartyService.svcRef getSvcRef() {
        return svcRef;
    }

    public void setSvcRef(PartyService.svcRef svcRef) {
        this.svcRef = svcRef;
    }

    @JsonProperty("discRef")
    public PartyService.discRef getDiscRef() {
        return discRef;
    }

    public void setDiscRef(PartyService.discRef discRef) {
        this.discRef = discRef;
    }

    public static class partySvcRelInfo{
        @JsonProperty("partyref")
        partyRef partyref;

        public partySvcRelInfo(partyRef partyRef) {
            this.partyref = partyRef;
        }
    }

    public static class svcRef{
        private svcInfo svcInfo;

        public svcRef(PartyService.svcInfo svcInfo) {
            this.svcInfo = svcInfo;
        }

        public PartyService.svcInfo getSvcInfo() {
            return svcInfo;
        }

        public void setSvcInfo(PartyService.svcInfo svcInfo) {
            this.svcInfo = svcInfo;
        }
    }

    public static class svcInfo{
        private String svcName;

        public svcInfo(String svcName) {
            this.svcName = svcName;
        }

        public String getSvcName() {
            return svcName;
        }

        public void setSvcName(String svcName) {
            this.svcName = svcName;
        }
    }

    public static class discRef{
        private discInfo discInfo;

        public discRef(PartyService.discInfo discInfo) {
            this.discInfo = discInfo;
        }

        public PartyService.discInfo getDiscInfo() {
            return discInfo;
        }

        public void setDiscInfo(PartyService.discInfo discInfo) {
            this.discInfo = discInfo;
        }
    }

    public static class discInfo{
        private String discName;
        private String version;

        public discInfo(String discName, String version) {
            this.discName = discName;
            this.version = version;
        }

        public String getDiscName() {
            return discName;
        }

        public void setDiscName(String discName) {
            this.discName = discName;
        }

        public String getVersion() {
            return version;
        }

        public void setVersion(String version) {
            this.version = version;
        }
    }


}
