
package pt.baieuropa.baieserv.models.PartyAcctRelRec;

import com.fasterxml.jackson.annotation.*;
import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.HashCodeBuilder;
import org.apache.commons.lang.builder.ToStringBuilder;

import java.util.HashMap;
import java.util.Map;

@JsonPropertyOrder({
    "partyKeys"
})
public class PartyRef {

    @JsonProperty("partyKeys")
    private PartyKeys partyKeys;
    @JsonIgnore
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();

    public PartyRef(PartyKeys partyKeys) {
    }

    @JsonProperty("partyKeys")
    public PartyKeys getPartyKeys() {
        return partyKeys;
    }

    @JsonProperty("partyKeys")
    public void setPartyKeys(PartyKeys partyKeys) {
        this.partyKeys = partyKeys;
    }

    public PartyRef withPartyKeys(PartyKeys partyKeys) {
        this.partyKeys = partyKeys;
        return this;
    }

    @Override
    public String toString() {
        return ToStringBuilder.reflectionToString(this);
    }

    @JsonAnyGetter
    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    @JsonAnySetter
    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }

    public PartyRef withAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
        return this;
    }

    @Override
    public int hashCode() {
        return new HashCodeBuilder().append(partyKeys).append(additionalProperties).toHashCode();
    }

    @Override
    public boolean equals(Object other) {
        if (other == this) {
            return true;
        }
        if ((other instanceof PartyRef) == false) {
            return false;
        }
        PartyRef rhs = ((PartyRef) other);
        return new EqualsBuilder().append(partyKeys, rhs.partyKeys).append(additionalProperties, rhs.additionalProperties).isEquals();
    }

}
