
package pt.baieuropa.baieserv.models.PartyAcctRelRec;

import com.fasterxml.jackson.annotation.*;
import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.HashCodeBuilder;
import org.apache.commons.lang.builder.ToStringBuilder;

import java.util.HashMap;
import java.util.Map;

@JsonPropertyOrder({
    "acctId"
})
public class AcctKeys {

    @JsonProperty("acctId")
    private String acctId;
    @JsonIgnore
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();

    @JsonProperty("acctId")
    public String getAcctId() {
        return acctId;
    }

    @JsonProperty("acctId")
    public void setAcctId(String acctId) {
        this.acctId = acctId;
    }

    public AcctKeys withAcctId(String acctId) {
        this.acctId = acctId;
        return this;
    }

    @Override
    public String toString() {
        return ToStringBuilder.reflectionToString(this);
    }

    @JsonAnyGetter
    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    @JsonAnySetter
    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }

    public AcctKeys withAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
        return this;
    }

    @Override
    public int hashCode() {
        return new HashCodeBuilder().append(acctId).append(additionalProperties).toHashCode();
    }

    @Override
    public boolean equals(Object other) {
        if (other == this) {
            return true;
        }
        if ((other instanceof AcctKeys) == false) {
            return false;
        }
        AcctKeys rhs = ((AcctKeys) other);
        return new EqualsBuilder().append(acctId, rhs.acctId).append(additionalProperties, rhs.additionalProperties).isEquals();
    }

}
