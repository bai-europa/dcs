
package pt.baieuropa.baieserv.models.PartyAcctRelRec;

import com.fasterxml.jackson.annotation.*;
import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.HashCodeBuilder;
import org.apache.commons.lang.builder.ToStringBuilder;

import java.util.HashMap;
import java.util.Map;

@JsonPropertyOrder({
    "acctKeys"
})
public class AcctRef {

    @JsonProperty("acctKeys")
    private AcctKeys acctKeys;
    @JsonIgnore
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();

    @JsonProperty("acctKeys")
    public AcctKeys getAcctKeys() {
        return acctKeys;
    }

    @JsonProperty("acctKeys")
    public void setAcctKeys(AcctKeys acctKeys) {
        this.acctKeys = acctKeys;
    }

    public AcctRef withAcctKeys(AcctKeys acctKeys) {
        this.acctKeys = acctKeys;
        return this;
    }

    @Override
    public String toString() {
        return ToStringBuilder.reflectionToString(this);
    }

    @JsonAnyGetter
    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    @JsonAnySetter
    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }

    public AcctRef withAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
        return this;
    }

    @Override
    public int hashCode() {
        return new HashCodeBuilder().append(acctKeys).append(additionalProperties).toHashCode();
    }

    @Override
    public boolean equals(Object other) {
        if (other == this) {
            return true;
        }
        if ((other instanceof AcctRef) == false) {
            return false;
        }
        AcctRef rhs = ((AcctRef) other);
        return new EqualsBuilder().append(acctKeys, rhs.acctKeys).append(additionalProperties, rhs.additionalProperties).isEquals();
    }

}
