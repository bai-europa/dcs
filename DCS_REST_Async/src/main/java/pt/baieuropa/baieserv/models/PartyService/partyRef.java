/*
  Created by: loliveira
*/

package pt.baieuropa.baieserv.models.PartyService;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonPropertyOrder({
        "partyKeys"
})
public class partyRef{

    @JsonProperty("partyKeys")
    private partyKeys partyKeys;

    public partyRef() {
    }

    public partyRef(partyKeys partyKeys) {
        this.partyKeys = partyKeys;
    }

    @JsonProperty("partyKeys")
    public partyKeys getPartyKeys() {
        return partyKeys;
    }

    @JsonProperty("partyKeys")
    public void setPartyKeys(partyKeys partyKeys) {
        this.partyKeys = partyKeys;
    }
}
