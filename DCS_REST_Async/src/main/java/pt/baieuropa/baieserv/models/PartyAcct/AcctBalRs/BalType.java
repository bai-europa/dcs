
package pt.baieuropa.baieserv.models.PartyAcct.AcctBalRs;

import com.fasterxml.jackson.annotation.*;
import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.HashCodeBuilder;
import org.apache.commons.lang.builder.ToStringBuilder;

import java.util.HashMap;
import java.util.Map;

@JsonPropertyOrder({
    "balTypeSource",
    "balTypeValues"
})
public class BalType {

    @JsonProperty("balTypeSource")
    private String balTypeSource;
    @JsonProperty("balTypeValues")
    private String balTypeValues;
    @JsonIgnore
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();

    public BalType() {
    }

    public BalType(String balTypeSource, String balTypeValues, Map<String, Object> additionalProperties) {
        this.balTypeSource = balTypeSource;
        this.balTypeValues = balTypeValues;
        this.additionalProperties = additionalProperties;
    }

    @JsonProperty("balTypeSource")
    public String getBalTypeSource() {
        return balTypeSource;
    }

    @JsonProperty("balTypeSource")
    public void setBalTypeSource(String balTypeSource) {
        this.balTypeSource = balTypeSource;
    }

    public BalType withBalTypeSource(String balTypeSource) {
        this.balTypeSource = balTypeSource;
        return this;
    }

    @JsonProperty("balTypeValues")
    public String getBalTypeValues() {
        return balTypeValues;
    }

    @JsonProperty("balTypeValues")
    public void setBalTypeValues(String balTypeValues) {
        this.balTypeValues = balTypeValues;
    }

    public BalType withBalTypeValues(String balTypeValues) {
        this.balTypeValues = balTypeValues;
        return this;
    }

    @Override
    public String toString() {
        return ToStringBuilder.reflectionToString(this);
    }

    @JsonAnyGetter
    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    @JsonAnySetter
    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }

    public BalType withAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
        return this;
    }

    @Override
    public int hashCode() {
        return new HashCodeBuilder().append(balTypeSource).append(balTypeValues).append(additionalProperties).toHashCode();
    }

    @Override
    public boolean equals(Object other) {
        if (other == this) {
            return true;
        }
        if ((other instanceof BalType) == false) {
            return false;
        }
        BalType rhs = ((BalType) other);
        return new EqualsBuilder().append(balTypeSource, rhs.balTypeSource).append(balTypeValues, rhs.balTypeValues).append(additionalProperties, rhs.additionalProperties).isEquals();
    }

}
