/*
  Created by: loliveira
*/

package pt.baieuropa.baieserv.models.OrgPartyInfo;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.HashCodeBuilder;
import org.apache.commons.lang.builder.ToStringBuilder;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

@JsonPropertyOrder({
        "IssuedIdentType",
        "IssuedIdentValue",
        "Issuer",
        "IssueDt",
        "ExpDt",
        "Desc",
        "IdentImg",
        "IdentReadMethod",
        "IdentVerifyMethod",
        "IdentVerifyResults",
        "IssuedLoc",
        "GovIssuedIdent",
        "UpDt"
})
public class IssuedIdent {
    @JsonProperty("IssuedIdentType")
    private String IssuedIdentType;
    @JsonProperty("IssuedIdentValue")
    private String IssuedIdentValue;
    @JsonProperty("Issuer")
    private String Issuer;
    @JsonProperty("IssueDt")
    private String IssueDt;
    @JsonProperty("ExpDt")
    private String ExpDt;
    @JsonProperty("Desc")
    private String Desc;
    @JsonProperty("IdentImg")
    private String IdentImg;
    @JsonProperty("IdentReadMethod")
    private String IdentReadMethod;
    @JsonProperty("IdentVerifyMethod")
    private String IdentVerifyMethod;
    @JsonProperty("IdentVerifyResults")
    private String IdentVerifyResults;
    @JsonProperty("IssuedLoc")
    private String IssuedLoc;
    @JsonProperty("GovIssuedIdent")
    private String GovIssuedIdent;
    @JsonProperty("UpDt")
    private String UpDt;

    @JsonProperty("IssuedIdentType")
    public String getIssuedIdentType() {
        return IssuedIdentType;
    }

    public void setIssuedIdentType(String issuedIdentType) {
        IssuedIdentType = issuedIdentType;
    }
    @JsonProperty("IssuedIdentValue")
    public String getIssuedIdentValue() {
        return IssuedIdentValue;
    }

    public void setIssuedIdentValue(String issuedIdentValue) {
        IssuedIdentValue = issuedIdentValue;
    }
    @JsonProperty("Issuer")
    public String getIssuer() {
        return Issuer;
    }

    public void setIssuer(String issuer) {
        Issuer = issuer;
    }
    @JsonProperty("IssueDt")
    public String getIssueDt() throws ParseException {
        if(IssueDt != null && IssueDt.equals("0")) {
            IssueDt = null;
        }

        if(IssueDt != null) {
            Date date = new SimpleDateFormat("yyyyMMdd").parse(IssueDt);
            SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd");
            IssueDt = simpleDateFormat.format(date);
        }
        return IssueDt;
    }

    public void setIssueDt(String issueDt) {
        IssueDt = issueDt;
    }
    @JsonProperty("ExpDt")
    public String getExpDt() throws ParseException {
        if(ExpDt != null && ExpDt.equals("0")) {
            ExpDt = null;
        }

        if(ExpDt != null) {
            Date date = new SimpleDateFormat("yyyyMMdd").parse(ExpDt);
            SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd");
            ExpDt = simpleDateFormat.format(date);
        }

        return ExpDt;
    }

    public void setExpDt(String expDt) {
        ExpDt = expDt;
    }
    @JsonProperty("Desc")
    public String getDesc() {
        return Desc;
    }

    public void setDesc(String desc) {
        Desc = desc;
    }
    @JsonProperty("IdentImg")
    public String getIdentImg() {
        return IdentImg;
    }

    public void setIdentImg(String identImg) {
        IdentImg = identImg;
    }
    @JsonProperty("IdentReadMethod")
    public String getIdentReadMethod() {
        return IdentReadMethod;
    }

    public void setIdentReadMethod(String identReadMethod) {
        IdentReadMethod = identReadMethod;
    }
    @JsonProperty("IdentVerifyMethod")
    public String getIdentVerifyMethod() {
        return IdentVerifyMethod;
    }

    public void setIdentVerifyMethod(String identVerifyMethod) {
        IdentVerifyMethod = identVerifyMethod;
    }
    @JsonProperty("IdentVerifyResults")
    public String getIdentVerifyResults() {
        return IdentVerifyResults;
    }

    public void setIdentVerifyResults(String identVerifyResults) {
        IdentVerifyResults = identVerifyResults;
    }
    @JsonProperty("IssuedLoc")
    public String getIssuedLoc() {
        return IssuedLoc;
    }

    public void setIssuedLoc(String issuedLoc) {
        IssuedLoc = issuedLoc;
    }
    @JsonProperty("GovIssuedIdent")
    public String getGovIssuedIdent() {
        return GovIssuedIdent;
    }

    public void setGovIssuedIdent(String govIssuedIdent) {
        GovIssuedIdent = govIssuedIdent;
    }
    @JsonProperty("UpDt")
    public String getUpDt() {
        return UpDt;
    }

    public void setUpDt(String upDt) {
        UpDt = upDt;
    }

    @Override
    public String toString() {
        return ToStringBuilder.reflectionToString(this);
    }





    @Override
    public int hashCode() {
        return new HashCodeBuilder().append(IssuedIdentType).append(IssuedIdentValue).append(Issuer).append(IssueDt).append(ExpDt).append(Desc).append(IdentImg).append(IdentReadMethod).append(IdentVerifyMethod).append(IdentVerifyResults).append(IssuedLoc).append(GovIssuedIdent).append(UpDt).toHashCode();
    }

    @Override
    public boolean equals(Object other) {
        if (other == this) {
            return true;
        }
        if ((other instanceof IssuedIdent) == false) {
            return false;
        }
        IssuedIdent rhs = ((IssuedIdent) other);
        return new EqualsBuilder().append(IssuedIdentType, rhs.IssuedIdentType).append(IssuedIdentValue, rhs.IssuedIdentValue).append(Issuer, rhs.Issuer).append(IssueDt, rhs.IssueDt)
                .append(ExpDt, rhs.ExpDt).append(Desc, rhs.Desc).append(IdentImg, rhs.IdentImg).append(IdentReadMethod, rhs.IdentReadMethod).append(IdentVerifyMethod, rhs.IdentVerifyMethod)
                .append(IdentVerifyResults, rhs.IdentVerifyResults).append(IssuedLoc, rhs.IssuedLoc).append(GovIssuedIdent, rhs.GovIssuedIdent).append(UpDt, rhs.UpDt).isEquals();
    }
}
