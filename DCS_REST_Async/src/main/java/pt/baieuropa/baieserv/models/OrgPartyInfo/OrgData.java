
package pt.baieuropa.baieserv.models.OrgPartyInfo;

import com.fasterxml.jackson.annotation.*;
import org.apache.commons.lang.builder.ToStringBuilder;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@JsonPropertyOrder({
    "orgName",
    "orgCategory",
    "IssuedIdents",
    "BeneficialOwnersList"
})
public class OrgData {
    @JsonProperty("orgName")
    private OrgName orgName;
    @JsonProperty("orgCategory")
    private OrgCategory orgCategory;
    @JsonProperty("contacts")
    private List<Contact> contacts = new ArrayList<Contact>();
    @JsonProperty("IssuedIdents")
    private List<IssuedIdent> IssuedIdents = new ArrayList<>();
    @JsonProperty("BeneficialOwnersList")
    private List<BeneficialOwners> beneficialOwners = new ArrayList<>();

    @JsonIgnore
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();

    @JsonProperty("orgName")
    public OrgName getOrgName() {
        return orgName;
    }

    @JsonProperty("orgName")
    public void setOrgName(OrgName orgName) {
        this.orgName = orgName;
    }

    public OrgData withOrgName(OrgName orgName) {
        this.orgName = orgName;
        return this;
    }

    @JsonProperty("orgCategory")
    public OrgCategory getOrgCategory() {
        return orgCategory;
    }

    @JsonProperty("orgCategory")
    public void setOrgCategory(OrgCategory orgCategory) {
        this.orgCategory = orgCategory;
    }

    public OrgData withOrgCategory(OrgCategory orgCategory) {
        this.orgCategory = orgCategory;
        return this;
    }

    @JsonProperty("contacts")
    public List<Contact> getContacts() {
        return contacts;
    }

    @JsonProperty("contacts")
    public void setContacts(List<Contact> contacts) {
        this.contacts = contacts;
    }

    public OrgData withContacts(List<Contact> contacts) {
        this.contacts = contacts;
        return this;
    }

    @JsonProperty("IssuedIdents")
    public List<IssuedIdent> getIssuedIdent() {
        return IssuedIdents;
    }

    public void setIssuedIdent(List<IssuedIdent> issuedIdents) {
        IssuedIdents = issuedIdents;
    }

    public void setAdditionalProperties(Map<String, Object> additionalProperties) {
        this.additionalProperties = additionalProperties;
    }

    @JsonProperty("BeneficialOwnersList")
    public List<BeneficialOwners> getBeneficialOwners() {
        return beneficialOwners;
    }

    public void setBeneficialOwners(List<BeneficialOwners> beneficialOwners) {
        this.beneficialOwners = beneficialOwners;
    }



    @Override
    public String toString() {
        return ToStringBuilder.reflectionToString(this);
    }

    @JsonAnyGetter
    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    @JsonAnySetter
    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }

    public OrgData withAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
        return this;
    }
}
