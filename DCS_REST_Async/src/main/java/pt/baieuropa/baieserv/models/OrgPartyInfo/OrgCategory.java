package pt.baieuropa.baieserv.models.OrgPartyInfo;

import com.fasterxml.jackson.annotation.*;

import java.util.HashMap;
import java.util.Map;

@JsonPropertyOrder({
        "orgCategoryCode",
        "desc"
})
public class OrgCategory {
    @JsonProperty("orgCategoryCode")
    private OrgCategoryCode orgCategoryCode;
    @JsonProperty("desc")
    private String desc;
    @JsonIgnore
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();

    @JsonProperty("orgCategoryCode")
    public OrgCategoryCode getOrgCategoryCode() {
        return orgCategoryCode;
    }

    @JsonProperty("orgCategoryCode")
    public void setOrgCategoryCode(OrgCategoryCode orgCategoryCode) {
        this.orgCategoryCode = orgCategoryCode;
    }

    @JsonProperty("desc")
    public String getDesc() {
        return desc;
    }

    @JsonProperty("desc")
    public void setDesc(String desc) {
        this.desc = desc;
    }

    @JsonAnySetter
    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }
}
