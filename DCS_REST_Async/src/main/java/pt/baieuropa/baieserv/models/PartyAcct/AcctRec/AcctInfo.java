
package pt.baieuropa.baieserv.models.PartyAcct.AcctRec;

import com.fasterxml.jackson.annotation.*;
import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.HashCodeBuilder;
import org.apache.commons.lang.builder.ToStringBuilder;
import pt.baieuropa.baieserv.models.PartyAcct.AcctBalRs.AcctBal;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@JsonPropertyOrder({
    "acctUse",
    "IBAN",
    "postAddr",
    "acctIdent",
    "acctBal",
    "ownership",
    "FIData",
    "acctType",
    "openDt",
    "productIdent",
    "relationshipMgr",
    "curCode",
    "desc",
    "closedDt"
})
public class AcctInfo {

    @JsonProperty("acctUse")
    private String acctUse;
    @JsonProperty("IBAN")
    private String iBAN;
    @JsonProperty("postAddr")
    private String postAddr;
    @JsonProperty("acctIdent")
    private AcctIdent acctIdent;
    @JsonProperty("acctBal")
    private List<AcctBal> acctBal = new ArrayList<>();
    @JsonProperty("ownership")
    private String ownership;
    @JsonProperty("FIData")
    private FIData fIData;
    @JsonProperty("acctType")
    private AcctType acctType;
    @JsonProperty("openDt")
    private String openDt;
    @JsonProperty("productIdent")
    private String productIdent;
    @JsonProperty("relationshipMgr")
    private RelationshipMgr relationshipMgr;
    @JsonProperty("curCode")
    private CurCode curCode;
    @JsonProperty("desc")
    private String desc;
    @JsonProperty("closedDt")
    private String closedDt;
    @JsonIgnore
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();

    public AcctInfo() {
    }

    public AcctInfo(String acctUse, String iBAN, String postAddr, AcctIdent acctIdent, List<AcctBal> acctBal, String ownership, FIData fIData, AcctType acctType, String openDt, String productIdent, RelationshipMgr relationshipMgr, CurCode curCode, String desc, String closedDt, Map<String, Object> additionalProperties) {
        this.acctUse = acctUse;
        this.iBAN = iBAN;
        this.postAddr = postAddr;
        this.acctIdent = acctIdent;
        this.acctBal = acctBal;
        this.ownership = ownership;
        this.fIData = fIData;
        this.acctType = acctType;
        this.openDt = openDt;
        this.productIdent = productIdent;
        this.relationshipMgr = relationshipMgr;
        this.curCode = curCode;
        this.desc = desc;
        this.closedDt = closedDt;
        this.additionalProperties = additionalProperties;
    }

    @JsonProperty("acctUse")
    public String getAcctUse() {
        return acctUse;
    }

    @JsonProperty("acctUse")
    public void setAcctUse(String acctUse) {
        this.acctUse = acctUse;
    }

    public AcctInfo withAcctUse(String acctUse) {
        this.acctUse = acctUse;
        return this;
    }

    @JsonProperty("IBAN")
    public String getIBAN() {
        return iBAN;
    }

    @JsonProperty("IBAN")
    public void setIBAN(String iBAN) {
        this.iBAN = iBAN;
    }

    public AcctInfo withIBAN(String iBAN) {
        this.iBAN = iBAN;
        return this;
    }

    @JsonProperty("postAddr")
    public String getPostAddr() {
        return postAddr;
    }

    @JsonProperty("postAddr")
    public void setPostAddr(String postAddr) {
        this.postAddr = postAddr;
    }

    public AcctInfo withPostAddr(String postAddr) {
        this.postAddr = postAddr;
        return this;
    }

    @JsonProperty("acctIdent")
    public AcctIdent getAcctIdent() {
        return acctIdent;
    }

    @JsonProperty("acctIdent")
    public void setAcctIdent(AcctIdent acctIdent) {
        this.acctIdent = acctIdent;
    }

    public AcctInfo withAcctIdent(AcctIdent acctIdent) {
        this.acctIdent = acctIdent;
        return this;
    }

    @JsonProperty("acctBal")
    public List<AcctBal> getAcctBal() {
        return acctBal;
    }

    @JsonProperty("acctBal")
    public void setAcctBal(List<AcctBal> acctBal) {
        this.acctBal = acctBal;
    }


    public AcctInfo withAcctBal(List<AcctBal> acctBal) {
        this.acctBal = acctBal;
        return this;
    }

    @JsonProperty("ownership")
    public String getOwnership() {
        return ownership;
    }

    @JsonProperty("ownership")
    public void setOwnership(String ownership) {
        this.ownership = ownership;
    }

    public AcctInfo withOwnership(String ownership) {
        this.ownership = ownership;
        return this;
    }

    @JsonProperty("FIData")
    public FIData getFIData() {
        return fIData;
    }

    @JsonProperty("FIData")
    public void setFIData(FIData fIData) {
        this.fIData = fIData;
    }

    public AcctInfo withFIData(FIData fIData) {
        this.fIData = fIData;
        return this;
    }

    @JsonProperty("acctType")
    public AcctType getAcctType() {
        return acctType;
    }

    @JsonProperty("acctType")
    public void setAcctType(AcctType acctType) {
        this.acctType = acctType;
    }

    public AcctInfo withAcctType(AcctType acctType) {
        this.acctType = acctType;
        return this;
    }

    @JsonProperty("openDt")
    public String getOpenDt() {
        return openDt;
    }

    @JsonProperty("openDt")
    public void setOpenDt(String openDt) {
        this.openDt = openDt;
    }

    public AcctInfo withOpenDt(String openDt) {
        this.openDt = openDt;
        return this;
    }

    @JsonProperty("productIdent")
    public String getProductIdent() {
        return productIdent;
    }

    @JsonProperty("productIdent")
    public void setProductIdent(String productIdent) {
        this.productIdent = productIdent;
    }

    public AcctInfo withProductIdent(String productIdent) {
        this.productIdent = productIdent;
        return this;
    }

    @JsonProperty("relationshipMgr")
    public RelationshipMgr getRelationshipMgr() {
        return relationshipMgr;
    }

    @JsonProperty("relationshipMgr")
    public void setRelationshipMgr(RelationshipMgr relationshipMgr) {
        this.relationshipMgr = relationshipMgr;
    }

    public AcctInfo withRelationshipMgr(RelationshipMgr relationshipMgr) {
        this.relationshipMgr = relationshipMgr;
        return this;
    }

    @JsonProperty("curCode")
    public CurCode getCurCode() {
        return curCode;
    }

    @JsonProperty("curCode")
    public void setCurCode(CurCode curCode) {
        this.curCode = curCode;
    }

    public AcctInfo withCurCode(CurCode curCode) {
        this.curCode = curCode;
        return this;
    }

    @JsonProperty("desc")
    public String getDesc() {
        return desc;
    }

    @JsonProperty("desc")
    public void setDesc(String desc) {
        this.desc = desc;
    }

    public AcctInfo withDesc(String desc) {
        this.desc = desc;
        return this;
    }

    @JsonProperty("closedDt")
    public String getClosedDt() {
        return closedDt;
    }

    @JsonProperty("closedDt")
    public void setClosedDt(String closedDt) {
        this.closedDt = closedDt;
    }

    public AcctInfo withClosedDt(String closedDt) {
        this.closedDt = closedDt;
        return this;
    }

    @Override
    public String toString() {
        return ToStringBuilder.reflectionToString(this);
    }

    @JsonAnyGetter
    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    @JsonAnySetter
    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }

    public AcctInfo withAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
        return this;
    }

    @Override
    public int hashCode() {
        return new HashCodeBuilder().append(acctUse).append(iBAN).append(postAddr).append(acctIdent).append(acctBal).append(ownership).append(fIData).append(acctType).append(openDt).append(productIdent).append(relationshipMgr).append(curCode).append(desc).append(closedDt).append(additionalProperties).toHashCode();
    }

    @Override
    public boolean equals(Object other) {
        if (other == this) {
            return true;
        }
        if ((other instanceof AcctInfo) == false) {
            return false;
        }
        AcctInfo rhs = ((AcctInfo) other);
        return new EqualsBuilder().append(acctUse, rhs.acctUse).append(iBAN, rhs.iBAN).append(postAddr, rhs.postAddr).append(acctIdent, rhs.acctIdent).append(acctBal, rhs.acctBal).append(ownership, rhs.ownership).append(fIData, rhs.fIData).append(acctType, rhs.acctType).append(openDt, rhs.openDt).append(productIdent, rhs.productIdent).append(relationshipMgr, rhs.relationshipMgr).append(curCode, rhs.curCode).append(desc, rhs.desc).append(closedDt, rhs.closedDt).append(additionalProperties, rhs.additionalProperties).isEquals();
    }

}
