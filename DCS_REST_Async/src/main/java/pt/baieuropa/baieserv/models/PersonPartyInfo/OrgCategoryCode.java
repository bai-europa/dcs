package pt.baieuropa.baieserv.models.PersonPartyInfo;


import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.util.HashMap;
import java.util.Map;

@JsonPropertyOrder({
        "orgCatIdent"
})
public class OrgCategoryCode {
    @JsonProperty("orgCatIdent")
    private OrgCatIdent orgCatIdent;

    @JsonIgnore
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();

    @JsonProperty("orgCatIdent")
    public OrgCatIdent getOrgCatIdent() {
        return orgCatIdent;
    }

    @JsonProperty("orgCatIdent")
    public void setOrgCatIdent(OrgCatIdent orgCatIdent) {
        this.orgCatIdent = orgCatIdent;
    }

    public Map<String, Object> getAdditionalProperties() {
        return additionalProperties;
    }

    public void setAdditionalProperties(Map<String, Object> additionalProperties) {
        this.additionalProperties = additionalProperties;
    }
}
