/*
  Created by: loliveira
*/

package pt.baieuropa.baieserv.models.PersonPartyInfo;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.HashCodeBuilder;
import org.apache.commons.lang.builder.ToStringBuilder;

@JsonPropertyOrder({
        "qualification",
        "qualificationId"
})
public class EducationalQualifications {

    @JsonProperty("qualification")
    private String qualification;
    @JsonProperty("qualificationId")
    private String qualificationId;


    @JsonProperty("qualification")
    public String getQualification() {
        return qualification;
    }

    public void setQualification(String qualification) {
        this.qualification = qualification;
    }

    @JsonProperty("qualificationId")
    public String getQualificationId() {
        return qualificationId;
    }

    public void setQualificationId(String qualificationId) {
        this.qualificationId = qualificationId;
    }


    @Override
    public String toString() {
        return ToStringBuilder.reflectionToString(this);
    }

    @Override
    public int hashCode() {
        return new HashCodeBuilder().append(qualification).append(qualificationId).toHashCode();
    }

    @Override
    public boolean equals(Object other) {
        if (other == this) {
            return true;
        }
        if ((other instanceof Parents) == false) {
            return false;
        }
        EducationalQualifications rhs = ((EducationalQualifications) other);
        return new EqualsBuilder().append(qualification, rhs.qualification).append(qualificationId, rhs.qualificationId).isEquals();
    }
}
