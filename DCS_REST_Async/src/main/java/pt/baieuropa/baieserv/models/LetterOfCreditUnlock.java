package pt.baieuropa.baieserv.models;

import com.fasterxml.jackson.annotation.JsonProperty;

import javax.validation.constraints.NotNull;

public class LetterOfCreditUnlock {

    @NotNull(message = "The property [masterReference] can't be NULL.")
    protected String masterReference;

    @NotNull(message = "The property [event] can't be NULL.")
    protected String event;

    public LetterOfCreditUnlock() {
    }

    public LetterOfCreditUnlock(@NotNull(message = "The property [masterReference] can't be NULL.") String masterReference, @NotNull(message = "The property [event] can't be NULL.") String event) {
        this.masterReference = masterReference;
        this.event = event;
    }

    public String getMasterReference() {
        return masterReference;
    }

    public void setMasterReference(String masterReference) {
        this.masterReference = masterReference;
    }

    public String getEvent() {
        return event;
    }

    public void setEvent(String event) {
        this.event = event;
    }
}
