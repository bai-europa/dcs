package pt.baieuropa.baieserv.models.Consults;

import com.fasterxml.jackson.annotation.JsonProperty;

public class ConsultBlock {
    @JsonProperty("EntityNumber")
    public String entityNumber;
    @JsonProperty("AccountNumber")
    public String accountNumber;
    @JsonProperty("BlockType")
    public String blockType;
    @JsonProperty("BlockDate")
    public String blockDate;
    @JsonProperty("User")
    public String user;
    @JsonProperty("Description")
    public String description;

    public ConsultBlock() {
    }

    public ConsultBlock(String entityNumber, String accountNumber, String blockType, String blockDate, String user, String description) {
        this.entityNumber = entityNumber;
        this.accountNumber = accountNumber;
        this.blockType = blockType;
        this.blockDate = blockDate;
        this.user = user;
        this.description = description;
    }

    public String getEntityNumber() {
        return entityNumber;
    }

    public void setEntityNumber(String entityNumber) {
        this.entityNumber = entityNumber;
    }

    public String getAccountNumber() {
        return accountNumber;
    }

    public void setAccountNumber(String accountNumber) {
        this.accountNumber = accountNumber;
    }

    public String getBlockType() {
        return blockType;
    }

    public void setBlockType(String blockType) {
        this.blockType = blockType;
    }

    public String getBlockDate() {
        return blockDate;
    }

    public void setBlockDate(String blockDate) {
        this.blockDate = blockDate;
    }

    public String getUser() {
        return user;
    }

    public void setUser(String user) {
        this.user = user;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }
}
