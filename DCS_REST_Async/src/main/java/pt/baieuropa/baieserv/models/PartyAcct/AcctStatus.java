package pt.baieuropa.baieserv.models.PartyAcct;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonPropertyOrder({
        "acctStatusCode"
})
public class AcctStatus {

    @JsonProperty("acctStatusCode")
    private String acctStatusCode;

    public AcctStatus() {
    }

    public AcctStatus(String acctStatusCode) {
        this.acctStatusCode = acctStatusCode;
    }

    @JsonProperty("acctStatusCode")
    public String getAcctStatusCode() {
        return acctStatusCode;
    }

    @JsonProperty("acctStatusCode")
    public void setAcctStatusCode(String acctStatusCode) {
        this.acctStatusCode = acctStatusCode;
    }
}
