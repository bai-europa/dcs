package pt.baieuropa.baieserv.models.OrgPartyInfo;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.HashCodeBuilder;
import org.apache.commons.lang.builder.ToStringBuilder;

import java.math.BigDecimal;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

@JsonPropertyOrder({
        "orgData",
        "residenceCountry",
        "OrgEstablishDt",
        "TaxExempt",
        "partyPref",
        "taxResidenceCountry",
        "relationshipMgr",
        "LegalForm",
        "OrgIdentification",
        "SocietyType",
        "SocialCapital"
})
public class OrgPartyInfo {

    @JsonProperty("residenceCountry")
    private ResidenceCountry residenceCountry;
    @JsonProperty("OrgEstablishDt")
    private String orgEstablishDt;
    @JsonProperty("TaxExempt")
    private String taxExempt;
    @JsonProperty("partyPref")
    private PartyPref partyPref;
    @JsonProperty("taxResidenceCountry")
    private TaxResidenceCountry taxResidenceCountry;
    @JsonProperty("orgData")
    private OrgData orgData;
    @JsonProperty("relationshipMgr")
    private RelationshipMgr relationshipMgr;
    @JsonProperty("LegalForm")
    private String legalForm;
    @JsonProperty("SocietyType")
    private String societyType;
    @JsonProperty("SocialCapital")
    private BigDecimal socialCapital;

    @JsonProperty("residenceCountry")
    public ResidenceCountry getResidenceCountry() {
        return residenceCountry;
    }

    @JsonProperty("residenceCountry")
    public void setResidenceCountry(ResidenceCountry residenceCountry) {
        this.residenceCountry = residenceCountry;
    }

    public OrgPartyInfo withResidenceCountry(ResidenceCountry residenceCountry) {
        this.residenceCountry = residenceCountry;
        return this;
    }

    @JsonProperty("OrgEstablishDt")
    public String getOrgEstablishDt() throws ParseException {
        if(orgEstablishDt != null && orgEstablishDt.equals("0")) {
            orgEstablishDt = null;
        }

        if(orgEstablishDt != null) {
            Date date = new SimpleDateFormat("yyyyMMdd").parse(orgEstablishDt);
            SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd");
            orgEstablishDt = simpleDateFormat.format(date);
        }

        return orgEstablishDt;
    }

    @JsonProperty("OrgEstablishDt")
    public void setOrgEstablishDt(String orgEstablishDt) {
        this.orgEstablishDt = orgEstablishDt;
    }

    public OrgPartyInfo withOrgEstablishDt(String orgEstablishDt) {
        this.orgEstablishDt = orgEstablishDt;
        return this;
    }

    @JsonProperty("TaxExempt")
    public String getTaxExempt() {
        return taxExempt;
    }

    @JsonProperty("TaxExempt")
    public void setTaxExempt(String taxExempt) {
        this.taxExempt = taxExempt;
    }

    public OrgPartyInfo withTaxExempt(String taxExempt) {
        this.taxExempt = taxExempt;
        return this;
    }

    @JsonProperty("partyPref")
    public PartyPref getPartyPref() {
        return partyPref;
    }

    @JsonProperty("partyPref")
    public void setPartyPref(PartyPref partyPref) {
        this.partyPref = partyPref;
    }

    public OrgPartyInfo withPartyPref(PartyPref partyPref) {
        this.partyPref = partyPref;
        return this;
    }

    @JsonProperty("taxResidenceCountry")
    public TaxResidenceCountry getTaxResidenceCountry() {
        return taxResidenceCountry;
    }

    @JsonProperty("taxResidenceCountry")
    public void setTaxResidenceCountry(TaxResidenceCountry taxResidenceCountry) {
        this.taxResidenceCountry = taxResidenceCountry;
    }

    public OrgPartyInfo withTaxResidenceCountry(TaxResidenceCountry taxResidenceCountry) {
        this.taxResidenceCountry = taxResidenceCountry;
        return this;
    }

    @JsonProperty("orgData")
    public OrgData getOrgData() {
        return orgData;
    }

    @JsonProperty("orgData")
    public void setOrgData(OrgData orgData) {
        this.orgData = orgData;
    }

    public OrgPartyInfo withOrgData(OrgData orgData) {
        this.orgData = orgData;
        return this;
    }

    @JsonProperty("relationshipMgr")
    public RelationshipMgr getRelationshipMgr() {
        return relationshipMgr;
    }

    @JsonProperty("relationshipMgr")
    public void setRelationshipMgr(RelationshipMgr relationshipMgr) {
        this.relationshipMgr = relationshipMgr;
    }

    public OrgPartyInfo withRelationshipMgr(RelationshipMgr relationshipMgr) {
        this.relationshipMgr = relationshipMgr;
        return this;
    }

    @JsonProperty("LegalForm")
    public String getLegalForm() {
        return legalForm;
    }

    @JsonProperty("LegalForm")
    public void setLegalForm(String legalForm) {
        this.legalForm = legalForm;
    }

    public OrgPartyInfo withLegalForm(String legalForm) {
        this.legalForm = legalForm;
        return this;
    }

    @JsonProperty("SocietyType")
    public String getSocietyType() {
        return societyType;
    }

    @JsonProperty("SocietyType")
    public void setSocietyType(String societyType) {
        this.societyType = societyType;
    }

    @JsonProperty("SocialCapital")
    public BigDecimal getSocialCapital() {
        return socialCapital;
    }

    @JsonProperty("SocialCapital")
    public void setSocialCapital(BigDecimal socialCapital) {
        this.socialCapital = socialCapital;
    }



    @Override
    public String toString() {
        return ToStringBuilder.reflectionToString(this);
    }


    @Override
    public int hashCode() {
        return new HashCodeBuilder().append(residenceCountry).append(orgEstablishDt).append(taxExempt).append(partyPref).append(taxResidenceCountry).append(orgData).append(relationshipMgr).append(legalForm).toHashCode();
    }

    @Override
    public boolean equals(Object other) {
        if (other == this) {
            return true;
        }
        if ((other instanceof OrgPartyInfo) == false) {
            return false;
        }
        OrgPartyInfo rhs = ((OrgPartyInfo) other);
        return new EqualsBuilder().append(residenceCountry, rhs.residenceCountry).append(orgEstablishDt, rhs.orgEstablishDt).append(taxExempt, rhs.taxExempt).append(partyPref, rhs.partyPref).append(taxResidenceCountry, rhs.taxResidenceCountry).append(orgData, rhs.orgData).append(relationshipMgr, rhs.relationshipMgr).append(legalForm, rhs.legalForm).isEquals();
    }

}
