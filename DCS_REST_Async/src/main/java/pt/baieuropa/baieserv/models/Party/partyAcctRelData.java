package pt.baieuropa.baieserv.models.Party;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonPropertyOrder({
        "partyAcctRelType"
})
public class partyAcctRelData{

    @JsonProperty("partyAcctRelType")
    private String partyAcctRelType;

    public partyAcctRelData() {}

    @JsonProperty("partyAcctRelType")
    public String getPartyAcctRelType() {
        return partyAcctRelType;
    }

    @JsonProperty("partyAcctRelType")
    public void setPartyAcctRelType(String partyAcctRelType) {
        this.partyAcctRelType = partyAcctRelType;
    }
}
