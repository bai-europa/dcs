package pt.baieuropa.baieserv.models.Party;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import pt.baieuropa.baieserv.models.PartyService.partyRef;

@JsonPropertyOrder({
        "partyRef",
        "partyAcctRelData",
})
public class partyAcctRelInfo {

    @JsonProperty("partyRef")
    private partyRef partyRef;

    @JsonProperty("partyAcctRelData")
    private partyAcctRelData partyAcctRelData;

    public partyAcctRelInfo() { }

    @JsonProperty("partyRef")
    public partyRef getPartyRef() {
        return partyRef;
    }

    @JsonProperty("partyRef")
    public void setPartyRef(partyRef partyRef) {
        this.partyRef = partyRef;
    }

    @JsonProperty("partyAcctRelData")
    public partyAcctRelData getPartyAcctRelData() {
        return partyAcctRelData;
    }

    @JsonProperty("partyAcctRelData")
    public void setPartyAcctRelData(partyAcctRelData partyAcctRelData) {
        this.partyAcctRelData = partyAcctRelData;
    }
}
