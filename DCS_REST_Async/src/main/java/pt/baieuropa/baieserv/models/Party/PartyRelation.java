/*
  Created by: loliveira
*/

package pt.baieuropa.baieserv.models.Party;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonPropertyOrder({
        "partyAcctRelId",
        "partyAcctRelInfo",
        "acctRef",
        "RequestUser",
        "RequestId"

})
public class PartyRelation {

    @JsonProperty("partyAcctRelId")
    private String partyAcctRelId;
    @JsonProperty("partyAcctRelInfo")
    private partyAcctRelInfo[] partyAcctRelInfo;
    @JsonProperty("acctRef")
    private acctRef acctRef;
    @JsonProperty("RequestUser")
    private String requestUser;
    @JsonProperty("RequestId")
    private String requestId;


    public PartyRelation() {
    }

    @JsonProperty("acctRef")
    public acctRef getAcctRef() {
        return acctRef;
    }

    @JsonProperty("acctRef")
    public void setAcctRef(acctRef acctRef) {
        this.acctRef = acctRef;
    }

    @JsonProperty("partyAcctRelInfo")
    public partyAcctRelInfo[] getPartyAcctRelInfo() {
        return partyAcctRelInfo;
    }

    @JsonProperty("partyAcctRelInfo")
    public void setPartyAcctRelInfo(partyAcctRelInfo[] partyAcctRelInfo) {
        this.partyAcctRelInfo = partyAcctRelInfo;
    }

    @JsonProperty("partyAcctRelId")
    public String getPartyAcctRelId() {
        return partyAcctRelId;
    }

    @JsonProperty("partyAcctRelId")
    public void setPartyAcctRelId(String partyAcctRelId) {
        this.partyAcctRelId = partyAcctRelId;
    }

    @JsonProperty("RequestUser")
    public String getRequestUser() {
        return requestUser;
    }

    @JsonProperty("RequestUser")
    public void setRequestUser(String requestUser) {
        this.requestUser = requestUser;
    }

    @JsonProperty("RequestId")
    public String getRequestId() {
        return requestId;
    }

    @JsonProperty("RequestId")
    public void setRequestId(String requestId) {
        this.requestId = requestId;
    }
}
