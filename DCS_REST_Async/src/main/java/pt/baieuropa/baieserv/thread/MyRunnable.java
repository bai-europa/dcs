//package pt.baieuropa.baieserv.thread;
//
//import org.apache.log4j.LogManager;
//import org.slf4j.Logger;
//import org.slf4j.LoggerFactory;
//import pt.baieuropa.baieserv.ServiceApp;
//import pt.baieuropa.baieserv.processor.Process;
//
//import static pt.baieuropa.baieserv.processor.NatsController.stanController;
//
//public class MyRunnable implements Runnable {
//    private int var;
//
//    private static final Logger log = LoggerFactory.getLogger(MyRunnable.class);
//
//    public MyRunnable(int var) {
//        super();
//        this.var = var;
//    }
//
//    private boolean doStop = false;
//
//    public synchronized void doStop() {
//        this.doStop = true;
//    }
//
//    private synchronized boolean keepRunning() {
//        return this.doStop == false;
//    }
//
//    public void run() {
//        if(stanController.getSc()==null){
//            try {
//                log.debug("Restarting NATS Connection...");
//                ServiceApp.proc = new Process("SERVICE");
//                log.debug("NATS Connected Successfully!");
//            } catch (Exception e) {
//                log.debug("Error while restarting NATS connection.\n" + e.getMessage());
//            }
//        } else {
//            log.warn("NOT DETECTING");
//        }
//    }
//}
