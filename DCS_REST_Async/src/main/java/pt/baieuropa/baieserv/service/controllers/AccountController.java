package pt.baieuropa.baieserv.service.controllers;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.ObjectWriter;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.tags.Tag;
import okhttp3.*;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.bind.annotation.RequestBody;
import pt.baieuropa.baieserv.ServiceApp;
import pt.baieuropa.baieserv.service.managers.APIConnectSecurityManager;
import pt.baieuropa.baieserv.utils.ConfigUtils;
import pt.baieuropa.storage.WSDLPojos.ActualizacaoNivelRiscoCliente.ActualizacaoNivelRiscoCliente;
import pt.baieuropa.storage.WSDLPojos.ActualizacaoNivelRiscoCliente.ObjectActualizacaoNivelRiscoClienteInput;
import pt.baieuropa.storage.WSDLPojos.Consultas.ConsultarMovimentosContasDO;
import pt.baieuropa.storage.WSDLPojos.Consultas.ObjectConsultaDeMovimentosContaDOOutput;
import pt.baieuropa.storage.WSDLPojos.CriarBloqueioConta.CriarBloqueioConta;
import pt.baieuropa.storage.WSDLPojos.CriarBloqueioConta.ObjectInputCriarBloqueioConta;
import pt.baieuropa.storage.WSDLPojos.DesactivarBloqueioConta.DesactivarBloqueioConta;
import pt.baieuropa.storage.WSDLPojos.DesactivarBloqueioConta.ObjectInputDesactivarBloqueioConta;

import javax.validation.Valid;
import java.util.*;

@RestController
@RequestMapping({"/aml-resolution"})
@Tag(name = "Account", description = "The Account API")
@Validated
public class AccountController {

    private static final Logger log = LoggerFactory.getLogger(AccountController.class);

    ObjectMapper mapper = new ObjectMapper();

    public static Properties config = ConfigUtils.loadProps(System.getenv("URLS-CONFIG"));
    private String urlCurrentAccounts = config.getProperty("currentAccounts");

    public static Properties config2 = ConfigUtils.loadProps(System.getenv("SERVICES-CONFIG"));
    private String clientID = config2.getProperty("clientID");
    private String clientSecret = config2.getProperty("clientSecret");

    @Operation(summary = "Locks an Account", description = "All required fields must be filled.", tags = { "Account" })
    @RequestMapping(value = {"/account/lock"}, method = {RequestMethod.POST})
    public int lockAccount(@Valid @RequestBody ObjectInputCriarBloqueioConta requestBody) {

        try {
            log.debug("Executing [ lockAccount ] API...");

            log.debug("JSON Received: " + new ObjectMapper().writer().withDefaultPrettyPrinter().writeValueAsString(requestBody));

            log.debug("Creating Object...");
            CriarBloqueioConta obj = new CriarBloqueioConta();
            ObjectInputCriarBloqueioConta input = requestBody;
            obj.setDados(input);
            log.debug("Object Created.");

            log.debug("Converting Object to JSON...");
            ObjectWriter ow2 = new ObjectMapper().writer().withDefaultPrettyPrinter();
            String result = ow2.writeValueAsString(obj);
            log.debug("Object Converted.");

            log.debug("Publishing Message on NATS...");
            ServiceApp.getProc().publish("LockAccount:Request", result);
            log.debug("Message Published.");

            log.info("API [ lockAccount ] Executed.\n" + result);
            return 1;
        } catch (Exception ex) {
            try {
                log.error("JSON Received: " + new ObjectMapper().writer().withDefaultPrettyPrinter().writeValueAsString(requestBody));
            } catch (JsonProcessingException e) {
                e.printStackTrace();
            }
            log.error("Error on [ lockAccount ] API: \n" + ex.getMessage());
            return 0;
        }
    }

    @Operation(summary = "Unlocks an Account", description = "All required fields must be filled.", tags = { "Account" })
    @RequestMapping(value = {"/account/unlock"}, method = {RequestMethod.POST})
    public int unlockAccount(@Valid @RequestBody ObjectInputDesactivarBloqueioConta requestBody) {

        try {
            log.debug("Executing [ unlockAccount ] API...");

            log.debug("JSON Received: " + new ObjectMapper().writer().withDefaultPrettyPrinter().writeValueAsString(requestBody));

            log.debug("Creating Object...");
            DesactivarBloqueioConta obj = new DesactivarBloqueioConta();
            ObjectInputDesactivarBloqueioConta input = requestBody;
            obj.setDados(input);
            log.debug("Object Created.");

            log.debug("Converting Object to JSON...");
            ObjectWriter ow = new ObjectMapper().writer().withDefaultPrettyPrinter();
            String result = ow.writeValueAsString(obj);
            log.debug("Object Converted.");

            log.debug("Publishing Message on NATS...");
            ServiceApp.getProc().publish("UnlockAccount:Request", result);
            log.debug("Message Published.");

            log.info("API [ unlockAccount ] Executed.\n" + result);
            return 1;
        } catch (Exception ex) {
            try {
                log.error("JSON Received: " + new ObjectMapper().writer().withDefaultPrettyPrinter().writeValueAsString(requestBody));
            } catch (JsonProcessingException e) {
                e.printStackTrace();
            }
            log.error("Error on [ unlockAccount ] API: \n" + ex.getMessage());
            return 0;
        }
    }

    @Operation(summary = "Updates the Account Risk", description = "All required fields must be filled.", tags = { "Account" })
    @RequestMapping(value = {"/account/risk"}, method = {RequestMethod.POST})
    public int updateAccountRisk(@Valid @RequestBody ObjectActualizacaoNivelRiscoClienteInput requestBody) {

        try {
            log.debug("Executing [ updateAccountRisk ] API...");

            log.debug("JSON Received: " + new ObjectMapper().writer().withDefaultPrettyPrinter().writeValueAsString(requestBody));

            log.debug("Creating Object...");
            ActualizacaoNivelRiscoCliente obj = new ActualizacaoNivelRiscoCliente();
            ObjectActualizacaoNivelRiscoClienteInput input = requestBody;
            obj.setDados(input);
            log.debug("Object Created.");

            log.debug("Converting Object to JSON...");
            ObjectWriter ow = new ObjectMapper().writer().withDefaultPrettyPrinter();
            String result = ow.writeValueAsString(obj);
            log.debug("Object Converted.");

            log.debug("Publishing Message on NATS...");
            ServiceApp.getProc().publish("UpdateAccountRisk:Request", result);
            log.debug("Message Published.");

            log.info("API [ updateAccountRisk ] Executed.\n" + result);
            return 1;
        } catch (Exception ex) {
            try {
                log.error("JSON Received: " + new ObjectMapper().writer().withDefaultPrettyPrinter().writeValueAsString(requestBody));
            } catch (JsonProcessingException e) {
                e.printStackTrace();
            }
            log.error("Error on [ updateAccountRisk ] API: \n" + ex.getMessage());
            return 0;
        }
    }

    @Operation(summary = "Consults the Account Movements", description = "All required fields must be filled.", tags = { "Account" })
    @RequestMapping(value = {"/account/transactions"}, method = {RequestMethod.POST})
    public String consultAccountMovements(@Valid @RequestBody ConsultarMovimentosContasDO requestBody) {

        OkHttpClient client = APIConnectSecurityManager.processSecurityProtocols();


        try {
            log.debug("Executing [ consultAccountMovements ] API...");

            log.debug("JSON Received: " + new ObjectMapper().writer().withDefaultPrettyPrinter().writeValueAsString(requestBody));

            log.debug("Creating Object...");
            ConsultarMovimentosContasDO obj = requestBody;
            log.debug("Object Created.");

            System.setProperty("javax.net.ssl.trustStore", "NULL");
            System.setProperty("javax.net.ssl.trustStoreType", "Windows-ROOT");

            Response response = null;

            String startDate = obj.getDataInicio();
            String endDate = obj.getDataFim();
            int limit = obj.getNumeroMovimentosConsultar();
            int offset = obj.getNumeroOrdemParaPosicionar();

//            if(offset >= 10) {
//                offset = offset / 10;
//            }

            try {
                log.debug("Calling WebService " + urlCurrentAccounts
                                                + obj.getNumeroConta() + "/transactions"
                                                + "?startDate=" + startDate
                                                + "&endDate=" + endDate
                                                + "&limit=" + limit
                                                + "&offset=" + offset);

                Request request = new Request.Builder()
                        .addHeader("X-IBM-Client-Id", clientID)
                        .addHeader("X-IBM-Client-Secret", clientSecret)
                        .url(urlCurrentAccounts + obj.getNumeroConta() + "/transactions"
                                                + "?startDate=" + startDate
                                                + "&endDate=" + endDate
                                                + "&limit=" + limit
                                                + "&offset=" + offset)
                        .build();

                response = client.newCall(request).execute();
                log.debug("Resquest sent successfully!");

                //BAI recebe resposta do DCS
                JSONObject responseBody = new JSONObject(response.body().string());
                log.debug("Response Received: \n" + responseBody.toString() + "\n");

                ObjectConsultaDeMovimentosContaDOOutput finalObject = mapper.readValue(responseBody.toString(), ObjectConsultaDeMovimentosContaDOOutput.class);

                log.debug("Converting Object to JSON...");
                ObjectWriter ow = new ObjectMapper().writer().withDefaultPrettyPrinter();
                String result = ow.writeValueAsString(finalObject);
                log.debug("Object Converted.");

                log.info("API [ consultAccountMovements ] Executed.\n" + result);
                return result;
            } catch (Exception e) {
                log.error("Error on Request/Response.\n" + e.getMessage());
                return "";
            }
        } catch (Exception ex) {
            log.error("Error on [ consultAccountMovements ] API: \n" + ex.getMessage());
            return "";
        }
    }

}
