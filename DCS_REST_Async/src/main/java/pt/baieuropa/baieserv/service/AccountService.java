package pt.baieuropa.baieserv.service;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.ObjectWriter;
import okhttp3.*;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import pt.baieuropa.baieserv.ServiceApp;
import pt.baieuropa.baieserv.models.Party.PartyRelation;
import pt.baieuropa.baieserv.models.Party.partyAcctRelInfo;
import pt.baieuropa.baieserv.models.PartyAcct.AcctRec.AcctRec;
import pt.baieuropa.baieserv.utils.ConfigUtils;
import pt.baieuropa.storage.WSDLPojos.ActualizacaoNivelRiscoCliente.ActualizacaoNivelRiscoCliente;
import pt.baieuropa.storage.WSDLPojos.ActualizacaoNivelRiscoCliente.ObjectActualizacaoNivelRiscoClienteInput;
import pt.baieuropa.storage.WSDLPojos.Evaluate.AccountCreationRequest;
import pt.baieuropa.storage.WSDLPojos.Evaluate.AccountRelationsByClientComplex;
import pt.baieuropa.storage.WSDLPojos.Evaluate.EntityAccount;

import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Properties;

public class AccountService {

    private static final Logger log = LoggerFactory.getLogger(AccountService.class);
    private static final MediaType JSON = MediaType.get("application/json; charset=utf-8");

    private static ObjectMapper mapper = new ObjectMapper();
    OkHttpClient client = new OkHttpClient();

    public static Properties config = ConfigUtils.loadProps(System.getenv("URLS-CONFIG"));
    private String urlEvaluateAccountOnline = config.getProperty("EvaluateAccountOnline");
    private String urlAccountRelations = config.getProperty("AccountRelationsByClient");

    public int accountCreatedOrChanged(String json, String action, String accountType) {
        log.debug("Requesting Account Creation...");
        try {

            log.debug("JSON Received: \n" + json);

            log.debug("Creating Object...");
            JSONObject objReceived = new JSONObject(json);

            JSONObject relationsJSON = new JSONObject(objReceived.getJSONObject("Relations").toString());
            log.debug("Relations JSON: \n" + relationsJSON.toString());
            PartyRelation relations = mapper.readValue(relationsJSON.toString(), PartyRelation.class);

            JSONObject accountJSON = new JSONObject(objReceived.getJSONObject("Account").toString());
            log.debug("Account JSON: \n" + accountJSON.toString());
            AcctRec account = mapper.readValue(accountJSON.toString(), AcctRec.class);

            AccountCreationRequest requestObj = new AccountCreationRequest();
            requestObj.setAccountNumber(account.getAcctInfo().getAcctIdent().getAcctIdentValue());
            requestObj.setAccountType(accountType);
            requestObj.setAccountCategory("1");
            requestObj.setAccountEntityType(account.getAcctInfo().getAcctUse());
            requestObj.setClientNumberExt(account.getAcctInfo().getOwnership());
            requestObj.setAccountStatus(account.getAcctStatus().getAcctStatusCode());
            requestObj.setSegmentType("");
            requestObj.setSubscriptionChannel("");
            requestObj.setAccountCurrency(account.getAcctInfo().getCurCode().getCurCodeValue());
            requestObj.setRequestUser(objReceived.getString("RequestUser"));
            requestObj.setRequestId(objReceived.getString("RequestId"));

            Date date = new SimpleDateFormat("yyyyMMdd").parse(account.getAcctInfo().getOpenDt());
            SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd");
            requestObj.setClientCreationDateExt(simpleDateFormat.format(date));
            requestObj.setAccountCreationDate(simpleDateFormat.format(date));

            requestObj.setBalanceAverage(new BigDecimal(0));
            requestObj.setDateBalanceAverage(null);
            requestObj.setAction(action);
            requestObj.setFlexfields(null);

            // BAIE - Criação do array de objetos das contas associadas
            List<EntityAccount> entityAccountList = new ArrayList<>();
            for (partyAcctRelInfo item: relations.getPartyAcctRelInfo()) {
                EntityAccount entityAccount = new EntityAccount();
                entityAccount.setEntityNumberExt(item.getPartyRef().getPartyKeys().getPartyId());

                try {
                    int entityTypeInt = Integer.parseInt(item.getPartyAcctRelData().getPartyAcctRelType());
                    entityAccount.setEntityAccountRelationType(Integer.toString(entityTypeInt));
                } catch (Exception e) {
                    entityAccount.setEntityAccountRelationType(item.getPartyAcctRelData().getPartyAcctRelType());
                }

                entityAccountList.add(entityAccount);
            }
            requestObj.setEntityAccountList(entityAccountList);
            log.debug("Object Created.");

            log.debug("Converting Object to JSON...");
            ObjectWriter ow = new ObjectMapper().writer().withDefaultPrettyPrinter();
            String body = ow.writeValueAsString(requestObj);
            log.debug("Object Converted.\n" + body);

            Response response = null;

            try {
                log.debug("Calling WebService " + urlEvaluateAccountOnline);
                RequestBody requestBody = RequestBody.Companion.create(body,JSON);
                Request request = new Request.Builder()
                        .url(urlEvaluateAccountOnline)
                        .post(requestBody)
                        .build();

                response = client.newCall(request).execute();
                log.debug("Resquest sent successfully!");

                //BAI recebe resposta do DCS
                JSONObject responseBody = new JSONObject(response.body().string());
                log.debug("Response Received: \n" + responseBody.toString() + "\n");

                if(responseBody.has("ReturnCode")) {
                    if (responseBody.getString("ReturnCode").equals("1")) {
                        log.debug("Publishing AccountRelationsUpdated:Response Message on NATS...");
                        ServiceApp.getProc().publish("AccountRelationsUpdated:Response", responseBody.toString());
                        log.debug("Message Published.");

                        // BAIE - NRC
                        log.debug("Creating Object...");
                        ActualizacaoNivelRiscoCliente obj = new ActualizacaoNivelRiscoCliente();
                        ObjectActualizacaoNivelRiscoClienteInput input = new ObjectActualizacaoNivelRiscoClienteInput();
                        input.setAccao("A");
                        input.setAutorizacao("999");
                        input.setCodigoCampo("NRC");
                        input.setValorCampo(responseBody.getString("Risk"));
                        input.setNumeroCliente(Integer.parseInt(requestObj.getClientNumberExt()));
                        input.setNumeroBalcao(9001);
                        input.setUtilizador("TYIUSER");
                        obj.setDados(input);
                        log.debug("Object Created.");

                        log.debug("Converting Object to JSON...");
                        String result = ow.writeValueAsString(obj);
                        log.debug("Object Converted.");

                        log.debug("Publishing UpdateAccountRisk:Request Message on NATS...");
                        ServiceApp.getProc().publish("UpdateAccountRisk:Request", result);
                        log.debug("Message Published.");

                        return 1;
                    } else {
                        log.error("Return code: " + responseBody.getString("ReturnCode") +
                                "\nError Message: " + responseBody.getString("ErrorMessage"));
                        return 0;
                    }
                } else {
                    log.error("Error while processing response message...");
                    return 0;
                }
            } catch (Exception e) {
                log.error("Error on Request/Response.\n" + e.getMessage());
                return 0;
            }
        } catch (Exception e) {
            log.error("Error while Requesting Account Creation!\n" + e.getMessage());
            return 0;
        }
    }

    public int accountRelationsCreatedOrChanged(String json, String action) {
        log.debug("Requesting Account Relations Update...");
        try {

            log.debug("JSON Received: \n" + json);

            log.debug("Creating Object...");
            PartyRelation party = mapper.readValue(json, PartyRelation.class);
            AccountRelationsByClientComplex requestObj = new AccountRelationsByClientComplex();
            requestObj.setClientNumberExt(party.getPartyAcctRelId());

            // BAIE - Criação do array de objetos das contas associadas
            List<EntityAccount> entityAccountList = new ArrayList<>();
            for (partyAcctRelInfo item: party.getPartyAcctRelInfo()) {
                EntityAccount entityAccount = new EntityAccount();
                entityAccount.setEntityNumberExt(item.getPartyRef().getPartyKeys().getPartyId());

                try {
                    int entityTypeInt = Integer.parseInt(item.getPartyAcctRelData().getPartyAcctRelType());
                    entityAccount.setEntityAccountRelationType(Integer.toString(entityTypeInt));
                } catch (Exception e) {
                    entityAccount.setEntityAccountRelationType(item.getPartyAcctRelData().getPartyAcctRelType());
                }

                entityAccountList.add(entityAccount);
            }
            requestObj.setEntityAccountList(entityAccountList);
            requestObj.setRequestUser(party.getRequestUser());
            log.debug("Object Created.");

            log.debug("Converting Object to JSON...");
            ObjectWriter ow = new ObjectMapper().writer().withDefaultPrettyPrinter();
            String body = ow.writeValueAsString(requestObj);
            log.debug("Object Converted.\n" + body);

            Response response = null;

            try {
                log.debug("Calling WebService " + urlAccountRelations);
                RequestBody requestBody = RequestBody.Companion.create(body,JSON);
                Request request = new Request.Builder()
                        .url(urlAccountRelations)
                        .post(requestBody)
                        .build();

                response = client.newCall(request).execute();
                log.debug("Resquest sent successfully!");

                //BAI recebe resposta do DCS
                JSONObject responseBody = new JSONObject(response.body().string());
                log.debug("Response Received: \n" + responseBody.toString() + "\n");

                if(responseBody.has("ReturnCode")) {
                    if (responseBody.getString("ReturnCode").equals("1")) {
                        log.debug("Publishing Response Mensage on NATS...");
                        ServiceApp.getProc().publish("AccountRelationsUpdated:Response", responseBody.toString());
                        log.error("Message Published.");

                        return 1;
                    } else {
                        log.error("Return code: " + responseBody.getString("ReturnCode") +
                                "\nError Message: " + responseBody.getString("ErrorMessage"));
                        return 0;
                    }
                } else {
                    log.error("Error while processing response message...");
                    return 0;
                }
            } catch (Exception e) {
                log.error("Error on Request/Response.\n" + e.getMessage());
                return 0;
            }
        } catch (Exception e) {
            log.error("Error while Requesting Account Relations Update!\n" + e.getMessage());
            return 0;
        }
    }

}
