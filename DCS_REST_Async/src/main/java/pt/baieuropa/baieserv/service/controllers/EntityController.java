package pt.baieuropa.baieserv.service.controllers;


import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.ObjectWriter;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.tags.Tag;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;
import pt.baieuropa.baieserv.ServiceApp;
import pt.baieuropa.storage.WSDLPojos.ActualizacaoNivelRiscoEntidade.ActualizacaoNivelRiscoEntidade;
import pt.baieuropa.storage.WSDLPojos.ActualizacaoNivelRiscoEntidade.ObjectActualizacaoNivelRiscoEntidadeInput;
import pt.baieuropa.storage.WSDLPojos.AtualizaEstadoFiltering.AtualizaEstadoFiltering;
import pt.baieuropa.storage.WSDLPojos.AtualizaEstadoFiltering.ObjectAtualizaEstadoFilteringInput;
import pt.baieuropa.storage.WSDLPojos.CriarBloqueioEntidade.CriarBloqueioEntidade;
import pt.baieuropa.storage.WSDLPojos.CriarBloqueioEntidade.ObjectInputCriarBloqueioEntidade;
import pt.baieuropa.storage.WSDLPojos.DesactivarBloqueioEntidade.DesactivarBloqueioEntidade;
import pt.baieuropa.storage.WSDLPojos.DesactivarBloqueioEntidade.ObjectInputDesactivarBloqueioEntidade;

import javax.validation.Valid;

@RestController
@RequestMapping(value = "/aml-resolution")
@Tag(name = "Entity", description = "The Entity API")
@Validated
public class EntityController {

    private static final Logger log = LoggerFactory.getLogger(EntityController.class);

    @Operation(summary = "Locks an Entity", description = "All required fields must be filled.", tags = { "Entity" })
    @RequestMapping(value = {"/entity/lock"}, method = {RequestMethod.POST})
    public int lockEntity(@Valid @RequestBody ObjectInputCriarBloqueioEntidade requestBody) {

        try {
            log.debug("Executing [ lockEntity ] API...");

            log.debug("Creating Object...");
            CriarBloqueioEntidade obj = new CriarBloqueioEntidade();
            ObjectInputCriarBloqueioEntidade input = requestBody;
            obj.setDados(input);
            log.debug("Object Created.");

            log.debug("Converting Object to JSON...");
            ObjectWriter ow = new ObjectMapper().writer().withDefaultPrettyPrinter();
            String result = ow.writeValueAsString(obj);
            log.debug("Object Converted.");

            log.debug("Publishing Message on NATS...");
            ServiceApp.getProc().publish("LockEntity:Request", result);
            log.debug("Message Published.");

            log.info("API [ lockEntity ] Executed.\n" + result);
            return 1;
        } catch (Exception ex) {
            log.error("JSON Received: " + requestBody);
            log.error("Error on [ lockEntity ] API: \n" + ex.getMessage());
            return 0;
        }
    }

    @Operation(summary = "Unlocks an Entity", description = "All required fields must be filled.", tags = { "Entity" })
    @RequestMapping(value = {"/entity/unlock"}, method = {RequestMethod.POST})
    public int unlockEntity(@Valid @RequestBody ObjectInputDesactivarBloqueioEntidade requestBody) {

        try {
            log.debug("Executing [ unlockEntity ] API...");

            log.debug("JSON Received: " + new ObjectMapper().writer().withDefaultPrettyPrinter().writeValueAsString(requestBody));

            log.debug("Creating Object...");
            DesactivarBloqueioEntidade obj = new DesactivarBloqueioEntidade();
            ObjectInputDesactivarBloqueioEntidade input = requestBody;
            obj.setDados(input);
            log.debug("Object Created.");

            log.debug("Converting Object to JSON...");
            ObjectWriter ow = new ObjectMapper().writer().withDefaultPrettyPrinter();
            String result = ow.writeValueAsString(obj);
            log.debug("Object Converted.");

            log.debug("Publishing Message on NATS...");
            ServiceApp.getProc().publish("UnlockEntity:Request", result);
            log.debug("Message Published.");

            log.info("API [ unlockEntity ] Executed. \n" + result);
            return 1;
        } catch (Exception ex) {
            try {
                log.error("JSON Received: " + new ObjectMapper().writer().withDefaultPrettyPrinter().writeValueAsString(requestBody));
            } catch (JsonProcessingException e) {
                e.printStackTrace();
            }
            log.error("Error on [ unlockEntity ] API: \n" + ex.getMessage());
            return 0;
        }
    }

    @Operation(summary = "Updates an Entity Risk", description = "All required fields must be filled.", tags = { "Entity" })
    @RequestMapping(value = {"/entity/risk"}, method = {RequestMethod.POST})
    public int updateEntityRisk(@Valid @RequestBody ObjectActualizacaoNivelRiscoEntidadeInput requestBody) {

        try {
            log.debug("Executing [ updateEntityRisk ] API...");

            log.debug("JSON Received: " + new ObjectMapper().writer().withDefaultPrettyPrinter().writeValueAsString(requestBody));

            log.debug("Creating Object...");
            ActualizacaoNivelRiscoEntidade obj = new ActualizacaoNivelRiscoEntidade();
            ObjectActualizacaoNivelRiscoEntidadeInput input = requestBody;
            obj.setDados(input);
            log.debug("Object Created.");

            log.debug("Converting Object to JSON...");
            ObjectWriter ow = new ObjectMapper().writer().withDefaultPrettyPrinter();
            String result = ow.writeValueAsString(obj);
            log.debug("Object Converted.");

            log.debug("Publishing Message on NATS...");
            ServiceApp.getProc().publish("UpdateEntityRisk:Request", result);
            log.debug("Message Published.");

            log.info("API [ updateEntityRisk ] Executed.\n" + result);
            return 1;
        } catch (Exception ex) {
            try {
                log.error("JSON Received: " + new ObjectMapper().writer().withDefaultPrettyPrinter().writeValueAsString(requestBody));
            } catch (JsonProcessingException e) {
                e.printStackTrace();
            }
            log.error("Error on [ updateEntityRisk ] API: \n" + ex.getMessage());
            return 0;
        }
    }

    @Operation(summary = "Updates an Entity Filtering Status", description = "All required fields must be filled.", tags = { "Entity" })
    @RequestMapping(value = {"/entity/filtering"}, method = {RequestMethod.POST})
    public int updateFiltering(@Valid @RequestBody ObjectAtualizaEstadoFilteringInput requestBody) {

        try {
            log.debug("Executing [ updateFiltering ] API...");

            log.debug("JSON Received: " + new ObjectMapper().writer().withDefaultPrettyPrinter().writeValueAsString(requestBody));

            log.debug("Creating Object...");
            AtualizaEstadoFiltering obj = new AtualizaEstadoFiltering();
            ObjectAtualizaEstadoFilteringInput input = requestBody;
            obj.setDados(input);
            log.debug("Object Created.");

            log.debug("Converting Object to JSON...");
            ObjectWriter ow = new ObjectMapper().writer().withDefaultPrettyPrinter();
            String result = ow.writeValueAsString(obj);
            log.debug("Object Converted.");

            log.debug("Publishing Message on NATS...");
            ServiceApp.getProc().publish("UpdateFiltering:Request", result);
            log.debug("Message Published.");

            log.info("API [ updateFiltering ] Executed.\n" + result);
            return 1;
        } catch (Exception ex) {
            try {
                log.error("JSON Received: " + new ObjectMapper().writer().withDefaultPrettyPrinter().writeValueAsString(requestBody));
            } catch (JsonProcessingException e) {
                e.printStackTrace();
            }
            log.error("Error on [ updateFiltering ] API: \n" + ex.getMessage());
            return 0;
        }
    }
}
