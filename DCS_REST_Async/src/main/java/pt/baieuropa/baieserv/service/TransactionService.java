package pt.baieuropa.baieserv.service;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.ObjectWriter;
import okhttp3.*;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import pt.baieuropa.baieserv.ServiceApp;
import pt.baieuropa.baieserv.utils.ConfigUtils;
import pt.baieuropa.storage.WSDLPojos.DesactivarBloqueioTransacao.DesactivarBloqueioTransacao;
import pt.baieuropa.storage.WSDLPojos.DesactivarBloqueioTransacao.ObjectInputDesactivarBloqueioTransacao;

import java.util.Properties;

public class TransactionService {

    private static final Logger log = LoggerFactory.getLogger(TransactionService.class);
    private static final MediaType JSON = MediaType.get("application/json; charset=utf-8");

    private static ObjectMapper mapper = new ObjectMapper();
    OkHttpClient client = new OkHttpClient();

    public static Properties config = ConfigUtils.loadProps(System.getenv("URLS-CONFIG"));
    private String urlEvaluateTransactionOnline = config.getProperty("EvaluateTransactionOnline");
    private String urlEvaluateTransactionCB = config.getProperty("EvaluateTransactionCB");
    private String urlEvaluateTradeFinanceOnline = config.getProperty("EvaluateTradeFinanceOnline");

    public int evaluateTransactionOnline(String json) {
        log.debug("Evaluating Transaction Online...");
        try {
            JSONObject input = new JSONObject(json);
            Response response = null;
            String topic = "EvaluateTransactionOnline:Response";

            log.debug("Calling WebService " + urlEvaluateTransactionOnline);
            RequestBody body = RequestBody.Companion.create(json, JSON);
            Request request = new Request.Builder()
                    .url(urlEvaluateTransactionOnline)
                    .post(body)
                    .build();

            response = client.newCall(request).execute();
            log.debug("Resquest sent successfully!");

            //BAI recebe resposta do DCS
            JSONObject responseBody = new JSONObject(response.body().string());
            log.debug("Response Received: \n" + responseBody.toString() + "\n");

            String transactionNumber = input.getString("TransactionNumber");

            responseBody.put("TransactionNumber", transactionNumber);
            // TODO - Está a ser preenchido o RequestId desta forma, devido ao serviço da
            // TODO - Dixtior ainda não retornar o RequestId no EvaluateTransactionOnline
            if(!responseBody.has("RequestId")) {
                responseBody.put("RequestId", input.getString("RequestId"));
            } else if(responseBody.getString("RequestId") == null || responseBody.getString("RequestId").equals("")) {
                responseBody.put("RequestId", input.getString("RequestId"));
            }

            if(transactionNumber.contains("OPA") || transactionNumber.contains("OPC") || transactionNumber.contains("OPR")) {
                topic = "EvaluateTransactionOnlineSP:Response";
            }

            log.debug("Publishing " + topic +  "on NATS...");
            ServiceApp.getProc().publish(topic, responseBody.toString());
            log.debug("Response published.");

            log.info("Evaluated Transaction Online successfully!\n" + responseBody.toString());
            return 1;
        } catch(Exception ex) {
            log.error("Error while Evaluating Transaction Online!\n" + ex.getMessage());
            return 0;
        }
    }

    public int evaluateTransactionCB(String json) {
        log.debug("Evaluating Transaction CB...");
        try {
            JSONObject input = new JSONObject(json);
            Response response = null;

            log.debug("Calling WebService " + urlEvaluateTransactionCB);
            RequestBody body = RequestBody.Companion.create(json, JSON);
            Request request = new Request.Builder()
                    .url(urlEvaluateTransactionCB)
                    .post(body)
                    .build();

            response = client.newCall(request).execute();
            log.debug("Resquest sent successfully!");

            //BAI recebe resposta do DCS
            JSONObject responseBody = new JSONObject(response.body().string());
            log.debug("Response Received: \n" + responseBody.toString() + "\n");

            responseBody.put("TransactionNumber", input.getString("TransactionNumber"));

            log.debug("Publishing Response on NATS...");
            ServiceApp.getProc().publish("EvaluateTransactionCB:Response", responseBody.toString());
            log.debug("Response published.");

            log.info("Evaluated Transaction CB successfully!\n" + responseBody.toString());
            return 1;
        } catch(Exception ex) {
            log.error("Error while Evaluating CB Online!\n" + ex.getMessage());
            return 0;
        }
    }

    public int evaluateTradeFinanceOnline(String json) {

        log.debug("Evaluating Trade Finance Online...");

        try {
            JSONObject input = new JSONObject(json);
            Response response = null;

            log.debug("Calling WebService " + urlEvaluateTradeFinanceOnline);
            RequestBody body = RequestBody.Companion.create(json, JSON);
            Request request = new Request.Builder()
                    .url(urlEvaluateTradeFinanceOnline)
                    .post(body)
                    .build();

            response = client.newCall(request).execute();
            log.debug("Resquest sent successfully!");

            // BAIE - recebe resposta do DCS
            JSONObject responseBody = new JSONObject(response.body().string());
            log.debug("Response Received: \n" + responseBody.toString() + "\n");

            try {
                responseBody.put("TransactionNumber", input.getString("TransactionNumber"));
            } catch (Exception ex) {

            }

            // BAIE - Verifica se é para desbloquear a carta de crédito
            if(!responseBody.getBoolean("IsToLock")) {
                log.debug("Creating Object...");
                DesactivarBloqueioTransacao obj = new DesactivarBloqueioTransacao();
                ObjectInputDesactivarBloqueioTransacao dados = new ObjectInputDesactivarBloqueioTransacao();
                dados.setRequestId(input.getString("RequestId"));
                dados.setTransactionNumberExt(input.getString("MasterReference"));
                dados.setNewStatusExt("3");
                obj.setDados(dados);
                log.debug("Object Created.");

                log.debug("Converting Object to JSON...");
                ObjectWriter ow = new ObjectMapper().writer().withDefaultPrettyPrinter();
                String result = ow.writeValueAsString(obj.getDados());
                log.debug("Object Converted.");

                log.debug("Publishing LettersOfCredit:Response on NATS...");
                ServiceApp.getProc().publish("LettersOfCredit:Response", result);
                log.debug("LettersOfCredit:Response published.");
            }

            log.debug("Publishing EvaluateTradeFinanceOnline:Response on NATS...");
            ServiceApp.getProc().publish("EvaluateTradeFinanceOnline:Response", responseBody.toString());
            log.debug("EvaluateTradeFinanceOnline:Response published.");

            log.info("Evaluated Trade Finance Online successfully!\n" + responseBody.toString());
            return 1;
        } catch(Exception ex) {
            log.error("Error while Evaluating Trade Finance Online!\n" + ex.getMessage());
            return 0;
        }
    }

}
