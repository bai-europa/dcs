package pt.baieuropa.baieserv.service.managers;

import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import pt.baieuropa.baieserv.service.AccountService;
import pt.baieuropa.baieserv.service.EntityService;
import pt.baieuropa.baieserv.service.TransactionService;
import pt.baieuropa.nats.Runner;

import java.io.FileWriter;
import java.nio.charset.StandardCharsets;

public class NATSRequestsManager extends Runner {

    private static final Logger log = LoggerFactory.getLogger(NATSRequestsManager.class);
    private String topic = "";

    public NATSRequestsManager(String topic) {
        this.topic = topic;
    }

    @Override
    public Object run(byte[] input) {
        String jsonString = new String(input, StandardCharsets.UTF_8);
        JSONObject jsonObject = new JSONObject(jsonString);

        EntityService entityService = new EntityService();
        AccountService accountService = new AccountService();
        TransactionService transactionService = new TransactionService();

        log.debug("Received JSON: \n" + jsonString);

        try {
            switch (topic) {
                case "EvaluateTransactionOnline:Request":
                // case "CurrentAccountMovement:Request": <- Foi removido devido à alteração do BankaMW.
                case "CurrentAccountMovementDeposit:Request":
                case "CurrentAccountMovementWithdrawal:Request":
                case "CurrentAccountMovementWithdrawalBAI:Request":
                case "CurrentAccountMovementDP:Request":
                case "CurrentAccountMovementCheckDeposit:Request":
                case "CurrentAccountMovementMixedDeposit:Request":
                case "CashierCheck:Request":
                case "ClearingCheck:Request":
                case "CertifiedCheck:Request":
                case "BankCheck:Request":
                case "DebitTransfer:Request":
                case "CreditTransfer:Request":
                case "PermanentDebitTransfer:Request":
                case "PermanentCreditTransfer:Request":
                case "OperationsDebitUNIBANCO:Request":
                case "CardOperationsUNIBANCO:Request":
                case "TermDepositTransfer:Request":
                case "CreditMovementDODOCataloged:Request":
                case "DebitMovementDODOCataloged:Request":
                case "SecuredAccountDebit:Request":
                case "SecuredAccountCredit:Request":
                case "CreditDOWithUseOfCorCC:Request":
                case "CurrentAccountMovementInternalSent:Request":
                case "CurrentAccountMovementInternalReceived:Request":
                case "CurrentAccountMovementDB:Request":
                case "CurrentAccountMovementCR:Request":
                case "CreditTransferSameDate:Request":
                case "ValuesDepositBAIECheck:Request":
                case "ValuesDepositBankCheck:Request":
                case "ValuesDepositCertifiedCheck:Request":
                case "CoinSelling:Request":
                case "CoinPurchase:Request":
                case "IssuingForeignCheck:Request":
                case "ExportDocumentaryCredit:Request":
                case "ForeignExchangePurchase:Request":
                case "ForeignExchangeSale:Request":
                case "ImportDocumentaryCredit:Request":
                case "CurrentAccountMovementDebitSEPASent:Request":
                case "CurrentAccountMovementCreditSEPASent:Request":
                case "CurrentAccountMovementCreditSEPAReceived:Request":
                case "ReturnRequestSEPA:Request":
                case "CurrentAccountReturnCreditSEPA:Request":
                case "ForeignCreditTEIDevolution:Request":
                case "ConstitutionDP:Request":
                case "SaleOffDP:Request":
                case "IncrementDP:Request":
                case "DebitCardTransaction:Request":
                case "Uploads:Request":
                case "Deposits:Request":
                case "CashAdvance:Request":
                case "Authorizations:Request":
                case "Compensations:Request":
                case "Commissions:Request":
                case "CommissionsSettlements:Request":
                case "OtherSettlements:Request":
                case "GeneralDebit:Request":
                case "GeneralCredit:Request":
                case "ReturnCredit:Request":
                case "UseTransferCredit:Request":
                case "UseTransfer:Request":
                case "LoanRepayment:Request":
                case "CheckWithoutOtherBanks:Request":
                case "CheckFromDepositedBank:Request":
                case "DepositedCheckReturn:Request":
                case "SEPADDCharged:Request":
                case "DirectDebitInstruction:Request":
                    transactionService.evaluateTransactionOnline(jsonString);
                    break;
                case "EvaluateTransactionCB:Request":
                    transactionService.evaluateTransactionCB(jsonString);
                    break;
                case "LettersOfCredit:Request":
                    transactionService.evaluateTradeFinanceOnline(jsonString);
                    break;
                case "ParticularCreated:Request":
                case "EnterpriseCreated:Request":
                    entityService.entityCreatedOrChanged(jsonString, "Add");
                    break;
                case "ParticularUpdated:Request":
                case "EnterpriseUpdated:Request":
                    entityService.entityCreatedOrChanged(jsonString, "Change");
                    break;
                case "CurrentAccountCreated:Request":
                    accountService.accountCreatedOrChanged(jsonString, "Add", "DO");
                    break;
                case "DepositAccountCreated:Request":
                    accountService.accountCreatedOrChanged(jsonString, "Add", "DP");
                    break;
                case "CreditAccountCreated:Request":
                    accountService.accountCreatedOrChanged(jsonString, "Add", "CR");
                    break;
                case "CurrentAccountUpdated:Request":
                    accountService.accountCreatedOrChanged(jsonString, "Change", "DO");
                    break;
                case "DepositAccountUpdated:Request":
                    accountService.accountCreatedOrChanged(jsonString, "Change", "DP");
                    break;
                case "CreditAccountUpdated:Request":
                    accountService.accountCreatedOrChanged(jsonString, "Change", "CR");
                    break;
//                case "CurrentAccountRelationsCreated:Request":
//                    accountService.accountRelationsCreatedOrChanged(jsonString, "Create");
//                    break;
                case "CurrentAccountRelationsUpdated:Request":
                    accountService.accountRelationsCreatedOrChanged(jsonString, "Change");
                    break;
                // case "AccountRelationsCreated:Request":
                //     accountRelationsCreatedOrChanged(jsonString, "Add");
                case "CurrentAccountSituationUpdated:Request":
                    accountService.accountRelationsCreatedOrChanged(jsonString, "Change");
                    break;
                case "Accumulated:Response":
                    accumulated(jsonString);
                    break;
            }

        } catch (Exception e) {
            log.error("Error on NATSRequestsManager.\n" + e.getMessage());
        }

        return jsonObject;
    }

    private int accumulated(String csv) {
        try {
            FileWriter myWriter = new FileWriter("C://tests/accumulated.csv");
            myWriter.write(csv);
            myWriter.close();
            return 1;
        }catch (Exception ex) {
            return 0;
        }
    }
}
