package pt.baieuropa.baieserv.service.managers;

import okhttp3.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.net.ssl.*;
import java.io.IOException;
import java.net.InetAddress;
import java.net.Socket;
import java.security.KeyStore;
import java.security.cert.X509Certificate;
import java.util.*;

public class APIConnectSecurityManager {

    private static final Logger log = LoggerFactory.getLogger(APIConnectSecurityManager.class);

    public static OkHttpClient processSecurityProtocols() {
        try {
            ConnectionSpec spec = new ConnectionSpec.Builder(ConnectionSpec.MODERN_TLS)
                    .tlsVersions(TlsVersion.TLS_1_2)
                    .allEnabledCipherSuites()
                    .build();

            TrustManagerFactory trustManagerFactory = TrustManagerFactory.getInstance(
                    TrustManagerFactory.getDefaultAlgorithm());
            trustManagerFactory.init((KeyStore) null);
            TrustManager[] trustManagers = trustManagerFactory.getTrustManagers();
            if (trustManagers.length != 1 || !(trustManagers[0] instanceof X509TrustManager)) {
                throw new IllegalStateException("Unexpected default trust managers:"
                        + Arrays.toString(trustManagers));
            }

            // for (int i = 0; i < trustManagers.length; i++) {
            //     X509TrustManager tm = (X509TrustManager) trustManagers[i];
            //     for (X509Certificate cert : Arrays.asList(tm.getAcceptedIssuers())) {
            //         log.debug("BAIE CERTIFICADOS -> " + cert.getSubjectDN().getName());
            //      }
            // }

            // TODO: verificar o que está no trustManagers
            // TODO: quais as  trusstores que estão a ser lidas (JVM, WS, Maquina)
            X509TrustManager trustManager = (X509TrustManager) trustManagers[0];

            SSLContext sslContext = SSLContext.getInstance("TLS");
            sslContext.init(null, new TrustManager[]{trustManager}, null);
            SSLSocketFactory sslSocketFactory
                    = new DelegatingSSLSocketFactory(sslContext.getSocketFactory()) {
                @Override
                protected SSLSocket configureSocket(SSLSocket socket) throws IOException {
                    socket.setEnabledProtocols(new String[]{
                            TlsVersion.TLS_1_0.javaName(),
                            TlsVersion.TLS_1_1.javaName(),
                            TlsVersion.TLS_1_2.javaName()
                    });
                    return socket;
                }
            };

            return new OkHttpClient.Builder()
                    .sslSocketFactory(sslSocketFactory, trustManager)
                    .retryOnConnectionFailure(false)
                    .connectionSpecs(Collections.singletonList(spec))
                    .build();

        } catch (Exception e) {
            log.error("Error while checking security protocols for API Connect.\n" + e.getMessage());
            return null;
        }
    }
}

class DelegatingSSLSocketFactory extends SSLSocketFactory {

    protected final SSLSocketFactory delegate;

    public DelegatingSSLSocketFactory(SSLSocketFactory delegate) {
        this.delegate = delegate;
    }

    @Override
    public String[] getDefaultCipherSuites() {
        return delegate.getDefaultCipherSuites();
    }

    @Override
    public String[] getSupportedCipherSuites() {
        return delegate.getSupportedCipherSuites();
    }

    @Override
    public Socket createSocket(
            Socket socket, String host, int port, boolean autoClose) throws IOException {
        return configureSocket((SSLSocket) delegate.createSocket(socket, host, port, autoClose));
    }

    @Override
    public Socket createSocket(String host, int port) throws IOException {
        return configureSocket((SSLSocket) delegate.createSocket(host, port));
    }

    @Override
    public Socket createSocket(
            String host, int port, InetAddress localHost, int localPort) throws IOException {
        return configureSocket((SSLSocket) delegate.createSocket(host, port, localHost, localPort));
    }

    @Override
    public Socket createSocket(InetAddress host, int port) throws IOException {
        return configureSocket((SSLSocket) delegate.createSocket(host, port));
    }

    @Override
    public Socket createSocket(
            InetAddress address, int port, InetAddress localAddress, int localPort) throws IOException {
        return configureSocket((SSLSocket) delegate.createSocket(
                address, port, localAddress, localPort));
    }

    protected SSLSocket configureSocket(SSLSocket socket) throws IOException {
        return socket;
    }
}
