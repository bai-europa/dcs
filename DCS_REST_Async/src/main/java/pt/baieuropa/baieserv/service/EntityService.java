package pt.baieuropa.baieserv.service;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.ObjectWriter;
import okhttp3.*;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import pt.baieuropa.baieserv.ServiceApp;
import pt.baieuropa.baieserv.models.OrgPartyInfo.BeneficialOwners;
import pt.baieuropa.baieserv.models.OrgPartyInfo.IssuedIdent;
import pt.baieuropa.baieserv.models.Party.Party;
import pt.baieuropa.baieserv.utils.ConfigUtils;
import pt.baieuropa.storage.WSDLPojos.ActualizacaoNivelRiscoEntidade.ActualizacaoNivelRiscoEntidade;
import pt.baieuropa.storage.WSDLPojos.ActualizacaoNivelRiscoEntidade.ObjectActualizacaoNivelRiscoEntidadeInput;
import pt.baieuropa.storage.WSDLPojos.AtualizaEstadoFiltering.AtualizaEstadoFiltering;
import pt.baieuropa.storage.WSDLPojos.AtualizaEstadoFiltering.ObjectAtualizaEstadoFilteringInput;
import pt.baieuropa.storage.WSDLPojos.DesactivarBloqueioEntidade.DesactivarBloqueioEntidade;
import pt.baieuropa.storage.WSDLPojos.DesactivarBloqueioEntidade.ObjectInputDesactivarBloqueioEntidade;
import pt.baieuropa.storage.WSDLPojos.Evaluate.Contact;
import pt.baieuropa.storage.WSDLPojos.Evaluate.Document;
import pt.baieuropa.storage.WSDLPojos.Evaluate.EntityBeneficialOwnersList;
import pt.baieuropa.storage.WSDLPojos.Evaluate.EntityEvaluationRequest;

import java.util.LinkedList;
import java.util.Properties;

public class EntityService {

    private static final Logger log = LoggerFactory.getLogger(EntityService.class);
    private static final MediaType JSON = MediaType.get("application/json; charset=utf-8");

    private static ObjectMapper mapper = new ObjectMapper();
    OkHttpClient client = new OkHttpClient();

    public static Properties config = ConfigUtils.loadProps(System.getenv("URLS-CONFIG"));
    private String urlEvaluateEntityOnline = config.getProperty("EvaluateEntityOnline");

    public int entityCreatedOrChanged(String json, String action) {
        log.debug("Requesting Entity Creation...");
        try {

            log.debug("JSON Received: \n" + json);

            Party party = mapper.readValue(json, Party.class);
            EntityEvaluationRequest requestObj = new EntityEvaluationRequest();
            LinkedList<Document> documents = new LinkedList<>();
            LinkedList<Contact> contacts = new LinkedList<>();
            LinkedList<EntityBeneficialOwnersList> beneficialOwners = new LinkedList<>();
            int documentsCounter = 0;

            log.debug("Creating Object...");
            requestObj = new EntityEvaluationRequest();

            // BAIE - Verificação se é um particular (true) ou empresa (false)
            boolean isParticular = party.getPersonPartyInfo() != null;

            // BAIE - Preenchimento de campos base
            requestObj.setNumberExt(party.getPartyId());
            requestObj.setEntityType(isParticular ? "P" : "E");
            requestObj.setEntityCategory(party.getPartyType());
            requestObj.setRequestUser(party.getRequestUser());
            requestObj.setRequestId(party.getRequestId());
            requestObj.setPresencial(party.isPresencial()) ;
            requestObj.setAction(action);

            // BAIE - Preenchimento de campos únicos do tipo de Party
            if(isParticular) {
                requestObj.setBirthDate(party.getPersonPartyInfo().getBirthDt());
                requestObj.setBirthPlace(party.getPersonPartyInfo().getBirthPlace());
                requestObj.setGender(party.getPersonPartyInfo().getGender());
                requestObj.setParent1Name(party.getPersonPartyInfo().getPersonData().getParents().getFather());
                requestObj.setParent2Name(party.getPersonPartyInfo().getPersonData().getParents().getMother());
                requestObj.setConstitutionDate(party.getPersonPartyInfo().getEstablishedDt());
                requestObj.setPEPDeclared(party.getPersonPartyInfo().getPEP());
                if(party.getPersonPartyInfo().getEmployment() != null) requestObj.setProfession(party.getPersonPartyInfo().getEmployment().getJobId());
                if(party.getPersonPartyInfo().getPersonData().getEducationalQualifications() != null) requestObj.setEducationalQualifications(party.getPersonPartyInfo().getPersonData().getEducationalQualifications().getQualificationId());
                // BAIE - Preenchimento dos documentos para Particulares
                for (pt.baieuropa.baieserv.models.PersonPartyInfo.IssuedIdent issuedIdent : party.getPersonPartyInfo().getPersonData().getIssuedIdents()) {
                    Document document = new Document();
                    document.setNumber(issuedIdent.getIssuedIdentValue());
                    document.setDocumentType(issuedIdent.getIssuedIdentType());
                    document.setCreatedAtExt(issuedIdent.getIssueDt());
                    document.setDocumentExpirationDate(issuedIdent.getExpDt());

                    documentsCounter++;

                    int docsNum = party.getPersonPartyInfo().getPersonData().getIssuedIdents().size();
                    if(docsNum == 1) {
                        document.setIsMainDocument(true);
                    }else if(docsNum > 1) {
                        if(issuedIdent.getIssuedIdentType().charAt(0) == '5') {
                            document.setIsMainDocument(true);
                        } else if(docsNum == documentsCounter) {
                            document.setIsMainDocument(true);
                        }
                    } else {
                        document.setIsMainDocument(false);
                    }
                    documents.add(document);
                }
                Document[] docs = documents.toArray(new Document[documents.size()]);
                requestObj.setDocumentList(docs);

                for(pt.baieuropa.baieserv.models.PersonPartyInfo.Contact contact : party.getPersonPartyInfo().getPersonData().getContacts()) {
                    if(contact.getLocator().getAddrType() != null) {
                        Contact newContact = new Contact();
                        newContact.setAddressLine1(contact.getLocator().getAddr1());
                        newContact.setAddressLine2(contact.getLocator().getAddr2());
                        newContact.setContactType(contact.getLocator().getAddrType());
                        newContact.setZipCode(contact.getLocator().getPostalCode());
                        contacts.add(newContact);
                    }
                }
                Contact[] contactsArray = contacts.toArray(new Contact[contacts.size()]);
                requestObj.setContactList(contactsArray);
            } else {
                requestObj.setConstitutionDate(party.getOrgPartyInfo().getOrgEstablishDt());
                requestObj.setSocietyType(party.getOrgPartyInfo().getSocietyType());
                requestObj.setSocialCapital(party.getOrgPartyInfo().getSocialCapital());
                // BAIE - Preenchimento dos documentos para Empresas
                for (IssuedIdent issuedIdent : party.getOrgPartyInfo().getOrgData().getIssuedIdent()) {
                    if(issuedIdent.getIssuedIdentType().equals("CAE")) {
                        requestObj.setCae(issuedIdent.getIssuedIdentValue());
                    }
                    if(issuedIdent.getIssuedIdentType().equals("SECONDARY CAE")) {
                        requestObj.setCae2(issuedIdent.getIssuedIdentValue());
                    }
                    if(issuedIdent.getIssuedIdentType().equals("SECTORIAL CODE")) {
                        requestObj.setSectorialCode(issuedIdent.getIssuedIdentValue());
                    }

                    documentsCounter++;

                    if(!issuedIdent.getIssuedIdentType().equals("CAE") &&
                            !issuedIdent.getIssuedIdentType().equals("SECONDARY CAE") &&
                            !issuedIdent.getIssuedIdentType().equals("SECTORIAL CODE")) {

                        Document document = new Document();
                        document.setNumber(issuedIdent.getIssuedIdentValue());
                        document.setDocumentType(issuedIdent.getIssuedIdentType());
                        document.setCreatedAtExt(issuedIdent.getIssueDt());
                        document.setDocumentExpirationDate(issuedIdent.getExpDt());

                        int docsNum = party.getOrgPartyInfo().getOrgData().getIssuedIdent().size();
                        if(docsNum == 1) {
                            document.setIsMainDocument(true);
                        }else if(docsNum > 1) {
                            if(issuedIdent.getIssuedIdentType().charAt(0) == '5') {
                                document.setIsMainDocument(true);
                            } else if(docsNum == documentsCounter) {
                                document.setIsMainDocument(true);
                            }
                        } else {
                            document.setIsMainDocument(false);
                        }
                        documents.add(document);
                    }
                }
                Document[] docs = documents.toArray(new Document[documents.size()]);
                requestObj.setDocumentList(docs);

                for(pt.baieuropa.baieserv.models.OrgPartyInfo.Contact contact : party.getOrgPartyInfo().getOrgData().getContacts()) {
                    if(contact.getLocator().getAddrType() != null) {
                        Contact newContact = new Contact();
                        newContact.setAddressLine1(contact.getLocator().getAddr1());
                        newContact.setAddressLine2(contact.getLocator().getAddr2());
                        newContact.setContactType(contact.getLocator().getAddrType());
                        newContact.setZipCode(contact.getLocator().getPostalCode());
                        contacts.add(newContact);
                    }
                }
                Contact[] contactsArray = contacts.toArray(new Contact[contacts.size()]);
                requestObj.setContactList(contactsArray);

                for(BeneficialOwners item : party.getOrgPartyInfo().getOrgData().getBeneficialOwners()) {
                    EntityBeneficialOwnersList beneficialOwner = new EntityBeneficialOwnersList();
                    beneficialOwner.setEntityType("P");
                    beneficialOwner.setName(item.getName());
                    beneficialOwner.setNumberExt(item.getNumberExt());
                    beneficialOwner.setDocumentNumber(item.getDocumentNumber());
                    beneficialOwner.setDocumentType(item.getDocumentType());
                    beneficialOwner.setPercentageInDirectCompany(item.getPercentageInDirectCompany());

                    beneficialOwners.add(beneficialOwner);
                }

                EntityBeneficialOwnersList[] beneficialOwnersArray = beneficialOwners.toArray(new EntityBeneficialOwnersList[beneficialOwners.size()]);
                requestObj.setBeneficialOwnersList(beneficialOwnersArray);
            }

            // BAIE - Preenchimento de campos coincidentes
            requestObj.setName(isParticular ? party.getPersonPartyInfo().getPersonData().getPersonName().getFullName()
                    : party.getOrgPartyInfo().getOrgData().getOrgName().getName());
            requestObj.setNationality(isParticular ? party.getPersonPartyInfo().getNationality()
                    : party.getOrgPartyInfo().getResidenceCountry().getCountryCodeValue());
            requestObj.setResidenceCode(isParticular ? party.getPersonPartyInfo().getResidenceCountry().getCountryCodeValue()
                    : party.getOrgPartyInfo().getResidenceCountry().getCountryCodeValue());
            requestObj.setCountryResidence(isParticular ? party.getPersonPartyInfo().getTaxResidenceCountry().getCountryCodeValue()
                    : party.getOrgPartyInfo().getTaxResidenceCountry().getCountryCodeValue());

            log.debug("Converting Object to JSON...");
            ObjectWriter ow = new ObjectMapper().writer().withDefaultPrettyPrinter();
            String body = ow.writeValueAsString(requestObj);
            log.debug("Object Converted.\n" + body);

            Response response = null;

            if(!requestObj.getEntityCategory().equals("5")) {
                try {
                    log.debug("Calling WebService " + urlEvaluateEntityOnline);
                    RequestBody requestBody = RequestBody.Companion.create(body, JSON);
                    Request request = new Request.Builder()
                            .url(urlEvaluateEntityOnline)
                            .post(requestBody)
                            .build();

                    response = client.newCall(request).execute();
                    log.debug("Resquest sent successfully!");

                    //BAI recebe resposta do DCS
                    JSONObject responseBody = new JSONObject(response.body().string());
                    log.debug("Response Received: \n" + responseBody.toString() + "\n");

                    if (responseBody.getString("ReturnCode").equals("1")) {
                        log.debug("Evaluating Entity...");
                        processResponseEntityOnline(body, responseBody);
                        log.info("Entity evaluated successfully!");
                    } else {
                        log.error("Return code: " + responseBody.getString("ReturnCode") +
                                "\nError Message: " + responseBody.getString("ErrorMessage"));
                        return 0;
                    }
                    return 1;
                } catch (Exception e) {
                    log.error("Error on Request/Response.\n" + e.getMessage());
                    return 0;
                }
            } else {
                log.warn("Non Client Entity TI (BAIE) detected. Evaluate Entity Online won't process.");
                return 1;
            }
        } catch (Exception ex) {
            log.error("Error while Requesting Entity Creation!\n" + ex.getMessage());
            return 0;
        }
    }

    private void processResponseEntityOnline(String json, JSONObject response) {
        JSONObject request = new JSONObject(json);

        int numberExt = Integer.parseInt(request.getString("Number_Ext"));
        String risk = response.getString("Risk");
        String filteringStatus = response.getString("FilteringStatusValue");

        // BAIE - Executa os 3 fluxos de retorno necessários
        if(response.getBoolean("IsToUnlock")) {
            unlockEntity(numberExt);
        }
        updateEntityRisk(risk, numberExt);
        updateFilteringStatus(filteringStatus, numberExt);
    }

    private int unlockEntity(int numberExt) {
        log.debug("Unlocking Entity...");

        try {
            log.debug("Creating Object...");
            DesactivarBloqueioEntidade obj = new DesactivarBloqueioEntidade();
            ObjectInputDesactivarBloqueioEntidade input = new ObjectInputDesactivarBloqueioEntidade();
            input.setAutoriz("999");
            input.setCodigoNotaBloqueio(0);
            input.setNumeroEntidade(numberExt);
            input.setObservacoes("DCS");
            input.setSiglaParaMensagemAlerta("AML");
            input.setTipoBloqueio("A");
            input.setUtilizador("TYIUSER");
            input.setValidarOuAplicar("A");
            obj.setDados(input);
            log.debug("Object Created.");

            log.debug("Converting Object to JSON...");
            ObjectWriter ow = new ObjectMapper().writer().withDefaultPrettyPrinter();
            String result = ow.writeValueAsString(obj);
            log.debug("Object Converted.");

            log.debug("Publishing Message on NATS...");
            ServiceApp.getProc().publish("UnlockEntity:Request", result);
            log.debug("Message Published.");

            log.info("Entity unlocked successfully!\n" + result);
            return 1;
        } catch (Exception ex) {
            log.error("Error while unlocking entity! \n" + ex.getMessage());
            return 0;
        }
    }

    private int updateEntityRisk(String risk, int numberExt) {
        log.debug("Updating Entity Risk...");

        try {
            log.debug("Creating Object...");
            ActualizacaoNivelRiscoEntidade obj = new ActualizacaoNivelRiscoEntidade();
            ObjectActualizacaoNivelRiscoEntidadeInput input = new ObjectActualizacaoNivelRiscoEntidadeInput();
            input.setCodigoElemento("NRE");
            input.setNovoValor(risk);
            input.setNumeroEntidade(numberExt);
            input.setUtilizador("TYIUSER");
            obj.setDados(input);
            log.debug("Object Created.");

            log.debug("Converting Object to JSON...");
            ObjectWriter ow = new ObjectMapper().writer().withDefaultPrettyPrinter();
            String result = ow.writeValueAsString(obj);
            log.debug("Object Converted.");

            log.debug("Publishing Message on NATS...");
            ServiceApp.getProc().publish("UpdateEntityRisk:Request", result);
            log.debug("Message Published.");

            log.info("Entity risk updated successfully!\n" + result);
            return 1;
        } catch(Exception ex) {
            log.error("Error while updating entity risk! \n" + ex.getMessage() + "\n");
            return 0;
        }
    }

    private int updateFilteringStatus(String filteringStatus, int numberExt) {
        log.debug("Updating Filtering Status...");

        try {
            log.debug("Creating Object...");
            AtualizaEstadoFiltering obj = new AtualizaEstadoFiltering();
            ObjectAtualizaEstadoFilteringInput input = new ObjectAtualizaEstadoFilteringInput();
            input.setCodigoElemento("EF");
            input.setNovoValor(filteringStatus);
            input.setNumeroEntidade(numberExt);
            input.setUtilizador("TYIUSER");
            obj.setDados(input);
            log.debug("Object Created.");

            log.debug("Converting Object to JSON...");
            ObjectWriter ow = new ObjectMapper().writer().withDefaultPrettyPrinter();
            String result = ow.writeValueAsString(obj);
            log.debug("Object Converted.");

            log.debug("Publishing Message on NATS...");
            ServiceApp.getProc().publish("UpdateFiltering:Request", result);
            log.debug("Message Published.");

            log.info("Filtering status updated successfully!\n" + result);
            return 1;
        } catch(Exception ex) {
            log.error("Error while updating filtering status! \n" + ex.getMessage() + "\n");
            return 0;
        }
    }
}
