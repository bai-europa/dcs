//package pt.baieuropa.baieserv.service.interceptors;
//
//import org.apache.tomcat.util.http.fileupload.IOUtils;
//import org.springframework.stereotype.Component;
//import org.springframework.web.servlet.HandlerInterceptor;
//import org.springframework.web.servlet.ModelAndView;
//import org.springframework.web.servlet.handler.HandlerInterceptorAdapter;
//
//import javax.servlet.http.HttpServletRequest;
//import javax.servlet.http.HttpServletResponse;
//import java.io.BufferedReader;
//import java.io.IOException;
//import java.io.InputStream;
//import java.io.InputStreamReader;
//import java.util.stream.Collectors;
//
//@Component
//public class EntityControllerInterceptor extends HandlerInterceptorAdapter {
//
//    @Override
//    public boolean preHandle(
//            HttpServletRequest request,
//            HttpServletResponse response,
//            Object handler) throws IOException {
////        System.out.println("PRE HANDLE: " + getBody(request));
//        String body = request.getReader().lines().collect(Collectors.joining("\n"));
//        System.out.println("PRE HANDLE: " + body);
//        return true;
//    }
//
//}
