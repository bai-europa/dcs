package pt.baieuropa.baieserv.service.controllers;

import pt.baieuropa.baieserv.processor.Process;

public class ControllersManager {

    private static String SERVICENAME = "SERVICE";

    private static Process proc = new Process(SERVICENAME);

    public static Process getProc() {
        return proc;
    }
}
