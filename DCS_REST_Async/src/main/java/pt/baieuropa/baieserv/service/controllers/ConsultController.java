package pt.baieuropa.baieserv.service.controllers;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.tags.Tag;
import okhttp3.*;
import org.json.JSONArray;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import pt.baieuropa.baieserv.models.Consults.ConsultBlock;
import pt.baieuropa.baieserv.service.managers.APIConnectSecurityManager;
import pt.baieuropa.baieserv.utils.ConfigUtils;
import pt.baieuropa.storage.WSDLPojos.Consultas.ConsultarBloqueiosContaOuEntidade;

import javax.validation.Valid;
import java.util.*;

@RestController
@RequestMapping({"/aml-resolution"})
@Tag(name = "Consult", description = "The Consult API")
@Validated
public class ConsultController {

    private static final Logger log = LoggerFactory.getLogger(ConsultController.class);

    ObjectMapper mapper = new ObjectMapper();

    public static Properties config = ConfigUtils.loadProps(System.getenv("URLS-CONFIG"));
    private String urlPartyV1 = config.getProperty("partyV1");
    private String urlCurrentAccounts = config.getProperty("currentAccounts");

    public static Properties config2 = ConfigUtils.loadProps(System.getenv("SERVICES-CONFIG"));
    private String clientID = config2.getProperty("clientID");
    private String clientSecret = config2.getProperty("clientSecret");

    @Operation(summary = "Consults All Account or Entity Locks", description = "All required fields must be filled.", tags = { "Consult" })
    @RequestMapping(value = {"/locks"}, method = {RequestMethod.POST})
    public ConsultBlock[] consultLocks(@Valid @RequestBody ConsultarBloqueiosContaOuEntidade requestBody) {

        try {
            log.debug("Executing [ consultLocks ] API...");

            log.debug("JSON Received: " + new ObjectMapper().writer().withDefaultPrettyPrinter().writeValueAsString(requestBody));

            log.debug("Creating Object...");
            ConsultarBloqueiosContaOuEntidade obj = requestBody;
            log.debug("Object Created.");

            System.setProperty("javax.net.ssl.trustStore", "NULL");
            System.setProperty("javax.net.ssl.trustStoreType", "Windows-ROOT");

            if(!obj.getAccountNumber_Ext().equals("")) {
                return consultAccountLocks(obj.getAccountNumber_Ext());
            } else {
                return consultPartyLocks(obj.getEntityNumber_Ext());
            }

        } catch (Exception ex) {
            try {
                log.error("JSON Received: " + new ObjectMapper().writer().withDefaultPrettyPrinter().writeValueAsString(requestBody));
            } catch (JsonProcessingException e) {
                e.printStackTrace();
            }
            log.error("Error on [ consultLocks ] API: \n" + ex.getMessage());
            return new ConsultBlock[0];
        }
    }

    private ConsultBlock[] consultAccountLocks(String accountNumber) {

        Response response = null;
        OkHttpClient client = APIConnectSecurityManager.processSecurityProtocols();

        try {
            log.debug("Calling WebService " + urlCurrentAccounts + accountNumber + "/locks");

            Request request = new Request.Builder()
                    .addHeader("X-IBM-Client-Id", clientID)
                    .addHeader("X-IBM-Client-Secret", clientSecret)
                    .url(urlCurrentAccounts + accountNumber + "/locks")
                    .build();
            response = client.newCall(request).execute();
            log.debug("Resquest sent successfully!");

            // BAIE - recebe resposta do DCS
            JSONArray responseBody = new JSONArray(response.body().string());
            log.debug("Response Received: \n" + responseBody.toString() + "\n");

            ConsultBlock[] finalObject = mapper.readValue(responseBody.toString(), ConsultBlock[].class);

            log.info("API [ consultLocks ] Executed.");
            return finalObject;
        } catch (Exception e) {
            log.error("Error on Request/Response.\n" + e.getMessage());
            return new ConsultBlock[0];
        }
    }

    private ConsultBlock[] consultPartyLocks(String partyID) {

        Response response = null;
        OkHttpClient client = APIConnectSecurityManager.processSecurityProtocols();

        try {
            log.debug("Calling WebService " + urlPartyV1 + partyID + "/locks");

            Request request = new Request.Builder()
                    .addHeader("X-IBM-Client-Id", clientID)
                    .addHeader("X-IBM-Client-Secret", clientSecret)
                    .url(urlPartyV1 + partyID + "/locks")
                    .build();

            response = client.newCall(request).execute();
            log.debug("Resquest sent successfully!");

            JSONArray responseBody = new JSONArray(response.body().string());
            log.debug("Response Received: \n" + responseBody.toString() + "\n");

            ConsultBlock[] finalObject = mapper.readValue(responseBody.toString(), ConsultBlock[].class);

            log.info("API [ consultLocks ] Executed.");
            return finalObject;
        } catch (Exception e) {
            log.error("Error on Request/Response.\n" + e.getMessage());
            return new ConsultBlock[0];
        }
    }

}
