//package pt.baieuropa.baieserv.service.interceptors;
//
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.context.annotation.Configuration;
//import org.springframework.web.servlet.config.annotation.InterceptorRegistry;
//import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;
//
//@Configuration
//public class EntityControllerConfig implements WebMvcConfigurer {
//
//    @Autowired
//    private EntityControllerInterceptor entityControllerInterceptor;
//
//    @Override
//    public void addInterceptors(InterceptorRegistry registry) {
//        registry.addInterceptor(entityControllerInterceptor)
//                .addPathPatterns("/aml-resolution/entity/**");
//    }
//}
