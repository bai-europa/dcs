package pt.baieuropa.baieserv.service.controllers;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.ObjectWriter;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.tags.Tag;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;
import pt.baieuropa.baieserv.ServiceApp;
import pt.baieuropa.baieserv.models.LetterOfCreditUnlock;
import pt.baieuropa.storage.WSDLPojos.DesactivarBloqueioTransacao.DesactivarBloqueioTransacao;
import pt.baieuropa.storage.WSDLPojos.DesactivarBloqueioTransacao.ObjectInputDesactivarBloqueioTransacao;

import javax.validation.Valid;

@RestController
@RequestMapping(value = "/aml-resolution")
@Tag(name = "Transaction", description = "The Transaction API")
@Validated
public class TransactionController {

    private static final Logger log = LoggerFactory.getLogger(EntityController.class);

    @Operation(summary = "Unlocks the specified transaction", description = "All required fields must be filled.", tags = { "Transaction" })
    @RequestMapping(value = {"/transaction/unlock"}, method = {RequestMethod.POST})
    public int unlockTransaction(@Valid @RequestBody ObjectInputDesactivarBloqueioTransacao requestBody) {

        try {
            log.debug("Executing [ unlockTransaction ] API...");

            log.debug("JSON Received: " + new ObjectMapper().writer().withDefaultPrettyPrinter().writeValueAsString(requestBody));

            log.debug("Creating Object...");
            DesactivarBloqueioTransacao obj = new DesactivarBloqueioTransacao();
            ObjectInputDesactivarBloqueioTransacao input = requestBody;
            obj.setDados(input);
            log.debug("Object Created.");

            String topic = obj.getDados().getRequestId().startsWith("LOC")
                    ? "LettersOfCredit:Response"
                    : "UnlockTransaction:Request";

            log.debug("Converting Object to JSON...");
            ObjectWriter ow = new ObjectMapper().writer().withDefaultPrettyPrinter();
            String result = ow.writeValueAsString(obj.getDados());
            log.debug("Object Converted.");

            log.debug("Publishing Message on NATS...");
            ServiceApp.getProc().publish(topic, result);
            log.debug("Message Published.");

            log.info("API [ unlockTransaction ] Executed.\n" + result);
            return 1;
        } catch (Exception ex) {
            try {
                log.error("JSON Received: " + new ObjectMapper().writer().withDefaultPrettyPrinter().writeValueAsString(requestBody));
            } catch (JsonProcessingException e) {
                e.printStackTrace();
            }
            log.error("Error on [ unlockTransaction ] API: \n" + ex.getMessage());
            return 0;
        }
    }

    @Operation(summary = "Request all accumulated by a single date (yyyyMMdd).", description = "All required fields must be filled.", tags = { "Transaction" })
    @RequestMapping(value = {"/accumulated"}, method = {RequestMethod.POST})
    public int accumulated(@Valid String date) {

        try {
            log.debug("Executing [ accumulated ] API...");

            log.debug("Date Received: " + date);

            log.debug("Publishing Message on NATS...");
            ServiceApp.getProc().publish("Accumulated:Request", date);
            log.debug("Message Published.");

            log.info("API [ unlockTransaction ] Executed.");
            return 1;
        } catch (Exception ex) {
            log.error("Date Received: " + date);
            log.error("Error on [ accumulated ] API: \n" + ex.getMessage());
            return 0;
        }
    }

    @Operation(summary = "Unlocks a Letter of Credit", description = "All required fields must be filled.", tags = { "Transaction" })
    @RequestMapping(value = {"/letter-of-credit/unlock"}, method = {RequestMethod.POST})
    public int unlockLetterOfCredit(@RequestBody LetterOfCreditUnlock requestBody) {
        try{
            log.debug("Executing [ unlockLetterOfCredit ] API...");

            log.debug("JSON Received: " + new ObjectMapper().writer().withDefaultPrettyPrinter().writeValueAsString(requestBody));

            log.debug("Creating Object...");
            LetterOfCreditUnlock obj = requestBody;
            log.debug("Object Created.");

            log.debug("Converting Object to JSON...");
            ObjectWriter ow = new ObjectMapper().writer().withDefaultPrettyPrinter();
            String result = ow.writeValueAsString(obj);
            log.debug("Object Converted.");

            log.debug("Publishing Message on NATS...");
            ServiceApp.getProc().publish("UnlockLetterOfCredit:Request", result);
            log.debug("Message Published.");

            log.info("API [ unlockLetterOfCredit ] Executed.\n" + result);
            return 1;
        } catch (Exception ex) {
            try {
                log.error("JSON Received: " + new ObjectMapper().writer().withDefaultPrettyPrinter().writeValueAsString(requestBody));
            } catch (JsonProcessingException e) {
                e.printStackTrace();
            }
            log.error("Error on [ unlockLetterOfCredit ] API: \n" + ex.getMessage());
            return 0;
        }
    }
}
