/*
  Created by: loliveira
*/

package pt.baieuropa.baieserv;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import pt.baieuropa.baieserv.processor.Process;
// import pt.baieuropa.baieserv.thread.MyThread;

@Configuration
@EnableAutoConfiguration
@ComponentScan
@ComponentScan({"pt.baieuropa.baieserv.service.controllers"}) // TODO -  adicionar - , "pt.baieuropa.baieserv.thread"
public class ServiceApp {

    public static Process proc;

    public static void main(String[] args) {
        SpringApplication.run(ServiceApp.class, args);
        proc = new Process("SERVICE");

        // MyThread.main();
    }

    public static Process getProc() {
        return proc;
    }

}
