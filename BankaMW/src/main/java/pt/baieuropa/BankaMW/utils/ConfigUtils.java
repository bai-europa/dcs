package pt.baieuropa.BankaMW.utils;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;
import java.util.logging.Level;
import java.util.logging.Logger;


public class ConfigUtils {
	
	private static Properties config;

	public static Properties loadProps(String fileName) {
		try {
			config = new Properties();

			InputStream inputStream = new FileInputStream(fileName);
			if (inputStream != null) {

				config.load(inputStream);

			} else {
				throw new FileNotFoundException("property file '" + fileName + "' not found in the classpath");

			}
		} catch (IOException ex) {
			Logger.getLogger(ConfigUtils.class.getName()).log(Level.SEVERE, null, ex);
		}
		return config;
	}

}
