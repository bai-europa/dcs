/*
  Created by: loliveira
*/

package pt.baieuropa.BankaMW.exceptions;

public class MissingParametersException extends Exception {

    public MissingParametersException(String input){
        super(input);
    }
}
