/*
  Created by: loliveira
*/

package pt.baieuropa.BankaMW.exceptions;

public class NatsRequestException extends Exception {

    public NatsRequestException(String input){
        super(input);
    }
}
