/*
  Created by: loliveira
*/

package pt.baieuropa.BankaMW.exceptions;

public class ConfigurationException extends Exception {

    public ConfigurationException(String message){
        super(message);
    }

}
