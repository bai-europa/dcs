/*
  Created by: loliveira
*/

package pt.baieuropa.BankaMW.processor;

import org.apache.log4j.Logger;
import org.yaml.snakeyaml.Yaml;
import pt.baieuropa.BankaMW.exceptions.MissingParametersException;
import pt.baieuropa.BankaMW.exceptions.NatsRequestException;
import pt.baieuropa.nats.Stan;

import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.security.InvalidParameterException;
import java.util.Map;
import java.util.concurrent.TimeoutException;

public class NatsController {

    private static Logger log = Logger.getLogger(NatsController.class.getName());

    private Stan stanController;

    private String hostname_String;
    private String port_String;
    private String client_ID;
    private String cluster_ID;
    private String parameterError;

    public boolean initializeNats(String serviceName) {

        try {
            stanController = new Stan();

            readPropertiesFromYAML(System.getenv("nats.configuration"));

            stanController.connectToNats(this.hostname_String, this.port_String, cluster_ID,client_ID);

            try {
                // BAIE - Zona de subscribes do NATS Streaming
            } catch (Exception ex) {
                ex.printStackTrace();
            }

            return true;
        } catch (MissingParametersException | IOException | InterruptedException | TimeoutException e) {
            log.error(e.getMessage());
            return false;
        }
    }

    public boolean insertRequest(String subject, String content) throws NatsRequestException {
        try {
            stanController.insertRequestOnTopic(subject, content);
            return true;
        } catch (IOException | TimeoutException | InterruptedException e) {
            log.error(e.getMessage());
            throw new NatsRequestException(e.getMessage());
        } finally {
//            log.info("Number of requests on topic "+ topic + " is : "+ stanController.getPendingRequestsFromTopic(topic));
        }
    }

    private void readPropertiesFromYAML(String AS400YAMLFileName) throws MissingParametersException, InvalidParameterException {

        // TODO change this to a method
        try {
            Yaml yaml = new Yaml();
            InputStream inputStream = new FileInputStream(AS400YAMLFileName);
            Map<String, Object> yamlObject = yaml.load(inputStream);

            yamlObject.forEach((name, value) -> {

                switch ((String) name) {
                    case "hostname_String":
                        hostname_String = (String) value;
                        break;
                    case "port_String":
                        port_String = (String) value;
                        break;
                    case "cluster_ID":
                        cluster_ID = (String) value;
                        break;
                    case "client_ID":
                        client_ID = (String) value;
                        break;
                    default:
                        parameterError = (String) name;
                        break;
                }

            });
        } catch (Exception ex) {
            log.error(ex.getMessage());
        } finally {
            if (parameterError != null)
                throw new InvalidParameterException("The parameter " + parameterError + " is not recognized by the application. (hostname_String, port_String)");

            if ((hostname_String == null) || (port_String == null))
                throw new MissingParametersException("The YAML file with the configuration to the AS400 is missing one or more parameters");
        }
    }

}
