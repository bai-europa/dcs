/*
  Created by: loliveira
*/

package pt.baieuropa.BankaMW.processor.database;

import com.mongodb.client.MongoClients;
import com.mongodb.client.MongoDatabase;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.MongoDbFactory;
import org.springframework.data.mongodb.core.MongoOperations;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.SimpleMongoClientDbFactory;
import org.springframework.data.mongodb.core.convert.DefaultMongoTypeMapper;
import org.springframework.data.mongodb.core.convert.MappingMongoConverter;
import org.springframework.data.mongodb.core.mapping.MongoMappingContext;

public class MongoDBManager {

    private SimpleMongoClientDbFactory mongoClientFactory;
    private MongoDatabase database;
    private MongoOperations mongoOps;
    private static Logger log = Logger.getLogger(MongoDBManager.class.getName());
    @Autowired
    MongoDbFactory mongoDbFactory;
    @Autowired
    MongoMappingContext mongoMappingContext;

    public MongoDBManager() {
        mongoClientFactory = new SimpleMongoClientDbFactory(MongoClients.create("mongodb://localhost"), "servicesaml");
        mongoOps = mongoTemplate(); //Call our configuration
//      mongoOps = new MongoTemplate(MongoClients.create(), "services");
    }

    public MongoTemplate mongoTemplate() {

        //remove _class
        MappingMongoConverter converter = new MappingMongoConverter(mongoClientFactory, new MongoMappingContext());
        converter.setTypeMapper(new DefaultMongoTypeMapper(null));

        MongoTemplate mongoTemplate = new MongoTemplate(mongoClientFactory, converter);

        return mongoTemplate;
    }

    public void insertOnCollection(String collection, Object object) {
        mongoOps.save(object, collection); //Save da estrutura na collection definida
    }
}
