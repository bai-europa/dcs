/*
  Created by: loliveira
*/

package pt.baieuropa.BankaMW.processor;


import org.apache.log4j.Logger;

public class Process implements Processor {

    private static Logger log = Logger.getLogger(Process.class.getName());

    public static NatsController natsController;


    /**
     * If the service is Asynchronous it will subscribe a NATS queue
     *
     * @param serviceName
     */
    public Process(String serviceName) {
        natsController = new NatsController();

        natsController.initializeNats(serviceName);
    }

    public Process() {}

    @Override
    public boolean initializeService() {
        return false;
    }

    @Override
    public boolean controlledStopService() {
        return false;
    }

    @Override
    public boolean forceStopService() {
        return false;
    }

    /**
     * Publish to nats
     * @param subject
     * @param content
     * @return
     */
    public String publish(String subject, String content) {
        try {
            // BAIE - Sends Approved JSON Message
            natsController.insertRequest(subject, content);
            return "OK";
        } catch (Exception e) {
            log.error("Erro a publicar mensagem: \n" + e.getMessage());
            return "NOT OK";
        }
    }
}
