
package pt.baieuropa.BankaMW.models.Evaluate;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for WSResponseEntityOnline complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="WSResponseEntityOnline">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="ReturnCode" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="ErrorMessage" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="FilteringStatusValue" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="FilteringCategories" type="{http://www.outsystems.com}ArrayOfCategory" minOccurs="0"/>
 *         &lt;element name="Risk" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="IsToUnlock" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         &lt;element name="AMLDecision" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="ListOfFieldValidates" type="{http://www.outsystems.com}ArrayOfValidateService" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "WSResponseEntityOnline", namespace = "http://www.outsystems.com", propOrder = {
    "returnCode",
    "errorMessage",
    "filteringStatusValue",
    "filteringCategories",
    "risk",
    "isToUnlock",
    "amlDecision",
    "listOfFieldValidates"
})
public class WSResponseEntityOnline {

    @XmlElement(name = "ReturnCode", namespace = "http://www.outsystems.com")
    protected String returnCode;
    @XmlElement(name = "ErrorMessage", namespace = "http://www.outsystems.com")
    protected String errorMessage;
    @XmlElement(name = "FilteringStatusValue", namespace = "http://www.outsystems.com")
    protected String filteringStatusValue;
    @XmlElement(name = "FilteringCategories", namespace = "http://www.outsystems.com")
    protected ArrayOfCategory filteringCategories;
    @XmlElement(name = "Risk", namespace = "http://www.outsystems.com")
    protected String risk;
    @XmlElement(name = "IsToUnlock", namespace = "http://www.outsystems.com")
    protected boolean isToUnlock;
    @XmlElement(name = "AMLDecision", namespace = "http://www.outsystems.com")
    protected String amlDecision;
    @XmlElement(name = "ListOfFieldValidates", namespace = "http://www.outsystems.com")
    protected ArrayOfValidateService listOfFieldValidates;

    /**
     * Gets the value of the returnCode property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getReturnCode() {
        return returnCode;
    }

    /**
     * Sets the value of the returnCode property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setReturnCode(String value) {
        this.returnCode = value;
    }

    /**
     * Gets the value of the errorMessage property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getErrorMessage() {
        return errorMessage;
    }

    /**
     * Sets the value of the errorMessage property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setErrorMessage(String value) {
        this.errorMessage = value;
    }

    /**
     * Gets the value of the filteringStatusValue property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getFilteringStatusValue() {
        return filteringStatusValue;
    }

    /**
     * Sets the value of the filteringStatusValue property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setFilteringStatusValue(String value) {
        this.filteringStatusValue = value;
    }

    /**
     * Gets the value of the filteringCategories property.
     * 
     * @return
     *     possible object is
     *     {@link ArrayOfCategory }
     *     
     */
    public ArrayOfCategory getFilteringCategories() {
        return filteringCategories;
    }

    /**
     * Sets the value of the filteringCategories property.
     * 
     * @param value
     *     allowed object is
     *     {@link ArrayOfCategory }
     *     
     */
    public void setFilteringCategories(ArrayOfCategory value) {
        this.filteringCategories = value;
    }

    /**
     * Gets the value of the risk property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getRisk() {
        return risk;
    }

    /**
     * Sets the value of the risk property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setRisk(String value) {
        this.risk = value;
    }

    /**
     * Gets the value of the isToUnlock property.
     * 
     */
    public boolean isIsToUnlock() {
        return isToUnlock;
    }

    /**
     * Sets the value of the isToUnlock property.
     * 
     */
    public void setIsToUnlock(boolean value) {
        this.isToUnlock = value;
    }

    /**
     * Gets the value of the amlDecision property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getAMLDecision() {
        return amlDecision;
    }

    /**
     * Sets the value of the amlDecision property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setAMLDecision(String value) {
        this.amlDecision = value;
    }

    /**
     * Gets the value of the listOfFieldValidates property.
     * 
     * @return
     *     possible object is
     *     {@link ArrayOfValidateService }
     *     
     */
    public ArrayOfValidateService getListOfFieldValidates() {
        return listOfFieldValidates;
    }

    /**
     * Sets the value of the listOfFieldValidates property.
     * 
     * @param value
     *     allowed object is
     *     {@link ArrayOfValidateService }
     *     
     */
    public void setListOfFieldValidates(ArrayOfValidateService value) {
        this.listOfFieldValidates = value;
    }

}
