
package pt.baieuropa.BankaMW.models.Evaluate;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for EntityAccount complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="EntityAccount">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="EntityNumber_Ext" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="EntityAccountRelationType" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "EntityAccount", namespace = "http://www.outsystems.com", propOrder = {
    "entityNumberExt",
    "entityAccountRelationType"
})
public class EntityAccount {

    @XmlElement(name = "EntityNumber_Ext", namespace = "http://www.outsystems.com")
    protected String entityNumberExt;
    @XmlElement(name = "EntityAccountRelationType", namespace = "http://www.outsystems.com")
    protected String entityAccountRelationType;

    /**
     * Gets the value of the entityNumberExt property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getEntityNumberExt() {
        return entityNumberExt;
    }

    /**
     * Sets the value of the entityNumberExt property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setEntityNumberExt(String value) {
        this.entityNumberExt = value;
    }

    /**
     * Gets the value of the entityAccountRelationType property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getEntityAccountRelationType() {
        return entityAccountRelationType;
    }

    /**
     * Sets the value of the entityAccountRelationType property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setEntityAccountRelationType(String value) {
        this.entityAccountRelationType = value;
    }

}
