
package pt.baieuropa.BankaMW.models.Evaluate;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for ArrayOfEntityAccount complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="ArrayOfEntityAccount">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="EntityAccount" type="{http://www.outsystems.com}EntityAccount" maxOccurs="unbounded" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ArrayOfEntityAccount", namespace = "http://www.outsystems.com", propOrder = {
    "entityAccount"
})
public class ArrayOfEntityAccount {

    @XmlElement(name = "EntityAccount", namespace = "http://www.outsystems.com", nillable = true)
    protected EntityAccount[] entityAccount;



    /**
     * Gets the value of the entityAccount property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the entityAccount property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getEntityAccount().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link EntityAccount }
     * 
     * 
     */
    public EntityAccount[] getEntityAccount() {
        return entityAccount;
    }

    public void setEntityAccount(EntityAccount[] entityAccount) {
        this.entityAccount = entityAccount;
    }
}
