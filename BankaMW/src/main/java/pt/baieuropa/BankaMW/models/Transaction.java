package pt.baieuropa.BankaMW.models;

public class Transaction {

    private String transaction;
    private String createOrUpdate;
    private boolean isSEPA;

    public Transaction() {
    }

    public Transaction(String transaction, String createOrUpdate, boolean isSEPA) {
        this.transaction = transaction;
        this.createOrUpdate = createOrUpdate;
        this.isSEPA = isSEPA;
    }

    public String getTransaction() {
        return transaction;
    }

    public void setTransaction(String transaction) {
        this.transaction = transaction;
    }

    public String getCreateOrUpdate() {
        return createOrUpdate;
    }

    public void setCreateOrUpdate(String createOrUpdate) {
        this.createOrUpdate = createOrUpdate;
    }

    public boolean isSEPA() {
        return isSEPA;
    }

    public void setSEPA(boolean SEPA) {
        isSEPA = SEPA;
    }
}
