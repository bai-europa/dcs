package pt.baieuropa.BankaMW.models;

public class Entity {

    private String transaction;
    private String createOrUpdate;
    private boolean isEntityOnline;
    private boolean isDocument;
    private boolean isAddress;
    private boolean isParticipant;

    public Entity() { }

    public Entity(String transaction, String createOrUpdate, boolean isEntityOnline, boolean isDocument, boolean isAddress, boolean isParticipant) {
        this.transaction = transaction;
        this.createOrUpdate = createOrUpdate;
        this.isEntityOnline = isEntityOnline;
        this.isDocument = isDocument;
        this.isAddress = isAddress;
        this.isParticipant = isParticipant;
    }

    public String getTransaction() {
        return transaction;
    }

    public void setTransaction(String transaction) {
        this.transaction = transaction;
    }

    public String getCreateOrUpdate() {
        return createOrUpdate;
    }

    public void setCreateOrUpdate(String createOrUpdate) {
        this.createOrUpdate = createOrUpdate;
    }

    public boolean isEntityOnline() {
        return isEntityOnline;
    }

    public void setEntityOnline(boolean entityOnline) {
        isEntityOnline = entityOnline;
    }

    public boolean isDocument() {
        return isDocument;
    }

    public void setDocument(boolean document) {
        isDocument = document;
    }

    public boolean isAddress() {
        return isAddress;
    }

    public void setAddress(boolean address) {
        isAddress = address;
    }

    public boolean isParticipant() {
        return isParticipant;
    }

    public void setParticipant(boolean participant) {
        isParticipant = participant;
    }
}
