package pt.baieuropa.BankaMW.models.Evaluate;

public class BankaTransactionRequest {

    String dataStructure;
    String content;
    String timestamp;

    public BankaTransactionRequest() {
    }

    public BankaTransactionRequest(String dataStructure, String content) {
        this.dataStructure = dataStructure;
        this.content = content;
    }

    public String getDataStructure() {
        return dataStructure;
    }

    public void setDataStructure(String dataStructure) {
        this.dataStructure = dataStructure;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public String getTimestamp() {
        return timestamp;
    }

    public void setTimestamp(String timestamp) {
        this.timestamp = timestamp;
    }
}
