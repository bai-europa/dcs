
package pt.baieuropa.BankaMW.models.Evaluate;

import javax.xml.bind.annotation.*;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="AccountCreationRequest" type="{http://www.outsystems.com}AccountCreationRequest" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "accountCreationRequest"
})
@XmlRootElement(name = "EvaluateAccountOnline", namespace = "http://www.outsystems.com")
public class EvaluateAccountOnline {

    @XmlElement(name = "AccountCreationRequest", namespace = "http://www.outsystems.com")
    protected AccountCreationRequest accountCreationRequest;

    /**
     * Gets the value of the accountCreationRequest property.
     * 
     * @return
     *     possible object is
     *     {@link AccountCreationRequest }
     *     
     */
    public AccountCreationRequest getAccountCreationRequest() {
        return accountCreationRequest;
    }

    /**
     * Sets the value of the accountCreationRequest property.
     * 
     * @param value
     *     allowed object is
     *     {@link AccountCreationRequest }
     *     
     */
    public void setAccountCreationRequest(AccountCreationRequest value) {
        this.accountCreationRequest = value;
    }

}
