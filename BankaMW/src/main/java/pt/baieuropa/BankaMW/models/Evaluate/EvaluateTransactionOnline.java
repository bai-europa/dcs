package pt.baieuropa.BankaMW.models.Evaluate;

import com.fasterxml.jackson.annotation.JsonProperty;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;
import java.math.BigDecimal;

@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "EntityEvaluationRequest", namespace = "http://www.outsystems.com", propOrder = {
    "AccountNumber_Ext",
    "TransactionNumber",
    "TransactionOperationCode",
    "TransactionAmount_Original",
    "TransactionCurrency_Original",
    "TransactionAmount_Converted",
    "TransactionAmount_Account",
    "TransactionCurrency_Account",
    "TransactionDateTime",
    "MovementLaunchDateTime",
    "CreditDebit",
    "CorrespondingBankCode",
    "CounterpartBankCode",
    "CounterpartName",
    "CounterpartCountryCode",
    "ClientNumber_Ext",
    "CounterpartAccountNumber",
    "TransactionType",
    "TransactionStatus",
    "CAE",
    "CAE2",
    "Branch",
    "Field70",
    "RequestUser",
    "Flexfields"
})
public class EvaluateTransactionOnline {

    @XmlElement(name = "AccountNumber_Ext", required = true)
    @JsonProperty("AccountNumber_Ext")
    String AccountNumber_Ext;
    @XmlElement(name = "TransactionNumber", required = true)
    @JsonProperty("TransactionNumber")
    String TransactionNumber;
    @XmlElement(name = "TransactionOperationCode", required = true)
    @JsonProperty("TransactionOperationCode")
    String TransactionOperationCode;
    @XmlElement(name = "TransactionAmount_Original", required = true)
    @JsonProperty("TransactionAmount_Original")
    BigDecimal TransactionAmount_Original;
    @XmlElement(name = "TransactionCurrency_Original", required = true)
    @JsonProperty("TransactionCurrency_Original")
    String TransactionCurrency_Original;
    @XmlElement(name = "TransactionAmount_Converted", required = true)
    @JsonProperty("TransactionAmount_Converted")
    BigDecimal TransactionAmount_Converted;
    @XmlElement(name = "TransactionAmount_Account")
    @JsonProperty("TransactionAmount_Account")
    BigDecimal TransactionAmount_Account = new BigDecimal(0);
    @XmlElement(name = "TransactionCurrency_Account")
    @JsonProperty("TransactionCurrency_Account")
    String TransactionCurrency_Account = "";
    @XmlElement(name = "TransactionDateTime", required = true)
    @JsonProperty("TransactionDateTime")
    String TransactionDateTime;
    @XmlElement(name = "MovementLaunchDateTime", required = true)
    @JsonProperty("MovementLaunchDateTime")
    String MovementLaunchDateTime;
    @XmlElement(name = "CreditDebit", required = true)
    @JsonProperty("CreditDebit")
    String CreditDebit;
    @XmlElement(name = "CorrespondingBankCode")
    @JsonProperty("CorrespondingBankCode")
    String CorrespondingBankCode = "";
    @XmlElement(name = "CounterpartBankCode")
    @JsonProperty("CounterpartBankCode")
    String CounterpartBankCode = "";
    @XmlElement(name = "CounterpartName")
    @JsonProperty("CounterpartName")
    String CounterpartName = "";
    @XmlElement(name = "CounterpartCountryCode")
    @JsonProperty("CounterpartCountryCode")
    String CounterpartCountryCode = "";
    @XmlElement(name = "ClientNumber_Ext")
    @JsonProperty("ClientNumber_Ext")
    String ClientNumber_Ext = "";
    @XmlElement(name = "CounterpartAccountNumber")
    @JsonProperty("CounterpartAccountNumber")
    String CounterpartAccountNumber = "";
    @XmlElement(name = "TransactionType", required = true)
    @JsonProperty("TransactionType")
    String TransactionType;
    @XmlElement(name = "TransactionStatus", required = true)
    @JsonProperty("TransactionStatus")
    String TransactionStatus;
    @XmlElement(name = "CAE")
    @JsonProperty("CAE")
    String CAE = "";
    @XmlElement(name = "CAE2")
    @JsonProperty("CAE2")
    String CAE2 = "";
    @XmlElement(name = "Branch")
    @JsonProperty("Branch")
    String Branch = "";
    @XmlElement(name = "Field70")
    @JsonProperty("Field70")
    String Field70 = "";
    @XmlElement(name = "RequestUser")
    @JsonProperty("RequestUser")
    String RequestUser = "";
    @XmlElement(name = "Flexfields")
    @JsonProperty("Flexfields")
    FlexfieldStructure[] Flexfields = new FlexfieldStructure[0];

    public EvaluateTransactionOnline() {
    }

    public EvaluateTransactionOnline(String accountNumber_Ext, String transactionNumber, String transactionOperationCode, BigDecimal transactionAmount_Original, String transactionCurrency_Original, BigDecimal transactionAmount_Converted, BigDecimal transactionAmount_Account, String transactionCurrency_Account, String transactionDateTime, String movementLaunchDateTime, String creditDebit, String correspondingBankCode, String counterpartBankCode, String counterpartName, String counterpartCountryCode, String clientNumber_Ext, String counterpartAccountNumber, String transactionType, String transactionStatus, String CAE, String CAE2, String branch, String field70, String requestUser, FlexfieldStructure[] flexfields) {
        AccountNumber_Ext = accountNumber_Ext;
        TransactionNumber = transactionNumber;
        TransactionOperationCode = transactionOperationCode;
        TransactionAmount_Original = transactionAmount_Original;
        TransactionCurrency_Original = transactionCurrency_Original;
        TransactionAmount_Converted = transactionAmount_Converted;
        TransactionAmount_Account = transactionAmount_Account;
        TransactionCurrency_Account = transactionCurrency_Account;
        TransactionDateTime = transactionDateTime;
        MovementLaunchDateTime = movementLaunchDateTime;
        CreditDebit = creditDebit;
        CorrespondingBankCode = correspondingBankCode;
        CounterpartBankCode = counterpartBankCode;
        CounterpartName = counterpartName;
        CounterpartCountryCode = counterpartCountryCode;
        ClientNumber_Ext = clientNumber_Ext;
        CounterpartAccountNumber = counterpartAccountNumber;
        TransactionType = transactionType;
        TransactionStatus = transactionStatus;
        this.CAE = CAE;
        this.CAE2 = CAE2;
        Branch = branch;
        Field70 = field70;
        RequestUser = requestUser;
        Flexfields = flexfields;
    }

    public String getAccountNumber_Ext() {
        return AccountNumber_Ext;
    }

    public void setAccountNumber_Ext(String accountNumber_Ext) {
        AccountNumber_Ext = accountNumber_Ext;
    }

    public String getTransactionNumber() {
        return TransactionNumber;
    }

    public void setTransactionNumber(String transactionNumber) {
        TransactionNumber = transactionNumber;
    }

    public String getTransactionOperationCode() {
        return TransactionOperationCode;
    }

    public void setTransactionOperationCode(String transactionOperationCode) {
        TransactionOperationCode = transactionOperationCode;
    }

    public BigDecimal getTransactionAmount_Original() {
        return TransactionAmount_Original;
    }

    public void setTransactionAmount_Original(BigDecimal transactionAmount_Original) {
        TransactionAmount_Original = transactionAmount_Original;
    }

    public String getTransactionCurrency_Original() {
        return TransactionCurrency_Original;
    }

    public void setTransactionCurrency_Original(String transactionCurrency_Original) {
        TransactionCurrency_Original = transactionCurrency_Original;
    }

    public BigDecimal getTransactionAmount_Converted() {
        return TransactionAmount_Converted;
    }

    public void setTransactionAmount_Converted(BigDecimal transactionAmount_Converted) {
        TransactionAmount_Converted = transactionAmount_Converted;
    }

    public BigDecimal getTransactionAmount_Account() {
        return TransactionAmount_Account;
    }

    public void setTransactionAmount_Account(BigDecimal transactionAmount_Account) {
        TransactionAmount_Account = transactionAmount_Account;
    }

    public String getTransactionCurrency_Account() {
        return TransactionCurrency_Account;
    }

    public void setTransactionCurrency_Account(String transactionCurrency_Account) {
        TransactionCurrency_Account = transactionCurrency_Account;
    }

    public String getTransactionDateTime() {
        return TransactionDateTime;
    }

    public void setTransactionDateTime(String transactionDateTime) {
        TransactionDateTime = transactionDateTime;
    }

    public String getMovementLaunchDateTime() {
        return MovementLaunchDateTime;
    }

    public void setMovementLaunchDateTime(String movementLaunchDateTime) {
        MovementLaunchDateTime = movementLaunchDateTime;
    }

    public String getCreditDebit() {
        return CreditDebit;
    }

    public void setCreditDebit(String creditDebit) {
        CreditDebit = creditDebit;
    }

    public String getCorrespondingBankCode() {
        return CorrespondingBankCode;
    }

    public void setCorrespondingBankCode(String correspondingBankCode) {
        CorrespondingBankCode = correspondingBankCode;
    }

    public String getCounterpartBankCode() {
        return CounterpartBankCode;
    }

    public void setCounterpartBankCode(String counterpartBankCode) {
        CounterpartBankCode = counterpartBankCode;
    }

    public String getCounterpartName() {
        return CounterpartName;
    }

    public void setCounterpartName(String counterpartName) {
        CounterpartName = counterpartName;
    }

    public String getCounterpartCountryCode() {
        return CounterpartCountryCode;
    }

    public void setCounterpartCountryCode(String counterpartCountryCode) {
        CounterpartCountryCode = counterpartCountryCode;
    }

    public String getClientNumber_Ext() {
        return ClientNumber_Ext;
    }

    public void setClientNumber_Ext(String clientNumber_Ext) {
        ClientNumber_Ext = clientNumber_Ext;
    }

    public String getCounterpartAccountNumber() {
        return CounterpartAccountNumber;
    }

    public void setCounterpartAccountNumber(String counterpartAccountNumber) {
        CounterpartAccountNumber = counterpartAccountNumber;
    }

    public String getTransactionType() {
        return TransactionType;
    }

    public void setTransactionType(String transactionType) {
        TransactionType = transactionType;
    }

    public String getTransactionStatus() {
        return TransactionStatus;
    }

    public void setTransactionStatus(String transactionStatus) {
        TransactionStatus = transactionStatus;
    }

    public String getCAE() {
        return CAE;
    }

    public void setCAE(String CAE) {
        this.CAE = CAE;
    }

    public String getCAE2() {
        return CAE2;
    }

    public void setCAE2(String CAE2) {
        this.CAE2 = CAE2;
    }

    public String getBranch() {
        return Branch;
    }

    public void setBranch(String branch) {
        Branch = branch;
    }

    public String getField70() {
        return Field70;
    }

    public void setField70(String field70) {
        Field70 = field70;
    }

    public String getRequestUser() {
        return RequestUser;
    }

    public void setRequestUser(String requestUser) {
        RequestUser = requestUser;
    }

    public FlexfieldStructure[] getFlexfields() {
        return Flexfields;
    }

    public void setFlexfields(FlexfieldStructure[] flexfields) {
        Flexfields = flexfields;
    }
}
