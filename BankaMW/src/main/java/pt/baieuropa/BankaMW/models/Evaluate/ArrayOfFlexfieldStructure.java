
package pt.baieuropa.BankaMW.models.Evaluate;

import com.fasterxml.jackson.annotation.JsonProperty;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for ArrayOfFlexfieldStructure complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="ArrayOfFlexfieldStructure">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="FlexfieldStructure" type="{http://www.outsystems.com}FlexfieldStructure" maxOccurs="unbounded" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ArrayOfFlexfieldStructure", namespace = "http://www.outsystems.com", propOrder = {
    "flexfieldStructure"
})
public class ArrayOfFlexfieldStructure {

    @XmlElement(name = "FlexfieldStructure", namespace = "http://www.outsystems.com", nillable = true)
    @JsonProperty("FlexfieldStructure")
    protected FlexfieldStructure[] flexfieldStructure;

    /**
     * Gets the value of the flexfieldStructure property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the flexfieldStructure property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getFlexfieldStructure().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link FlexfieldStructure }
     * 
     * 
     */
    public FlexfieldStructure[] getFlexfieldStructure() {
        return flexfieldStructure;
    }

    public void setFlexfieldStructure(FlexfieldStructure[] flexfieldStructure) {
        this.flexfieldStructure = flexfieldStructure;
    }
}
