package pt.baieuropa.BankaMW.models;

public class OperationCode {

    private String operationCode;
    private String description;
    private String topic;
    private String identificationNATS;

    public OperationCode() {
    }

    public OperationCode(String operationCode, String description, String topic, String identificationNATS) {
        this.operationCode = operationCode;
        this.description = description;
        this.topic = topic;
        this.identificationNATS = identificationNATS;
    }

    public String getOperationCode() {
        return operationCode;
    }

    public void setOperationCode(String operationCode) {
        this.operationCode = operationCode;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getTopic() {
        return topic;
    }

    public void setTopic(String topic) {
        this.topic = topic;
    }

    public String getIdentificationNATS() {
        return identificationNATS;
    }

    public void setIdentificationNATS(String identificationNATS) {
        this.identificationNATS = identificationNATS;
    }


}
