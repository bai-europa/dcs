
package pt.baieuropa.BankaMW.models.Evaluate;

import javax.xml.bind.annotation.*;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="AccountRelationsByClientResponse" type="{http://www.outsystems.com}WSResponse" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "accountRelationsByClientResponse"
})
@XmlRootElement(name = "AccountRelationsByClientResponse", namespace = "http://www.outsystems.com")
public class AccountRelationsByClientResponse {

    @XmlElement(name = "AccountRelationsByClientResponse", namespace = "http://www.outsystems.com")
    protected WSResponse accountRelationsByClientResponse;

    /**
     * Gets the value of the accountRelationsByClientResponse property.
     * 
     * @return
     *     possible object is
     *     {@link WSResponse }
     *     
     */
    public WSResponse getAccountRelationsByClientResponse() {
        return accountRelationsByClientResponse;
    }

    /**
     * Sets the value of the accountRelationsByClientResponse property.
     * 
     * @param value
     *     allowed object is
     *     {@link WSResponse }
     *     
     */
    public void setAccountRelationsByClientResponse(WSResponse value) {
        this.accountRelationsByClientResponse = value;
    }

}
