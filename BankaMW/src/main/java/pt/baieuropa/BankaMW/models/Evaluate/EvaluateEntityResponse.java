
package pt.baieuropa.BankaMW.models.Evaluate;

import javax.xml.bind.annotation.*;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="EntityEvaluationResponse" type="{http://www.outsystems.com}WSResponse" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "entityEvaluationResponse"
})
@XmlRootElement(name = "EvaluateEntityResponse", namespace = "http://www.outsystems.com")
public class EvaluateEntityResponse {

    @XmlElement(name = "EntityEvaluationResponse", namespace = "http://www.outsystems.com")
    protected WSResponse entityEvaluationResponse;

    /**
     * Gets the value of the entityEvaluationResponse property.
     * 
     * @return
     *     possible object is
     *     {@link WSResponse }
     *     
     */
    public WSResponse getEntityEvaluationResponse() {
        return entityEvaluationResponse;
    }

    /**
     * Sets the value of the entityEvaluationResponse property.
     * 
     * @param value
     *     allowed object is
     *     {@link WSResponse }
     *     
     */
    public void setEntityEvaluationResponse(WSResponse value) {
        this.entityEvaluationResponse = value;
    }

}
