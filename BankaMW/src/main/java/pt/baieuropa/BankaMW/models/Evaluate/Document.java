
package pt.baieuropa.BankaMW.models.Evaluate;

import com.fasterxml.jackson.annotation.JsonProperty;

import javax.xml.bind.annotation.*;
import javax.xml.datatype.XMLGregorianCalendar;


/**
 * <p>Java class for Document complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="Document">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="Number" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="DocumentType" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="DocumentExpirationDate" type="{http://www.w3.org/2001/XMLSchema}date"/>
 *         &lt;element name="CreatedAt_Ext" type="{http://www.w3.org/2001/XMLSchema}date"/>
 *         &lt;element name="IsMainDocument" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "Document", namespace = "http://www.outsystems.com", propOrder = {
    "number",
    "documentType",
    "documentExpirationDate",
    "createdAtExt",
    "isMainDocument"
})
public class Document {

    @XmlElement(name = "Number", namespace = "http://www.outsystems.com")
    @JsonProperty("Number")
    protected String number;
    @XmlElement(name = "DocumentType", namespace = "http://www.outsystems.com")
    @JsonProperty("DocumentType")
    protected String documentType;
    @XmlElement(name = "DocumentExpirationDate", namespace = "http://www.outsystems.com", required = true)
    @XmlSchemaType(name = "date")
    @JsonProperty("DocumentExpirationDate")
    protected String documentExpirationDate;
    @XmlElement(name = "CreatedAt_Ext", namespace = "http://www.outsystems.com", required = true)
    @XmlSchemaType(name = "date")
    @JsonProperty("CreatedAt_Ext")
    protected String createdAtExt;
    @XmlElement(name = "IsMainDocument", namespace = "http://www.outsystems.com")
    @JsonProperty("IsMainDocument")
    protected boolean isMainDocument;

    /**
     * Gets the value of the number property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getNumber() {
        return number;
    }

    /**
     * Sets the value of the number property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setNumber(String value) {
        this.number = value;
    }

    /**
     * Gets the value of the documentType property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDocumentType() {
        return documentType;
    }

    /**
     * Sets the value of the documentType property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDocumentType(String value) {
        this.documentType = value;
    }

    /**
     * Gets the value of the documentExpirationDate property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public String getDocumentExpirationDate() {
        return documentExpirationDate;
    }

    /**
     * Sets the value of the documentExpirationDate property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setDocumentExpirationDate(String value) {
        this.documentExpirationDate = value;
    }

    /**
     * Gets the value of the createdAtExt property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public String getCreatedAtExt() {
        return createdAtExt;
    }

    /**
     * Sets the value of the createdAtExt property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCreatedAtExt(String value) {
        this.createdAtExt = value;
    }

    /**
     * Gets the value of the isMainDocument property.
     * 
     */
    public boolean isIsMainDocument() {
        return isMainDocument;
    }

    /**
     * Sets the value of the isMainDocument property.
     * 
     */
    public void setIsMainDocument(boolean value) {
        this.isMainDocument = value;
    }

}
