
package pt.baieuropa.BankaMW.models.Evaluate;

import javax.xml.bind.annotation.*;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="AccountRelationsByClientComplex" type="{http://www.outsystems.com}AccountRelationsByClientComplex" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "accountRelationsByClientComplex"
})
@XmlRootElement(name = "AccountRelationsByClient", namespace = "http://www.outsystems.com")
public class AccountRelationsByClient {

    @XmlElement(name = "AccountRelationsByClientComplex", namespace = "http://www.outsystems.com")
    protected AccountRelationsByClientComplex accountRelationsByClientComplex;

    /**
     * Gets the value of the accountRelationsByClientComplex property.
     * 
     * @return
     *     possible object is
     *     {@link AccountRelationsByClientComplex }
     *     
     */
    public AccountRelationsByClientComplex getAccountRelationsByClientComplex() {
        return accountRelationsByClientComplex;
    }

    /**
     * Sets the value of the accountRelationsByClientComplex property.
     * 
     * @param value
     *     allowed object is
     *     {@link AccountRelationsByClientComplex }
     *     
     */
    public void setAccountRelationsByClientComplex(AccountRelationsByClientComplex value) {
        this.accountRelationsByClientComplex = value;
    }

}
