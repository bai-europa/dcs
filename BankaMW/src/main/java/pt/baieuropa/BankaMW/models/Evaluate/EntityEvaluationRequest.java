
package pt.baieuropa.BankaMW.models.Evaluate;

import com.fasterxml.jackson.annotation.JsonProperty;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;
import java.math.BigDecimal;


/**
 * <p>Java class for EntityEvaluationRequest complex type.
 *
 * <p>The following schema fragment specifies the expected content contained within this class.
 *
 * <pre>
 * &lt;complexType name="EntityEvaluationRequest">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="Number_Ext" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="Name" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="EntityCategory" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="EntityType" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="BirthDate" type="{http://www.w3.org/2001/XMLSchema}date"/>
 *         &lt;element name="Gender" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="MaritalStatus" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="Nationality" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="Nationality2" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="Nationality3" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="CountryResidence" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="ResidenceCode" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="BirthPlace" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="ConstitutionDate" type="{http://www.w3.org/2001/XMLSchema}date"/>
 *         &lt;element name="Profession" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="ProfessionalActivity" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="EducationalQualifications" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="SectorialCode" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="SocialCapital" type="{http://www.w3.org/2001/XMLSchema}decimal"/>
 *         &lt;element name="EntityIncome" type="{http://www.w3.org/2001/XMLSchema}decimal"/>
 *         &lt;element name="Parent1Name" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="Parent2Name" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="Currency" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="CAE" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="CAE2" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="CAE3" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="SocietyType" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="StockMarketCode" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="CreatedAt_Ext" type="{http://www.w3.org/2001/XMLSchema}dateTime"/>
 *         &lt;element name="RequestUser" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="Action" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="IsPEPDeclared" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         &lt;element name="IsPEPDescription" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="ImportDate" type="{http://www.w3.org/2001/XMLSchema}dateTime"/>
 *         &lt;element name="Flexfields" type="{http://www.outsystems.com}ArrayOfFlexfieldStructure" minOccurs="0"/>
 *         &lt;element name="DocumentList" type="{http://www.outsystems.com}ArrayOfDocument" minOccurs="0"/>
 *         &lt;element name="ContactList" type="{http://www.outsystems.com}ArrayOfContact" minOccurs="0"/>
 *         &lt;element name="Photo" type="{http://www.w3.org/2001/XMLSchema}base64Binary" minOccurs="0"/>
 *         &lt;element name="BeneficialOwnersList" type="{http://www.outsystems.com}ArrayOfEntityBeneficialOwnersList" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 *
 *
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "EntityEvaluationRequest", namespace = "http://www.outsystems.com", propOrder = {
        "numberExt",
        "name",
        "entityCategory",
        "entityType",
        "birthDate",
        "gender",
        "maritalStatus",
        "nationality",
        "nationality2",
        "nationality3",
        "countryResidence",
        "residenceCode",
        "birthPlace",
        "constitutionDate",
        "profession",
        "professionalActivity",
        "educationalQualifications",
        "sectorialCode",
        "socialCapital",
        "entityIncome",
        "parent1Name",
        "parent2Name",
        "currency",
        "cae",
        "cae2",
        "cae3",
        "societyType",
        "stockMarketCode",
        "createdAtExt",
        "requestUser",
        "action",
        "isPEPDeclared",
        "isPEPDescription",
        "importDate",
        "flexfields",
        "documentList",
        "contactList",
        "photo",
        "beneficialOwnersList"
})
public class EntityEvaluationRequest {

    @XmlElement(name = "Number_Ext", namespace = "http://www.outsystems.com", required = true)
    @JsonProperty("Number_Ext")
    protected String numberExt;
    @XmlElement(name = "Name", namespace = "http://www.outsystems.com", required = true)
    @JsonProperty("Name")
    protected String name;
    @XmlElement(name = "EntityCategory", namespace = "http://www.outsystems.com", required = true)
    @JsonProperty("EntityCategory")
    protected String entityCategory;
    @XmlElement(name = "EntityType", namespace = "http://www.outsystems.com", required = true)
    @JsonProperty("EntityType")
    protected String entityType;
    @XmlElement(name = "BirthDate", namespace = "http://www.outsystems.com")
    @JsonProperty("BirthDate")
    protected String birthDate = null;
    @XmlElement(name = "Gender", namespace = "http://www.outsystems.com")
    @JsonProperty("Gender")
    protected String gender = "";
    @XmlElement(name = "MaritalStatus", namespace = "http://www.outsystems.com")
    @JsonProperty("MaritalStatus")
    protected String maritalStatus = "";
    @XmlElement(name = "Nationality", namespace = "http://www.outsystems.com")
    @JsonProperty("Nationality")
    protected String nationality = "";
    @XmlElement(name = "Nationality2", namespace = "http://www.outsystems.com")
    @JsonProperty("Nationality2")
    protected String nationality2 = "";
    @XmlElement(name = "Nationality3", namespace = "http://www.outsystems.com")
    @JsonProperty("Nationality3")
    protected String nationality3 = "";
    @XmlElement(name = "CountryResidence", namespace = "http://www.outsystems.com")
    @JsonProperty("CountryResidence")
    protected String countryResidence = "";
    @XmlElement(name = "ResidenceCode", namespace = "http://www.outsystems.com")
    @JsonProperty("ResidenceCode")
    protected String residenceCode = "";
    @XmlElement(name = "BirthPlace", namespace = "http://www.outsystems.com")
    @JsonProperty("BirthPlace")
    protected String birthPlace = "";
    @XmlElement(name = "ConstitutionDate", namespace = "http://www.outsystems.com")
    @JsonProperty("ConstitutionDate")
    protected String constitutionDate = null;
    @XmlElement(name = "Profession", namespace = "http://www.outsystems.com")
    @JsonProperty("Profession")
    protected String profession = "";
    @XmlElement(name = "ProfessionalActivity", namespace = "http://www.outsystems.com")
    @JsonProperty("ProfessionalActivity")
    protected String professionalActivity = "";
    @XmlElement(name = "EducationalQualifications", namespace = "http://www.outsystems.com")
    @JsonProperty("EducationalQualifications")
    protected String educationalQualifications = "";
    @XmlElement(name = "SectorialCode", namespace = "http://www.outsystems.com")
    @JsonProperty("SectorialCode")
    protected String sectorialCode = "";
    @XmlElement(name = "SocialCapital", namespace = "http://www.outsystems.com")
    @JsonProperty("SocialCapital")
    protected BigDecimal socialCapital = new BigDecimal(0);
    @XmlElement(name = "EntityIncome", namespace = "http://www.outsystems.com")
    @JsonProperty("EntityIncome")
    protected BigDecimal entityIncome = new BigDecimal(0);
    @XmlElement(name = "Parent1Name", namespace = "http://www.outsystems.com")
    @JsonProperty("Parent1Name")
    protected String parent1Name = "";
    @XmlElement(name = "Parent2Name", namespace = "http://www.outsystems.com")
    @JsonProperty("Parent2Name")
    protected String parent2Name = "";
    @XmlElement(name = "Currency", namespace = "http://www.outsystems.com")
    @JsonProperty("Currency")
    protected String currency = "";
    @XmlElement(name = "CAE", namespace = "http://www.outsystems.com")
    @JsonProperty("CAE")
    protected String cae = "";
    @XmlElement(name = "CAE2", namespace = "http://www.outsystems.com")
    @JsonProperty("CAE2")
    protected String cae2 = "";
    @XmlElement(name = "CAE3", namespace = "http://www.outsystems.com")
    @JsonProperty("CAE3")
    protected String cae3 = "";
    @XmlElement(name = "SocietyType", namespace = "http://www.outsystems.com")
    @JsonProperty("SocietyType")
    protected String societyType = "";
    @XmlElement(name = "StockMarketCode", namespace = "http://www.outsystems.com")
    @JsonProperty("StockMarketCode")
    protected String stockMarketCode = "";
    @XmlElement(name = "CreatedAt_Ext", namespace = "http://www.outsystems.com")
    @JsonProperty("CreatedAt_Ext")
    protected String createdAtExt = null;
    @XmlElement(name = "RequestUser", namespace = "http://www.outsystems.com")
    @JsonProperty("RequestUser")
    protected String requestUser = "";
    @XmlElement(name = "Action", namespace = "http://www.outsystems.com", required = true)
    @JsonProperty("Action")
    protected String action;
    @XmlElement(name = "IsPEPDeclared", namespace = "http://www.outsystems.com")
    @JsonProperty("IsPEPDeclared")
    protected boolean isPEPDeclared = false;
    @XmlElement(name = "IsPEPDescription", namespace = "http://www.outsystems.com")
    @JsonProperty("IsPEPDescription")
    protected String isPEPDescription = "";
    @XmlElement(name = "ImportDate", namespace = "http://www.outsystems.com")
    @JsonProperty("ImportDate")
    protected String importDate = null;
    @XmlElement(name = "Flexfields", namespace = "http://www.outsystems.com")
    @JsonProperty("Flexfields")
    protected FlexfieldStructure[] flexfields = new FlexfieldStructure[0];
    @XmlElement(name = "DocumentList", namespace = "http://www.outsystems.com")
    @JsonProperty("DocumentList")
    protected Document[] documentList = new Document[0];
    @XmlElement(name = "ContactList", namespace = "http://www.outsystems.com")
    @JsonProperty("ContactList")
    protected Contact[] contactList = new Contact[0];
    @XmlElement(name = "Photo", namespace = "http://www.outsystems.com")
    @JsonProperty("Photo")
    protected byte[] photo = new byte[0];
    @XmlElement(name = "BeneficialOwnersList", namespace = "http://www.outsystems.com")
    @JsonProperty("BeneficialOwnersList")
    protected EntityBeneficialOwnersList[] beneficialOwnersList = new EntityBeneficialOwnersList[0];



    /**
     * Gets the value of the numberExt property.
     *
     * @return
     *     possible object is
     *     {@link String }
     *
     */
    public String getNumberExt() {
        return numberExt;
    }

    /**
     * Sets the value of the numberExt property.
     *
     * @param value
     *     allowed object is
     *     {@link String }
     *
     */
    public void setNumberExt(String value) {
        this.numberExt = value;
    }

    /**
     * Gets the value of the name property.
     *
     * @return
     *     possible object is
     *     {@link String }
     *
     */
    public String getName() {
        return name;
    }

    /**
     * Sets the value of the name property.
     *
     * @param value
     *     allowed object is
     *     {@link String }
     *
     */
    public void setName(String value) {
        this.name = value;
    }

    /**
     * Gets the value of the entityCategory property.
     *
     * @return
     *     possible object is
     *     {@link String }
     *
     */
    public String getEntityCategory() {
        return entityCategory;
    }

    /**
     * Sets the value of the entityCategory property.
     *
     * @param value
     *     allowed object is
     *     {@link String }
     *
     */
    public void setEntityCategory(String value) {
        this.entityCategory = value;
    }

    /**
     * Gets the value of the entityType property.
     *
     * @return
     *     possible object is
     *     {@link String }
     *
     */
    public String getEntityType() {
        return entityType;
    }

    /**
     * Sets the value of the entityType property.
     *
     * @param value
     *     allowed object is
     *     {@link String }
     *
     */
    public void setEntityType(String value) {
        this.entityType = value;
    }

    /**
     * Gets the value of the birthDate property.
     *
     * @return
     *     possible object is
     *     {@link String }
     *
     */
    public String getBirthDate() {
        return birthDate;
    }

    /**
     * Sets the value of the birthDate property.
     *
     * @param value
     *     allowed object is
     *     {@link String }
     *
     */
    public void setBirthDate(String value) {
        this.birthDate = value;
    }

    /**
     * Gets the value of the gender property.
     *
     * @return
     *     possible object is
     *     {@link String }
     *
     */
    public String getGender() {
        return gender;
    }

    /**
     * Sets the value of the gender property.
     *
     * @param value
     *     allowed object is
     *     {@link String }
     *
     */
    public void setGender(String value) {
        this.gender = value;
    }

    /**
     * Gets the value of the maritalStatus property.
     *
     * @return
     *     possible object is
     *     {@link String }
     *
     */
    public String getMaritalStatus() {
        return maritalStatus;
    }

    /**
     * Sets the value of the maritalStatus property.
     *
     * @param value
     *     allowed object is
     *     {@link String }
     *
     */
    public void setMaritalStatus(String value) {
        this.maritalStatus = value;
    }

    /**
     * Gets the value of the nationality property.
     *
     * @return
     *     possible object is
     *     {@link String }
     *
     */
    public String getNationality() {
        return nationality;
    }

    /**
     * Sets the value of the nationality property.
     *
     * @param value
     *     allowed object is
     *     {@link String }
     *
     */
    public void setNationality(String value) {
        this.nationality = value;
    }

    /**
     * Gets the value of the nationality2 property.
     *
     * @return
     *     possible object is
     *     {@link String }
     *
     */
    public String getNationality2() {
        return nationality2;
    }

    /**
     * Sets the value of the nationality2 property.
     *
     * @param value
     *     allowed object is
     *     {@link String }
     *
     */
    public void setNationality2(String value) {
        this.nationality2 = value;
    }

    /**
     * Gets the value of the nationality3 property.
     *
     * @return
     *     possible object is
     *     {@link String }
     *
     */
    public String getNationality3() {
        return nationality3;
    }

    /**
     * Sets the value of the nationality3 property.
     *
     * @param value
     *     allowed object is
     *     {@link String }
     *
     */
    public void setNationality3(String value) {
        this.nationality3 = value;
    }

    /**
     * Gets the value of the countryResidence property.
     *
     * @return
     *     possible object is
     *     {@link String }
     *
     */
    public String getCountryResidence() {
        return countryResidence;
    }

    /**
     * Sets the value of the countryResidence property.
     *
     * @param value
     *     allowed object is
     *     {@link String }
     *
     */
    public void setCountryResidence(String value) {
        this.countryResidence = value;
    }

    /**
     * Gets the value of the residenceCode property.
     *
     * @return
     *     possible object is
     *     {@link String }
     *
     */
    public String getResidenceCode() {
        return residenceCode;
    }

    /**
     * Sets the value of the residenceCode property.
     *
     * @param value
     *     allowed object is
     *     {@link String }
     *
     */
    public void setResidenceCode(String value) {
        this.residenceCode = value;
    }

    /**
     * Gets the value of the birthPlace property.
     *
     * @return
     *     possible object is
     *     {@link String }
     *
     */
    public String getBirthPlace() {
        return birthPlace;
    }

    /**
     * Sets the value of the birthPlace property.
     *
     * @param value
     *     allowed object is
     *     {@link String }
     *
     */
    public void setBirthPlace(String value) {
        this.birthPlace = value;
    }

    /**
     * Gets the value of the constitutionDate property.
     *
     * @return
     *     possible object is
     *     {@link String }
     *
     */
    public String getConstitutionDate() {
        return constitutionDate;
    }

    /**
     * Sets the value of the constitutionDate property.
     *
     * @param value
     *     allowed object is
     *     {@link String }
     *
     */
    public void setConstitutionDate(String value) {
        this.constitutionDate = value;
    }

    /**
     * Gets the value of the profession property.
     *
     * @return
     *     possible object is
     *     {@link String }
     *
     */
    public String getProfession() {
        return profession;
    }

    /**
     * Sets the value of the profession property.
     *
     * @param value
     *     allowed object is
     *     {@link String }
     *
     */
    public void setProfession(String value) {
        this.profession = value;
    }

    /**
     * Gets the value of the professionalActivity property.
     *
     * @return
     *     possible object is
     *     {@link String }
     *
     */
    public String getProfessionalActivity() {
        return professionalActivity;
    }

    /**
     * Sets the value of the professionalActivity property.
     *
     * @param value
     *     allowed object is
     *     {@link String }
     *
     */
    public void setProfessionalActivity(String value) {
        this.professionalActivity = value;
    }

    /**
     * Gets the value of the educationalQualifications property.
     *
     * @return
     *     possible object is
     *     {@link String }
     *
     */
    public String getEducationalQualifications() {
        return educationalQualifications;
    }

    /**
     * Sets the value of the educationalQualifications property.
     *
     * @param value
     *     allowed object is
     *     {@link String }
     *
     */
    public void setEducationalQualifications(String value) {
        this.educationalQualifications = value;
    }

    /**
     * Gets the value of the sectorialCode property.
     *
     * @return
     *     possible object is
     *     {@link String }
     *
     */
    public String getSectorialCode() {
        return sectorialCode;
    }

    /**
     * Sets the value of the sectorialCode property.
     *
     * @param value
     *     allowed object is
     *     {@link String }
     *
     */
    public void setSectorialCode(String value) {
        this.sectorialCode = value;
    }

    /**
     * Gets the value of the socialCapital property.
     *
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *
     */
    public BigDecimal getSocialCapital() {
        return socialCapital;
    }

    /**
     * Sets the value of the socialCapital property.
     *
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *
     */
    public void setSocialCapital(BigDecimal value) {
        this.socialCapital = value;
    }

    /**
     * Gets the value of the entityIncome property.
     *
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *
     */
    public BigDecimal getEntityIncome() {
        return entityIncome;
    }

    /**
     * Sets the value of the entityIncome property.
     *
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *
     */
    public void setEntityIncome(BigDecimal value) {
        this.entityIncome = value;
    }

    /**
     * Gets the value of the parent1Name property.
     *
     * @return
     *     possible object is
     *     {@link String }
     *
     */
    public String getParent1Name() {
        return parent1Name;
    }

    /**
     * Sets the value of the parent1Name property.
     *
     * @param value
     *     allowed object is
     *     {@link String }
     *
     */
    public void setParent1Name(String value) {
        this.parent1Name = value;
    }

    /**
     * Gets the value of the parent2Name property.
     *
     * @return
     *     possible object is
     *     {@link String }
     *
     */
    public String getParent2Name() {
        return parent2Name;
    }

    /**
     * Sets the value of the parent2Name property.
     *
     * @param value
     *     allowed object is
     *     {@link String }
     *
     */
    public void setParent2Name(String value) {
        this.parent2Name = value;
    }

    /**
     * Gets the value of the currency property.
     *
     * @return
     *     possible object is
     *     {@link String }
     *
     */
    public String getCurrency() {
        return currency;
    }

    /**
     * Sets the value of the currency property.
     *
     * @param value
     *     allowed object is
     *     {@link String }
     *
     */
    public void setCurrency(String value) {
        this.currency = value;
    }

    /**
     * Gets the value of the cae property.
     *
     * @return
     *     possible object is
     *     {@link String }
     *
     */
    public String getCAE() {
        return cae;
    }

    /**
     * Sets the value of the cae property.
     *
     * @param value
     *     allowed object is
     *     {@link String }
     *
     */
    public void setCAE(String value) {
        this.cae = value;
    }

    /**
     * Gets the value of the cae2 property.
     *
     * @return
     *     possible object is
     *     {@link String }
     *
     */
    public String getCAE2() {
        return cae2;
    }

    /**
     * Sets the value of the cae2 property.
     *
     * @param value
     *     allowed object is
     *     {@link String }
     *
     */
    public void setCAE2(String value) {
        this.cae2 = value;
    }

    /**
     * Gets the value of the cae3 property.
     *
     * @return
     *     possible object is
     *     {@link String }
     *
     */
    public String getCAE3() {
        return cae3;
    }

    /**
     * Sets the value of the cae3 property.
     *
     * @param value
     *     allowed object is
     *     {@link String }
     *
     */
    public void setCAE3(String value) {
        this.cae3 = value;
    }

    /**
     * Gets the value of the societyType property.
     *
     * @return
     *     possible object is
     *     {@link String }
     *
     */
    public String getSocietyType() {
        return societyType;
    }

    /**
     * Sets the value of the societyType property.
     *
     * @param value
     *     allowed object is
     *     {@link String }
     *
     */
    public void setSocietyType(String value) {
        this.societyType = value;
    }

    /**
     * Gets the value of the stockMarketCode property.
     *
     * @return
     *     possible object is
     *     {@link String }
     *
     */
    public String getStockMarketCode() {
        return stockMarketCode;
    }

    /**
     * Sets the value of the stockMarketCode property.
     *
     * @param value
     *     allowed object is
     *     {@link String }
     *
     */
    public void setStockMarketCode(String value) {
        this.stockMarketCode = value;
    }

    /**
     * Gets the value of the createdAtExt property.
     *
     * @return
     *     possible object is
     *     {@link String }
     *
     */
    public String getCreatedAtExt() {
        return createdAtExt;
    }

    /**
     * Sets the value of the createdAtExt property.
     *
     * @param value
     *     allowed object is
     *     {@link String }
     *
     */
    public void setCreatedAtExt(String value) {
        this.createdAtExt = value;
    }

    /**
     * Gets the value of the requestUser property.
     *
     * @return
     *     possible object is
     *     {@link String }
     *
     */
    public String getRequestUser() {
        return requestUser;
    }

    /**
     * Sets the value of the requestUser property.
     *
     * @param value
     *     allowed object is
     *     {@link String }
     *
     */
    public void setRequestUser(String value) {
        this.requestUser = value;
    }

    /**
     * Gets the value of the action property.
     *
     * @return
     *     possible object is
     *     {@link String }
     *
     */
    public String getAction() {
        return action;
    }

    /**
     * Sets the value of the action property.
     *
     * @param value
     *     allowed object is
     *     {@link String }
     *
     */
    public void setAction(String value) {
        this.action = value;
    }

    /**
     * Gets the value of the isPEPDeclared property.
     *
     */
    public boolean isIsPEPDeclared() {
        return isPEPDeclared;
    }

    /**
     * Sets the value of the isPEPDeclared property.
     *
     */
    public void setIsPEPDeclared(boolean value) {
        this.isPEPDeclared = value;
    }

    /**
     * Gets the value of the isPEPDescription property.
     *
     * @return
     *     possible object is
     *     {@link String }
     *
     */
    public String getIsPEPDescription() {
        return isPEPDescription;
    }

    /**
     * Sets the value of the isPEPDescription property.
     *
     * @param value
     *     allowed object is
     *     {@link String }
     *
     */
    public void setIsPEPDescription(String value) {
        this.isPEPDescription = value;
    }

    /**
     * Gets the value of the importDate property.
     *
     * @return
     *     possible object is
     *     {@link String }
     *
     */
    public String getImportDate() {
        return importDate;
    }

    /**
     * Sets the value of the importDate property.
     *
     * @param value
     *     allowed object is
     *     {@link String }
     *
     */
    public void setImportDate(String value) {
        this.importDate = value;
    }

    /**
     * Gets the value of the flexfields property.
     *
     * @return
     *     possible object is
     *     {@link ArrayOfFlexfieldStructure }
     *
     */
    public FlexfieldStructure[] getFlexfields() {
        return flexfields;
    }

    /**
     * Sets the value of the flexfields property.
     *
     * @param value
     *     allowed object is
     *     {@link ArrayOfFlexfieldStructure }
     *
     */
    public void setFlexfields(FlexfieldStructure[] value) {
        this.flexfields = value;
    }

    /**
     * Gets the value of the documentList property.
     *
     * @return
     *     possible object is
     *     {@link ArrayOfDocument }
     *
     */
    public Document[] getDocumentList() {
        return documentList;
    }

    /**
     * Sets the value of the documentList property.
     *
     * @param value
     *     allowed object is
     *     {@link ArrayOfDocument }
     *
     */
    public void setDocumentList(Document[] value) {
        this.documentList = value;
    }

    /**
     * Gets the value of the contactList property.
     *
     * @return
     *     possible object is
     *     {@link ArrayOfContact }
     *
     */
    public Contact[] getContactList() {
        return contactList;
    }

    /**
     * Sets the value of the contactList property.
     *
     * @param value
     *     allowed object is
     *     {@link ArrayOfContact }
     *
     */
    public void setContactList(Contact[] value) {
        this.contactList = value;
    }

    /**
     * Gets the value of the photo property.
     *
     * @return
     *     possible object is
     *     byte[]
     */
    public byte[] getPhoto() {
        return photo;
    }

    /**
     * Sets the value of the photo property.
     *
     * @param value
     *     allowed object is
     *     byte[]
     */
    public void setPhoto(byte[] value) {
        this.photo = value;
    }

    /**
     * Gets the value of the beneficialOwnersList property.
     *
     * @return
     *     possible object is
     *     {@link ArrayOfEntityBeneficialOwnersList }
     *
     */
    public EntityBeneficialOwnersList[] getBeneficialOwnersList() {
        return beneficialOwnersList;
    }

    /**
     * Sets the value of the beneficialOwnersList property.
     *
     * @param value
     *     allowed object is
     *     {@link ArrayOfEntityBeneficialOwnersList }
     *
     */
    public void setBeneficialOwnersList(EntityBeneficialOwnersList[] value) {
        this.beneficialOwnersList = value;
    }

}
