package pt.baieuropa.BankaMW.models;

public class CurrentAccountMovement {

    private String transaction;
    private String createOrUpdate;

    public CurrentAccountMovement() {
    }

    public CurrentAccountMovement(String transaction, String createOrUpdate) {
        this.transaction = transaction;
        this.createOrUpdate = createOrUpdate;
    }

    public String getTransaction() {
        return transaction;
    }

    public void setTransaction(String transaction) {
        this.transaction = transaction;
    }

    public String getCreateOrUpdate() {
        return createOrUpdate;
    }

    public void setCreateOrUpdate(String createOrUpdate) {
        this.createOrUpdate = createOrUpdate;
    }
}
