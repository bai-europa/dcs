
package pt.baieuropa.BankaMW.models.Evaluate;

import javax.xml.bind.annotation.*;
import java.math.BigDecimal;


/**
 * <p>Java class for AccountCreationRequest complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="AccountCreationRequest">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="AccountNumber" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="AccountType" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="AccountCategory" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="AccountEntityType" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="ClientNumber_Ext" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="AccountStatus" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="SegmentType" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="SubscriptionChannel" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="AccountCurrency" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="EntityAccountList" type="{http://www.outsystems.com}ArrayOfEntityAccount" minOccurs="0"/>
 *         &lt;element name="RequestUser" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="ClientCreationDate_Ext" type="{http://www.w3.org/2001/XMLSchema}dateTime"/>
 *         &lt;element name="AccountCreationDate" type="{http://www.w3.org/2001/XMLSchema}dateTime"/>
 *         &lt;element name="BalanceAverage" type="{http://www.w3.org/2001/XMLSchema}decimal"/>
 *         &lt;element name="DateBalanceAverage" type="{http://www.w3.org/2001/XMLSchema}date"/>
 *         &lt;element name="Action" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="Flexfields" type="{http://www.outsystems.com}ArrayOfFlexfieldStructure" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "AccountCreationRequest", namespace = "http://www.outsystems.com", propOrder = {
    "accountNumber",
    "accountType",
    "accountCategory",
    "accountEntityType",
    "clientNumberExt",
    "accountStatus",
    "segmentType",
    "subscriptionChannel",
    "accountCurrency",
    "entityAccountList",
    "requestUser",
    "clientCreationDateExt",
    "accountCreationDate",
    "balanceAverage",
    "dateBalanceAverage",
    "action",
    "flexfields"
})
public class AccountCreationRequest {

    @XmlElement(name = "AccountNumber", namespace = "http://www.outsystems.com")
    protected String accountNumber;
    @XmlElement(name = "AccountType", namespace = "http://www.outsystems.com")
    protected String accountType;
    @XmlElement(name = "AccountCategory", namespace = "http://www.outsystems.com")
    protected String accountCategory;
    @XmlElement(name = "AccountEntityType", namespace = "http://www.outsystems.com")
    protected String accountEntityType;
    @XmlElement(name = "ClientNumber_Ext", namespace = "http://www.outsystems.com")
    protected String clientNumberExt;
    @XmlElement(name = "AccountStatus", namespace = "http://www.outsystems.com")
    protected String accountStatus;
    @XmlElement(name = "SegmentType", namespace = "http://www.outsystems.com")
    protected String segmentType;
    @XmlElement(name = "SubscriptionChannel", namespace = "http://www.outsystems.com")
    protected String subscriptionChannel;
    @XmlElement(name = "AccountCurrency", namespace = "http://www.outsystems.com")
    protected String accountCurrency;
    @XmlElement(name = "EntityAccountList", namespace = "http://www.outsystems.com")
    protected EntityAccount[] entityAccountList;
    @XmlElement(name = "RequestUser", namespace = "http://www.outsystems.com")
    protected String requestUser;
    @XmlElement(name = "ClientCreationDate_Ext", namespace = "http://www.outsystems.com", required = true)
    protected String clientCreationDateExt;
    @XmlElement(name = "AccountCreationDate", namespace = "http://www.outsystems.com", required = true)
    protected String accountCreationDate;
    @XmlElement(name = "BalanceAverage", namespace = "http://www.outsystems.com", required = true)
    protected BigDecimal balanceAverage;
    @XmlElement(name = "DateBalanceAverage", namespace = "http://www.outsystems.com", required = true)
    protected String dateBalanceAverage;
    @XmlElement(name = "Action", namespace = "http://www.outsystems.com")
    protected String action;
    @XmlElement(name = "Flexfields", namespace = "http://www.outsystems.com")
    protected FlexfieldStructure[] flexfields;

    /**
     * Gets the value of the accountNumber property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getAccountNumber() {
        return accountNumber;
    }

    /**
     * Sets the value of the accountNumber property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setAccountNumber(String value) {
        this.accountNumber = value;
    }

    /**
     * Gets the value of the accountType property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getAccountType() {
        return accountType;
    }

    /**
     * Sets the value of the accountType property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setAccountType(String value) {
        this.accountType = value;
    }

    /**
     * Gets the value of the accountCategory property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getAccountCategory() {
        return accountCategory;
    }

    /**
     * Sets the value of the accountCategory property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setAccountCategory(String value) {
        this.accountCategory = value;
    }

    /**
     * Gets the value of the accountEntityType property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getAccountEntityType() {
        return accountEntityType;
    }

    /**
     * Sets the value of the accountEntityType property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setAccountEntityType(String value) {
        this.accountEntityType = value;
    }

    /**
     * Gets the value of the clientNumberExt property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getClientNumberExt() {
        return clientNumberExt;
    }

    /**
     * Sets the value of the clientNumberExt property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setClientNumberExt(String value) {
        this.clientNumberExt = value;
    }

    /**
     * Gets the value of the accountStatus property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getAccountStatus() {
        return accountStatus;
    }

    /**
     * Sets the value of the accountStatus property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setAccountStatus(String value) {
        this.accountStatus = value;
    }

    /**
     * Gets the value of the segmentType property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getSegmentType() {
        return segmentType;
    }

    /**
     * Sets the value of the segmentType property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setSegmentType(String value) {
        this.segmentType = value;
    }

    /**
     * Gets the value of the subscriptionChannel property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getSubscriptionChannel() {
        return subscriptionChannel;
    }

    /**
     * Sets the value of the subscriptionChannel property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setSubscriptionChannel(String value) {
        this.subscriptionChannel = value;
    }

    /**
     * Gets the value of the accountCurrency property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getAccountCurrency() {
        return accountCurrency;
    }

    /**
     * Sets the value of the accountCurrency property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setAccountCurrency(String value) {
        this.accountCurrency = value;
    }

    /**
     * Gets the value of the entityAccountList property.
     * 
     * @return
     *     possible object is
     *     {@link ArrayOfEntityAccount }
     *     
     */
    public EntityAccount[] getEntityAccountList() {
        return entityAccountList;
    }

    /**
     * Sets the value of the entityAccountList property.
     * 
     * @param value
     *     allowed object is
     *     {@link ArrayOfEntityAccount }
     *     
     */
    public void setEntityAccountList(EntityAccount[] value) {
        this.entityAccountList = value;
    }

    /**
     * Gets the value of the requestUser property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getRequestUser() {
        return requestUser;
    }

    /**
     * Sets the value of the requestUser property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setRequestUser(String value) {
        this.requestUser = value;
    }

    /**
     * Gets the value of the clientCreationDateExt property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getClientCreationDateExt() {
        return clientCreationDateExt;
    }

    /**
     * Sets the value of the clientCreationDateExt property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setClientCreationDateExt(String value) {
        this.clientCreationDateExt = value;
    }

    /**
     * Gets the value of the accountCreationDate property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getAccountCreationDate() {
        return accountCreationDate;
    }

    /**
     * Sets the value of the accountCreationDate property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setAccountCreationDate(String value) {
        this.accountCreationDate = value;
    }

    /**
     * Gets the value of the balanceAverage property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getBalanceAverage() {
        return balanceAverage;
    }

    /**
     * Sets the value of the balanceAverage property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setBalanceAverage(BigDecimal value) {
        this.balanceAverage = value;
    }

    /**
     * Gets the value of the dateBalanceAverage property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDateBalanceAverage() {
        return dateBalanceAverage;
    }

    /**
     * Sets the value of the dateBalanceAverage property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDateBalanceAverage(String value) {
        this.dateBalanceAverage = value;
    }

    /**
     * Gets the value of the action property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getAction() {
        return action;
    }

    /**
     * Sets the value of the action property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setAction(String value) {
        this.action = value;
    }

    /**
     * Gets the value of the flexfields property.
     * 
     * @return
     *     possible object is
     *     {@link ArrayOfFlexfieldStructure }
     *     
     */
    public FlexfieldStructure[] getFlexfields() {
        return flexfields;
    }

    /**
     * Sets the value of the flexfields property.
     * 
     * @param value
     *     allowed object is
     *     {@link ArrayOfFlexfieldStructure }
     *     
     */
    public void setFlexfields(FlexfieldStructure[] value) {
        this.flexfields = value;
    }

}
