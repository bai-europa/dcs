package pt.baieuropa.BankaMW.models;

public class Account {

    private String transaction;
    private String createOrUpdate;
    private boolean isAccountOnline = false;
    private boolean isAccountSituation = false;
    private boolean isRelatations = false;
    private boolean isDepositAccount = false;
    private boolean isCreditAccount = false;

    public Account() { }

    public Account(String transaction, String createOrUpdate, boolean isAccountOnline, boolean isAccountSituation, boolean isRelatations, boolean isDepositAccount, boolean isCreditAccount) {
        this.transaction = transaction;
        this.createOrUpdate = createOrUpdate;
        this.isAccountOnline = isAccountOnline;
        this.isAccountSituation = isAccountSituation;
        this.isRelatations = isRelatations;
        this.isDepositAccount = isDepositAccount;
        this.isCreditAccount = isCreditAccount;
    }

    public String getTransaction() {
        return transaction;
    }

    public void setTransaction(String transaction) {
        this.transaction = transaction;
    }

    public String getCreateOrUpdate() {
        return createOrUpdate;
    }

    public void setCreateOrUpdate(String createOrUpdate) {
        this.createOrUpdate = createOrUpdate;
    }

    public boolean isAccountOnline() {
        return isAccountOnline;
    }

    public void setAccountOnline(boolean accountOnline) {
        isAccountOnline = accountOnline;
    }

    public boolean isAccountSituation() {
        return isAccountSituation;
    }

    public void setAccountSituation(boolean accountSituation) {
        isAccountSituation = accountSituation;
    }

    public boolean isRelatations() {
        return isRelatations;
    }

    public void setRelatations(boolean relatations) {
        isRelatations = relatations;
    }

    public boolean isDepositAccount() {
        return isDepositAccount;
    }

    public void setDepositAccount(boolean depositAccount) {
        isDepositAccount = depositAccount;
    }

    public boolean isCreditAccount() {
        return isCreditAccount;
    }

    public void setCreditAccount(boolean creditAccount) {
        isCreditAccount = creditAccount;
    }
}
