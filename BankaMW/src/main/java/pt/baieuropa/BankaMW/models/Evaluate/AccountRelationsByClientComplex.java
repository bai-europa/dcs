
package pt.baieuropa.BankaMW.models.Evaluate;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for AccountRelationsByClientComplex complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="AccountRelationsByClientComplex">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="ClientNumber_Ext" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="EntityAccountList" type="{http://www.outsystems.com}ArrayOfEntityAccount" minOccurs="0"/>
 *         &lt;element name="RequestUser" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "AccountRelationsByClientComplex", namespace = "http://www.outsystems.com", propOrder = {
    "clientNumberExt",
    "entityAccountList",
    "requestUser"
})
public class AccountRelationsByClientComplex {

    @XmlElement(name = "ClientNumber_Ext", namespace = "http://www.outsystems.com")
    protected String clientNumberExt;
    @XmlElement(name = "EntityAccountList", namespace = "http://www.outsystems.com")
    protected EntityAccount[] entityAccountList;
    @XmlElement(name = "RequestUser", namespace = "http://www.outsystems.com")
    protected String requestUser;

    /**
     * Gets the value of the clientNumberExt property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getClientNumberExt() {
        return clientNumberExt;
    }

    /**
     * Sets the value of the clientNumberExt property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setClientNumberExt(String value) {
        this.clientNumberExt = value;
    }

    /**
     * Gets the value of the entityAccountList property.
     * 
     * @return
     *     possible object is
     *     {@link ArrayOfEntityAccount }
     *     
     */
    public EntityAccount[] getEntityAccountList() {
        return entityAccountList;
    }

    /**
     * Sets the value of the entityAccountList property.
     * 
     * @param value
     *     allowed object is
     *     {@link ArrayOfEntityAccount }
     *     
     */
    public void setEntityAccountList(EntityAccount[] value) {
        this.entityAccountList = value;
    }

    /**
     * Gets the value of the requestUser property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getRequestUser() {
        return requestUser;
    }

    /**
     * Sets the value of the requestUser property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setRequestUser(String value) {
        this.requestUser = value;
    }

}
