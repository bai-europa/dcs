
package pt.baieuropa.BankaMW.models.Evaluate;

import javax.xml.bind.annotation.*;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="EntityEvaluationRequest" type="{http://www.outsystems.com}EntityEvaluationRequest" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "entityEvaluationRequest"
})
@XmlRootElement(name = "EvaluateEntityOnline", namespace = "http://www.outsystems.com")
public class EvaluateEntityOnline {

    @XmlElement(name = "EntityEvaluationRequest", namespace = "http://www.outsystems.com")
    protected EntityEvaluationRequest entityEvaluationRequest;

    /**
     * Gets the value of the entityEvaluationRequest property.
     * 
     * @return
     *     possible object is
     *     {@link EntityEvaluationRequest }
     *     
     */
    public EntityEvaluationRequest getEntityEvaluationRequest() {
        return entityEvaluationRequest;
    }

    /**
     * Sets the value of the entityEvaluationRequest property.
     * 
     * @param value
     *     allowed object is
     *     {@link EntityEvaluationRequest }
     *     
     */
    public void setEntityEvaluationRequest(EntityEvaluationRequest value) {
        this.entityEvaluationRequest = value;
    }

}
