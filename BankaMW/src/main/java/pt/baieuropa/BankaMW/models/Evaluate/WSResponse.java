
package pt.baieuropa.BankaMW.models.Evaluate;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for WSResponse complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="WSResponse">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="ReturnCode" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="ErrorMessage" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="ID" type="{http://www.w3.org/2001/XMLSchema}long"/>
 *         &lt;element name="ListOfFieldValidates" type="{http://www.outsystems.com}ArrayOfValidateService" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "WSResponse", namespace = "http://www.outsystems.com", propOrder = {
    "returnCode",
    "errorMessage",
    "id",
    "listOfFieldValidates"
})
public class WSResponse {

    @XmlElement(name = "ReturnCode", namespace = "http://www.outsystems.com")
    protected String returnCode;
    @XmlElement(name = "ErrorMessage", namespace = "http://www.outsystems.com")
    protected String errorMessage;
    @XmlElement(name = "ID", namespace = "http://www.outsystems.com")
    protected long id;
    @XmlElement(name = "ListOfFieldValidates", namespace = "http://www.outsystems.com")
    protected ArrayOfValidateService listOfFieldValidates;

    /**
     * Gets the value of the returnCode property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getReturnCode() {
        return returnCode;
    }

    /**
     * Sets the value of the returnCode property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setReturnCode(String value) {
        this.returnCode = value;
    }

    /**
     * Gets the value of the errorMessage property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getErrorMessage() {
        return errorMessage;
    }

    /**
     * Sets the value of the errorMessage property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setErrorMessage(String value) {
        this.errorMessage = value;
    }

    /**
     * Gets the value of the id property.
     * 
     */
    public long getID() {
        return id;
    }

    /**
     * Sets the value of the id property.
     * 
     */
    public void setID(long value) {
        this.id = value;
    }

    /**
     * Gets the value of the listOfFieldValidates property.
     * 
     * @return
     *     possible object is
     *     {@link ArrayOfValidateService }
     *     
     */
    public ArrayOfValidateService getListOfFieldValidates() {
        return listOfFieldValidates;
    }

    /**
     * Sets the value of the listOfFieldValidates property.
     * 
     * @param value
     *     allowed object is
     *     {@link ArrayOfValidateService }
     *     
     */
    public void setListOfFieldValidates(ArrayOfValidateService value) {
        this.listOfFieldValidates = value;
    }

}
