
package pt.baieuropa.BankaMW.models.Evaluate;

import javax.xml.bind.annotation.*;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="EntityEvaluationResponse" type="{http://www.outsystems.com}WSResponseEntityOnline" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "entityEvaluationResponse"
})
@XmlRootElement(name = "EvaluateEntityOnlineResponse", namespace = "http://www.outsystems.com")
public class EvaluateEntityOnlineResponse {

    @XmlElement(name = "EntityEvaluationResponse", namespace = "http://www.outsystems.com")
    protected WSResponseEntityOnline entityEvaluationResponse;

    /**
     * Gets the value of the entityEvaluationResponse property.
     * 
     * @return
     *     possible object is
     *     {@link WSResponseEntityOnline }
     *     
     */
    public WSResponseEntityOnline getEntityEvaluationResponse() {
        return entityEvaluationResponse;
    }

    /**
     * Sets the value of the entityEvaluationResponse property.
     * 
     * @param value
     *     allowed object is
     *     {@link WSResponseEntityOnline }
     *     
     */
    public void setEntityEvaluationResponse(WSResponseEntityOnline value) {
        this.entityEvaluationResponse = value;
    }

}
