package pt.baieuropa.BankaMW.service.connections;

import com.ibm.as400.access.AS400;
import com.ibm.as400.access.AS400SecurityException;
import org.apache.log4j.Logger;
import pt.bai.apireader.apisimples.LOGIN;
import pt.bai.apireader.utils.Utils;
import pt.baieuropa.BankaMW.service.apis.APIBanka;
import pt.baieuropa.BankaMW.utils.ConfigUtils;

import java.beans.PropertyVetoException;
import java.io.IOException;
import java.util.Properties;

public class BankaTransactionConnection {

    public static Logger logger = Logger.getLogger(APIBanka.class);
    public static AS400 system;
    public static LOGIN login;
    public static Properties config = ConfigUtils.loadProps(System.getenv("AS400.configuration"));
    public static String systemName = config.getProperty("systemName");
    public static String user = config.getProperty("user");
    public static String password = config.getProperty("password");
    public static String station = config.getProperty("station");
    public static String softwareVersion = config.getProperty("softwareVersion");
    public static String libVersion = config.getProperty("libVersion");

    public static void connectAS400() {
        try {
            // BAIE - Inicia gestores de API
            system = new AS400();
            system.setGuiAvailable(false);
            system.setSystemName(systemName);
            system.setUserId(user);
            system.setPassword(password);
            system.connectService(AS400.FILE);
            Utils.enterContext(softwareVersion, libVersion, system);
            login = new LOGIN(system, systemName, user, password, softwareVersion, libVersion);
            try {
                login.login(user, station, "1", "BM");
            } catch (Exception ex) {
                ex.printStackTrace();
            }
        } catch (ClassCastException | PropertyVetoException | AS400SecurityException | IOException ex) {
            ex.printStackTrace();
        }
    }

    public static void disconnectAS400() {
        login.logof(user, station, "1", "BM");
        system.disconnectAllServices();
        logger.info("AS400 DISCONNECT " + systemName + " AND LOGOF WITH " + user + "\n");
    }

}
