/*
  Created by: loliveira
*/

package pt.baieuropa.BankaMW.service;
import pt.baieuropa.BankaMW.processor.Processor;


/**
 * Contains all the functions of the service that will be public.
 * <p>
 * Each one of this functions will be available to execute
 */
// TODO web service functions test
public interface Service {
    /**
     * Get the service name
     *
     * @return the name of the service
     */
    String getServiceName();

    /**
     * Set the name of the service.
     * <p>
     * This will be used across all the service to identify the service in logging
     *
     * @param inputServiceName the that the service will have
     */
    void setServiceName(String inputServiceName);

    /**
     * A service must have a processor, that must have, at least, as much functions as the number of functions of the
     * service
     *
     * @return the processor of the service
     *
     * @see Processor
     */
    default Processor getProcessor() {
        return null;
    }
}
