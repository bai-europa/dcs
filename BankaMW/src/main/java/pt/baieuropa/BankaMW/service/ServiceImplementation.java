/*
  Created by: loliveira
*/

package pt.baieuropa.BankaMW.service;

public class ServiceImplementation implements Service {

    private static String DEFAULT_SERVICE_NAME = "baieuropaservice";

    private Process process;

    private String processName_String = null;

    public ServiceImplementation(String serviceName_String) {
        setServiceName(serviceName_String);
    }

    @Override
    public String getServiceName() {
        if (processName_String == null)
            return DEFAULT_SERVICE_NAME;
        return processName_String;
    }

    @Override
    public void setServiceName(String inputServiceName) {
        processName_String = inputServiceName;
    }

    public String getServicePath() {
        if (processName_String == null)
            return "/" + DEFAULT_SERVICE_NAME;
        return "/" + processName_String;
    }
}
