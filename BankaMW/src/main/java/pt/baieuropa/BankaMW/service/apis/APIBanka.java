package pt.baieuropa.BankaMW.service.apis;

import com.ibm.as400.access.AS400;
import org.apache.log4j.Logger;
import org.json.JSONObject;
import pt.bai.apireader.apisimples.GenericAPI;

public class APIBanka extends GenericAPI {

    public static Logger logger = Logger.getLogger(APIBanka.class);

    public APIBanka(AS400 system, String hostname, String username, String password, String SOFTWARE, String LIB, String structureDataName) {
        super(system, hostname, username, password, SOFTWARE, LIB, structureDataName, structureDataName);
    }

    public JSONObject bankaRequest(String structureDataName, byte[] apiStructure) {

        if (apiFields_List == null) {
            //BAIE função nova para procurar pela DS
            initializeDataStructures(structureDataName);
        } else {
            resetValues();
        }
        ;

        try {
            //BAIE função nova para funcionar no apiReader
            JSONObject r = readerAPI.invokerByReader("GBANCA", "#MENTRCODE", apiStructure);

            String returnCode = readerAPI.getField("#IVPPRCODE");
            String transacao = readerAPI.getField("#MENTAPTR");

            return r;
        } catch (Exception e) {
            e.printStackTrace();
            logger.error("Erro na inserção da API NC5040");
            return null;
        }
    }

}
