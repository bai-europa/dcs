package pt.baieuropa.BankaMW.service;

import pt.baieuropa.BankaMW.models.Evaluate.BankaTransactionRequest;
import pt.baieuropa.BankaMW.processor.database.MongoDBManager;

import java.sql.Timestamp;

public class FraudEvaluationToMongoService {

    /**
     * Inserir no Mongo a informação referente à Fraud
     * @param transaction
     * @param content
     */
    public void insertNewMessage(String transaction, String content) {
        MongoDBManager mongo = new MongoDBManager();
        Timestamp timestamp = new Timestamp(System.currentTimeMillis());
        BankaTransactionRequest fraudEvaluation= new BankaTransactionRequest() ;
        fraudEvaluation.setDataStructure(transaction);
        fraudEvaluation.setContent(content);
        fraudEvaluation.setTimestamp(timestamp.toString());
        mongo.insertOnCollection("BankaFraudeEvaluation", fraudEvaluation);
    }
}
