package pt.baieuropa.BankaMW.endpoints;

import com.google.gson.Gson;
import okhttp3.Request;
import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;
import okhttp3.OkHttpClient;
import okhttp3.Response;
import org.json.JSONArray;
import org.json.JSONObject;
import org.springframework.ws.config.annotation.EnableWs;
import pt.baieuropa.BankaMW.BankaMWApplication;
import pt.baieuropa.BankaMW.models.Account;
import pt.baieuropa.BankaMW.models.Entity;
import pt.baieuropa.BankaMW.models.Evaluate.BankaTransactionRequest;
import pt.baieuropa.BankaMW.models.CurrentAccountMovement;
import pt.baieuropa.BankaMW.service.apis.APIBanka;
import pt.baieuropa.BankaMW.service.connections.BankaTransactionConnection;
import pt.baieuropa.BankaMW.utils.ConfigUtils;
import pt.baieuropa.BankaMW.utils.NATSUtils;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;
import javax.jws.soap.SOAPBinding;
import java.util.LinkedList;
import java.util.Properties;

@WebService
@SOAPBinding(style = SOAPBinding.Style.RPC)
@EnableWs
public class BankaTransaction {

    private static final Logger log = LogManager.getLogger(BankaTransaction.class);
    public static APIBanka apiBanka;
    public static Properties config = ConfigUtils.loadProps(System.getenv("urls.configuration"));
    private static String partyUrl = config.getProperty("parties");
    private static String accountUrl = config.getProperty("account");
    private static String accountRelationsUrl = config.getProperty("accountRelations");
    private static String depositAccountUrl = config.getProperty("depositAccount");
    private static String creditAccountUrl = config.getProperty("creditAccount");

    @WebMethod(operationName = "Request")
    public int createTransaction(@WebParam(partName = "transactionCode", name = "transactionCode") String dataStructure,
                                 @WebParam(partName = "payload", name = "payload") byte[] payload) {

        try {
            log.debug("Executing Banka Transaction Request...");
            BankaTransactionConnection.connectAS400();
            apiBanka = new APIBanka(BankaTransactionConnection.system,
                    BankaTransactionConnection.systemName,
                    BankaTransactionConnection.user,
                    BankaTransactionConnection.password,
                    BankaTransactionConnection.softwareVersion,
                    BankaTransactionConnection.libVersion,
                    dataStructure);
            JSONObject transacao = apiBanka.bankaRequest(dataStructure, payload);
            BankaTransactionConnection.disconnectAS400();

            log.debug("Creating Object...");
            BankaTransactionRequest bankaTransactionRequest = new BankaTransactionRequest(dataStructure, transacao.toString());
            log.debug("Object Created.");

            log.debug("Converting FraudEvaluationRequest Object to JSON...");
            Gson gson = new Gson();
            String fraudEvaluationRequestJSON = gson.toJson(bankaTransactionRequest);
            log.debug("Object Converted with GSON.\n" + fraudEvaluationRequestJSON);

            String transaction = transacao.getString("#Transacao");

            Entity entity = checkEntityTransaction(transaction);
            Account account = checkAccountTransaction(transaction);
            CurrentAccountMovement currentAccountMovement = checkCurrentAccountMovement(transaction);

            if(entity != null) {
                log.debug("Executing entity transaction processor...");
                entityTransactionProcessor(entity, transacao);
                log.debug("Entity transaction processor executed.");
            } else if(account != null) {
                log.debug("Executing account transaction processor...");
                accountTransactionProcessor(account, transacao);
                log.debug("Account transaction processor executed.");
            } else if(currentAccountMovement != null) {
                log.debug("Executing current account movement transaction processor...");
                Thread thread = new Thread(
                        new CurrentAccountMovementTransactionProcessor(currentAccountMovement, transacao));
                thread.start();
                log.debug("Current account movement transaction processor executed.");
            } else {
                log.debug("Publishing message BankaTransaction:Request on NATS...");
                BankaMWApplication.getProc().publish("BankaTransaction:Request", fraudEvaluationRequestJSON);
                log.debug("Message published.");
            }

            log.info("Banka Transaction Request Executed.");
            return 1;
        } catch(Exception ex) {
            log.error("Error on Banka Transaction Request:\n" + ex.getMessage());
            return 0;
        }
    }

    private Entity checkEntityTransaction(String transaction) {

        LinkedList<Entity> entityTransactions = new LinkedList<>();
        Entity entity = null;

        // BAIE - Preencher quando o objetivo é a **CRIAÇÃO** de uma **ENTIDADE**
        entityTransactions.add(new Entity("MC002", "CREATE", true, false, false, false));
        entityTransactions.add(new Entity("MC008", "CREATE", true, false, false, false));

        // BAIE - Preencher quando o objetivo é a **ALTERAÇÃO** de uma **ENTIDADE**
        entityTransactions.add(new Entity("MC003", "UPDATE", true, false, false, false));
        entityTransactions.add(new Entity("MC009", "UPDATE", true, false, false, false));

        // BAIE - Preencher quando o objetivo é a **ALTERAÇÃO** de **DOCUMENTOS**
        entityTransactions.add(new Entity("DOCEC", "UPDATE", false, true, false, false));
        entityTransactions.add(new Entity("DOCEA", "UPDATE", false, true, false, false));
        entityTransactions.add(new Entity("DOCED", "UPDATE", false, true, false, false));

        // BAIE - Preencher quando o objetivo é a **ALTERAÇÃO** de **MORADAS**
        entityTransactions.add(new Entity("MORDC", "UPDATE", false, false, true, false));
        entityTransactions.add(new Entity("MORDA", "UPDATE", false, false, true, false));
        entityTransactions.add(new Entity("MORDD", "UPDATE", false, false, true, false));
        entityTransactions.add(new Entity("MORBA", "UPDATE", false, false, true, false));

        // BAIE - Preencher quando o objetivo é a **ALTERAÇÃO** de **Entidades Participantes**
        entityTransactions.add(new Entity("ENTPC", "UPDATE", false, false, false, true));
        entityTransactions.add(new Entity("ENTPD", "UPDATE", false, false, false, true));
        entityTransactions.add(new Entity("ENTPA", "UPDATE", false, false, false, true));

        for (Entity item: entityTransactions) {
            if(item.getTransaction().equals(transaction)) {
                entity = item;
            }
        }

        return entity;
    }

    private void entityTransactionProcessor(Entity entity, JSONObject transacao) {

        if(entity.getCreateOrUpdate().equals("CREATE") || entity.getCreateOrUpdate().equals("UPDATE")) {

            Response response = null;
            OkHttpClient client = new OkHttpClient();
            String topic = "";

            try {
                String entityNumber = "";
                String user = "";
                String isPresencial = "";
                String actionType = "";

                // BAIE - Verifica se são da API de Entidades
                if(entity.isEntityOnline()) {
                    entityNumber = transacao.getString("#MENTNUMT");
                    user = transacao.getString("#MENTUSERC");
                    // BAIE - Verifica se são da API de Documentos
                } else if(entity.isDocument()) {
                    entityNumber = transacao.getString("#NCDENUMT");
                    user = transacao.getString("#NCDEUSERC");
                    // BAIE - Verifica se são da API de Moradas
                } else if(entity.isAddress()) {
                    entityNumber = transacao.getString("#GBMONTIT");
                    user = transacao.getString("#GBMOUSERC");

                    if(transacao.getString("#GBMOTMOR").equals("EM")){
                        actionType = "MailChng";
                    }else if(transacao.getString("#GBMOTMOR").equals("ES")){
                        actionType = "CntChng";
                    }else{
                        actionType = "AddChng";
                    }
                } else if(entity.isParticipant()) {
                    entityNumber = transacao.getString("#ENPANENT");
                    user = transacao.getString("#ENPAUSERC");
                }

                log.debug("Calling WebService " + partyUrl + entityNumber);
                Request request = new Request.Builder()
                        .url(partyUrl + entityNumber)
                        .build();

                response = client.newCall(request).execute();
                log.debug("Resquest sent successfully!");
                log.debug("Response Received: \n" + response.body().toString() + "\n");
                JSONArray responseBody = new JSONArray(response.body().string());

                for(int i = 0; i < responseBody.length(); i++) {

                    if(entity.getCreateOrUpdate().equals("UPDATE")) {
                        try {
                            responseBody.getJSONObject(i).getJSONObject("orgPartyInfo");
                            topic = "EnterpriseUpdated:Request";
                        } catch (Exception ex) {
                            topic = "ParticularUpdated:Request";
                        }
                    } else {
                        try {
                            responseBody.getJSONObject(i).getJSONObject("orgPartyInfo");
                            topic = "EnterpriseCreated:Request";
                        } catch (Exception ex) {
                            topic = "ParticularCreated:Request";
                        }
                    }

                    JSONObject objectToSend = responseBody.getJSONObject(i);
                    objectToSend.put("RequestUser", user);

                    if(responseBody.getJSONObject(i).has(actionType)) {
                        if (responseBody.getJSONObject(i).getString("actionType").equals("")) {
                            objectToSend.put("actionType", actionType);
                        }
                    }

                    if(responseBody.getJSONObject(i).has("faceToFaceOpening"))
                        objectToSend.put("IsPresencial", responseBody.getJSONObject(i).getBoolean("faceToFaceOpening"));

                    if(responseBody.getJSONObject(i).has("partyType")) {
                        String partyType = responseBody.getJSONObject(i).getString("partyType");
                        log.debug("PARTY TYPE RECEIVED: " + partyType);
                        switch (partyType) {
                            case "G":
                                objectToSend.put("PartyType", "5");
                                break;
                            case "R":
                            case "W":
                                objectToSend.put("PartyType", "3");
                                break;
                            default:
                                objectToSend.put("PartyType", "1");
                                break;
                        }
                    }

                    log.debug("Publishing "+topic+" on NATS...");
                    BankaMWApplication.getProc().publish(topic, objectToSend.toString());
                    log.debug("Message published. \n" + objectToSend.toString());
                }
            } catch (Exception e) {
                log.error("Error while calling web service.\n" + e.getMessage());
            }
        }
    }

    private Account checkAccountTransaction(String transaction) {
        LinkedList<Account> accountTransactions = new LinkedList<>();
        Account account = null;

        // BAIE - Alterar situação das contas DO
        accountTransactions.add(new Account("MC020", "UPDATE", false, true, false, false, false));

        // BAIE - Abertura conta DO
            accountTransactions.add(new Account("MC029", "CREATE", true, false, false, false, false));
        // BAIE - Abertura conta DO e Cliente ao mesmo tempo
        accountTransactions.add(new Account("MC037", "CREATE", true, false, false, false, false));
        // BAIE - Alterar contas DO
        accountTransactions.add(new Account("MC030", "UPDATE", true, false, false, false, false));
        // BAIE- Encerramento contas DO
        accountTransactions.add(new Account("DOENC", "UPDATE", true, false, false, false, false));

        // BAIE - Account Relations
        accountTransactions.add(new Account("MC067", "UPDATE", false, false, true, false, false));

        // BAIE - Abertura de conta de depósito a prazo
        accountTransactions.add(new Account("MV031", "CREATE", false, false, false, true, false));
        // BAIE - Alteração de conta de depósito a prazo
        accountTransactions.add(new Account("MC028", "UPDATE", false, false, false, true, false));
        // BAIE - Encerramento de conta de depósito a prazo
        accountTransactions.add(new Account("DPENC", "UPDATE", true, false, false, false, false));

        // BAIE - Abertura de conta a crédito
        accountTransactions.add(new Account("DPCGI", "CREATE", false, false, false, false, true));
        // BAIE - Alteração de conta a crédito
        accountTransactions.add(new Account("ELEMC", "CREATE", false, false, false, false, true));

        // MOSVS


        for (Account item: accountTransactions) {
            if(item.getTransaction().equals(transaction)) {
                account = item;
            }
        }

        return account;
    }

    private void accountTransactionProcessor(Account account, JSONObject transacao) {

        if(account.getCreateOrUpdate().equals("CREATE") || account.getCreateOrUpdate().equals("UPDATE")) {

            Response responseRelations = null;
            JSONArray responseBodyRelations = new JSONArray();
            String topic = "";
            String url = "";
            // String accountUrl = "http://10.0.4.3/current-accounts/";
            // String relationsUrl = partyUrl + "relations?partyAcctRelId=";
            // String DepositAccountUrl = "http://10.0.4.3/deposit-accounts/";
            // String creditAccountUrl = "http://10.0.4.3/credit-accounts/";

            try {
                String accountNumber = "";
                String clientNumber = "";
                String user = "";

                if(account.isAccountOnline() || account.isAccountSituation() ||
                        account.isDepositAccount() || account.isCreditAccount()) {

                    if (account.isAccountOnline() || account.isAccountSituation()) {
                        url = accountUrl;
                        accountNumber = transacao.getString("#MCDOCONTA");
                        user = transacao.getString("#MCDOUSERC");
                    }

                    if(account.isDepositAccount()) {
                        url = depositAccountUrl;
                        if(account.getCreateOrUpdate().equals("CREATE")) {
                            accountNumber = transacao.getString("#CMDPCONTA");
                            user = transacao.getString("#CMDPUSERC");
                        } else {
                            accountNumber = transacao.getString("#ALDPCONTA");
                            user = transacao.getString("#ALDPUSERC");
                        }
                    }

                    if(account.isCreditAccount()) {
                        url = creditAccountUrl;
                        if(account.getCreateOrUpdate().equals("CREATE")) {
                            accountNumber = transacao.getString("#PCRFCONTA");
                            user = transacao.getString("#PCRFUSERC");
                        } else {
                            accountNumber = transacao.getString("#MCRDCONTA");
                            user = transacao.getString("#MCRDUSERC");
                        }
                    }

                    switch (account.getCreateOrUpdate()) {
                        case "CREATE":
                            if(account.isAccountOnline()) topic = "CurrentAccountCreated:Request";
                            if(account.isDepositAccount()) topic = "DepositAccountCreated:Request";
                            if(account.isCreditAccount()) topic = "CreditAccountCreated:Request";
                            break;
                        case "UPDATE":
                            if(account.isAccountOnline()) topic = "CurrentAccountUpdated:Request";
                            if(account.isAccountSituation()) topic = "CurrentAccountSituationUpdated:Request";
                            if(account.isDepositAccount()) topic = "DepositAccountUpdated:Request";
                            if(account.isCreditAccount()) topic = "CreditAccountUpdated:Request";
                            break;
                    }

                    processAccountRequest(url, accountNumber, accountRelationsUrl, topic, user);

                }else if(account.isRelatations()) {
                    clientNumber = transacao.getString("#MCLENCLI");
                    user = transacao.getString("#MCLEUSERC");
                    topic = "CurrentAccountRelationsUpdated:Request";

                    log.debug("Calling WebService " + accountRelationsUrl + accountNumber);
                    Request request2 = new Request.Builder()
                            .url(accountRelationsUrl + clientNumber)
                            .build();

                    OkHttpClient client = new OkHttpClient();
                    responseRelations = client.newCall(request2).execute();
                    log.debug("Resquest sent successfully!");

                    log.debug("Response Received: \n" + responseRelations.body().toString() + "\n");
                    responseBodyRelations = new JSONArray(responseRelations.body().string());

                    for(int i = 0; i < responseBodyRelations.length(); i++) {
                        log.debug("Publishing" + topic + " message on NATS...");
                        JSONObject objectToSend = responseBodyRelations.getJSONObject(i);
                        objectToSend.put("RequestUser", user);
                        objectToSend.put("RequestId", NATSUtils.generateRequestID("CARU"));
                        BankaMWApplication.getProc().publish(topic, objectToSend.toString());
                        log.debug("Message published.");
                    }
                }

            } catch (Exception e) {
                log.error("Error while calling web service.\n" + e.getMessage());
            }
        }
    }

    private void processAccountRequest(String accountUrl, String accountNum, String relationsUrl, String topic, String user) {

        Response responseAccount = null;
        JSONArray responseBodyAccount = new JSONArray();
        Response responseRelations = null;
        JSONArray responseBodyRelations = new JSONArray();

        try {
            log.debug("Calling WebService " + accountUrl + accountNum);
            Request request = new Request.Builder()
                    .url(accountUrl + accountNum)
                    .build();

            OkHttpClient clientAccount = new OkHttpClient();
            responseAccount = clientAccount.newCall(request).execute();
            log.debug("Resquest sent successfully!");

            log.debug("Response Received: \n" + responseAccount + "\n");
            responseBodyAccount = new JSONArray(responseAccount.body().string());

            for(int i = 0; i < responseBodyAccount.length(); i++) {

                JSONObject objectToSend = new JSONObject();

                String clientNum = responseBodyAccount.getJSONObject(i).getJSONObject("acctInfo").getString("ownership");

                log.debug("Calling WebService " + relationsUrl+ clientNum);
                Request relationsRequest = new Request.Builder()
                        .url(relationsUrl + clientNum)
                        .build();

                OkHttpClient clientRelations = new OkHttpClient();
                responseRelations = clientRelations.newCall(relationsRequest).execute();
                log.debug("Resquest sent successfully!");

                log.debug("Response Received: \n" + responseRelations.body() + "\n");
                responseBodyRelations = new JSONArray(responseRelations.body().string());

                log.debug("Publishing " + topic + " on NATS...");
                objectToSend.put("Account", responseBodyAccount.getJSONObject(i));
                objectToSend.put("Relations", responseBodyRelations.getJSONObject(i));
                objectToSend.put("RequestId", NATSUtils.generateRequestID("ACRT"));
                objectToSend.put("RequestUser", user);
                BankaMWApplication.getProc().publish(topic, objectToSend.toString());
                log.debug("Message published.\n" + objectToSend.toString());
            }
        } catch (Exception e) {
            log.error("Error while processing account request.\n" + e.getMessage());
        }
    }

    private CurrentAccountMovement checkCurrentAccountMovement(String transaction) {
        LinkedList<CurrentAccountMovement> currentAccountMovements = new LinkedList<>();
        CurrentAccountMovement currentAccountMovement = null;

        currentAccountMovements.add(new CurrentAccountMovement("MV002", "CREATE"));
        currentAccountMovements.add(new CurrentAccountMovement("MOSVS", "CREATE"));
        currentAccountMovements.add(new CurrentAccountMovement("MV016", "CREATE"));

        for (CurrentAccountMovement item: currentAccountMovements) {
            if(item.getTransaction().equals(transaction)) {
                currentAccountMovement = item;
            }
        }

        return currentAccountMovement;
    }

//    private void currentAccountMovementTransactionProcessor(CurrentAccountMovement currentAccountMovement, JSONObject transacao) {
//
//        Response response = null;
//        OkHttpClient client = new OkHttpClient();
//        String topic = "";
//        String url = "http://10.0.4.3/payment-order/movements-search";
//
//        try {
//            String operationNumber = transacao.getString("#MVDONOPR");
//            String operationCode = transacao.getString("#MVDOCOPE");
//            String documentNumber = transacao.getString("#MVDONDOC");
//            String user = transacao.getString("#MVDOUSERC");
//
//            if(operationCode.equals("1") || operationCode.equals("2") || operationCode.equals("5") ||
//                    operationCode.equals("120") || operationCode.equals("121") || operationCode.equals("580") ||
//                    operationCode.equals("583")) {
//
//   mc             url = url + "?operationNumber=" + operationNumber +
//                        "&operationCode=" + operationCode +
//                        "&documentNumber=" + documentNumber;
//
//
//                log.debug("Calling WebService " + url);
//                Request request = new Request.Builder()
//                        .url(url)
//                        .build();
//
//                response = client.newCall(request).execute();
//                log.debug("Resquest sent successfully!");
//
//                JSONArray responseBody = new JSONArray(response.body().string());
//
//                for (int i = 0; i < responseBody.length(); i++) {
//
//                    log.debug("Response Received: \n" + responseBody.getJSONObject(i).toString() + "\n");
//                    JSONObject objectToSend;
//                    switch (operationCode) {
//                        case "1":
//                            //BAIE --> 1 - Depósito Numerário
//                            topic = "CurrentAccountMovementDeposit:Request";
//                            log.debug("Publishing " + topic + " message on NATS...");
//                            objectToSend = responseBody.getJSONObject(i);
//                            objectToSend.put("RequestUser", user);
//                            BankaMWApplication.getProc().publish(topic, objectToSend.toString());
//                            log.debug("Message published. \n" + objectToSend.toString());
//                            break;
//                        case "2":
//                            //BAIE --> 2 - Levantamento
//                            topic = "CurrentAccountMovementWithdrawal:Request";
//                            log.debug("Publishing " + topic + " message on NATS...");
//                            objectToSend = responseBody.getJSONObject(i);
//                            objectToSend.put("RequestUser", user);
//                            BankaMWApplication.getProc().publish(topic, objectToSend.toString());
//                            log.debug("Message published. \n" + objectToSend.toString());
//                            break;
//                        case "5":
//                            //BAIE --> 5 - Depósito de cheques
//                            topic = "CurrentAccountMovementCheckDeposit:Request";
//                            log.debug("Publishing " + topic + " message on NATS...");
//                            objectToSend = responseBody.getJSONObject(i);
//                            objectToSend.put("RequestUser", user);
//                            BankaMWApplication.getProc().publish(topic, objectToSend.toString());
//                            log.debug("Message published. \n" + objectToSend.toString());
//                            break;
//                        case "120":
//                            //BAIE --> 120   Transferencia a DEBITO (Interna)
//                            topic = "CurrentAccountMovementInternalSent:Request";
//                            log.debug("Publishing " + topic + " message on NATS...");
//                            objectToSend = responseBody.getJSONObject(i);
//                            objectToSend.put("RequestUser", user);
//                            BankaMWApplication.getProc().publish(topic, objectToSend.toString());
//                            log.debug("Message published. \n" + objectToSend.toString());
//                            break;
//                        case "121":
//                            //BAIE -->  121   Transferencia a CREDITO (Interna)
//                            topic = "CurrentAccountMovementInternalReceived:Request";
//                            log.debug("Publishing " + topic + " message on NATS...");
//                            objectToSend = responseBody.getJSONObject(i);
//                            objectToSend.put("RequestUser", user);
//                            BankaMWApplication.getProc().publish(topic, objectToSend.toString());
//                            log.debug("Message published. \n" + objectToSend.toString());
//                            break;
//                        case "580":
//                            //BAIE --> 580   Emissão Trf. Crédito - Débito DO
//                            topic = "CurrentAccountMovementSEPASent:Request";
//                            log.debug("Publishing " + topic + " message on NATS...");
//                            objectToSend = responseBody.getJSONObject(i);
//                            objectToSend.put("RequestUser", user);
//                            BankaMWApplication.getProc().publish(topic, objectToSend.toString());
//                            log.debug("Message published. \n" + objectToSend.toString());
//                            break;
//                        case "583":
//                            //BAIE -->  583   Recepção Trf. Crédito - Crédito DO
//                            topic = "CurrentAccountMovementSEPAReceived:Request";
//                            log.debug("Publishing " + topic + " message on NATS...");
//                            objectToSend = responseBody.getJSONObject(i);
//                            objectToSend.put("RequestUser", user);
//                            BankaMWApplication.getProc().publish(topic, objectToSend.toString());
//                            log.debug("Message published. \n" + objectToSend.toString());
//                            break;
//                    }
//                }
//            }
//        } catch (Exception e) {
//            log.error("Error while calling web service.\n" + e.getMessage());
//        }
//    }

}
