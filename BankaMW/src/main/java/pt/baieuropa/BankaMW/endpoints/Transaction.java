package pt.baieuropa.BankaMW.endpoints;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.ObjectWriter;
import com.google.gson.Gson;
import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;
import org.springframework.ws.config.annotation.EnableWs;
import pt.baieuropa.BankaMW.BankaMWApplication;
import pt.baieuropa.BankaMW.models.Evaluate.*;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;
import javax.jws.soap.SOAPBinding;
import javax.xml.bind.annotation.XmlElement;
import java.math.BigDecimal;

@WebService
@SOAPBinding(style = SOAPBinding.Style.RPC)
@EnableWs
public class Transaction {

    private static final Logger log = LogManager.getLogger(BankaTransaction.class);

    @WebMethod(operationName = "EvaluateTransactionOnline")
    public int EvaluateTransactionOnline(@XmlElement(required=true) @WebParam(partName = "AccountNumber_Ext", name = "AccountNumber_Ext") String AccountNumber_Ext,
                                     @XmlElement(required=true) @WebParam(partName = "TransactionNumber", name = "TransactionNumber") String TransactionNumber,
                                     @XmlElement(required=true) @WebParam(partName = "TransactionOperationCode", name = "TransactionOperationCode") String TransactionOperationCode,
                                     @XmlElement(required=true) @WebParam(partName = "TransactionAmount_Original", name = "TransactionAmount_Original") BigDecimal TransactionAmount_Original,
                                     @XmlElement(required=true) @WebParam(partName = "TransactionCurrency_Original", name = "TransactionCurrency_Original") String TransactionCurrency_Original,
                                     @XmlElement(required=true) @WebParam(partName = "TransactionAmount_Converted", name = "TransactionAmount_Converted") BigDecimal TransactionAmount_Converted,
                                     @WebParam(partName = "TransactionAmount_Account", name = "TransactionAmount_Account") BigDecimal TransactionAmount_Account,
                                     @WebParam(partName = "TransactionCurrency_Account", name = "TransactionCurrency_Account") String TransactionCurrency_Account,
                                     @XmlElement(required=true) @WebParam(partName = "TransactionDateTime", name = "TransactionDateTime") String TransactionDateTime,
                                     @XmlElement(required=true) @WebParam(partName = "MovementLaunchDateTime", name = "MovementLaunchDateTime") String MovementLaunchDateTime,
                                     @XmlElement(required=true) @WebParam(partName = "CreditDebit", name = "CreditDebit") String CreditDebit,
                                     @WebParam(partName = "CorrespondingBankCode", name = "CorrespondingBankCode") String CorrespondingBankCode,
                                     @WebParam(partName = "CounterpartBankCode", name = "CounterpartBankCode") String CounterpartBankCode,
                                     @WebParam(partName = "CounterpartName", name = "CounterpartName") String CounterpartName,
                                     @WebParam(partName = "CounterpartCountryCode", name = "CounterpartCountryCode") String CounterpartCountryCode,
                                     @WebParam(partName = "ClientNumber_Ext", name = "ClientNumber_Ext") String ClientNumber_Ext,
                                     @WebParam(partName = "CounterpartAccountNumber", name = "CounterpartAccountNumber") String CounterpartAccountNumber,
                                     @XmlElement(required=true) @WebParam(partName = "TransactionType", name = "TransactionType") String TransactionType,
                                     @XmlElement(required=true) @WebParam(partName = "TransactionStatus", name = "TransactionStatus") String TransactionStatus,
                                     @WebParam(partName = "CAE", name = "CAE") String CAE,
                                     @WebParam(partName = "CAE2", name = "CAE2") String CAE2,
                                     @WebParam(partName = "Branch", name = "Branch") String Branch,
                                     @WebParam(partName = "Field70", name = "Field70") String Field70,
                                     @WebParam(partName = "RequestUser", name = "RequestUser") String RequestUser,
                                     @WebParam(partName = "Flexfields", name = "Flexfields") FlexfieldStructure[] Flexfields) {

        // BAIE - Verificação de campos obrigatórios
        if(AccountNumber_Ext == null || TransactionNumber == null || TransactionOperationCode == null || TransactionAmount_Original == null ||
            TransactionCurrency_Original == null || TransactionAmount_Converted == null || TransactionDateTime == null || MovementLaunchDateTime == null ||
            CreditDebit == null || TransactionType == null || TransactionStatus == null) {
            if(AccountNumber_Ext == null) log.error("The property AccountNumber_Ext can't be NULL.");
            if(TransactionNumber == null) log.error("The property TransactionNumber can't be NULL.");
            if(TransactionOperationCode == null) log.error("The property TransactionOperationCode can't be NULL.");
            if(TransactionAmount_Original == null) log.error("The property TransactionAmount_Original can't be NULL.");
            if(TransactionCurrency_Original == null) log.error("The property TransactionCurrency_Original can't be NULL.");
            if(TransactionAmount_Converted == null) log.error("The property TransactionAmount_Converted can't be NULL.");
            if(TransactionDateTime == null) log.error("The property TransactionDateTime can't be NULL.");
            if(MovementLaunchDateTime == null) log.error("The property MovementLaunchDateTime can't be NULL.");
            if(CreditDebit == null) log.error("The property CreditDebit can't be NULL.");
            if(TransactionType == null) log.error("The property TransactionType can't be NULL.");
            if(TransactionStatus == null) log.error("The property TransactionStatus can't be NULL.");
            return 0;
        }

        try {
            log.debug("Executing EvaluateTransactionOnline");

            log.debug("Creating Object...");
            EvaluateTransactionOnline request = new EvaluateTransactionOnline();
            request.setAccountNumber_Ext(AccountNumber_Ext);
            request.setTransactionNumber(TransactionNumber);
            request.setTransactionOperationCode(TransactionOperationCode);
            request.setTransactionAmount_Original(TransactionAmount_Original);
            request.setTransactionCurrency_Original(TransactionCurrency_Original);
            request.setTransactionAmount_Converted(TransactionAmount_Converted);
            if(TransactionAmount_Account != null) request.setTransactionAmount_Account(TransactionAmount_Account);
            if(TransactionCurrency_Account != null) request.setTransactionCurrency_Account(TransactionCurrency_Account);
            request.setTransactionDateTime(TransactionDateTime);
            request.setMovementLaunchDateTime(MovementLaunchDateTime);
            request.setCreditDebit(CreditDebit);
            if(CorrespondingBankCode != null) request.setCorrespondingBankCode(CorrespondingBankCode);
            if(CounterpartBankCode != null) request.setCounterpartBankCode(CounterpartBankCode);
            if(CounterpartName != null) request.setCounterpartName(CounterpartName);
            if(CounterpartCountryCode != null) request.setCounterpartCountryCode(CounterpartCountryCode);
            if(ClientNumber_Ext != null) request.setClientNumber_Ext(ClientNumber_Ext);
            if(CounterpartAccountNumber != null) request.setCounterpartAccountNumber(CounterpartAccountNumber);
            request.setTransactionType(TransactionType);
            request.setTransactionStatus(TransactionStatus);
            if(CAE != null) request.setCAE(CAE);
            if(CAE2 != null) request.setCAE2(CAE2);
            if(Branch != null) request.setBranch(Branch);
            if(Field70 != null) request.setField70(Field70);
            if(RequestUser != null) request.setRequestUser(RequestUser);
            // BAIE -   FlexFields
            if (Flexfields != null) {
                FlexfieldStructure[] arrayOfFlexfieldStructure = Flexfields;
                request.setFlexfields(arrayOfFlexfieldStructure);
            }
            log.debug("Object Created.");

            log.debug("Converting Object to JSON...");
            ObjectWriter ow = new ObjectMapper().writer().withDefaultPrettyPrinter();
            String json = ow.writeValueAsString(request);
            log.debug("Object Converted.");

            //BAI PUBLISH no topic EvaluateTransactionOnline
            log.debug("Publishing message on NATS...");
            BankaMWApplication.getProc().publish("EvaluateTransactionOnline:Request", json);
            log.debug("Message published.");

            log.info("EvaluateTransactionOnline Executed.\n" + json);
            return 1;
        } catch (Exception ex) {
            log.error("Error on EvaluateTransactionOnline: \n" + ex.getMessage());
            return 0;
        }
    }

    @WebMethod(operationName = "EvaluateTransactionCB")
    public int EvaluateTransactionCB(@XmlElement(required=true) @WebParam(partName = "TransactionNumber", name = "TransactionNumber") String TransactionNumber,
                                        @XmlElement(required=true) @WebParam(partName = "TransactionOperationCode", name = "TransactionOperationCode") String TransactionOperationCode,
                                        @XmlElement(required=true) @WebParam(partName = "TransactionAmount_Original", name = "TransactionAmount_Original") BigDecimal TransactionAmount_Original,
                                        @XmlElement(required=true) @WebParam(partName = "TransactionCurrency_Original", name = "TransactionCurrency_Original") String TransactionCurrency_Original,
                                        @XmlElement(required=true) @WebParam(partName = "TransactionAmount_Converted", name = "TransactionAmount_Converted") BigDecimal TransactionAmount_Converted,
                                        @XmlElement(required=true) @WebParam(partName = "TransactionDateTime", name = "TransactionDateTime") String TransactionDateTime,
                                        @XmlElement(required=true) @WebParam(partName = "TransactionStatus", name = "TransactionStatus") String TransactionStatus,
                                        @XmlElement(required=true) @WebParam(partName = "OriginNClientID", name = "OriginNClientID") String OriginNClientID,
                                        @XmlElement(required=true) @WebParam(partName = "OriginNClientName", name = "OriginNClientName") String OriginNClientName,
                                        @XmlElement(required=true) @WebParam(partName = "OriginCountryISO", name = "OriginCountryISO") String OriginCountryISO,
                                        @XmlElement(required=true) @WebParam(partName = "OriginCorrespondingBank", name = "OriginCorrespondingBank") String OriginCorrespondingBank,
                                        @XmlElement(required=true) @WebParam(partName = "DestinationNClientID", name = "DestinationNClientID") String DestinationNClientID,
                                        @XmlElement(required=true) @WebParam(partName = "DestinationNClientName", name = "DestinationNClientName") String DestinationNClientName,
                                        @XmlElement(required=true) @WebParam(partName = "DestinationCountryISO", name = "DestinationCountryISO") String DestinationCountryISO,
                                        @XmlElement(required=true) @WebParam(partName = "DestinationCorrespondingBank", name = "DestinationCorrespondingBank") String DestinationCorrespondingBank,
                                        @WebParam(partName = "Field70", name = "Field70") String Field70,
                                        @WebParam(partName = "RequestUser", name = "RequestUser") String RequestUser,
                                        @WebParam(partName = "Flexfields", name = "Flexfields") FlexfieldStructure[] Flexfields) {

        // BAIE - Verificação de campos obrigatórios
        if(TransactionNumber == null || TransactionOperationCode == null || TransactionAmount_Original == null ||
            TransactionCurrency_Original == null || TransactionAmount_Converted == null || TransactionDateTime == null || TransactionStatus == null ||
            OriginNClientID == null || OriginNClientName == null || OriginCountryISO == null || OriginCorrespondingBank == null || DestinationNClientID == null ||
            DestinationNClientName == null || DestinationCountryISO == null || DestinationCorrespondingBank == null) {

            if(TransactionNumber == null) log.error("The property TransactionNumber can't be NULL.");
            if(TransactionOperationCode == null) log.error("The property TransactionOperationCode can't be NULL.");
            if(TransactionAmount_Original == null) log.error("The property TransactionAmount_Original can't be NULL.");
            if(TransactionCurrency_Original == null) log.error("The property TransactionCurrency_Original can't be NULL.");
            if(TransactionAmount_Converted == null) log.error("The property TransactionAmount_Converted can't be NULL.");
            if(TransactionDateTime == null) log.error("The property TransactionDateTime can't be NULL.");
            if(TransactionStatus == null) log.error("The property TransactionStatus can't be NULL.");
            if(OriginNClientID == null) log.error("The property OriginNClientID can't be NULL.");
            if(OriginNClientName == null) log.error("The property OriginNClientName can't be NULL.");
            if(OriginCountryISO == null) log.error("The property OriginCountryISO can't be NULL.");
            if(OriginCorrespondingBank == null) log.error("The property OriginCorrespondingBank can't be NULL.");
            if(DestinationNClientID == null) log.error("The property DestinationNClientID can't be NULL.");
            if(DestinationNClientName == null) log.error("The property DestinationNClientName can't be NULL.");
            if(DestinationCountryISO == null) log.error("The property DestinationCountryISO can't be NULL.");
            if(DestinationCorrespondingBank == null) log.error("The property DestinationCorrespondingBank can't be NULL.");
            return 0;
        }

        try {
            log.debug("Executing EvaluateAccountOnline");

            log.debug("Creating Object...");
            EvaluateTransactionCB evaluateTransactionCB = new EvaluateTransactionCB(TransactionNumber,TransactionOperationCode, TransactionAmount_Original,TransactionCurrency_Original,
                    TransactionAmount_Converted,TransactionDateTime,TransactionStatus,OriginNClientID,OriginNClientName,OriginCountryISO,OriginCorrespondingBank,DestinationNClientID,
                    DestinationNClientName, DestinationCountryISO,DestinationCorrespondingBank,Field70,RequestUser,Flexfields);
            log.debug("Object Created.");

            log.debug("Converting Object to JSON...");
            Gson gson = new Gson();
            String json = gson.toJson(evaluateTransactionCB);
            log.debug("Object Converted.");

            log.debug("Publishing message on NATS...");
            BankaMWApplication.getProc().publish("EvaluateTransactionCB:Request", json);
            log.debug("Message published.");

            log.info("EvaluateTransactionCB Executed.\n" + json);
            return 1;
        } catch (Exception ex) {
            log.error("Error on EvaluateTransactionCB: \n" + ex.getMessage());
            return 0;
        }
    }
}
