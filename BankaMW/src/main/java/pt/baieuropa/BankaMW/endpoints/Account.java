package pt.baieuropa.BankaMW.endpoints;

import com.google.gson.Gson;
import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;
import org.springframework.ws.config.annotation.EnableWs;
import pt.baieuropa.BankaMW.BankaMWApplication;
import pt.baieuropa.BankaMW.models.Evaluate.*;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;
import javax.jws.soap.SOAPBinding;
import javax.xml.bind.annotation.XmlElement;
import java.math.BigDecimal;

@WebService
@SOAPBinding(style = SOAPBinding.Style.RPC)
@EnableWs
public class Account {
    private static final Logger log = LogManager.getLogger(Account.class);

    @WebMethod(operationName = "EvaluateAccountOnline")
    public int EvaluateAccountOnline(@XmlElement(required=true) @WebParam(partName = "AccountNumber", name = "AccountNumber") String AccountNumber,
                                     @XmlElement(required=true) @WebParam(partName = "AccountType", name = "AccountType") String AccountType,
                                     @XmlElement(required=true) @WebParam(partName = "AccountCategory", name = "AccountCategory") String AccountCategory,
                                     @XmlElement(required=true) @WebParam(partName = "AccountEntityType", name = "AccountEntityType") String AccountEntityType,
                                     @WebParam(partName = "ClientNumber_Ext", name = "ClientNumber_Ext") String ClientNumber_Ext,
                                     @WebParam(partName = "AccountStatus", name = "AccountStatus") String AccountStatus,
                                     @WebParam(partName = "SegmentType", name = "SegmentType") String SegmentType,
                                     @WebParam(partName = "SubscriptionChannel", name = "SubscriptionChannel") String SubscriptionChannel,
                                     @WebParam(partName = "AccountCurrency", name = "AccountCurrency") String AccountCurrency,
                                     @WebParam(partName = "EntityAccountList", name = "EntityAccountList") EntityAccount[] EntityAccountList,
                                     @WebParam(partName = "RequestUser", name = "RequestUser") String RequestUser,
                                     @WebParam(partName = "ClientCreationDate_Ext", name = "ClientCreationDate_Ext") String ClientCreationDate_Ext,
                                     @WebParam(partName = "AccountCreationDate", name = "AccountCreationDate") String AccountCreationDate,
                                     @WebParam(partName = "BalanceAverage", name = "BalanceAverage") BigDecimal BalanceAverage,
                                     @WebParam(partName = "DateBalanceAverage", name = "DateBalanceAverage") String DateBalanceAverage,
                                     @WebParam(partName = "Action", name = "Action") String Action,
                                     @WebParam(partName = "Flexfields", name = "Flexfields") FlexfieldStructure[] Flexfields) {

        // BAIE - Verificação de campos obrigatórios
        if(AccountNumber == null || AccountType == null || AccountCategory == null || AccountEntityType == null) {
            if(AccountNumber == null) log.error("The property AccountNumber can't be NULL.");
            if(AccountType == null) log.error("The property AccountType can't be NULL.");
            if(AccountCategory == null) log.error("The property AccountCategory can't be NULL.");
            if(AccountEntityType == null) log.error("The property AccountEntityType can't be NULL.");
            return 0;
        }

        try {
            log.debug("Executing EvaluateAccountOnline");

            log.debug("Creating Object...");
            AccountCreationRequest request = new AccountCreationRequest();
            request.setAccountNumber(AccountNumber);
            request.setAccountType(AccountType);
            request.setAccountCategory(AccountCategory);
            request.setAccountEntityType(AccountEntityType);
            if(ClientNumber_Ext != null) request.setClientNumberExt(ClientNumber_Ext);
            if(AccountStatus != null) request.setAccountStatus(AccountStatus);
            if(SegmentType != null) request.setSegmentType(SegmentType);
            if(SubscriptionChannel != null) request.setSubscriptionChannel(SubscriptionChannel);
            if(AccountCurrency != null) request.setAccountCurrency(AccountCurrency);
            if(EntityAccountList != null) {
                EntityAccount[] arrayOfEntityAccount = EntityAccountList;
                request.setEntityAccountList(arrayOfEntityAccount);
            }
            if(RequestUser != null) request.setRequestUser(RequestUser);
            if(ClientCreationDate_Ext != null) request.setClientCreationDateExt(ClientCreationDate_Ext);
            if(AccountCreationDate != null) request.setAccountCreationDate(AccountCreationDate);
            if(BalanceAverage != null) request.setBalanceAverage(BalanceAverage);
            if(DateBalanceAverage != null) request.setDateBalanceAverage(DateBalanceAverage);
            if(Action != null) request.setAction(Action);
            if (Flexfields != null) {
                FlexfieldStructure[] arrayOfFlexfieldStructure = Flexfields;
                request.setFlexfields(arrayOfFlexfieldStructure);
            }
            log.debug("Object Created.");

            log.debug("Converting Object to JSON...");
            Gson gson = new Gson();
            String json = gson.toJson(request);
            log.debug("Object Converted.");

            log.debug("Publishing message on NATS...");
            BankaMWApplication.getProc().publish("A", json);
            log.debug("Message Published.");

            log.info("EvaluateAccountOnline Executed.\n" + json);
            return 1;
        } catch (Exception ex) {
            log.error("Error on EvaluateAccountOnline: \n" + ex.getMessage());
            return 0;
        }
    }

    @WebMethod(operationName = "AccountRelationsByClient")
    public int AccountRelationsByClient(@XmlElement(required=true) @WebParam(partName = "ClientNumber_Ext", name = "ClientNumber_Ext") String ClientNumber_Ext,
                                        @XmlElement(required=true) @WebParam(partName = "EntityAccountList", name = "EntityAccountList") EntityAccount[] EntityAccountList) {

        // BAIE - Verificação de campos obrigatórios
        if(ClientNumber_Ext == null || EntityAccountList == null) {
            if(ClientNumber_Ext == null) log.error("The property ClientNumber_Ext can't be NULL.");
            if(EntityAccountList == null) log.error("The property EntityAccountList can't be NULL.");
            return 0;
        }

        try {
            log.debug("Executing AccountRelationsByClient");

            log.debug("Creating Object...");
            AccountRelationsByClient accountRelationsByClient = new AccountRelationsByClient();
            AccountRelationsByClientComplex request = new AccountRelationsByClientComplex();
            request.setClientNumberExt(ClientNumber_Ext);
            if(EntityAccountList != null) {
                EntityAccount[] arrayOfEntityAccount = EntityAccountList;
                request.setEntityAccountList(arrayOfEntityAccount);
            }
            accountRelationsByClient.setAccountRelationsByClientComplex(request);
            log.debug("Object Created.");

            log.debug("Converting Object to JSON...");
            Gson gson = new Gson();
            String json = gson.toJson(accountRelationsByClient);
            log.debug("Object Converted.");

            log.debug("Publishing message on NATS...");
            BankaMWApplication.getProc().publish("AccountRelationsByClient", json);
            log.debug("Message Published.");

            log.info("AccountRelationsByClient Executed.\n" + json);
            return 1;
        } catch (Exception ex) {
            log.error("Error on AccountRelationsByClient: \n" + ex.getMessage());
            return 0;
        }
    }
}
