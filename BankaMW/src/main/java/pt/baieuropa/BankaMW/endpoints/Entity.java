package pt.baieuropa.BankaMW.endpoints;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.ObjectWriter;
import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;
import org.springframework.ws.config.annotation.EnableWs;
import pt.baieuropa.BankaMW.BankaMWApplication;
import pt.baieuropa.BankaMW.models.Evaluate.*;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;
import javax.jws.soap.SOAPBinding;
import javax.xml.bind.annotation.*;
import java.math.BigDecimal;

@WebService
@SOAPBinding(style = SOAPBinding.Style.RPC)
@EnableWs
public class Entity {

    private static final Logger log = LogManager.getLogger(Entity.class);

    @WebMethod(operationName = "EvaluateEntityOnline")
    public int EvaluateEntityOnline(@XmlElement(required = true) @WebParam(partName = "Number_Ext", name = "Number_Ext") String Number_Ext,
                                    @XmlElement(required = true) @WebParam(partName = "Name", name = "Name") String Name,
                                    @XmlElement(required = true) @WebParam(partName = "EntityCategory", name = "EntityCategory") String EntityCategory,
                                    @XmlElement(required = true) @WebParam(partName = "EntityType", name = "EntityType") String EntityType,
                                    @WebParam(partName = "BirthDate", name = "BirthDate") String BirthDate,
                                    @WebParam(partName = "Gender", name = "Gender") String Gender,
                                    @WebParam(partName = "MaritalStatus", name = "MaritalStatus") String MaritalStatus,
                                    @WebParam(partName = "Nationality", name = "Nationality") String Nationality,
                                    @WebParam(partName = "Nationality2", name = "Nationality2") String Nationality2,
                                    @WebParam(partName = "Nationality3", name = "Nationality3") String Nationality3,
                                    @WebParam(partName = "CountryResidence", name = "CountryResidence") String CountryResidence,
                                    @WebParam(partName = "ResidenceCode", name = "ResidenceCode") String ResidenceCode,
                                    @WebParam(partName = "BirthPlace", name = "BirthPlace") String BirthPlace,
                                    @WebParam(partName = "ConstitutionDate", name = "ConstitutionDate") String ConstitutionDate,
                                    @WebParam(partName = "Profession", name = "Profession") String Profession,
                                    @WebParam(partName = "ProfessionalActivity", name = "ProfessionalActivity") String ProfessionalActivity,
                                    @WebParam(partName = "EducationalQualifications", name = "EducationalQualifications") String EducationalQualifications,
                                    @WebParam(partName = "SectorialCode", name = "SectorialCode") String SectorialCode,
                                    @WebParam(partName = "SocialCapital", name = "SocialCapital") BigDecimal SocialCapital,
                                    @WebParam(partName = "EntityIncome", name = "EntityIncome") BigDecimal EntityIncome,
                                    @WebParam(partName = "Parent1Name", name = "Parent1Name") String Parent1Name,
                                    @WebParam(partName = "Parent2Name", name = "Parent2Name") String Parent2Name,
                                    @WebParam(partName = "Currency", name = "Currency") String Currency,
                                    @WebParam(partName = "CAE", name = "CAE") String CAE,
                                    @WebParam(partName = "CAE2", name = "CAE2") String CAE2,
                                    @WebParam(partName = "CAE3", name = "CAE3") String CAE3,
                                    @WebParam(partName = "SocietyType", name = "SocietyType") String SocietyType,
                                    @WebParam(partName = "StockMarketCode", name = "StockMarketCode") String StockMarketCode,
                                    @WebParam(partName = "CreatedAt_Ext", name = "CreatedAt_Ext") String CreatedAt_Ext,
                                    @WebParam(partName = "RequestUser", name = "RequestUser") String RequestUser,
                                    @XmlElement(required=true) @WebParam(partName = "action", name = "action") String action,
                                    @WebParam(partName = "IsPEPDeclared", name = "IsPEPDeclared") Boolean IsPEPDeclared,
                                    @WebParam(partName = "IsPEPDescription", name = "IsPEPDescription") String IsPEPDescription,
                                    @WebParam(partName = "IsPresencial", name = "IsPresencial") Boolean IsPresencial,
                                    @WebParam(partName = "ImportDate", name = "ImportDate") String ImportDate,
                                    @WebParam(partName = "Flexfields", name = "Flexfields") FlexfieldStructure[] Flexfields,
                                    @WebParam(partName = "DocumentList", name = "DocumentList") Document[] DocumentList,
                                    @WebParam(partName = "ContactList", name = "ContactList") Contact[] ContactList,
                                    @WebParam(partName = "Photo", name = "Photo") byte[] Photo,
                                    @WebParam(partName = "BeneficialOwnersList", name = "BeneficialOwnersList") EntityBeneficialOwnersList[] BeneficialOwnersList) {

        // BAIE - Verificação de campos obrigatórios
        if(Number_Ext == null || Name == null || EntityCategory == null || EntityType == null || action == null) {
            if(Number_Ext == null) log.error("The property Number_Ext can't be NULL.");
            if(Name == null) log.error("The property Name can't be NULL.");
            if(EntityCategory == null) log.error("The property EntityCategory can't be NULL.");
            if(EntityType == null) log.error("The property EntityType can't be NULL.");
            if(action == null) log.error("The property action can't be NULL.");
            return 0;
        }
        try {
            log.debug("Executing EvaluateEntityOnline");

            log.debug("Creating Object...");
            EntityEvaluationRequest request = new EntityEvaluationRequest();
            request.setNumberExt(Number_Ext);
            request.setName(Name);
            request.setEntityCategory(EntityCategory);
            request.setEntityType(EntityType);
            if (BirthDate != null) request.setBirthDate(BirthDate);
            if (Gender != null) request.setGender(Gender);
            if (MaritalStatus != null) request.setMaritalStatus(MaritalStatus);
            if (Nationality != null) request.setNationality(Nationality);
            if (Nationality2 != null) request.setNationality2(Nationality2);
            if (Nationality3 != null) request.setNationality3(Nationality3);
            if (CountryResidence != null) request.setCountryResidence(CountryResidence);
            if (ResidenceCode != null) request.setResidenceCode(ResidenceCode);
            if (BirthPlace != null) request.setBirthPlace(BirthPlace);
            if (ConstitutionDate != null) request.setConstitutionDate(ConstitutionDate);
            if (Profession != null) request.setProfession(Profession);
            if (ProfessionalActivity != null) request.setProfessionalActivity(ProfessionalActivity);
            if (EducationalQualifications != null) request.setEducationalQualifications(EducationalQualifications);
            if (SectorialCode != null) request.setSectorialCode(SectorialCode);
            if (SocialCapital != null) request.setSocialCapital(SocialCapital);
            if (EntityIncome != null) request.setEntityIncome(EntityIncome);
            if (Parent1Name != null) request.setParent1Name(Parent1Name);
            if (Parent2Name != null) request.setParent2Name(Parent2Name);
            if (Currency != null) request.setCurrency(Currency);
            if (CAE != null) request.setCAE(CAE);
            if (CAE2 != null) request.setCAE2(CAE2);
            if (CAE3 != null) request.setCAE3(CAE3);
            if (SocietyType != null) request.setSocietyType(SocietyType);
            if (StockMarketCode != null) request.setStockMarketCode(StockMarketCode);
            if (CreatedAt_Ext != null) request.setCreatedAtExt(CreatedAt_Ext);
            if (RequestUser != null) request.setRequestUser(RequestUser);
            request.setAction(action);
            if (IsPEPDeclared != null) request.setIsPEPDeclared(IsPEPDeclared);
            if (IsPEPDescription != null) request.setIsPEPDescription(IsPEPDescription);
            if (IsPresencial != null) request.setIsPEPDeclared(IsPEPDeclared);
            if (ImportDate != null) request.setImportDate(ImportDate);

            // BAIE - RECEBE COMO byte[] MAS TRANSFORMA EM String
            if (Photo != null) request.setPhoto(Photo);

            // BAIE - FlexFields
            if (Flexfields != null) {
                FlexfieldStructure[] arrayOfFlexfieldStructure = Flexfields;
                request.setFlexfields(arrayOfFlexfieldStructure);
            }

            // BAIE - DocumentList
            if (DocumentList != null) {
                Document[] arrayOfDocument = DocumentList;
                request.setDocumentList(arrayOfDocument);
            }

            // BAIE - ContactList
            if (ContactList != null) {
                Contact[] arrayOfContact = ContactList;
                request.setContactList(arrayOfContact);
            }

            // BAIE - BeneficialOwnersList
            if (BeneficialOwnersList != null) {
                EntityBeneficialOwnersList[] arrayOfEntityBeneficialOwnersList = BeneficialOwnersList;
                request.setBeneficialOwnersList(arrayOfEntityBeneficialOwnersList);
            }
            log.debug("Object Created.");

            log.debug("Converting Object to JSON...");
            ObjectWriter ow = new ObjectMapper().writer().withDefaultPrettyPrinter();
            String json = ow.writeValueAsString(request);
            log.debug("Object Converted.");

            //BAI PUBLISH no topic EvaluateTransactionOnline
            log.debug("Publishing message on NATS...");
            BankaMWApplication.getProc().publish("BankaTransaction:Request", json);
            log.debug("Message Published.");

            log.info("EvaluateEntityOnline Executed.\n" + json);
            return 1;
        } catch (Exception ex) {
            log.error("Error on EvaluateEntityOnline: \n" + ex.getMessage());
            ex.printStackTrace();
            return 0;
        }
    }

}
