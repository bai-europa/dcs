package pt.baieuropa.BankaMW.endpoints;

import com.opencsv.CSVReader;
import okhttp3.*;
import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;
import org.json.JSONArray;
import org.json.JSONObject;
import pt.baieuropa.BankaMW.BankaMWApplication;
import pt.baieuropa.BankaMW.models.CurrentAccountMovement;
import pt.baieuropa.BankaMW.models.OperationCode;
import pt.baieuropa.BankaMW.utils.ConfigUtils;
import pt.baieuropa.BankaMW.utils.NATSUtils;

import java.io.FileReader;
import java.io.IOException;
import java.security.NoSuchAlgorithmException;
import java.util.HashMap;
import java.util.Properties;

public class CurrentAccountMovementTransactionProcessor implements Runnable {

    private static final Logger log = LogManager.getLogger(CurrentAccountMovementTransactionProcessor.class);

    public static Properties config = ConfigUtils.loadProps(System.getenv("urls.configuration"));
    private static String movementsSearchUrl = config.getProperty("movementsSearch");

    private CurrentAccountMovement currentAccountMovement;
    private JSONObject transacao;

    public CurrentAccountMovementTransactionProcessor() {
    }

    public CurrentAccountMovementTransactionProcessor(CurrentAccountMovement currentAccountMovement, JSONObject transacao) {
        this.currentAccountMovement = currentAccountMovement;
        this.transacao = transacao;
    }

    public CurrentAccountMovement getCurrentAccountMovement() {
        return currentAccountMovement;
    }

    public void setCurrentAccountMovement(CurrentAccountMovement currentAccountMovement) {
        this.currentAccountMovement = currentAccountMovement;
    }

    public JSONObject getTransacao() {
        return transacao;
    }

    public void setTransacao(JSONObject transacao) {
        this.transacao = transacao;
    }

    @Override
    public void run() {
        Response response = null;
        OkHttpClient client = new OkHttpClient();
        String topic = "";
        HashMap<String, OperationCode> operations = new HashMap<>();

        try {
            CSVReader csvReader = null;
            String[] nextLine;

            csvReader = new CSVReader(new FileReader(System.getenv("DCSOperationCodes")), ',');

            while ((nextLine = csvReader.readNext()) != null) {
                OperationCode operationCode = new OperationCode(
                        nextLine[0], // BAIE - Operation Code
                        nextLine[1], // BAIE - Description
                        nextLine[2], // BAIE - Topic NATS
                        nextLine[3]  // BAIE - Identification NATS
                );
                operations.put(operationCode.getOperationCode(), operationCode);
            }

            // TODO - Arranjar solução elegante
            Thread.sleep(1000);

            String operationNumber = "";
            String operationCode = "";
            String documentNumber = "";
            String user = "";

            if(this.currentAccountMovement.getTransaction().equals("MV002") ||
               this.currentAccountMovement.getTransaction().equals("MOSVS") ||
               this.currentAccountMovement.getTransaction().equals("MV016")) {
                operationNumber = transacao.getString("#MVDONOPR");
                operationCode = transacao.getString("#MVDOCOPE");
                documentNumber = transacao.getString("#MVDONDOC");
                user = transacao.getString("#MVDOUSERC");
            }

            if(operations.containsKey(operationCode)) {

                String newMovementsSearchUrl = movementsSearchUrl + "?operationNumber=" + operationNumber
                        + "&operationCode=" + operationCode
                        + "&documentNumber=" + documentNumber;

                log.debug("Calling WebService " + newMovementsSearchUrl);
                Request request = new Request.Builder()
                        .url(newMovementsSearchUrl)
                        .build();

                response = client.newCall(request).execute();
                log.debug("Resquest sent successfully!");

                JSONArray responseBody = new JSONArray(response.body().string());

                for (int i = 0; i < responseBody.length(); i++) {

                    log.debug("Response Received: \n" + responseBody.getJSONObject(i).toString() + "\n");
                    JSONObject objectToSend;

                    OperationCode specificOperationCode = operations.get(operationCode);
                    topic = specificOperationCode.getTopic();
                    log.debug("Publishing " + topic + " (" + specificOperationCode.getDescription() + ") message on NATS...");
                    objectToSend = responseBody.getJSONObject(i);

                    // BAIE - Se for "C" esmaga o TransactionType com "Crédito"
                    if(objectToSend.getString("CreditDebit").equals("C"))
                        objectToSend.put("TransactionType", "Crédito");

                    // BAIE - Se for "D" esmaga o TransactionType com "Débito"
                    if(objectToSend.getString("CreditDebit").equals("D"))
                        objectToSend.put("TransactionType", "Débito");

                    // BAIE - Preenchimento do RequestUser e do RequestId
                    objectToSend.put("RequestUser", user);
                    objectToSend.put("RequestId", NATSUtils.generateRequestID(specificOperationCode.getIdentificationNATS()));

                    BankaMWApplication.getProc().publish(topic, objectToSend.toString());
                    log.debug("Message published. \n" + objectToSend.toString());

                }

            }
        } catch (IOException | InterruptedException | NoSuchAlgorithmException e) {
            log.error("Error while calling web service.\n" + e.getMessage());
        }
    }
}
