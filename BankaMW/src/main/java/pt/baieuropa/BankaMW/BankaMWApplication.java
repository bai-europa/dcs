package pt.baieuropa.BankaMW;

import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;
import pt.baieuropa.BankaMW.endpoints.*;
import pt.baieuropa.BankaMW.processor.Process;
import pt.baieuropa.BankaMW.utils.ConfigUtils;
import pt.baieuropa.nats.Stan;

import javax.xml.ws.Endpoint;
import java.util.Properties;

public class BankaMWApplication {

    private static String SERVICENAME = "SERVICE";

    public static Process proc;

    private Stan stanController = new Stan();

    private static final Logger log = LogManager.getLogger(BankaMWApplication.class);

    public static Properties config = ConfigUtils.loadProps(System.getenv("urls.configuration"));
    public static String URI = config.getProperty("url");
    public static String trytecomMW = config.getProperty("trytecomMW");

    public BankaMWApplication() {
    }

    public void start() {

        // BAIE - Initializes NATS
        proc = new Process(SERVICENAME);

        log.debug("Starting Server " + URI);
        try {
            log.debug("Start of publish");
            Endpoint.publish(URI + "Account", new Account());
            Endpoint.publish(URI + "Entity",new Entity());
            Endpoint.publish(URI + "Transaction", new Transaction());
            Endpoint.publish(URI + "BankaTransaction", new BankaTransaction());
            log.debug("End of publish");
        } catch(Exception ex) {
            log.error("Error Starting Server: \n" + ex.getMessage());
        }
        log.info("Server Running " + URI);
    }

    public static void main(String[] args) {
        if (args.length == 0) {
            BankaMWApplication ss = new BankaMWApplication();
            ss.start();
        } else {
            System.out.println();
            return;
        }
    }

    public static Process getProc() {
        return proc;
    }
}
